#############################################################
# Models of Separation Logics for a simple imperative lambda-calculus

This archive contains definitions and proofs of soundness for several
Separation Logics.

The plain Separation Logic and the characteristic formulae
(used for more smoothly integrating Separation Logic into interactive
proofs) is as described in Arthur Charguéraud's lecture notes, 
available from:
  http://www.chargueraud.org/teach/verif/seplogic.pdf


The Separation Logic equipped with time credits is described in:
__Verifying the correctness and amortized complexity of a union-find
implementation in separation logic with time credits__
by Arthur Charguéraud and François Pottier
(this is a submitted journal article, extending an ITP 2015 article).
  http://gallium.inria.fr/~fpottier/publis/chargueraud-pottier-uf-sltc.pdf

The Separation Logic equipped with read-only permissions is described in:
__Temporary Read-Only Permissions for Separation Logic__
by Arthur Charguéraud and François Pottier
(ESOP 2017).
  http://www.chargueraud.org/research/2017/readonlysep/readonlysep.pdf


#############################################################
# To play with the files in the seplogic.tar.gz archive 
  (produced by export.sh), proceed as follows:

 * Type __make__ in the root folder.

 * Type __coqide -R TLC TLC -R model MODEL model/LambdaExamples.v__
   from the root folder.



#############################################################
# Organisation of the subdirectories:

* The subdirectory __TLC__
  contains Charguéraud's general-purpose Coq library.

* The subdirectory __model__
  contains definitions and proofs about Separation Logic.

 * The file __TLCbuffer.v__
   contains scripts to be later merged into TLC.

 * The file __Fmap.v__
   defines a representation of finite maps, used to represent stores.

 * The file __SepFunctor.v__
   contains a functor with derived properties for Separation Logic.

 * The file __SepTactics.v__
   contains a functor with tactics for Separation Logic operations.

 * The file __LambdaSemantics.v__
   defines the syntax and semantics of an imperative lambda-calculus.

 * The file __LambdaSep.v__
   defines a plain Separation Logic (and proves its soundness).

 * The file __LambdaSepCredits.v__
   defines a Separation Logic with time credits.

 * The file __LambdaSepRO.v__
   defines a Separation Logic with read-only permissions.

 * The file __LambdaSep.v__
   defines a plain Separation Logic (and proves its soundness).

 * The file __LambdaCF.v__
   defines characteristic formulae for plain Separation Logic.

 * The file __LambdaCFCredits.v__
   defines characteristic formulae for Separation Logic with credits.

 * The file __LambdaSepLifted.v__
   defines a plain Separation Logic with heap predicates and
   triples lifted so as to directly manipulate logical values.

 * The file __LambdaCFLifted.v__
   defines characteristic formulae for lifted Separation Logic.

 * The file __LambdaStruct.v__
   defines specifications for basic derived operations, for records 
   and for arrays, for plain Separation Logic.

 * The file __LambdaStructLifted.v__
   defines specifications for basic derived operations, for records 
   and for arrays, for lifted Separation Logic.

 * The file __LambdaExamples.v__
   gives examples of proofs in plain Separation Logic, both using
   triples directly and using characteristic formulae.

 * The file __LambdaExamplesLifted.v__
   gives examples of proofs in lifted Separation Logic, using 
   lifted characteristic formulae.


