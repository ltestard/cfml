Set Implicit Arguments.
Require Import CFLib.
Require Import QueueSig_ml QueueSig_proof.
Require Import BankersQueue_ml.
Generalizable Variables a A.

(*

Notation "'AppRep' f x1 ;" :=
  (!A (app_1 f x1))
  (at level 68, f at level 0, x1 at level 0) : charac.

Notation "'AppRep' f x1 x2 ;" :=
  (!A (app_2 f x1 x2))
  (at level 68, f at level 0, x1 at level 0, 
   x2 at level 0) : charac.

Notation "'AppRep' f x1 x2 x3 ;" :=
  (!A (app_3 f x1 x2 x3))
  (at level 68, f at level 0, x1 at level 0, x2 at level 0,
   x3 at level 0) : charac.


Notation "'AppRep' f ( X1 ';' A1 )  | R >> H"
  := (Spec_1 f (fun (x1:A1) R => forall X1, rep x1 X1 -> H))
     (at level 69, f at level 0, X1 ident, 
      R ident, H at level 90,
      A1 at level 0) : func.

Notation "'RepSpec' f ( X1 ';' A1 ) ( X2 ';' A2 )  | R >> H"
  := (Spec_2 f (fun (x1:A1) (x2:A2) R => 
       forall X1 X2, rep x1 X1 -> rep x2 X2 -> H))
     (at level 69, f at level 0, X1 ident, X2 ident, 
      R ident, H at level 90,
      A1 at level 0, A2 at level 0) : func.

Notation "'RepSpec' f ( X1 ; A1 ) ( X2 ; A2 ) ( X3 ; A3 )  | R >> H"
  := (Spec_3 f (fun (x1:A1) (x2:A2) (x3:A3) R => 
       forall X1 X2 X3, rep x1 X1 -> rep x2 X2 -> rep x3 X3 -> H))
     (at level 69, f at level 0, X1 ident, X2 ident, X3 ident, 
      R ident, H at level 90,
      A1 at level 0, A2 at level 0, A3 at level 0) : func.

*)


(*

Notation "'RepSpec' f ( X1 ';' A1 )  | R >> H"
  := (Spec_1 f (fun (x1:A1) R => forall X1, rep x1 X1 -> H))
     (at level 69, f at level 0, X1 ident, 
      R ident, H at level 90,
      A1 at level 0) : func.

Notation "'RepSpec' f ( X1 ';' A1 ) ( X2 ';' A2 )  | R >> H"
  := (Spec_2 f (fun (x1:A1) (x2:A2) R => 
       forall X1 X2, rep x1 X1 -> rep x2 X2 -> H))
     (at level 69, f at level 0, X1 ident, X2 ident, 
      R ident, H at level 90,
      A1 at level 0, A2 at level 0) : func.

Notation "'RepSpec' f ( X1 ; A1 ) ( X2 ; A2 ) ( X3 ; A3 )  | R >> H"
  := (Spec_3 f (fun (x1:A1) (x2:A2) (x3:A3) R => 
       forall X1 X2 X3, rep x1 X1 -> rep x2 X2 -> rep x3 X3 -> H))
     (at level 69, f at level 0, X1 ident, X2 ident, X3 ident, 
      R ident, H at level 90,
      A1 at level 0, A2 at level 0, A3 at level 0) : func.

Notation "'RepSpec' f ( X1 ; A1 ) ( X2 ; A2 ) ( X3 ; A3 ) ( X4 ; A4 )  | R >> H"
  := (Spec_4 f (fun (x1:A1) (x2:A2) (x3:A3) (x4:A4) R => 
        forall X1 X2 X3 X4, rep x1 X1 -> rep x2 X2 -> rep x3 X3 -> rep x4 X4 -> H))
     (at level 69, f at level 0, X1 ident, X2 ident, X3 ident, X4 ident, 
      R ident, H at level 90,
      A1 at level 0, A2 at level 0, A3 at level 0, A4 at level 0) : func.

*)



Definition rep_st `{Rep a A} P :=
  fun (x:a) => exists X:A, rep x X /\ P X.

Definition rep_eq `{Rep a A} X :=
  fun (x:a) => rep x X.

Implicit Arguments rep_st [[A] [H]].
Implicit Arguments rep_eq [[A] [H]].

Module Import Q <: MLQueue := MLBankersQueue.

(** invariant *)

Definition inv (d:int) `{Rep a A} (q:queue a) (Q:list A) :=
  let '(lenf,f,lenr,r) := q in 
     rep (f ++ rev r) Q
  /\ lenf = length f
  /\ lenr = length r
  /\ lenr <= lenf + d.

(** model *)

Global Instance queue_rep `{Rep a A} : Rep (queue a) (list A).
Proof.
  intros. apply (Build_Rep (inv 0)).
  destruct x as (((lenf,f),lenr),r).
  introv K1 K2. intuit K1. intuit K2. prove_rep.
Defined.

(** automation *)

Hint Constructors Forall2.
Hint Resolve Forall2_last.
Hint Unfold inv.

Ltac auto_tilde ::= eauto 7 with maths.

Section Polymorphic.
Variables (a A : Type) (RA:Rep a A).

(** useful facts *)

Lemma empty_from_lenf : forall f lenr r Q,
  rep (0, f, lenr, r) Q -> Q = nil.
Proof.
  introv (H&LF&LR&LE). 
  rewrite~ (@length_zero_inv _ r) in H.
  rewrite~ (@length_zero_inv _ f) in H.
  inverts~ H.
Qed.

Lemma empty_from_f : forall lenf lenr r Q,
  rep (lenf, nil, lenr, r) Q -> Q = nil.
Proof.
  introv (H&LF&LR&LE). rew_list in LF. 
  apply~ empty_from_lenf. constructors~. 
Qed.

Lemma empty_to_lenf : forall lenf f lenr r,
  rep (lenf, f, lenr, r) nil -> lenf = 0.
Proof.
  introv (H&LF&LR&LE). inverts H.
  destruct (nil_eq_app_rev_inv H1). subst~.
Qed.

(*
Notation "X ';-' T" := (fun (x:T) => rep x X) (at level 68). 
Notation "P ';;' T" := (fun (x:T) => exists X, rep x X /\ P X) (at level 80). 
*)

Ltac xisspec_core ::=
  solve [ intros_all; unfolds rel_le, pred_le, pure; auto; auto* ].

Notation "'Alias' x ':=' v 'in' R" :=
  (!S (fun H Q => forall x, x = v -> R H Q))
  (at level 69, x ident) : charac.

Tactic Notation "xalias" :=
  apply local_erase; intro; 
  let H := get_last_hyp tt in
  let HE := fresh "E" H in 
  intro HE.

Tactic Notation "xmatch" :=
   xmatch_keep_alias; repeat xalias.

Hint Unfold rep_eq.

Lemma check_spec : Spec check (q:queue a) |R>>
  forall Q, inv 1 q Q -> pure R (rep_eq (queue a) Q).
Proof.
  xcf. intros (((lenf,f),lenr),r) Q K. unfold pure.
  (* xgo *)
  xcase. xalias. xgos.
  destructs K. simpl; subst~.
  destructs K. simpl. rew_list~.
Qed.

Hint Extern 1 (RegisterSpec check) => Provide check_spec.

Lemma snoc_spec : 
  Spec snoc (Q;queue a) (X;a) >> (Q & X) ;- queue a.
Proof.
  xcf. intros (((lenf,f),lenr),r) x. introv (H&LF&LR&LE) RX.
  xgo~; ximpl_nointros. unfolds. rew_list. rewrite~ <- app_assoc.
Qed.

Hint Extern 1 (RegisterSpec snoc) => Provide snoc_spec.

Lemma head_spec : 
  RepSpec head (Q;queue a) |R>>
     Q <> nil -> R (is_head Q ;; a).
Proof.
  xcf. intros (((lenf,f),lenr),r) Q RQ NE. xgo.
  apply NE. apply~ empty_from_f.
  intuit RQ. inverts* H.
Qed.

Hint Extern 1 (RegisterSpec head) => Provide head_spec.

Lemma tail_spec :
  RepSpec tail (Q;queue a) |R>> 
     Q <> nil -> R (is_tail Q ;; queue a).
Proof.
  xcf. intros (((lenf,f),lenr),r) Q RQ NE. xmatch.
  apply NE. apply~ empty_from_f.
  intuit RQ. rew_list in *. inverts H. xapp~. ximpl~.
Qed.

Hint Extern 1 (RegisterSpec tail) => Provide tail_spec.





(*-------------------

Module BankersQueueSpec <: QueueSigSpec.

(** instantiations *)

Module Import Q <: MLQueue := MLBankersQueue.

(** invariant *)

Definition inv (d:int) `{Rep a A} (q:queue a) (Q:list A) :=
  let '(lenf,f,lenr,r) := q in 
     rep (f ++ rev r) Q
  /\ lenf = length f
  /\ lenr = length r
  /\ lenr <= lenf + d.

(** model *)

Global Instance queue_rep `{Rep a A} : Rep (queue a) (list A).
Proof.
  intros. apply (Build_Rep (inv 0)).
  destruct x as (((lenf,f),lenr),r).
  introv K1 K2. intuit K1. intuit K2. prove_rep.
Defined.

(** automation *)

Hint Constructors Forall2.
Hint Resolve Forall2_last.
Hint Unfold inv.

Ltac auto_tilde ::= eauto 7 with maths.

Section Polymorphic.
Variables (a A : Type) (RA:Rep a A).

(** useful facts *)

Lemma empty_from_lenf : forall f lenr r Q,
  rep (0, f, lenr, r) Q -> Q = nil.
Proof.
  introv (H&LF&LR&LE). 
  rewrite~ (@length_zero_inv _ r) in H.
  rewrite~ (@length_zero_inv _ f) in H.
  inverts~ H.
Qed.

Lemma empty_from_f : forall lenf lenr r Q,
  rep (lenf, nil, lenr, r) Q -> Q = nil.
Proof.
  introv (H&LF&LR&LE). rew_list in LF. 
  apply~ empty_from_lenf. constructors~. 
Qed.

Lemma empty_to_lenf : forall lenf f lenr r,
  rep (lenf, f, lenr, r) nil -> lenf = 0.
Proof.
  introv (H&LF&LR&LE). inverts H.
  destruct (nil_eq_app_rev_inv H1). subst~.
Qed.

(** verification *)

Lemma empty_spec : 
  rep (@empty a) (@nil A).
Proof.
  generalizes RA A. apply (empty_cf a). xgo.
  intros. simpl. rew_list~.
Qed.

Hint Extern 1 (RegisterSpec empty) => Provide empty_spec.

Lemma is_empty_spec : 
  RepTotal is_empty (Q;queue a) >> bool_of (Q = nil).
Proof.
  xcf. intros (((lenf,f),lenr),r) Q RQ. xgo.
  unfolds. iff Z; fold_prop; subst.
  apply~ empty_from_lenf. apply~ empty_to_lenf.
Qed. 

Hint Extern 1 (RegisterSpec is_empty) => Provide is_empty_spec.

Lemma check_spec : 
  Spec check (q:queue a) |R>>
    forall Q, inv 1 q Q -> R (Q ;- queue a).
Proof.
  xcf. intros (((lenf,f),lenr),r) Q K. xgo.
  destructs K. subst. simple~.
  destructs K. simpl. rew_list~.
Qed.

Hint Extern 1 (RegisterSpec check) => Provide check_spec.

Lemma snoc_spec : 
  RepTotal snoc (Q;queue a) (X;a) >> (Q & X) ;- queue a.
Proof.
  xcf. intros (((lenf,f),lenr),r) x. introv (H&LF&LR&LE) RX.
  xgo~; ximpl_nointros. unfolds. rew_list. rewrite~ <- app_assoc.
Qed.

Hint Extern 1 (RegisterSpec snoc) => Provide snoc_spec.

Lemma head_spec : 
  RepSpec head (Q;queue a) |R>>
     Q <> nil -> R (is_head Q ;; a).
Proof.
  xcf. intros (((lenf,f),lenr),r) Q RQ NE. xgo.
  apply NE. apply~ empty_from_f.
  intuit RQ. inverts* H.
Qed.

Hint Extern 1 (RegisterSpec head) => Provide head_spec.

Lemma tail_spec :
  RepSpec tail (Q;queue a) |R>> 
     Q <> nil -> R (is_tail Q ;; queue a).
Proof.
  xcf. intros (((lenf,f),lenr),r) Q RQ NE. xmatch.
  apply NE. apply~ empty_from_f.
  intuit RQ. rew_list in *. inverts H. xapp~. ximpl~.
Qed.

Hint Extern 1 (RegisterSpec tail) => Provide tail_spec.

End Polymorphic.

End BankersQueueSpec.

*)
