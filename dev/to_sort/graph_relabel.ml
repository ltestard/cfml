(* usage: 
     ocamlc test.ml && ./a.out 2 

   where 2 is the seed, and can be replaced by any value
*)

let printf = Printf.printf 

let swap t i j =
   let v = t.(i) in
   t.(i) <- t.(j);
   t.(j) <- v

let reverse t =
  let nb_nodes = Array.length t in
  (* build a random permutation of the interval [0,n) *)
  let labels = Array.make nb_nodes 0 in (* uninitialized array *)
  for i = 0 to nb_nodes-1 do (* for every valid index *)
     labels.(i) <- i;
     let j = Random.int (i+1) in
     swap labels i j
  done;
  (* relabel edges (push the labelling through the edge lists) *)
  for i = 0 to nb_nodes-1 do (* for every valid index *)
     let vs = t.(i) in
     for j = 0 to Array.length vs - 1 do
         vs.(j) <- labels.(vs.(j))
     done;
  done; 
  (* relabel nodes (in place inverse of a permutation) *)
  let treated = -1 in
  for i = 0 to nb_nodes-1 do
     if labels.(i) <> treated then begin
        let s = ref (t.(i)) in
        let k = ref (labels.(i)) in
        while labels.(!k) <> treated do
           let s2 = t.(!k) in
           t.(!k) <- !s;
           s := s2;
           let k2 = labels.(!k) in
           labels.(!k) <- treated;
           k := k2;
         done
     end
  done


let demo = 
   [|  [| 0; 1 |];
       [| 3; 4 |];
       [| 0; 1; 4 |];
       [| 2 |];
       [| |]; |]

let print_graph t = 
   Array.iteri (fun i js ->
      printf "%d: " i;
      Array.iter (fun j -> printf "%d " j) js;
      printf "\n") t
   
let _ =
   Random.init (int_of_string (Sys.argv.(1)));
   printf "Before: \n";
   print_graph demo;
   printf "After: \n";
   reverse demo;
   print_graph demo 
  



(*
    Array.iteri (fun i _ -> labels.(i) <- [| 3;1;4;5;6;0;2;8;7 |].(i)) labels;
  printf "Labels: \n";
  Array.iteri (fun i j -> printf "(%d -> %d) " i j) labels;
  printf "\n";

  let demo = 
   Array.init 9 (fun i -> Array.make 1 i)

*)
