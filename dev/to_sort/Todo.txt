===========================

> J'en avais besoin pour les exemples de combinateur de points fixes.
> > Du coup, je l'avais mis par d�faut dans le makefile, car je ne sais pas
> > comment proprement sp�cifi� dans un Makefile que certains fichiers
> > doivent etre compil�s avec une certaine option et pas d'autres...
> > Tu sais peut �tre ?
Tu peux peut-�tre utiliser les "Target-specific Variable Values" (voir
la doc de make). En gros, tu peux �crire:

mon-point-fixe.cmo: OCAMLFLAGS += -rectypes

en plus de la recette g�n�rique:

%.cmo: %.ml
       ocamlc $(OCAMLFLAGS) -c $<

Je n'ai pas test�.


===========================



(* todo: coqbug: pas de warning si un autre format existe d�j� *)

(*-----------------------------------------------------------*)
dijkstra:
(* todo: prettyprint for  "let (x,y) =" and "fun (x,y) ="
(* todo bug when writing Hexists *)
(* todo: name card_int as a function *)
(* todo: lost notation  on while *)
apply local_erase (* todo :tactic *)

(*-----------------------------------------------------------*)
xapp_spec~ ml_get_spec_group. xextracts. (*todo:xapps_spec*) 
(* todo: bug de congruence qui unfold toutes les defs *)

(*-----------------------------------------------------------*)

*) exists ___ ne doit pas ouvrir les /\

*) rew_list doit checker les evars avant

*) la r�cursion polymorphe doit acc�pter
    let rec f : 'a. 'a t -> 'a t = failwith "not implemented"
   et ne pas forcer
    let rec f : 'a. 'a t -> 'a t = fun _ -> failwith "not implemented"

*) avoid copy-pasting statments by using definitions 

*) Physicists queue, check_spec, avant xgo:
   bug si l'hypoth�se "True" est dans le contexte 

*) "myauto" dans les set et les multiset,
   pour contourner le bug de eauto sur des x = y :> TA

*) SplayHeap_proof : r�introduire un hint
   Hint Extern 1 ((_ < _)%nat) => simpl; math.*)

*) UnbalancedSet : specialis� le hint simpl sur rep ?
*) BottomUpMergeSort : Tactics to improve

*) FuncTactics : ximpl_nointros => ximpl_refl

*) Polymorhisme � essayer !!!
      Record RepType :=
         { RepType_base :> Type;
           RepType_model : Type;
           RepType_rep : rep RepType_base RepType_model }.

      Notation "'#' RT" := RepType_model RT

*) BatchedQueue, tail_spec :
   use ximpl for ( ; ) ==> ( ;; ) 


*) Tactic if_eq : renommer et syst�matiser dans xclean ?

*) todo: ImplicitQueueSpec: queue_rep: si on met queue a_, 
   on n'a pas d'erreur de bound name !

*) todo: xisspec loop if evars are in the goal 

*) [inductions] that works

*) todo: bug des variables nomm�es comme les types 

*) SkewBinaryRandomAccesList slightly modified compared with original code;

*) todo : �galit� polymorphe pour tester si les listes sont vides

*) todo : v�rification de types habit�s


*) factoriser les : Hint Extern 1 (@rep (queue _) _ _ _ _) => simpl.

*) todo: bug dispaly let '(f,r,s) := q in 

*) todo: tactic wf :: rotate_spec -> realtimequeue

*) todo: remove::  instantiate (1:=a); xisspec_core. 

*) todo: bootstrapped_queue to_empty -- bug de inversion ! 

*) Pairing heap !  applys (@foreach_weaken _ (is_ge X0)).
   avec applys �a laisse des evars

*) rew_card. de lazy pairing: move
   and improve  merge_spec


*) binominal heap:
 ( todo: E1 <> \{} -> E1 \u E2 <> \{} 

*) splay heap: beautify merge_spec, bug in permutsimpl

*) induction sur nat pour �viter les %nat; ou bien 
   typeclass pour + sur nat.

*) Redblackset proof:
  - check my_lt_trans  needed?
  - avoid xisspec by automatic tuple destruction

*) Bottom_up : mrg_spec pb du cbv beta sur l'hyp d'ind

*) xmatch leaving [X = X] should clear hyp

*) catenable: comment r�cup�rer les hints de queue_sig

*) replace   intros. xintros.  with   xintros

*) Splay heap : use repspec for partition_spec 

*) Binominal heap : false~ 
   xapp. specializes HR __. unfold uncurry2. auto~. (*todo: un simpl en trop! *)
    ins_tree_spec : (* todo: xapp~ on rank should fold partial eq in post condition *)
   (*  btree_inv: fix induction *)

*) Binary ral
  (*todo: cleanup lookup_tree_spec *)
   is_empty_spec todo: down C *) 
  (* todo! exists___; auto~ =>  exists~*)
  (* todo: ximpl_rep *) 


*) CatenableListImpl : xret. eexact P_x0. (* todo: �viter l'�ta expansion *) 





(* Original code : "b r a l"

   let rec lookup_tree i t =
      match i, t with
      | 0, Leaf x -> x
      | i, Leaf x -> raise OutOfBound
      | i, Node (w, t1, t2) ->
          if i < w/2  
            then lookup_tree i t1
            else lookup_tree (i - w/2) t2

   let rec update_tree i y t = 
      match i, t with
      | 0, Leaf x -> Leaf y
      | _, Leaf x -> raise OutOfBound
      | _, Node (w, t1, t2) ->
          if i < w/2 
            then Node (w, update_tree i y t1, t2)
            else Node (w, t1, update_tree (i - w/2) y t2)

*) HoodMelville : "Done", state -> status,
   clear useless equalities from xmatch
    (* rewriteb Test in C1   todo: substb bug *)
    rewrite rev_cons. rewrite <- app_assoc. factorize


    
(*- todo: conflit entre types et termes -*)
(* todo: pkoi le nom est perdu apres xcf ? *)


Axiom heap_is_single_disjoint : forall x y A B (X:A) (Y:B),
  heap_is_single x X \* heap_is_single y Y ==>+ [x <> y].


Lemma Group_disjoint : forall a (x:a) A (M:map a A) (G:htype A a) (X:A),
  Heapdata G -> 
  Group G M \* (x ~> G X) ==>+ [x \notindom M].
Proof.
  skip. (* under construction *)
Qed.
Parameter ml_ref_spec_group :
Proof.
  intros. xfind. intros S. destruct (S a) as [IS HS].
  xintros. split. split. auto. intros x M.
  xapplys HS. skip.
 xweaken. intros v R LR HR M. simpls. 
  xapplys HR.
  xframe - []. eauto. intros l. 
  hchange Group_add. hsimpl. hsimpl.
Qed.

------------------



Global Instance Ref_heapdata : forall a A (T:htype A a), 
  Heapdata (Ref T).
Proof.
  intros. constructor. intros. hdata_simpl Ref. hextract.
  hchange heap_is_single_disjoint. hextract. hsimpl~.
Qed.



(* TODO: ideally, references should be treated simply
   as a record with a single field, so the following
   material would be generated automatically. *)