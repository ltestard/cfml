Set Implicit Arguments.
Require Import CFLib Dom_ml.
Require Import LibListZ LibFix.

(* todo: move *)

Parameter ml_list_nth_spec : forall a,
  Spec ml_list_nth (l:list a) (i:int) |R>> 
    ZInbound i l ->
    R [] (fun x => [ZNth i l x]).

Hint Extern 1 (RegisterSpec ml_list_nth) => Provide ml_list_nth_spec.


(**********************)

Inductive tree : Type :=
  | Cut : loc -> tree
  | Node : list tree -> tree.

Instance tree_inhab : Inhab tree.
Proof. intros. apply (prove_Inhab (Node nil)). Qed.

(** An induction principle for trees *)

Lemma map_congr : forall A B (f1 f2 : A->B) l,
  (forall x, Mem x l -> f1 x = f2 x) ->
  LibList.map f1 l = LibList.map f2 l.
Proof.
  introv H. induction l. auto. rew_map. fequals~.
Qed.

Section Tree_induct.
Variables
(P : tree -> Prop)
(Q : list tree -> Prop)  
(P1 : forall n, P (Cut n)) 
(P2 : forall l, Q l -> P (Node l)) 
(Q1 : Q nil)
(Q2 : forall t l, P t -> Q l -> Q (t::l)).

Fixpoint tree_induct_gen (T : tree) : P T :=
  match T as x return P x with
  | Cut c => P1 c
  | Node l => P2
      ((fix tree_list_induct (l : list tree) : Q l :=
      match l as x return Q x with 
      | nil   => Q1
      | t::l' => Q2 (tree_induct_gen t) (tree_list_induct l')
      end) l)
  end.

End Tree_induct.

Lemma tree_induct : forall (P : tree -> Prop),
  (forall c : loc, P (Cut c)) ->
  (forall l : list tree, 
    (forall t, Mem t l -> P t) -> P (Node l)) ->
  forall T : tree, P T.
Proof.
  introv Hc Hn. eapply tree_induct_gen with (Q := fun l =>
    forall t, Mem t l -> P t); intros.
  auto. auto. inversions H. inversions~ H1.
Qed.

Inductive subtree : binary tree :=
  | subtree_intro : forall t l, 
     Mem t l -> subtree t (Node l). 

Hint Constructors subtree.

Lemma subtree_wf : wf subtree.
Proof.
  intros t. induction t using tree_induct;
  constructor; introv K; inversions~ K.
Qed.

Fixpoint Size (t:tree) : Z := 
  match t with 
  | Cut c => 0
  | Node ts => 
    (fix fold l :=
     match l with  
     | nil => 0
     | x :: l' => Size x + fold l'
     end) ts
  end.

(*
Fixpoint Size (t:tree) : Z := 
  match t with Node ts => 
    List.fold_right (fun x acc => acc + Size x) 0 ts
  end.

Fixpoint Size (t:tree) : Z := 
  match t with Node ts => 
    List.fold_left (fun acc x  => acc + Size x)  ts 0
  end.

Definition SizeFunc Size (t:tree) : Z := 
  match t with Node ts => 
    fold_left Zplus 0 (LibList.map Size ts) 
  end.

Definition Size := FixFun SizeFunc.

Lemma Size_fix : forall t, Size t = SizeFunc Size t.
Proof.
  applys (FixFun_fix subtree).
  reflexivity. apply subtree_wf.
  introv H. unfold SizeFunc. destruct x.
  fequals. apply~ map_congr.
Qed.

*)

(**********************)

Fixpoint Tree (t:tree) (n:node) {struct t} : hprop :=
  match t with
  | Cut c => [n = c]
  | Node ts => Hexists ns, n ~~> ns \*  
    (fix fold l ms :=
     match l with  
     | nil => []
     | x :: l' =>
       match ms with
       | nil => [False]
       | m :: ms' => m ~> Tree x \* fold l' ms'
       end
     end) ts ns
  end.

Lemma Tree_focus : forall n ts,
  n ~> Tree (Node ts) ==> Hexists ns, n ~~> ns \*
    (fix fold l ms :=
     match l with  
     | nil => []
     | x :: l' =>
       match ms with
       | nil => [False]
       | m :: ms' => m ~> Tree x \* fold l' ms'
       end
     end) ts ns.
Proof. intros. hdata_simpl Tree. fequals. Qed.

Lemma size_spec : 
  Spec size (n:node) |R>> forall t,
    keep R (n ~> Tree t) (\= Size t).
Proof.
  xcf. intros. xapps.
  xfun_noxbody (fun aux => Spec aux (n:node) |R>> forall t p,
    R (s ~~> p \* n ~> Tree t) 
      (# s ~~> (p + Size t) \* n ~> Tree t)). 
    xinduction_heap subtree_wf. clear n t. xbody. 
    intros IH p. xapps. skip.
  xapps. xapps. hsimpl. math.
Qed.

Inductive ispath : list int -> tree -> tree -> Prop :=
  | ispath_nil : forall t,
     ispath nil t t
  | ispath_cons : forall i p ts t t',
     ZNth i ts t ->
     ispath p t t' ->
     ispath (i::p) (Node ts) t'.

Lemma size_spec : 
  Spec size (n:node) |R>> forall t,
    keep R (n ~> Tree t) (\= Size t).
Proof.
let rec find p (n:node) =
  match p with
  | [] -> n
  | x::p' -> find p' (List.nth !n x)

let append_child (n:node) m =
  n := !n @ [m]



(**********************


type node = (node list) ref 

let c l : node =
  ref l

let rec find p n =
  match p with
  | [] -> n
  | x::p' -> find p' (List.nth x !n)

let append_child n m =
  n := !n @ [m]

let replace_child n i m =
  let rec aux i l = 
    match l with
    | [] -> failwith "error"
    | x::l' -> if i = 0 then m::l' else x::(aux (i-1) l')
    in
  aux i

let size n =
  let s = ref 0 in 
  let rec aux n =
    incr s;
    List.iter aux !n
    in
  aux n

let demo () =
  let r = c [ c[]; c[ c[ c[]; c[] ] ] ] in
  let s1 = size r in (* 6 *)
  let a = find [1;1] r in
  let b = find [1] r in
  append_child b a;
  replace_child b 1 (c[]);
  let s2 = size r in (* 7 *)
  (s1,s2) 







(**********************)(**********************)

(*
Hint Extern 1 (RegisterSpec ml_get) => Provide ml_get_spec.
*)


Fixpoint Split (p:list bool) (t:tree) : tree * tree := 
  match p with
  | nil => (Cut t, t)
  | x::p' =>
    match t with 
    | Cut c => arbitrary
    | Leaf => arbitrary
    | Node a b =>  
       if x then 
         let '(ctx, hole) := Split p' a in
         (Node ctx b, hole)
       else 
         let '(ctx, hole) := Split p' a in
         (Node a ctx, hole)
    end
  end.

(*
Inductive ispath : list bool -> tree -> tree -> Prop :=
  | ispath_nil : forall c,
     ispath nil (Cut c) c
  | ispath_cons_true : forall (x:bool) p a b c,
     ispath p a c ->
     ispath (true::p) (Node a b) c
  | ispath_cons_false : forall (x:bool) p a b c,
     ispath p b c ->
     ispath (false::p) (Node a b) c.
*)


Lemma Tree_notcut_focus : forall n t,
  Notcut t ->
  n ~> Tree t ==>
  n ~~> (match t with
  | Node t u => Hexists a b,
     n ~~> Branch2 a b \* a ~> Tree t \* b ~> Tree u
  | _ => Branch0
  end.
Opaque Tree.