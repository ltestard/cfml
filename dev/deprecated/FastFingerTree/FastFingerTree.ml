open ReducerSig
open CapacityParamSig
open FingerTree

(*-----------------------------------------------------------------------------*)

module Make (C : CapacityParam) (R : Reducer) = 
struct

type item = R.item


(*-----------------------------------------------------------------------------*)

(** Circular buffer of size capacity,
   with head inclusive, tail exclusive,
   with no more than capacity items in it. *)

(** Capacity of the queues -- power of two preferable *)

let capacity = C.capacity

(** Representation of a queue *)

type queue = {
  mutable queue_head : int;
  mutable queue_size : int;
  mutable queue_data : item array; }

(*-----------------------------------------------------------------------------*)
(*-----------------------------------------------------------------------------*)
(* TODO: fix bug with local modules in the middle of the file *)

(** Construction of a finger tree of queues *)

(*
module ReducerCount (T : InhabType) (* : (Reducer with type item = T.t and type meas = int) *) =
  struct
    type item = C.t
    type meas = int
    let zero = 0
    let combine = (+)
    let measure = (fun _ -> 1)
  end
*)

module ReducerCount =
  struct
    type item = queue
    type meas = int
    let inhab : item = NullPointers.null
    let zero = 0
    let combine = (+)
    let measure = (fun q -> q.queue_size)
  end

module Ftree (* : (FingerTree.S with type meas = int and type item = queue) *) =
  FingerTree.Make(ReducerCount)
  (* ReducerCount(struct type t = queue end)*)


(*-----------------------------------------------------------------------------*)

(** Builds a new queue *)

let queue_create () = 
  { queue_head = 0;
    queue_size = 0;
    queue_data = Array.make capacity R.inhab; }

(** Queue : empty, full or partial *)

let queue_size q =
  q.queue_size

let queue_is_empty q =
  q.queue_size = 0

let queue_is_full q =
  q.queue_size = capacity

let queue_is_partial q =
      not (queue_is_empty q) 
   && not (queue_is_full q)

(** Push or pop, at head or tail into a queue *)

let queue_pop_head q = 
  let x = q.queue_data.(q.queue_head) in
  q.queue_head <- (q.queue_head + 1) mod capacity;
  q.queue_size <- q.queue_size - 1;
  x
  
let queue_pop_tail q = 
  q.queue_size <- q.queue_size - 1;
  let i = (q.queue_head + q.queue_size) mod capacity in
  q.queue_data.(i)

let queue_push_head x q =
  q.queue_head <- (q.queue_head - 1 + capacity) mod capacity;
  q.queue_data.(q.queue_head) <- x;
  q.queue_size <- q.queue_size + 1

let queue_push_tail x q =
  let i = (q.queue_head + q.queue_size) mod capacity in
  q.queue_data.(i) <- x;
  q.queue_size <- q.queue_size + 1


(*-----------------------------------------------------------------------------*)

(** A fast-finger-tree is made of a finger tree of queues, plus
    a pair of queues to the right of the finger tree, plus a pair of
    queues to the left of the finger tree.
    The data in the side queues is always packed as close as possible
    to the tree. More precisely, the valid combination of status for
    a pair of queues (outer,inner) is:
    (empty, empty), (empty, partial), (empty,full), (partial, full), (full,full) *)

(** A fast-finger-tree buffer  *)

type fftree = {
  mutable fftree_front_outer : queue;
  mutable fftree_front_inner : queue;
  mutable fftree_middle : Ftree.t;
  mutable fftree_back_inner : queue;
  mutable fftree_back_outer : queue; }

type t = fftree

(** Emptiness test on fftree *)    
  
let is_empty ff =
     queue_is_empty ff.fftree_front_inner
  && queue_is_empty ff.fftree_back_inner
  && Ftree.is_empty ff.fftree_middle

(** Creation of a fftree *)

let create () = {
   fftree_front_outer = queue_create();
   fftree_front_inner = queue_create();
   fftree_middle = Ftree.empty;
   fftree_back_inner = queue_create();
   fftree_back_outer = queue_create();
   }

(** Push front *)

let push_front x ff =
  let qi = ff.fftree_front_inner in
  if not (queue_is_full qi) then begin
    queue_push_head x qi
  end else begin
    let qo = ff.fftree_front_outer in
    if queue_is_full qo then begin
      (* both are full, shift and push in tree to make space *)
      ff.fftree_middle <- Ftree.push_front qi ff.fftree_middle;
      ff.fftree_front_inner <- qo;
      ff.fftree_front_outer <- queue_create();
    end;
    queue_push_head x ff.fftree_front_outer
  end

(** Push back *)

let push_back x ff =
  let qi = ff.fftree_back_inner in
  if not (queue_is_full qi) then begin
    queue_push_tail x qi
  end else begin
    let qo = ff.fftree_back_outer in
    if queue_is_full qo then begin
      (* both are full, shift and push in tree to make space *)
      ff.fftree_middle <- Ftree.push_back qi ff.fftree_middle;
      ff.fftree_back_inner <- qo;
      ff.fftree_back_outer <- queue_create();
    end;
    queue_push_tail x ff.fftree_back_outer
  end

(** Representation of either a single element or an entire queue *)

type item_or_queue = 
  | ItemOrQueue_item of item
  | ItemOrQueue_queue of queue

(** Auxiliary: pops either single element or an entire queue
    from the front of the middle tree or the back buffers *)

let fftree_pop_front_from_back ff empty_queue =
  let f = ff.fftree_middle in
  if not (Ftree.is_empty f) then begin
    (* pop from the non-empty finger tree *)
    (* note: can delete the empty_queue here *)
    let (q,f) = Ftree.pop_front f in
    ff.fftree_middle <- f;
    ItemOrQueue_queue q
  end else begin
    (* pop from the other buffer because the finger tree is empty *)
    let qo = ff.fftree_back_outer in
    let qi = ff.fftree_back_inner in
    if queue_is_empty qo then begin
      (* assert (not (queue_is_empty qi));*)
      let x = queue_pop_head qi in
      ItemOrQueue_item x
    end else begin
      (* send the entire inner buffer to the other side *)
      ff.fftree_back_inner <- qo;
      ff.fftree_back_outer <- empty_queue;
      ItemOrQueue_queue qi
    end
  end

(** Auxiliary: pops either single element or an entire queue
    from the front of the middle tree or the front buffers *)

let fftree_pop_back_from_front ff empty_queue =
  let f = ff.fftree_middle in
  if not (Ftree.is_empty f) then begin
    (* pop from the non-empty finger tree *)
    (* note: can delete the empty_queue here *)
    let (q,f) = Ftree.pop_back f in
    ff.fftree_middle <- f;
    ItemOrQueue_queue q
  end else begin
    (* pop from the other buffer because the finger tree is empty *)
    let qo = ff.fftree_front_outer in
    let qi = ff.fftree_front_inner in
    if queue_is_empty qo then begin
      (*assert (not (queue_is_empty qi));*)
      let x = queue_pop_tail qi in
      ItemOrQueue_item x
    end else begin
      (* send the entire inner buffer to the other side *)
      ff.fftree_front_inner <- qo;
      ff.fftree_front_outer <- empty_queue;
      ItemOrQueue_queue qi
    end
  end

(** Pop front *)

let pop_front ff =
  let qo = ff.fftree_front_outer in
  if not (queue_is_empty qo) then begin
    queue_pop_head qo
  end else begin
    let qi = ff.fftree_front_inner in
    if not (queue_is_empty qi) then
       queue_pop_head ff.fftree_front_inner
    else begin
      (* both are empty, so pop from the tree or the other queues,
         passing the empty inner buffer for possible reuse *)
      match fftree_pop_front_from_back ff qi with
      | ItemOrQueue_item x -> x
      | ItemOrQueue_queue q -> 
          ff.fftree_front_inner <- q;
          queue_pop_head ff.fftree_front_inner
    end
  end

(** Pop back *)

let pop_back ff =
  let qo = ff.fftree_back_outer in
  if not (queue_is_empty qo) then begin
    queue_pop_tail qo
  end else begin
    let qi = ff.fftree_back_inner in
    if not (queue_is_empty qi) then 
      queue_pop_tail ff.fftree_back_inner
    else begin
      (* both are empty, so pop from the tree or the other queues,
         passing the empty inner buffer for possible reuse *)
      match fftree_pop_back_from_front ff qi with
      | ItemOrQueue_item x -> x
      | ItemOrQueue_queue q -> 
          ff.fftree_back_inner <- q;
          queue_pop_tail ff.fftree_back_inner
    end
  end



end