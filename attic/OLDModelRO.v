









(* Set Implicit Arguments. *)
Require Import LibCore Shared ModelLambda.
Open Scope state_scope.




(** Predicate over pairs *)

Definition prod_func A B (F:A->A->A) (G:B->B->B) (v1 v2:A*B) := (* todo move to TLC *)
  let (x1,y1) := v1 in 
  let (x2,y2) := v2 in 
  (F x1 x2, G y1 y2).

Tactic Notation "exists_dep" "as" ident(E) :=
  match goal with |- exists (_:?T), _ => 
    assert (E:T); [ | exists E ] end.

Tactic Notation "exists_dep" :=
  let H := fresh "H" in exists_dep as H.

Implicit Arguments exist [A P].


(********************************************************************)
(* ** Heaps *)

(*------------------------------------------------------------------*)
(* ** Representation of heaps *)

(** Representation of heaps *)

Definition heap : Type := 
  { h : (state*state)%type | let '(f,g) := h in state_disjoint f g }.  

(** Empty heap *)

Program Definition heap_empty : heap :=
  (state_empty, state_empty).

Coercion heap_obj (h:heap) : (state*state)%type :=
  proj1_sig h.

(** Starable heaps: disjoint owned heaps, agreeible read-only heaps *)

Definition heap_compat f1 g1 f2 g2 : Prop :=
    state_agree g1 g2 
 /\ (\# f1 f2 (g1 \+ g2)).


(*------------------------------------------------------------------*)
(* ** Definition of heap predicates *)

(** [hprop] is the type of predicates on heaps *)

Definition hprop := heap -> Prop.

(** Empty heap *)

Definition hprop_empty : hprop := 
  fun h => 
    let '(f,g) := heap_obj h in
    f = state_empty /\ g = state_empty.

(** Lifting of predicates *)

Definition hprop_empty_st (H:Prop) : hprop :=
  fun h => h = heap_empty /\ H.

(** Singleton heap *)

Definition hprop_single (l:loc) (v:val) : hprop := 
  fun h => 
    let '(f,g) := heap_obj h in
    f = state_single l v /\ g = state_empty.

(** Heap union *)

Program Definition hprop_star (H1 H2 : hprop) : hprop := 
  fun h => 
    let '(f,g) := heap_obj h in
    exists f1 g1 f2 g2,
    exists (S1: \# f1 g1) (S2: \# f2 g2), 
       heap_compat f1 g1 f2 g2  
    /\ H1 (f1,g1)
    /\ H2 (f2,g2)
    /\ f = f1 \+ f2 
    /\ g = g1 \+ g2.

(** Lifting of existentials *)

Definition hprop_exists A (Hof : A -> hprop) : hprop := 
  fun hg => exists x, Hof x hg.

Implicit Arguments hprop_exists [A].

(** Lifting of disjunctions *)

Definition hprop_or (H1 H2 : hprop) : hprop := 
  fun h => H1 h \/ H2 h.

(** Lifting of conjonction *)

Definition hprop_and (H1 H2 : hprop) : hprop := 
  fun h => H1 h /\ H2 h.

(** Garbage collection predicate: [Hexists H, H]. *)

Definition hprop_gc : hprop := 
  hprop_exists (fun H => H).

Global Opaque hprop_empty hprop_empty_st hprop_single 
              hprop_star hprop_exists hprop_gc. 


(*------------------------------------------------------------------*)
(* ** Notation for heap predicates *)

Notation "\[]" := (hprop_empty) 
  (at level 0) : heap_scope.

Notation "\[ L ]" := (hprop_empty_st L) 
  (at level 0, L at level 99) : heap_scope.

Notation "r '~~>' v" := (hprop_single r v)
  (at level 32, no associativity) : heap_scope.

Notation "H1 '\*' H2" := (hprop_star H1 H2)
  (at level 41, right associativity) : heap_scope.

Notation "Q \*+ H" := (fun x => hprop_star (Q x) H)
  (at level 40) : heap_scope.

Notation "'Hexists' x1 , H" := (hprop_exists (fun x1 => H))
  (at level 39, x1 ident, H at level 50) : heap_scope.
Notation "'Hexists' x1 : T1 , H" := (hprop_exists (fun x1:T1 => H))
  (at level 39, x1 ident, H at level 50, only parsing) : heap_scope.
Notation "'Hexists' ( x1 : T1 ) , H" := (hprop_exists (fun x1:T1 => H))
  (at level 39, x1 ident, H at level 50, only parsing) : heap_scope.

Notation "\GC" := (hprop_gc) : heap_scope. 

Open Scope heap_scope.
Bind Scope heap_scope with hprop.
Delimit Scope heap_scope with h.



(********************************************************************)
(* ** Properties *)

(*------------------------------------------------------------------*)
(* ** Tactic for unfolding terms associated with proof obligations *)

Ltac unf :=
  unfolds hprop_star_obligation_1, hprop_star_obligation_2.


(*------------------------------------------------------------------*)
(* ** Additional properties of [state_agree], needed for
      proving [frame] and [let] rules. *)

Lemma state_agree_transfer : forall f1 f2 f3,
  state_agree f1 f2 ->
  state_agree (f1 \+ f2) f3 ->
  state_agree f1 (f2 \+ f3).
Proof using.
  introv M1 M2. intros l v1 v2 E1 E2. 
  specializes M1 l v1 v2. specializes M2 l v1 v2.
  simpls. unfolds pfun_union.
  specializes M1 E1. rewrite E1 in M2. specializes~ M2 __.
  clear E1.
  cases (state_data f2 l). 
  { inverts E2. applys* M1. }
  { applys* M2. }
Qed.

Lemma state_agree_transfer' : forall f1 f2 f3,
  state_agree f1 f2 ->
  state_agree (f1 \+ f2) f3 ->
  state_agree f2 (f1 \+ f3).
Proof using.
  hint state_agree_sym.
  introv M1 M2. applys* state_agree_transfer.
  rewrite~ state_union_comm_agree. 
Qed.


(*------------------------------------------------------------------*)
(* ** Properties of [heap_compat] *)

Lemma heap_compat_def : forall f1 g1 f2 g2,
    heap_compat f1 g1 f2 g2
  = (state_agree g1 g2 /\ (\# f1 f2 (g1 \+ g2))).
Proof using. auto. Qed.

Hint Rewrite heap_compat_def : rew_disjoint.

Lemma heap_compat_sym : forall f1 g1 f2 g2,
  heap_compat f1 g1 f2 g2 -> 
  heap_compat f2 g2 f1 g1.
Proof using.
  introv (M1&M2). split.
  { applys~ state_agree_sym. }
  { state_disjoint. }
Qed.


(*------------------------------------------------------------------*)
(* ** Properties of [hprop] and [==>] *)

Global Instance hprop_inhab : Inhab hprop.
Proof using. intros. apply (prove_Inhab hprop_empty). Qed.

Section HeapProp.

Implicit Types H : hprop.
Transparent hprop_empty hprop_empty_st hprop_single hprop_star hprop_gc.

Lemma hprop_empty_prove : 
  \[] heap_empty.
Proof using. hnfs~. Qed.

(* NOT USED
Lemma hprop_empty_inv : forall h,
  \[] h -> h = heap_empty.
Proof using. intros [h g] (Mh&Mg). subst*. Qed.
*)

Lemma hprop_empty_inv' : forall f g (S:\# f g),
  \[] (exist (f,g) S) -> f = state_empty /\ g = state_empty.
Proof using. introv (Mh&Mg). subst*. Qed.

Lemma hprop_empty_st_prove : forall (P:Prop),
  P -> 
  \[P] heap_empty.
Proof using. intros. hnfs~. Qed.

Lemma hprop_empty_st_prove' : forall (P:Prop) (S: \# state_empty state_empty),
  P -> 
  \[P] (exist (state_empty,state_empty) S).
Proof using. intros. hnfs. split~. unfold heap_empty. fequals. Qed.

Lemma hprop_empty_st_inv : forall h (P:Prop),
  \[P] h -> 
  h = heap_empty /\ P.
Proof using. intros. auto. Qed.

Lemma hprop_empty_st_inv' : forall f g (S:\# f g) (P:Prop),
  \[P] (exist (f,g) S) -> f = state_empty /\ g = state_empty /\ P.
Proof using. introv (Mh&Mg). inverts Mh. autos*. Qed.

Lemma hprop_star_comm : forall H1 H2,
   H1 \* H2 = H2 \* H1.
Proof using. 
  intros. unfold hprop, hprop_star. extens. intros ((f,g)&D).
  simpl. unf. hint state_agree_sym. 
  iff (f1&g1&f2&g2&D1&D2&U1&U2&U3&U4&U5); lets (U1a&U1b): U1.
  { exists f2 g2 f1 g1.
    exists_dep as V1. state_disjoint.
    exists_dep as V2. state_disjoint. splits~. 
    { applys~ heap_compat_sym. }
    { applys_eq U3 1. fequals. }
    { applys_eq U2 1. fequals. }
    { state_eq. }
    { rewrite~ state_union_comm_agree. } }
  { exists f2 g2 f1 g1.
    exists_dep as V1. state_disjoint.
    exists_dep as V2. state_disjoint. splits~.  
    { applys~ heap_compat_sym. }
    { applys_eq U3 1. fequals. }
    { applys_eq U2 1. fequals. }
    { state_eq. }
    { rewrite~ state_union_comm_agree. } }
Qed. (* TODO: exploit symmetry *)

Lemma hprop_star_empty_l : forall H,
   \[] \* H = H.
Proof using.
  intro. unfold hprop, hprop_star. extens. 
  intros ((f,g),D). simpl. unf. iff M. 
  { destruct M as (f1&g1&f2&g2&S1&S2&U1&U2&U3&U4&U5). 
    lets (E1&E2): hprop_empty_inv' U2.
    rewrite E1 in U4. rewrite state_union_empty_l in U4.
    rewrite E2 in U5. rewrite state_union_empty_l in U5.
    applys_eq U3 1. subst. fequals. }
  { exists state_empty state_empty f g. 
    exists_dep as E1. state_disjoint.
    exists_dep as E2. state_disjoint.
    splits.
    { split. apply state_agree_empty_l. state_disjoint. }
    { split~. }
    { applys_eq M 1. fequals. }
    { state_eq. }
    { state_eq. } }
Qed.

Lemma hprop_star_empty_r : forall H, 
  H \* \[] = H.
Proof using.
  apply neutral_r_from_comm_neutral_l.
  applys hprop_star_comm. applys hprop_star_empty_l.
Qed.

Lemma state_agree_union_ll_inv : forall f1 f2 f3,
  state_agree (f1 \+ f2) f3 ->
  state_agree f1 f3.
Proof using.
  introv M. intros l v1 v2 E1 E2.
  specializes M l. simpls. unfolds pfun_union.
  rewrite E1 in M. applys* M.
Qed.

Lemma state_agree_union_rl_inv : forall f1 f2 f3,
  state_agree f1 (f2 \+ f3) ->
  state_agree f1 f2.
Proof using.
  hint state_agree_union_ll_inv, state_agree_sym. eauto.
Qed.

Lemma state_agree_union_lr_inv_agree : forall f1 f2 f3,
  state_agree (f1 \+ f2) f3 ->
  state_agree f1 f2 ->
  state_agree f2 f3.
Proof using.
  introv M D. rewrite~ (@state_union_comm_agree f1 f2) in M.
  applys* state_agree_union_ll_inv.
Qed.

Lemma state_agree_union_rr_inv_agree : forall f1 f2 f3,
  state_agree f1 (f2 \+ f3) ->
  state_agree f2 f3 ->
  state_agree f1 f3.
Proof using.
  hint state_agree_union_lr_inv_agree, state_agree_sym. eauto.
Qed.

(* NOT NEEDED 
Lemma state_agree_union_lr_inv_agree_disjoint : forall f1 f2 f3,
  state_agree (f1 \+ f2) f3 ->
  state_disjoint f1 f2 ->
  state_agree f2 f3.
Proof using.
  introv M D. rewrite~ (@state_union_comm_disjoint f1 f2) in M.
  applys* state_agree_union_ll_inv.
Qed.

Lemma state_agree_union_rr_inv_agree_disjoint : forall f1 f2 f3,
  state_agree f1 (f2 \+ f3) ->
  state_disjoint f2 f3 ->
  state_agree f1 f3.
Proof using.
  hint state_agree_union_lr_inv_agree, state_agree_sym. eauto.
Qed.
*)


Lemma hprop_star_assoc : forall H1 H2 H3,
  (H1 \* H2) \* H3 = H1 \* (H2 \* H3).
Proof using. 
  intros H1 H2 H3. unfold hprop, hprop_star. 
  extens. intros ((f,g)&D). unf; simpl. split. 
  { intros (f'&g'&f3&g3&D1&D2&(U1a&U1b)&U2&U3&U4&U5). 
    destruct U2 as (f1&g1&f2&g2&E1&E2&(M1a&M1b)&M2&M4&M5&M6). 
    subst f' g'. 
    exists f1 g1 (f2 \+ f3) (g2 \+ g3).
    exists_dep. state_disjoint.
    exists_dep. state_disjoint. splits.
    { split. 
      { applys~ state_agree_union_r.
        applys* state_agree_union_ll_inv. }  
      { state_disjoint. } }
    { applys_eq M2 1. fequals. } 
    { exists f2 g2 f3 g3.
      exists_dep as V1. state_disjoint.
      exists_dep as V2. state_disjoint. splits~.
      split.
      { applys* state_agree_union_lr_inv_agree. }  
      { state_disjoint. }
      { applys_eq M4 1. fequals. }
      { applys_eq U3 1. fequals. } } 
    { state_eq. }
    { state_eq. } }
  { intros (f1&g1&f'&g'&D1&D2&(U1a&U1b)&U2&U3&U4&U5). 
    destruct U3 as (f2&g2&f3&g3&E1&E2&(M1a&M1b)&M2&M4&M5&M6). 
    subst f' g'.
    exists (f1 \+ f2) (g1 \+ g2) f3 g3.
    exists_dep. state_disjoint.
    exists_dep. state_disjoint. splits.
    { split. 
      { applys~ state_agree_union_l.
        applys* state_agree_union_rr_inv_agree. }  
      { state_disjoint. } }
    { exists f1 g1 f2 g2.
      exists_dep as V1. state_disjoint.
      exists_dep as V2. state_disjoint. splits~.
      split.
      { applys* state_agree_union_rl_inv. }  
      { state_disjoint. }
      { applys_eq U2 1. fequals. }
      { applys_eq M2 1. fequals. } } 
    { applys_eq M4 1. fequals. } 
    { state_eq. }
    { state_eq. } }
Qed. (* later: exploit symmetry in the proof *)


(* NOT NEEDED HERE
Lemma hprop_star_single_same_loc_disjoint : forall (l:loc) (v1 v2:val),
  (hprop_single l v1) \* (hprop_single l v2) ==> \[False].
Proof using.
  intros. unfold hprop_single. intros h (h1&h2&E1&E2&D&E). false.
  subst. applys* state_single_same_loc_disjoint.
Qed.
*)

Lemma hprop_star_prop_elim : forall (P:Prop) H h,
  (\[P] \* H) h -> P /\ H h.
Proof using.
  intros P H ((f,g)&D) (f1&g1&f2&g2&D1&D2&(M0&M1)&M2&M3&M4&M5).
  unf. inverts M2 as M2a M2b. inverts M2a.
  split~. applys_eq M3 1. rewrite state_union_empty_l in M4,M5.
  subst. fequals.
Qed.

Lemma himpl_refl : forall H,
  H ==> H.
Proof using. intros h. auto. Qed.

Lemma himpl_trans : forall H1 H2 H3,
  (H1 ==> H2) -> (H2 ==> H3) -> (H1 ==> H3).
Proof using. introv M1 M2. intros h H1h. eauto. Qed.

Lemma himpl_antisym : forall H1 H2,
  (H1 ==> H2) -> (H2 ==> H1) -> (H1 = H2).
Proof using. introv M1 M2. applys prop_ext_1. intros h. iff*. Qed.

Lemma himpl_cancel : forall H1 H2 H2',
  H2 ==> H2' -> (H1 \* H2) ==> (H1 \* H2').
Proof using.
  introv W. intros ((f,g)&D) (f1&g1&f2&g2&D1&D2&M1&M2&M3&M4&M5).
  unf. exists f1 g1 f2 g2.
  exists_dep as V1. state_disjoint.
  exists_dep as V2. state_disjoint. splits~.
  { applys_eq M2 1. fequals. }
  { applys W. applys_eq M3 1. fequals. }
Qed.

Lemma himpl_extract_prop : forall (P:Prop) H H',
  (P -> H ==> H') -> (\[P] \* H) ==> H'.
Proof using. 
  introv W Hh. lets (?&?): hprop_star_prop_elim Hh. applys* W.
Qed.

Lemma himpl_extract_exists : forall A H (J:A->hprop),
  (forall x, J x ==> H) -> (hprop_exists J) ==> H.
Proof using. introv W. intros h (x&Hh). applys* W. Qed.

Lemma himpl_inst_prop : forall (P:Prop) H H',
  P -> H ==> H' -> H ==> (\[P] \* H').
Proof using.
  introv HP W. intros ((f,g)&D) M.
  exists state_empty state_empty f g.
  exists_dep as V1. state_disjoint.
  exists_dep as V2. state_disjoint. splits~.
  { split.
    { applys~ state_agree_empty_l. }
    { rewrite state_union_empty_l. state_disjoint. } }
  { applys* hprop_empty_st_prove'. }
  { applys W. applys_eq M 1. fequals. }
  { state_eq. }
  { state_eq. }
Qed.

Lemma himpl_inst_exists : forall A (x:A) H J,
  (H ==> J x) -> H ==> (hprop_exists J).
Proof using. introv W h. exists x. apply~ W. Qed.

Lemma himpl_remove_gc : forall H H',
  H ==> H' ->
  H ==> H' \* \GC.
Proof using.
  introv M. intros ((f,g)&D) N. 
  exists f g state_empty state_empty. unf.
  exists_dep as V1. state_disjoint.
  exists_dep as V2. state_disjoint. splits~.
  { split. 
    { applys state_agree_empty_r. }
    { state_disjoint. } }
  { applys M. applys_eq N 1. fequals. }
  { exists \[]. applys hprop_empty_prove. }
  { state_eq. }
  { state_eq. }
Qed.

End HeapProp.

(*
Hint Resolve hprop_empty_prove hprop_empty_st_prove.
*)


(********************************************************************)
(* ** Definition of the [normal] predicate *)

Program Definition normal (H:hprop) : Prop := 
  forall f g (S:\#f g), H (f,g) -> g = state_empty.

Definition normal' A (Q:A->hprop) :=
  forall x, normal (Q x).

Implicit Arguments normal' [A].

Lemma normal_empty : 
  normal hprop_empty.
Proof using. introv (M1&M2). auto. Qed.

Lemma normal_single : forall l v,
  normal (hprop_single l v).
Proof using. introv (M1&M2). auto. Qed.

Lemma normal_star : forall H1 H2,
  normal H1 ->
  normal H2 ->
  normal (H1 \* H2).
Proof using.
  introv M1 M2 (f1&g1&f2&g2&D1&D2&N0&N1&N2&N3&N4).
  unf.
  subst g. rewrites (>> M1 N1). rewrites (>> M2 N2).
  rewrite~ state_union_empty_r.
Qed.

(* TODO: state as an equality : FALSE!
Lemma normal_star' : forall H1 H2,
  normal (H1 \* H2) -> normal H1.
Proof using.
  introv M1. intros f g S N.
  unfolds normal_obligation_1. (* optional *)
  unfolds in M1.
  asserts D: (\# (f \+ state_empty) (g \+ state_empty)).
  { state_disjoint. }
  lets K': M1 (f \+ state_empty) (g \+ state_empty).
  lets K: ((rm K') D). 
  unfolds normal_obligation_1.
  rewrite <- K. state_eq.
  exists f state_empty g state_empty.
  exists_dep as N1. state_disjoint. 
  exists_dep as N2. state_disjoint. 
  unf. splits.
  skip.
  applys_eq 
 
 intros (f1&g1&f2&g2&D1&D2&N0&N1&N2&N3&N4).
  unf.
  subst g. rewrites (>> M1 N1). rewrites (>> M2 N2).
  rewrite~ state_union_empty_r.
Qed.
*)

Lemma normal_exists : forall A (J:A->hprop),
  normal' J ->
  normal (hprop_exists J).
Proof using. introv M (x&N). rewrites~ (>> M N). Qed.

Lemma normal_or : forall H1 H2,
  normal H1 ->
  normal H2 ->
  normal (hprop_or H1 H2).
Proof using.
  introv M1 M2 [N|N].
  { rewrites~ (>> M1 N). }
  { rewrites~ (>> M2 N). }
Qed.

Lemma normal_and : forall H1 H2,
  normal H1 \/ normal H2 ->
  normal (hprop_and H1 H2).
Proof using.
  introv [M|M] (N1&N2).
  rewrites~ (>> M N1).
  rewrites~ (>> M N2).
Qed.

Lemma normal_himpl : forall H1 H2,
  normal H2 ->
  (H1 ==> H2) ->
  normal H1.
Proof using. introv HS HI M. lets: HI M. applys* HS. Qed.



(********************************************************************)
(* ** Definition of the [Duplicatable] predicate *)

Definition duplicatable (H:hprop) : Prop :=
  H ==> H \* H.


(********************************************************************)
(* ** Definition of the [RO] modifier *)

Program Definition RO (H:hprop) : hprop :=
  fun h => 
    let '(f,g) := heap_obj h in
       f = state_empty
    /\ exists f1 g1, exists (D : \# f1 g1), 
            H (f1,g1)
         /\ g = f1 \+ g1.

Ltac unf ::=
  unfolds hprop_star_obligation_1, hprop_star_obligation_2,
          RO_obligation_1.

Program Lemma RO_elim : forall (H:hprop) f g (D : \# f g),
  (RO H) (exist (f,g) D) -> f = state_empty.
Proof using. introv (M1&M2). auto. Qed.

Lemma RO_duplicatable : forall H,
  duplicatable (RO H). 
Proof using.
  intros H ((f,g)&D) M.
  lets E: RO_elim M. subst.
  exists state_empty g state_empty g D D. unf. splits~. 
  { split.
    { apply state_agree_refl. }
    { state_disjoint. } }
  { state_eq. }
  { rewrite~ state_union_idempotent. } 
Qed.

Lemma RO_covariant : forall H1 H2,
  H1 ==> H2 ->
  RO (H1) ==> (RO H2).
Proof using.
  introv M. intros ((f,g)&D) (E1&f1&g1&D1&M1&N1). unf.
  lets M1': (rm M) (rm M1). 
  split. auto. 
  exists f1 g1. exists_dep. state_disjoint. split.
  applys_eq M1' 1. fequals.
  auto. 
Qed.


Lemma RO_RO : forall H,
    RO (RO H) = RO H. 
Proof using.
  intros. apply prop_ext_1. intros ((f,g)&D).
  iff (M1&(f1&g1&N1&(M1'&(f1'&g1'&N1'&N2'&N3'))&N3)) (M1&(f1&g1&N1&N2&N3)). 
  { unfolds RO_obligation_1. split~. exists f1' g1'. 
    exists_dep. state_disjoint. 
    split; [| state_eq].
    unfold RO_obligation_1.
    applys_eq N2' 1. fequals. }
  { split~. exists f g.
    exists_dep. state_disjoint. 
    split; [| state_eq].
    split~. exists f1 g1.
    exists_dep. state_disjoint. 
    split; [| state_eq].
    unfold RO_obligation_1.
    applys_eq N2 1. fequals. }
Qed.


Lemma RO_exists : forall A (J:A->hprop),
    RO (hprop_exists J) 
  = Hexists x, RO (J x).
Proof using.
  intros. apply prop_ext_1. intros ((f,g)&D). 
  iff (M1&(f1&g1&N1&(x&N2)&N3)) (x&(E&f1&g1&N1&N2&N3)).
  { exists x. split*. }
  { split*. exists f1 g1 N1. splits*. exists* x. }
Qed.

Lemma RO_or : forall H1 H2,
     RO (hprop_or H1 H2) 
  ==> hprop_or (RO H1) (RO H2).
Proof using.
  intros. intros ((f,g)&D) (M1&(f1&g1&N1&[N2a|N2b]&N3)); unf.
  { left. split~. exists f1 g1 N1. split~. }
  { right. split~. exists f1 g1 N1. split~. }
Qed.

Lemma RO_and : forall H1 H2,
     RO (hprop_and H1 H2) 
  ==> hprop_and (RO H1) (RO H2).
Proof using.
  intros. intros ((f,g)&D) (M1&(f1&g1&N1&(N2a&N2b)&N3)); unf.
  split.
  { split~. exists f1 g1 N1. split~. }
  { split~. exists f1 g1 N1. split~. }
Qed.

Lemma RO_star : forall H1 H2,
  RO (H1 \* H2) ==> (RO H1 \* RO H2).
Proof using.
  intros. intros ((h,g)&D) (M1&(f3&g3&D3&U&E)). unf.
  destruct U as (f1&g1&f2&g2&D1&D2&(U3&U5)&U1&U2&U4&U6). unf.
  exists state_empty (f1 \+ g1) state_empty (f2 \+ g2). unf.
  exists_dep as N1. state_disjoint.
  exists_dep as N2. state_disjoint.
  splits.
  { split.
    { applys~ state_agree_union_lr. }
    { state_disjoint. } }
  { split~. exists___. splits*. (* unfold RO_obligation_1. *) }
  { split~. exists___. splits*. }
  { state_eq. }
  { state_eq. }
Qed.

(** Equivalence with a direct definition of read-only singleton heap *)

Definition hprop_single_ro (l:loc) (v:val) : hprop := 
  fun h =>
    let '(f,g) := heap_obj h in
    f = state_empty /\ g = state_single l v.

Lemma RO_single : forall l v,
    (RO (hprop_single l v))
  = hprop_single_ro l v.
Proof using. 
  Transparent hprop_single.
  intros. unfold hprop_single, hprop_single_ro.
  apply prop_ext_1. intros ((f,g)&D). 
  iff (M1&h1&g1&M2&(M3a&M3b)&M4) (M1&M2).
  { subst. split~. state_eq. }
  { split. auto. simpl. exists g f.
    exists_dep. state_disjoint.
    subst. splits~. state_eq. }
Qed.


(* Remark: "RO (r ~~> v \* r ~~:> v)"  iff \[False] *)


(* NOT TRUE
Lemma RO_star : forall H1 H2,
  RO (H1 \* H2) = (RO H1 \* RO H2).
Proof using.
  intros. apply prop_ext_1. intros (h,g). split. 
  { intros (M1&(h3&g3&(h1&g1&h2&g2&U1&U2&U3&U4&U5&U6)&N1&N2)).
    skip. }
  { intros (h1&g1&h2&g2&(M1a&h11&g11&M1b&M1c&M1d)
             &(M2a&h12&g12&M2b&M2c&M2d)&M3&M4&M5&M6).
    split.
    { subst~. }
    { exists (h11 \+ h12) (g11 \+ g12). splits~.
      { exists h11 g11 h12 g12. splits~. subst. skip. skip. }
    { subst. skip. }
    { subst. skip. } } 
Qed.
*)


(********************************************************************)
(* ** Rules *)

Implicit Types H : hprop.
Implicit Types Q : val -> hprop.

(* DEPRECATEd
Coercion state_of_heap (h:heap) : state :=
  let '(f,g) := h in f \+ g.


Definition triple t (H:hprop) (Q:val->hprop) :=
  forall f1 g1 h2,
  heap_compat (f1,g1) h2 -> 
  H (f1,g1) -> 
  exists f1' f1'' r, 
     (\# f1' f1'' /\ heap_compat ((f1' \+ f1''), g1) h2)
    /\ red (f1 \+ g1 \+ h2) t (f1' \+ f1'' \+ g1 \+ h2) r
    /\ Q r (f1',state_empty).


*)

(** Triples *)

(* Lemma used in the definition of triple *)
Lemma heap_compat_sep1 : forall f1 g1 f2 g2,
  heap_compat f1 g1 f2 g2 ->
  \# f1 g1.
Proof using. introv (M1&M2). state_disjoint. Qed.

Program Definition triple t (H:hprop) (Q:val->hprop) :=
  forall f1 g1 f2 g2 (D: heap_compat f1 g1 f2 g2),
  H (f1,g1) -> 
  exists f1' f1'' r, 
     (\# f1' f1'' /\ heap_compat (f1' \+ f1'') g1 f2 g2)
    /\ red (f1 \+ g1 \+ f2 \+ g2) t (f1' \+ f1'' \+ g1 \+ f2 \+ g2) r
    /\ Q r (f1',state_empty).
Next Obligation. applys heap_compat_sep1 D. Defined.

Ltac unf ::=
  unfolds hprop_star_obligation_1, hprop_star_obligation_2,
          RO_obligation_1, 
          triple_obligation_1, triple_obligation_2.

(** Structural rules *)

Lemma rule_extract_prop : forall t (P:Prop) H Q,
  (P -> triple t H Q) ->
  triple t (\[P] \* H) Q.
Proof using.
  introv M. intros f1 g1 f2 g2 D. unf. 
  intros (h11&g11&h12&g12&D11&D12&N1&N2&N3&N4&N5).
  lets (N11a&N11b&HP): hprop_empty_st_inv' N2. subst h11 g11.
  rewrite state_union_empty_l in *. subst h12 g12.
  asserts R: (heap_compat f1 g1 f2 g2). auto.
  lets K: M HP f1 g1 f2 g2; lets K': ((rm K) R). unf; simpls.
  applys K'.
  { applys_eq N3 1. fequals. }
Qed.

(* TODO: simplify all use of "lets K" *)

Lemma rule_extract_exists : forall t (J:val->hprop) Q,
  (forall x, triple t (J x) Q) ->
  triple t (hprop_exists J) Q.
Proof using.
  introv M. intros f1 g1 f2 g2 D (x&Jx).
  unf; simpl. applys* M.
Qed.

Lemma rule_extract_or : forall t H1 H2 Q,
  triple t H1 Q ->
  triple t H2 Q ->
  triple t (hprop_or H1 H2) Q.
Proof using.
  introv M1 M2. intros f1 g1 f2 g2 D [M|M].
  unf; simpl. applys* M1. applys* M2.
Qed.

Lemma rule_extract_and : forall t H1 H2 Q,
  triple t H1 Q \/ triple t H2 Q ->
  triple t (hprop_and H1 H2) Q.
Proof using.
  introv M0. intros f1 g1 f2 g2 D (M1&M2). unf; simpl.
  destruct M0 as [M0|M0]. applys* M0. applys* M0.
Qed.

Lemma rule_gc_pre : forall t H Q,
  triple t H Q ->
  triple t (H \* \GC) Q.
Proof using.
  introv M. intros f1 g1 f2 g2 D. unf. 
  intros (f11&g11&f12&g12&D11&D12&N1&N2&N3&N4&N5). unf.
  destruct N3 as (H'&N3').
  asserts R: (heap_compat f11 g11 (f12 \+ f2) (g12 \+ g2)).
  { split.
    { applys state_agree_transfer; state_disjoint. }
    { state_disjoint. } }
  lets K: M f11 g11 (f12 \+ f2) (g12 \+ g2).
  forwards (f1'&f1''&ra&(Da&Sa)&Ra&Qa): ((rm K) R); unf; simpls.
  { applys_eq N2 1. fequals. }
  exists f1' (f1'' \+ f12) ra. splits.
  { split.  
    { state_disjoint. }
    { split.
      { state_disjoint. }
      { state_disjoint. } } }
  { applys_eq Ra 2 4; state_eq. }
  { auto. }
Qed. 


Lemma rule_gc_post : forall t H Q,
  triple t H (Q \*+ \GC) ->
  triple t H Q.
Proof using.
  introv M. intros f1 g1 f2 g2 D HP. unf.
  lets K: M f1 g1 f2 g2.
  forwards (f1'&f1''&ra&(Da&Sa)&Ra&Qa): ((rm K) D (rm HP)); 
   unf; simpls.
  destruct Qa as (f11&g11&f12&g12&D11&D12&N1&N2&N3&N4&N5); unf.
  asserts G11: (g11 = state_empty).
  { applys* state_union_eq_empty_inv_l. }
  exists f11 (f1'' \+ f12) ra. splits.
  { split. 
    { state_disjoint. }
    { split; state_disjoint. } }
  { applys_eq Ra 2; state_eq. }
  { applys_eq N2 1. subst. fequals. }
Qed. 

Lemma rule_consequence : forall t H' Q' H Q,
  H ==> H' ->
  triple t H' Q' ->
  Q' ===> Q ->
  triple t H Q.
Proof using. 
  introv HH M HQ. intros f1 g1 f2 g2 D H1.
  lets H1': (rm HH) (rm H1). 
  lets K: M f1 g1 f2 g2.
  forwards (f1'&f1''&ra&(Da&Sa)&Ra&Qa): ((rm K) D (rm H1')); 
   unf; simpls.
  lets Qa': (rm HQ) (rm Qa).
  exists f1' f1'' ra. splits~.
Qed.

Lemma rule_or_symmetric : forall t H1 H2 Q1 Q2,
  triple t H1 Q1 ->
  triple t H2 Q2 ->
  triple t (hprop_or H1 H2) (fun x => hprop_or (Q1 x) (Q2 x)).
Proof using.
  introv M1 M2. apply~ rule_extract_or.
  applys~ rule_consequence M1.
  { intros_all. hnfs*. }
  applys~ rule_consequence M2.
  { intros_all. hnfs*. }
Qed.

(* DEPRECATED
Lemma rule_consequence_gc : forall t H Q H' Q',
  H ==> H' \* \GC ->
  triple t H' Q' ->
  Q' ===> Q \*+ \GC ->
  triple t H Q.
Proof using. 
Admitted. 
  introv IH M IQ. intros h1 h2 D1 H1. 
  lets H1': IH (rm H1).
  lets (h1a&h1b&H1a&H1b&D1ab&E12): (rm H1').
  lets (h1'&h3&r&Da&Ra&Qa): M (h1b \+ h2) (rm H1a).
  { subst. state_disjoint. }
  lets Qa': IQ (rm Qa).
  lets (h1a'&h1b'&H1a'&H1b'&D1ab'&E12'): (rm Qa').  
  exists h1a' (h1b' \+ h1b \+ h3) r. splits.
  { subst. state_disjoint. }
  { subst. applys_eq Ra 2 4.
    { rewrite~ heap_union_assoc. }
    { rewrite (@heap_union_comm h1b h2).
      repeat rewrite heap_union_assoc. fequals.
      rewrite <- heap_union_comm_assoc.
      rewrite <- heap_union_comm_assoc.
      repeat rewrite heap_union_assoc. fequals. fequals.
      rewrite~ heap_union_comm.
      state_disjoint. state_disjoint. state_disjoint. state_disjoint. } }
  { auto. }
Qed.*)

(* DEPRECATED
Lemma rule_consequence : forall H' Q' t H Q,
  H ==> H' ->
  triple t H' Q ->
  Q ===> Q' ->
  triple t H Q'.
Proof using. 
  introv WH M WQ. applys rule_consequence_gc M.
  { applys~ himpl_remove_gc. }
  { intros r. applys~ himpl_remove_gc. }
Qed.
*)

(* INCORRECT
Lemma rule_frame : forall t H2 Q1 H1,
  triple t H1 Q1 ->
  triple t (H1 \* H2) (Q1 \*+ H2).
Proof using. 
  introv M. intros f1 g1 f2 g2 (D1a&D1b). unf.
  intros (f1a&g1a&f1b&g1b&S1a&S1b&(R3a&R3b)&R1&R2&R4&R5). unf.
  asserts D: (heap_compat f1a g1a (f1b \+ f2) (g1b \+ g2)).
  { split. 
    { applys state_agree_transfer; subst~. }
    { state_disjoint. } }
  lets K': (>> M f1a g1a (f1b \+ f2) (g1b \+ g2)); 
   forwards K: ((rm K') D); unf.
  { applys_eq R1 1. fequals. }
  { destruct K as (f1'a&f1''a&ra&(Da&Sa)&Ra&Qa).
    exists (f1'a \+ f1b) f1''a ra. splits.
    { state_disjoint. }
    { applys_eq Ra 2 4; state_eq. }
    { exists f1'a state_empty f1b state_empty. unf.
      exists_dep as V1. state_disjoint.
      exists_dep as V2. state_disjoint.
      splits~.
      { split. 
        { applys state_agree_empty_l. }
        { state_disjoint. } }
      { applys_eq Qa 1. fequals. }
      { applys_eq R2 1. fequals. }
      { state_eq. } } }
Qed. 
*)

Lemma rule_frame_read_only : forall t H1 Q1 H2,
  triple t (H1 \* RO H2) Q1 ->
  normal H2 ->
  triple t (H1 \* H2) (Q1 \*+ H2).
Proof using. 
  introv M1 SH2.
  intros f1 g1 f2 g2 (D1a&D1b). unf.
  intros (f1a&g1a&f1b&g1b&S1a&S1b&(R3a&R3b)&R1&R2&R4&R5). unf.
  lets E: SH2 R2. subst g1b.
  rewrite state_union_empty_r in R5. subst g1.  
  asserts D: (heap_compat f1a (g1a \+ f1b) f2 g2).
  { split. 
    { applys state_agree_union_l.
      { auto. }
      { applys~ state_agree_disjoint. state_disjoint. } }
    { state_disjoint. } }
  lets K': (>> M1 f1a (g1a \+ f1b) f2 g2); 
   forwards K: ((rm K') D); unf.
  { exists f1a g1a state_empty f1b. unf.
    exists_dep as V1. state_disjoint.
    exists_dep as V2. state_disjoint.
    splits.
    { split.   
      { applys~ state_agree_disjoint. state_disjoint. }
      { state_disjoint. } } 
    { applys_eq R1 1. fequals. }
    { split.
      { auto. }
      { exists f1b state_empty. unf.
        exists_dep. state_disjoint.
        splits~. { applys_eq R2 1. fequals. } { state_eq. } } }
    { state_eq. }
    { auto. } }
  { destruct K as (f1'a&f1''a&ra&(Da&Sa)&Ra&Qa).
    exists (f1'a \+ f1b) f1''a ra. splits.
    { state_disjoint. }
    { applys_eq Ra 2 4; state_eq. }
    { exists f1'a state_empty f1b state_empty. unf.
      exists_dep as V1. state_disjoint.
      exists_dep as V2. state_disjoint.
      splits~.
      { split. 
        { applys state_agree_refl. }
        { state_disjoint. } }
      { applys_eq Qa 1. fequals. }
      { applys_eq R2 1. fequals. }
      { state_eq. } } }
Qed. (* TODO: more automation for assoc/comm rewrite *)


(* TODO: simplify the pattern [applys_eq R1 1. fequals.] *)

(* TODO: devrait se prouver avec l'aide de GC-pre 
   (qui n'est plus derivable) ?
Lemma rule_frame_basic : forall t H1 Q1 H2,
  triple t H1 Q1 ->
  normal H2 ->
  triple t (H1 \* H2) (Q1 \*+ H2).
Proof using. 
  introv M1 SH2.
  intros h1 g1 h2 D1 (h1a&g1a&h1b&g1b&R1&R2&R3&R4&R5&R6).
  lets E: SH2 R2. subst g1b.
  lets (h1'a&h3a&ra&Da&Ra&Qa): M1 h1a g1a (h1b \+ h2).
  { subst h1. rew_disjoint. jauto. skip. }
  { auto. }
  { subst h1. rewrite <- state_union_assoc in Ra.
    exists (h1'a \+ h1b) h3a ra. splits.
    { rew_disjoint. jauto. }
    { do 2 rewrite <- state_union_assoc. applys_eq Ra 2 4.
      { subst. skip. }
      { subst. skip. } }
    { exists h1'a state_empty h1b state_empty. splits~.
      { skip. }
      { skip. } }
Qed.
*)
  
 


(** Term rules *)

Lemma rule_val : forall v H Q,
  H ==> Q v ->
  normal H -> 
  triple (trm_val v) H Q.
Proof using.
  introv M HS. intros f1 g1 f2 g2 (D1&D2) H1.
  specializes HS H1. subst g1. 
  exists f1 state_empty v.
  rewrite state_union_empty_r; repeat rewrite state_union_empty_l in *.
  splits.
  { split. state_disjoint. split.
    { applys state_agree_empty_l. }
    { clear H1. rewrite state_union_empty_l in D2. state_disjoint. } }
  { applys red_val. }
  { applys M. applys_eq H1 1. fequals. }
Qed.


(* todo: automate calls to state_agree_empty_l *)

Lemma rule_if : forall v t1 t2 H Q,
  triple (If v = val_int 0 then t2 else t1) H Q ->
  triple (trm_if v t1 t2) H Q.
Proof using.
  introv M. intros f1 g1 f2 g2 D H1.
  forwards (h1'&h3&r&R1&R2&R3): M H1.
  exists h1' h3 r. splits.
  { auto. }
  { applys~ red_if. }
  { auto. }
Qed.

Lemma rule_let : forall x t1 t2 H1 H2 Q Q1,
  triple t1 H1 Q1 ->
  (forall (X:val), triple (subst_trm x X t2) (Q1 X \* H2) Q) ->
  triple (trm_let x t1 t2) (H1 \* H2) Q.
Proof using.
  introv M1 M2. intros f1 g1 f2 g2 (D1a&D1b). unf.
  intros (f11&g11&f12&g12&D1&D2&(R3a&R3b)&R4&R5&R6&R7). unf. 
  asserts D: (heap_compat f11 g11 (f12 \+ f2) (g12 \+ g2)).
  { split. 
    { subst. applys* state_agree_transfer. }
    { state_disjoint. } }
  lets K': M1 f11 g11 (f12 \+ f2) (g12 \+ g2); forwards K: ((rm K') D).
  { unf. applys_eq R4 1. fequals. } 
  destruct K as (f1'a&f1''a&ra&(Da1&Da2)&Ra&Qa). unf.
  asserts D': (heap_compat (f1'a \+ f12) g12 (f1''a \+ f2) (g11 \+ g2)).
  { split.
    { subst. applys~ state_agree_transfer'. }
    { state_disjoint. } }
  lets K2': M2 ra (f1'a \+ f12) g12 (f1''a \+ f2) (g11 \+ g2); 
  forwards K2: ((rm K2') D').
  { exists f1'a state_empty f12 g12. 
    exists_dep as N1. state_disjoint.
    exists_dep as N2. state_disjoint.
    splits*.
      { split.
        { applys state_agree_empty_l. }
        { state_disjoint. } }
      { applys_eq Qa 1. fequals. }
      { applys_eq R5 1. fequals. }
      { state_eq. } }
   unf. destruct K2 as (h1'b&f1''b&rb&Db&Rb&Qb).
   exists h1'b (f1''a \+ f1''b) rb. splits.
    { subst. state_disjoint. }
    { applys red_let (f1'a \+ f12 \+ f1''a \+ g1 \+ f2 \+ g2) ra.
      { applys_eq Ra 4 2; state_eq. }
      { subst. rewrite~ (@state_union_comm_agree g11 g12).
        applys_eq Rb 2 4; state_eq. } }
    { applys_eq Qb 1. fequals. }
Qed.

Lemma rule_let_simple : forall x t1 t2 H Q Q1,
  triple t1 H Q1 ->
  (forall (X:val), triple (subst_trm x X t2) (Q1 X) Q) ->
  triple (trm_let x t1 t2) H Q.
Proof using.
  introv M1 M2. 
  applys_eq~ (>> rule_let \[] M1) 1 2.
  { intros X. rewrite* hprop_star_empty_r. }
  { rewrite* hprop_star_empty_r. }
Qed.


Lemma rule_let_read_only : forall x t1 t2 H1 H2 Q Q1,
  triple t1 (H1 \* RO H2) Q1 ->
  (forall (X:val), triple (subst_trm x X t2) (Q1 X \* RO H2) Q) ->
  triple (trm_let x t1 t2) (H1 \* RO H2) Q.
Proof using.
  skip.
Qed.

Lemma rule_let_val : forall x v1 t2 H Q,
  (forall (X:val), X = v1 -> triple (subst_trm x X t2) H Q) ->
  triple (trm_let x (trm_val v1) t2) H Q.
Proof using. 
  introv M. forwards~ M': M.
  applys_eq (>> rule_let \[] (fun x => \[x = v1])) 2.
  { applys rule_val. rewrite <- (@hprop_star_empty_r \[v1=v1]). 
    applys~ himpl_inst_prop. applys normal_empty. }
  { intros X. applys rule_extract_prop. applys M. } 
  { rewrite~ hprop_star_empty_l. }
Qed.

Lemma rule_app : forall f x F V t1 H Q,
  F = (val_fix f x t1) ->
  triple (subst_trm f F (subst_trm x V t1)) H Q ->
  triple (trm_app F V) H Q.
Proof using.
  introv EF M. subst F. intros f1 g1 f2 g2 D1 H1.
  lets (h1'a&h3a&ra&Da&Ra&Qa): M H1.
  exists h1'a h3a ra. splits. 
  { state_disjoint. }
  { applys~ red_app. }
  { auto. }
Qed.

Lemma rule_let_fix : forall f x t1 t2 H Q,
  (forall (F:val), 
    (forall X H' Q', 
      triple (subst_trm f F (subst_trm x X t1)) H' Q' ->
      triple (trm_app F X) H' Q') ->
    triple (subst_trm f F t2) H Q) ->
  triple (trm_let f (trm_val (val_fix f x t1)) t2) H Q.
Proof using.
  introv M. applys rule_let_val. intros F EF. 
  applys (rm M). clears H Q. intros X H Q.
  applys rule_app. auto.
Qed.

Lemma rule_new : forall v,
  triple (trm_new v) \[] (fun r => Hexists l, \[r = val_loc l] \* l ~~> v).
Proof using.
  Transparent hprop_single.
  introv D H1. unfolds hprop_single.
  lets: hprop_empty_inv H1. subst h1. rewrite heap_union_empty_l.
  asserts (l&Hl): (exists l, (state_data h2) l = None).
  { skip. } (* infinitely many locations -- TODO *)
  asserts Fr: (state_disjoint h2 (state_single l v)).
  { unfolds state_disjoint, pfun_disjoint, state_single. simpls.
    intros x. case_if~. }
  exists (state_single l v) heap_empty (val_loc l).
  rewrite heap_union_empty_r. splits.
  { rew_disjoint. splits*. }
  { applys~ red_new. }
  { exists l heap_empty (state_single l v). splits~. } 
Qed.

Lemma rule_get : forall v l,
  triple (trm_get (val_loc l)) (l ~~> v) (fun x => \[x = v] \* l ~~> v).
Proof using.
  Transparent hprop_single.
  introv D H1. unfolds hprop_single.  
  exists h1 heap_empty v. rewrite heap_union_empty_r. 
  splits.
  { state_disjoint. }
  { applys~ red_get. }
  { exists heap_empty h1. splits~. }
Qed.

Lemma rule_set : forall l v w,
  triple (trm_set (val_loc l) w) (l ~~> v) (fun r => \[r = val_unit] \* l ~~> w).
Proof using.
  Transparent hprop_single.
  introv D H1. unfolds hprop_single.  
  exists (state_single l w) heap_empty val_unit.
  rewrite heap_union_empty_r. 
  splits.
  { rew_disjoint. splits*. subst h1. 
    unfolds heap_disjoint, state_disjoint, pfun_disjoint, state_single. simpls.
    intros x. specializes D x. case_if; intuition auto_false. }
  { applys~ red_set v. }
  { exists heap_empty (state_single l w). splits~. }
Qed.



(********************************************************************)
(* ** DEPRECATED *)


    (*------------------------------------------------------------------*)
    (* ** Properties of heap empty *)

    (*
    Section HeapEmpty.
    Transparent hprop_empty_st.

    Lemma hprop_empty_prove : 
      \[] hprop_empty.
    Proof using. hnfs~. Qed.

    Lemma hprop_empty_st_prove : forall (P:Prop),
      P -> \[P] hprop_empty.
    Proof using. intros. hnfs~. Qed.

    End HeapEmpty.

    Hint Resolve hprop_empty_prove hprop_empty_st_prove.
    *)

    (*------------------------------------------------------------------*)
    (* ** Properties of [star] and [pack] *)

    Section HeapStar.
    (* Transparent hprop_star heap_union. *)

    (* TODO; move *)
    Axiom state_agree_sym : sym state_agree.

    Lemma star_comm : comm hprop_star. 
    Proof using. 
      intros H1 H2. unfold hprop, hprop_star. extens. intros (h,g).
      iff (h1&g1&h2&g2&M1&M2&M3&M4&M5&M6);
       rewrite state_union_comm_disjoint in M4, M6; auto;
       applys_to M5 state_agree_sym.
       exists* h2 g2 h1 g1. 
    (*lets: disjoint_sym. *) skip.
       exists* h2 g2 h1 g1. 
    (*lets: disjoint_sym. *) skip.
    Qed.

    Lemma star_neutral_l : neutral_l hprop_star \[].
    Proof using.  Admitted.
    (*
      intros H. unfold hprop, hprop_star. extens. intros h.
      iff (h1&h2&M1&M2&D&U) M. 
      hnf in M1. subst h1 h. rewrite~ heap_union_neutral_l.
      exists hprop_empty h. splits~. 
    Qed.*)

    Lemma star_neutral_r : neutral_r hprop_star \[].
    Proof using. Admitted.
    (*
      apply neutral_r_from_comm_neutral_l.
      apply star_comm. apply star_neutral_l.
    Qed.
    *)

    Lemma star_assoc : LibOperation.assoc hprop_star. 
    Proof using.  Admitted.
    (*
      intros H1 H2 H3. unfold hprop, hprop_star. extens. intros h. split.
      intros (h1&h'&M1&(h2&h3&M3&M4&D'&U')&D&U). subst h'.
       exists (heap_union h1 h2) h3. rewrite heap_disjoint_union_eq_r in D.
       splits.
        exists h1 h2. splits*.
        auto.
        rewrite* heap_disjoint_union_eq_l.
        rewrite~ <- heap_union_assoc.
      intros (h'&h3&(h1&h2&M3&M4&D'&U')&M2&D&U). subst h'.
       exists h1 (heap_union h2 h3). rewrite heap_disjoint_union_eq_l in D.
       splits.
        auto.
        exists h2 h3. splits*.
        rewrite* heap_disjoint_union_eq_r.
        rewrite~ heap_union_assoc.
    Qed. (* later: exploit symmetry in the proof *)
    *)

    Lemma star_comm_assoc : comm_assoc hprop_star.
    Proof using. apply comm_assoc_prove. apply star_comm. apply star_assoc. Qed.

    Lemma starpost_neutral : forall B (Q:B->hprop),
      Q \*+ \[] = Q.
    Proof using. extens. intros. (* unfold starpost. *) rewrite~ star_neutral_r. Qed.

    Lemma star_cancel : forall H1 H2 H2',
      H2 ==> H2' -> H1 \* H2 ==> H1 \* H2'.
    (*
    Proof using. introv W (h1&h2&?). exists* h1 h2. Qed.
    *)
    Admitted.

    Lemma star_is_single_same_loc : forall (l:loc) (v1 v2:val),
      (hprop_single l v1) \* (hprop_single l v2) ==> \[False].
    Proof using.
    (*
      Transparent hprop_single state_single.
      intros. intros h ((m1&n1)&(m2&n2)&E1&E2&D&E).
      unfolds heap_disjoint, state_disjoint, prod_st2, pfun_disjoint. 
      specializes D l. rewrite E1, E2 in D.  
      unfold state_single in D. simpls. case_if. destruct D; tryfalse.
    Qed.
    *)
    Admitted.

    Lemma heap_star_prop_elim : forall (P:Prop) H h,
      (\[P] \* H) h -> P /\ H h.
    Proof using.
    Admitted.
    (*
      introv (?&?&N&?&?&?). destruct N. subst. rewrite~ heap_union_neutral_l.
    Qed.
    *)

    Lemma heap_extract_prop : forall (P:Prop) H H',
      (P -> H ==> H') -> (\[P] \* H) ==> H'.
    Proof using. introv W Hh. applys_to Hh heap_star_prop_elim. autos*. Qed.

    Lemma heap_weaken_pack : forall A (x:A) H J,
      H ==> J x -> H ==> (hprop_exists J).
    Proof using. introv W h. exists x. apply~ W. Qed.


    End HeapStar.


    (*------------------------------------------------------------------*)
    (* ** Normalization of [star] *)

    Hint Rewrite 
      star_neutral_l star_neutral_r starpost_neutral : rew_heap.
    Hint Rewrite <- star_assoc : rew_heap.

    Tactic Notation "rew_heap" :=
      autorewrite with rew_heap.
    Tactic Notation "rew_heap" "in" "*" :=
      autorewrite with rew_heap in *.
    Tactic Notation "rew_heap" "in" hyp(H) :=
      autorewrite with rew_heap in H.

    Tactic Notation "rew_heap" "~" :=
      rew_heap; auto_tilde.
    Tactic Notation "rew_heap" "~" "in" "*" :=
      rew_heap in *; auto_tilde.
    Tactic Notation "rew_heap" "~" "in" hyp(H) :=
      rew_heap in H; auto_tilde.

    Tactic Notation "rew_heap" "*" :=
      rew_heap; auto_star.
    Tactic Notation "rew_heap" "*" "in" "*" :=
      rew_heap in *; auto_star.
    Tactic Notation "rew_heap" "*" "in" hyp(H) :=
      rew_heap in H; auto_star.





















(* DEPRECATED
Definition heap_disjoint (f1 f2 : heap) : Prop :=
  let '(h1,g1) := f1 in
  let '(h2,g2) := f2 in
  \# h1 h2 /\ \# h1 g2 /\ \# g1 h2 /\ \# g1 g2.

Lemma heap_disjoint_unfold : forall h1 h2 h3 h4,
  \# h1 h2 h3 h4 = (\# h1 h2 /\ \# h2 h3 /\ \# h1 h3).
Proof using. auto. Qed.
*)


(*------------------------------------------------------------------*)
(* ** Properties of [heap] *)
(* DEPRECATED
(** Disjointness *)

Lemma heap_disjoint_sym : forall h1 h2,
  \# h1 h2 -> \# h2 h1.
Proof using. introv M. applys~ state_disjoint_sym. Qed.

Lemma heap_disjoint_comm : forall h1 h2,
  \# h1 h2 = \# h2 h1.
Proof using. intros. applys~ state_disjoint_comm. Qed.

Lemma heap_disjoint_empty_l : forall h,
  \# heap_empty h.
Proof using. intros. applys~ state_disjoint_empty_l. Qed. 

Lemma heap_disjoint_empty_r : forall h,
  \# h heap_empty.
Proof using. intros. applys~ state_disjoint_empty_r. Qed. 

Lemma heap_disjoint_union_eq_l : forall h1 h2 h3,
  \# (h2 \+ h3) h1 =
  (\# h1 h2 /\ \# h1 h3).
Proof using. intros. applys~ state_disjoint_union_eq_l. Qed.

Lemma heap_disjoint_union_eq_r : forall h1 h2 h3,
  \# h1 (h2 \+ h3) =
  (\# h1 h2 /\ \# h1 h3).
Proof using. intros. applys~ state_disjoint_union_eq_r. Qed.

(** Union *)

Lemma heap_union_empty_l : forall h,
  heap_empty \+ h = h.
Proof using. intros. applys~ state_union_empty_l. Qed.

Lemma heap_union_empty_r : forall h,
  h \+ heap_empty = h.
Proof using. intros. applys~ state_union_empty_r. Qed.

Lemma heap_union_comm : forall h1 h2,
  \# h1 h2 ->
  h1 \+ h2 = h2 \+ h1.
Proof using. intros. applys~ state_union_comm_disjoint. Qed.

Lemma heap_union_assoc : forall h1 h2 h3,
  (h1 \+ h2) \+ h3 = h1 \+ (h2 \+ h3).
  (* LibOperation.assoc heap_union. *)
Proof using. intros. applys~ state_union_assoc. Qed.

(** Disjoint3 *)

Definition heap_disjoint_3 h1 h2 h3 :=
  \# h1 h2 /\ \# h2 h3 /\ \# h1 h3.

Notation "\# h1 h2 h3" := (heap_disjoint_3 h1 h2 h3)
  (at level 40, h1 at level 0, h2 at level 0, h3 at level 0, no associativity).

Lemma heap_disjoint_3_unfold : forall h1 h2 h3,
  \# h1 h2 h3 = (\# h1 h2 /\ \# h2 h3 /\ \# h1 h3).
Proof using. auto. Qed.

(** Hints and tactics *)

Hint Resolve 
   heap_disjoint_sym 
   heap_disjoint_empty_l heap_disjoint_empty_r
   heap_union_empty_l heap_union_empty_r.

Hint Rewrite 
  heap_disjoint_union_eq_l
  heap_disjoint_union_eq_r
  heap_disjoint_3_unfold : rew_disjoint.

Tactic Notation "rew_disjoint" :=
  autorewrite with rew_disjoint in *.
Tactic Notation "rew_disjoint" "*" :=
  rew_disjoint; auto_star.

*)

(* DEPRECATED

  (** Disjointness *)

  Lemma heap_disjoint_sym : forall h1 h2,
    heap_disjoint h1 h2 -> heap_disjoint h2 h1.
  Proof using.
    intros [h1 g1] [h2 g2] H. simpls.
   
    unfolds state_disjoint_4.  rew_disjoint.
    hint state_disjoint_sym. autos*.
  Qed.
  Admitted.

  Lemma heap_disjoint_comm : forall h1 h2,
    \# h1 h2 = \# h2 h1.
  Proof using. 
  (*
    intros [m1 n1] [m2 n2]. simpls.
    hint state_disjoint_sym. extens*.
  Qed. *)
  Admitted.

  Lemma heap_disjoint_empty_l : forall h,
    heap_disjoint hprop_empty h.
  Proof using. Admitted.
  (* intros [m n]. hint state_disjoint_empty_l. simple*. Qed. *)

  Lemma heap_disjoint_empty_r : forall h,
    heap_disjoint h hprop_empty.
  Proof using. Admitted.
  (* intros [m n]. hint state_disjoint_empty_r. simple*. Qed. *)

  Hint Resolve heap_disjoint_sym heap_disjoint_empty_l heap_disjoint_empty_r.

  Lemma heap_disjoint_union_eq_r : forall h1 h2 h3,
    heap_disjoint h1 (heap_union h2 h3) =
    (heap_disjoint h1 h2 /\ heap_disjoint h1 h3).
  Proof using. Admitted.
  (*
    intros [m1 n1] [m2 n2] [m3 n3].
    unfolds heap_disjoint, heap_union. simpls.
    rewrite state_disjoint_union_eq_r. extens*.
  Qed. *)

  Lemma heap_disjoint_union_eq_l : forall h1 h2 h3,
    heap_disjoint (heap_union h2 h3) h1 =
    (heap_disjoint h1 h2 /\ heap_disjoint h1 h3).
  Proof using.
    intros. rewrite heap_disjoint_comm. 
    apply heap_disjoint_union_eq_r.
  Qed.

  Definition heap_disjoint_3 h1 h2 h3 :=
    heap_disjoint h1 h2 /\ heap_disjoint h2 h3 /\ heap_disjoint h1 h3.

  Lemma heap_disjoint_3_unfold : forall h1 h2 h3,
    \# h1 h2 h3 = (\# h1 h2 /\ \# h2 h3 /\ \# h1 h3).
  Proof using. auto. Qed.

  (** Union *)

  Lemma heap_union_neutral_l : forall h,
    heap_union hprop_empty h = h.
  Proof using. Admitted.
  (*
    intros [m n]. unfold heap_union, hprop_empty. simpl.
    fequals. apply~ state_union_neutral_l.
  Qed. *)

  Lemma heap_union_neutral_r : forall h,
    heap_union h hprop_empty = h.
  Proof using. Admitted.
  (*
    intros [m n]. unfold heap_union, hprop_empty. simpl.
    fequals. apply~ state_union_neutral_r. math.
  Qed. *) (*--TODO: comm+neutral_r *)

  Lemma heap_union_comm : forall h1 h2,
    heap_disjoint h1 h2 ->
    heap_union h1 h2 = heap_union h2 h1.
  Proof using. Admitted.
  (*
    intros [m1 n1] [m2 n2] H. simpls. fequals.
    applys* state_union_comm_disjoint.
    math.
  Qed. *)

  Lemma heap_union_assoc : 
    LibOperation.assoc heap_union.
  Proof using.
  Admitted.
  (*
    intros [m1 n1] [m2 n2] [m3 n3]. unfolds heap_union. simpls.
    fequals. applys state_union_assoc. math.
  Qed.*)

  (** Hints and tactics *)

  Hint Resolve heap_union_neutral_l heap_union_neutral_r.

  Hint Rewrite 
    heap_disjoint_union_eq_l
    heap_disjoint_union_eq_r
    heap_disjoint_3_unfold : rew_disjoint.

  Tactic Notation "rew_disjoint" :=
    autorewrite with rew_disjoint in *.
  Tactic Notation "rew_disjoint" "*" :=
    rew_disjoint; auto_star.

*)


(*
Definition triple t (H:hprop) (Q:val->hprop) :=
  forall h1 g1 h2, \# h1 g1 h2 ->
  heap_compat (f1,g1) h2 -> 
  H (h1,g1) -> 
  exists h1' h3 r, 
       (\# h1' g1 /\ \# (h1' \+ g1) h2 h3)
       
    /\ red (f1 \+ g1 \+ h2) t (f1' \+ g1 \+ h2 \+ h3) r
    /\ Q r (f1',state_empty). 


Definition triple t (H:hprop) (Q:val->hprop) :=
  forall f1 g1 f2 h2, 
  heap_disjoint (f1,g1) (f2,g2) -> 
  H (f1,g1) -> 
  exists h1' h3 r, 
       (\# h1' g1 /\ \# (h1' \+ g1) h2 h3)
    /\ red (h1 \+ g1 \+ f2 \+ g2) t (h1' \+ g1 \+ f2 \+ g2 \+ h3) r
    /\ Q r (h1',state_empty). 

  (* heap_disjoint (f1,g1) (f2,g2) *)

state_agree g1 g2 
  /\ (\# f1 f2 (g1 \+ g2)).

*)



(* DEPRECATED
(** Union of heaps *)

Program Definition heap_union (h1 h2 : heap) : heap :=
  prod_func state_union state_union h1 h2.
Next Obligation. 
  destruct h1 as [(f1,g1) D1].
  destruct h2 as [(f2,g2) D2].
  simpl. state_disjoint.
*)

(*
Definition heap_compat (h1 h2 : heap) : Prop :=
  let '(f1,g1) := heap_obj h1 in
  let '(f2,g2) := heap_obj h2 in
     state_agree g1 g2 
  /\ (\# f1 f2 (g1 \+ g2)).
*)


(* NOT USED
Definition prod_st A B (v:A*B) (P:A->Prop) (Q:B->Prop) := (* todo move to TLC *)
  let (x,y) := v in P x /\ Q y.

Definition prod_st2 A B (P:binary A) (Q:binary B) (v1 v2:A*B) := (* todo move to TLC *)
  let (x1,y1) := v1 in 
  let (x2,y2) := v2 in 
  P x1 x2 /\ Q y1 y2.
*)


(* ALTERNATIVE EQUIVALENT
Program Definition hprop_star (H1 H2 : hprop) : hprop := 
  fun h => 
    let '(f,g) := heap_obj h in
    exists f1 g1 f2 g2, 
    exists (S: heap_compat f1 g1 f2 g2),  
       H1 (f1,g1)
    /\ H2 (f2,g2)
    /\ f = f1 \+ f2 
    /\ g = g1 \+ g2.
Next Obligation. destruct S. state_disjoint. Defined.
Next Obligation. destruct S. state_disjoint. Defined.
*)

  (* state_disjoint f1 f2 /\ state_agree g1 g2 *)



(*
Lemma heap_compat_transfer : forall h1 h2 h3,
  heap_compat h1 h2 ->
  heap_compat (heap_union h1 h2) h3 ->
  heap_compat h1 (heap_union h2 h3).

Lemma heap_compat_transfer : forall f1 f2 f3 h,
  heap_compat (f1 \+ f2, f3) h ->
  heap_compat (f1, f3 \+ f2) h.
Proof using. skip. Qed.
*)


(* DEPRECATED
Lemma state_union_comm_assoc'' : forall h1 h2 h3,
  \# h1 h2 h3 ->
  h1 \+ h2 \+ h3 = h2 \+ h1 \+ h3.
Proof using. 
  introv M. rewrite <- state_union_comm_assoc.
  rewrite <- state_union_comm_assoc.
  do 2 rewrite~ state_union_assoc. fequals.
  rewrite~ state_union_comm_disjoint. state_disjoint.
  state_disjoint. state_disjoint.
Qed.
*)

(* FALSE
Lemma hprop_decompose : forall H, 
  exists H1 H2, 
     H = H1 \* H2 
  /\ normal H1
  /\ duplicatable H2.
Proof using.
  intros. 
  exists (fun (f:heap) => let '(h,g) := f in = (h,state_empty)).


Lemma hprop_decompose : forall H, 
  H ==> Hexists H1, Hexists H2, H1 \* H2 \* \[ normal H1 ] \* \[ duplicatable H2 ].
Proof using.
  intros H (h,g) M.
  exists (= (h,state_empty)). 
  exists (= (state_empty,g)).
  exists h g state_empty state_empty. splits~.
  exists (fun (f:heap) => let '(h,g) := f in = (h,state_empty)).
*)



(* NOT NEEDED

Lemma heap_compat_sub_union_l : forall f g f' g' h,
  heap_compat (f \+ f', g \+ g') h -> 
  heap_compat (f,g) h.
Proof using.
  introv M. destruct h as [f'' g''].
  unfolds heap_compat. destruct M as (M1&M2). split.
  { intros l v1 v2 E1 E2. specializes M1 l v1 v2.
    applys* M1. simpl. unfold pfun_union. rewrite~ E1. }   
  { state_disjoint. }
Qed.

Lemma heap_compat_sub_union_r : forall f g f' g' h,
  heap_compat (f' \+ f, g' \+ g) h -> 
  state_agree g' g ->
  heap_compat (f,g) h.
Proof using.
  introv M N. destruct h as [f'' g''].
  unfolds heap_compat. destruct M as (M1&M2). split.
  { intros l v1 v2 E1 E2. specializes M1 l. 
    specializes N l.
    cases (state_data g' l) as C.
    { forwards~: N E1. subst. simpls. unfolds pfun_union.
      rewrite C in M1. applys* M1. }
    { simpls. unfolds pfun_union. rewrite C in M1. applys* M1. } }
  { state_disjoint. }
Qed.

Lemma state_of_heap_heap_union_l : forall f g h,
  heap_compat (f, g) h ->
    state_of_heap (heap_union (f, g) h) 
  = f \+ g \+ state_of_heap h.
Proof using.
  intros f g [f' g'] M. simpl. st_eq.
  unfolds in M. state_disjoint.
Qed.

*)


==============================


-----


Program Definition on_rw_sub H h :=
  exists f1 f2, state_disjoint f1 f2 
             /\ h^f = f1 \+ f2 
             /\ H (f1,state_empty).

Lemma on_rw_sub_base : forall H h,
  H h -> 
  h^r = state_empty ->
  on_rw_sub H h.
Proof using.
  intros H ((f,g)&D) M N. exists f state_empty. simpls. 
  subst g. splits~.
  { state_eq. }
  { applys_eq M 1. fequals. }
Qed.

Lemma on_rw_sub_gc : forall H h,
  on_rw_sub (H \* \GC) h ->
  on_rw_sub H h.
Proof using.
  introv (f1&f2&N1&N2&(h3&h4&M1&M2&(H'&M3)&M4)).
  lets (E1&E2): heap_eq_forward M4. simpls.
  rew_heap~ in *. exists (h3^f) (h4^f \+ f2). splits.
  { state_disjoint. }
  { rewrite N2, E1. state_eq. }
  { applys_eq M2 1. applys heap_eq. simpl. split~.
    lets~: state_union_eq_empty_inv_l (eq_sym E2). }
Qed.

Lemma on_rw_sub_union : forall H h1 h2,
  on_rw_sub H h1 ->
  heap_compat h1 h2 ->
  on_rw_sub H (h1 \u h2).
Proof using.
  introv (f1&f2&N1&N2&N3) C. exists f1 (f2 \+ h2^f). splits.
  { lets (A&B): C. rewrite N2 in B. state_disjoint. }
  { rew_heap~. rewrite N2. state_eq. }
  { applys_eq N3 1. fequals. } 
Qed.

Lemma on_rw_sub_weaken : forall Q Q' v h,
  on_rw_sub (Q v) h ->
  Q ===> Q' ->
  on_rw_sub (Q' v) h.
Proof using.
  introv (f1&f2&N1&N2&N3) HQ. lets N3': (rm HQ) (rm N3). unfolds*.
Qed.


Program Definition on_rw_sub' H h :=
  exists h1 h2, heap_compat h1 h2 
             /\ h = h1 \u h2 
             /\ h1^r = state_empty
             /\ H h1.

(*
Lemma on_rw_sub'_equiv : on_rw_sub' = on_rw_sub.
Proof using.
  applys func_ext_2. intros H h. extens. iff M.
  { destruct M as (h1&h2&N1&N2&N3&N4). exists (h1^f) (h2^f). splits.
    { state_disjoint. }
    { rewrite N2. rew_heap~. }
    { applys_eq N4 1. applys heap_eq. simpl. split~. } }
  { destruct M as (f1&f2&N1&N2&N3). 
    forwards (h1&E1&F1): (heap_make f1 (h^r)).
  }
Qed.
*)



(* 
Lemma heap_union_empty_both : forall h1 h2,
  h1^r = state_empty ->
  h2^r = state_empty ->
  (h1 \u h2)^r = state_empty.
Proof using.
  introv M1 M2. tests D: (heap_compat h1 h2).
  { rew_heap~. rewrite M1,M2. state_eq. }
  { unfold heap_union. rewrite (classicT_right D).
    asserts E: (heap_empty = arbitrary ). { reflexivity. }
   { unfold heap_inhab. unfold prove_Inhab. reflexivity. }
  destruct (classicT (heap_compat h1 h2)).
  rewrite
  skip.
Qed.




(************************************************************)
(* DEPRECATED *)

(** Auxiliary lemmas 

Lemma state_union_comm_assoc : forall h1 h2 h3,
  \# h1 h2 h3 ->
  (h1 \+ h2) \+ h3 = h2 \+ (h3 \+ h1).
Proof using. 
  introv M. rewrite (@state_union_comm h1 h2).
  rewrite~ state_union_assoc. fequals.
  rewrite~ state_union_comm_disjoint. rew_disjoint*. rew_disjoint*.
Qed.

Lemma state_union_comm_assoc' : forall h1 h2 h3,
  \# h1 h2 h3 ->
  (h1 \+ h2) \+ h3 = h1 \+ (h3 \+ h2).
Proof using. 
  introv M. rewrite state_union_assoc. fequals.
  rewrite~ state_union_comm_disjoint. rew_disjoint*.
Qed.

Lemma state_union_comm_comm : forall h1 h2 h3,
  \# h1 h2 h3 ->
  (h1 \+ h2) \+ h3 = h3 \+ (h1 \+ h2).
Proof using. 
  introv M. rewrite (@state_union_comm h1 h2).
  rewrite~ (@state_union_comm h3 (h2 \+ h1)).
  rew_disjoint*. rew_disjoint*.
Qed.
*)


(*

Definition state_disjoint_4 h1 h2 h3 h4 :=
  state_disjoint_3 h1 h2 h3 /\ 
  state_disjoint_3 h1 h2 h4 /\ 
  state_disjoint h4 h3.

*)


(*
Notation "\# h1 h2" := (heap_disjoint h1 h2)
  (at level 40, h1 at level 0, h2 at level 0, no associativity) : heap_scope.

Notation "\# h1 h2 h3" := (heap_disjoint_3 h1 h2 h3)
  (at level 40, h1 at level 0, h2 at level 0, h3 at level 0, no associativity) : heap_scope.

Notation "\# h1 h2 h3 h4" := (heap_disjoint_4 h1 h2 h3 h4)
  (at level 40, h1 at level 0, h2 at level 0, h3 at level 0, h4 at level 0, no associativity) : heap_scope.

Notation "h1 \+ h2" := (state_union h1 h2)
   (at level 51, right associativity) : heap_scope.

Open Scope heap_scope.
Bind Scope heap_scope with hprop.
Delimit Scope heap_scope with h.
*)

(*

Hint Resolve state_union_empty_l state_union_empty_r.

*)


Lemma state_agree_union_lr_inv_agree_agree_disjoint : forall f1 f2 f3,
  state_agree (f1 \+ f2) f3 ->
  state_disjoint f1 f2 ->
  state_agree f2 f3.
Proof using.
  introv M D. rewrite~ (@state_union_comm_disjoint f1 f2) in M.
  applys* state_agree_union_ll_inv.
Qed.

Lemma state_agree_union_rr_inv_agree_disjoint : forall f1 f2 f3,
  state_agree f1 (f2 \+ f3) ->
  state_disjoint f2 f3 ->
  state_agree f1 f3.
Proof using.
  hint state_agree_union_lr_inv_agree_agree, state_disjoint, state_agree_sym. 
  eauto.
Qed.