
(* ---------------------------------------------------------------------- *)

Module Normally2.

Program Definition normally (H:hprop) := 
  fun h => H (h^f,fmap_empty).

Lemma normally_intro : forall H,
  normal H ->
  H ==> normally H.
Proof using.
  introv N. unfolds normal, normally.
  intros h M. lets: (>> N M).
  skip.
Qed.

(*
Lemma normal_normally : forall H,
  normal (normally H).
Proof using.
  intros H. unfolds normal, normally. intros h M.
   
Qed.
*)

Lemma normally_himpl : forall H1 H2,
  (H1 ==> H2) ->
  normally H1 ==> normally H2.
Proof using.
  introv M. unfold normal, normally.
  intros h N. hhsimpl~.
Qed.

Lemma normally_hstar : forall H1 H2,
  normally H1 \* normally H2 ==> normally (H1 \* H2).
Proof using.
  intros. unfold normal, normally. intros h (h1&h2&M1&M2&M3&M4).
  (* exists (h1^f \+ h2^f, (fmap_empty:state)).*)
  skip.
Qed.

Lemma normally_idempotent1 : forall H,
  normally H ==> normally (normally H).
Proof using.
  intros. unfolds normal, normally. intros h M. simpl.
  (* *)
  skip.
Qed.

End Normally2.


(* ---------------------------------------------------------------------- *)

Module Normally3.

Definition normally H := 
  Hexists H1 H2, \[normal H1] \* H1 \* \[H1 \* RO H2 ==> H].

Lemma normally_intro : forall H,
  normal H ->
  H ==> normally H.
Proof using.
  introv N. unfold normally. hsimpl H \[]. 
  { rewrite RO_empty. hsimpl. }
  { auto. }
Qed.

Lemma normal_normally : forall H,
  normal (normally H).
Proof using.
  intros H. applys normal_hexists. intros H1.
  applys normal_hexists. intros H2.
  applys normal_pure_star_hprop. intros N1.
  applys normal_star. auto.
  applys normal_pure.
Qed.

Lemma normally_himpl : forall H1 H2,
  (H1 ==> H2) ->
  normally H1 ==> normally H2.
Proof using.
  introv M. unfold normally. hpull ;=> H1a H1b N1 R1.
  hsimpl~ H1a H1b. hchanges~ R1.
Qed.

Lemma normally_hstar : forall H1 H2,
  normally H1 \* normally H2 ==> normally (H1 \* H2).
Proof using.
  intros. unfold normally. hpull ;=> H1a H1b N1 R1 H2a H2b N2 R2.
  hsimpl (H1a \* H2a) (H1b \* H2b).
  { rew_heap. hchange (RO_star H1b H2b). hchange R1. hchanges R2. }
  { applys~ normal_star. }
Qed.

Lemma normally_idempotent1 : forall H,
  normally H ==> normally (normally H).
Proof using.
  intros. applys normally_intro. applys normal_normally.
Qed.

End Normally3.




Lemma normally_idempotent2 : forall H,
  normally (normally H) ==> normally H.
Proof using.
  intros. unfold normally at 1. hpull ;=> H1 H2 N1 R.
  asserts E: (\[] ==> RO H2). (* equivalent to [RO H2 hempty] *)
  { intros h M. rewrites (hempty_inv (rm M)).
    (* This is almost true. I think we would need to know that H2 is not [False],
       i.e. that at least one heap satisfies [RO H2] in order to conclude.
       We might try to add \[exists h2, RO H2 h2], or equivalently
       \[ ~ (RO H2 ==> \[False]) ] to the definition. *)
    admit.
  }
  hchange E. hchanges R.
Abort.
(* It this was true, we would have a proper idempotence result:
   [(normally (normally H)) = normally H]
*)

End Normally.


Program Definition normally' H h1 := (* alternative *)
  exists h2,  heap_compat h1 h2
           /\ h1^r = fmap_empty
           /\ h2^f = fmap_empty
           /\ H (h1 \u h2).

Lemma normally_of_normally' : forall H,
  normally' H ==> normally H.
Proof using.
  intros H h1 (h2&M1&M2&M3&M4).
  hnf. exists (=h1) (=h2). 
  rewrite hstar_pure. split.
  { intros h' E. subst h'. auto. }
  { exists h1 h2. splits~.




Tactic Notation "exists_dep" "as" ident(E) :=
  match goal with |- exists (_:?T), _ => 
    assert (E:T); [ | exists E ] end.
Tactic Notation "exists_dep" "~" "as" ident(E) :=
  exists_dep as E; auto_tilde.
Tactic Notation "exists_dep" "*" "as" ident(E) :=
  exists_dep as E; auto_star.

Tactic Notation "exists_dep" :=
  let H := fresh "H" in exists_dep as H.
Tactic Notation "exists_dep" "~" :=
  exists_dep; auto_tilde.
Tactic Notation "exists_dep" "*" :=
  exists_dep; auto_star.

(* NOT NEEDED
  (** Equivalence with a direct definition of read-only singleton heap *)

  Definition hprop_single_ro (l:loc) (v:val) : hprop := 
    fun h =>
      let '(f,g) := heap_obj h in
      f = state_empty /\ g = state_single l v.

  Lemma RO_single : forall l v,
      (RO (hprop_single l v))
    = hprop_single_ro l v.
  Proof using. 
    Transparent hprop_single.
    intros. unfold hprop_single, hprop_single_ro.
    apply prop_ext_1. intros ((f,g)&D). 
    iff (M1&h1&g1&M2&(M3a&M3b)&M4) (M1&M2).
    { subst. split~. state_eq. }
    { split. auto. simpl. exists g f.
      exists_dep. state_disjoint.
      subst. splits~. state_eq. }
  Qed.
*)

(* NOT NEEDED
Lemma heap_union_transfer : forall h1 h2 h3,
  heap_compat (h1 \u h2) h3 ->
  heap_compat h1 h2 ->
  heap_compat h1 (h2 \u h3).
Proof using.
  introv M1 M2. lets (N1&N2): heap_compat_union_l_inv M1 M2.
  applys* heap_compat_union_r.
Qed.  

Lemma heap_union_transfer' : forall h1 h2 h3,
  heap_compat (h1 \u h2) h3 ->
  heap_compat h1 h2 ->
  heap_compat h2 (h1 \u h3).
Proof using.
  introv M2 M1. lets (N1&N2): heap_compat_union_r_inv M1 M2.
  applys* heap_compat_union_r.
Qed.  
*)
(* NOT NEEDED YET
Lemma hprop_star_single_same_loc_disjoint : forall (l:loc) (v1 v2:val),
  (hprop_single l v1) \* (hprop_single l v2) ==> \[False].
Proof using.
  intros. unfold hprop_single. intros h (h1&h2&E1&E2&D&E). false.
  subst. applys* state_single_same_loc_disjoint.
Qed.
*)

(* NOT NEEDED YET
Lemma himpl_cancel : forall H1 H2 H2',
  H2 ==> H2' -> (H1 \* H2) ==> (H1 \* H2').
Proof using.
  introv W. intros ((f,g)&D) (f1&g1&f2&g2&D1&D2&M1&M2&M3&M4&M5).
  unf. exists f1 g1 f2 g2.
  exists_dep as V1. state_disjoint.
  exists_dep as V2. state_disjoint. splits~.
  { applys_eq M2 1. fequals. }
  { applys W. applys_eq M3 1. fequals. }
Qed.
*)

(* NOT NEEDED?
Lemma himpl_remove_gc : forall H H',
  H ==> H' ->
  H ==> H' \* \GC.
Proof using.
  introv M. intros ((f,g)&D) N. 
  exists f g state_empty state_empty. unf.
  exists_dep as V1. state_disjoint.
  exists_dep as V2. state_disjoint. splits~.
  { split. 
    { applys state_agree_empty_r. }
    { state_disjoint. } }
  { applys M. applys_eq N 1. fequals. }
  { exists \[]. applys hprop_empty_prove. }
  { state_eq. }
  { state_eq. }
Qed.
*)






Lemma rule_get_traditional : forall v l,
  triple (trm_get (val_loc l)) (l ~~> v) (fun x => \[x = v] \* l ~~> v).
Proof using.
  (* LATER: derive this from above proof using frame-ro *)
  intros. intros h1 (E1&E2) h2 D. exists v h1. splits~.
  { rew_state; auto. applys red_get.
    { unstate. rewrite E1,E2. state_eq. }
    { unstate. rewrite E2. state_disjoint. } }
  { applys~ on_rw_sub_base. applys~ himpl_inst_prop (l ~~> v). split~. }
Qed.


Lemma rule_let_read_only : forall x t1 t2 H1 H2 Q Q1,
  triple t1 (H1 \* RO H2) Q1 ->
  (forall (X:val), triple (subst_trm x X t2) (Q1 X \* RO H2) Q) ->
  triple (trm_let x t1 t2) (H1 \* RO H2) Q.



(** More lemmas on [agree] *)

Lemma state_agree_transfer : forall f1 f2 f3,
  state_agree f1 f2 ->
  state_agree (f1 \+ f2) f3 ->
  state_agree f1 (f2 \+ f3).
Proof using.
  introv M1 M2. intros l v1 v2 E1 E2. 
  specializes M1 l v1 v2. specializes M2 l v1 v2.
  simpls. unfolds pfun_union.
  specializes M1 E1. rewrite E1 in M2. specializes~ M2 __.
  clear E1.
  cases (state_data f2 l). 
  { inverts E2. applys* M1. }
  { applys* M2. }
Qed.

Lemma state_agree_transfer' : forall f1 f2 f3,
  state_agree f1 f2 ->
  state_agree (f1 \+ f2) f3 ->
  state_agree f2 (f1 \+ f3).
Proof using.
  hint state_agree_sym.
  introv M1 M2. applys* state_agree_transfer.
  rewrite~ state_union_comm_agree. 
Qed.




Tactic Notation "exists_dep" "as" ident(E) :=
  match goal with |- exists (_:?T), _ => 
    assert (E:T); [ | exists E ] end.
Tactic Notation "exists_dep" "~" "as" ident(E) :=
  exists_dep as E; auto_tilde.
Tactic Notation "exists_dep" "*" "as" ident(E) :=
  exists_dep as E; auto_star.

Tactic Notation "exists_dep" :=
  let H := fresh "H" in exists_dep as H.
Tactic Notation "exists_dep" "~" :=
  exists_dep; auto_tilde.
Tactic Notation "exists_dep" "*" :=
  exists_dep; auto_star.

(* NOT NEEDED
  (** Equivalence with a direct definition of read-only singleton heap *)

  Definition hprop_single_ro (l:loc) (v:val) : hprop := 
    fun h =>
      let '(f,g) := heap_obj h in
      f = state_empty /\ g = state_single l v.

  Lemma RO_single : forall l v,
      (RO (hprop_single l v))
    = hprop_single_ro l v.
  Proof using. 
    Transparent hprop_single.
    intros. unfold hprop_single, hprop_single_ro.
    apply prop_ext_1. intros ((f,g)&D). 
    iff (M1&h1&g1&M2&(M3a&M3b)&M4) (M1&M2).
    { subst. split~. state_eq. }
    { split. auto. simpl. exists g f.
      exists_dep. state_disjoint.
      subst. splits~. state_eq. }
  Qed.
*)

(* NOT NEEDED
Lemma heap_union_transfer : forall h1 h2 h3,
  heap_compat (h1 \u h2) h3 ->
  heap_compat h1 h2 ->
  heap_compat h1 (h2 \u h3).
Proof using.
  introv M1 M2. lets (N1&N2): heap_compat_union_l_inv M1 M2.
  applys* heap_compat_union_r.
Qed.  

Lemma heap_union_transfer' : forall h1 h2 h3,
  heap_compat (h1 \u h2) h3 ->
  heap_compat h1 h2 ->
  heap_compat h2 (h1 \u h3).
Proof using.
  introv M2 M1. lets (N1&N2): heap_compat_union_r_inv M1 M2.
  applys* heap_compat_union_r.
Qed.  
*)
(* NOT NEEDED YET
Lemma hprop_star_single_same_loc_disjoint : forall (l:loc) (v1 v2:val),
  (hprop_single l v1) \* (hprop_single l v2) ==> \[False].
Proof using.
  intros. unfold hprop_single. intros h (h1&h2&E1&E2&D&E). false.
  subst. applys* state_single_same_loc_disjoint.
Qed.
*)

(* NOT NEEDED YET
Lemma himpl_cancel : forall H1 H2 H2',
  H2 ==> H2' -> (H1 \* H2) ==> (H1 \* H2').
Proof using.
  introv W. intros ((f,g)&D) (f1&g1&f2&g2&D1&D2&M1&M2&M3&M4&M5).
  unf. exists f1 g1 f2 g2.
  exists_dep as V1. state_disjoint.
  exists_dep as V2. state_disjoint. splits~.
  { applys_eq M2 1. fequals. }
  { applys W. applys_eq M3 1. fequals. }
Qed.
*)

(* NOT NEEDED?
Lemma himpl_remove_gc : forall H H',
  H ==> H' ->
  H ==> H' \* \GC.
Proof using.
  introv M. intros ((f,g)&D) N. 
  exists f g state_empty state_empty. unf.
  exists_dep as V1. state_disjoint.
  exists_dep as V2. state_disjoint. splits~.
  { split. 
    { applys state_agree_empty_r. }
    { state_disjoint. } }
  { applys M. applys_eq N 1. fequals. }
  { exists \[]. applys hprop_empty_prove. }
  { state_eq. }
  { state_eq. }
Qed.
*)






Lemma state_agree_union_lr_inv_agree_agree_disjoint : forall f1 f2 f3,
  state_agree (f1 \+ f2) f3 ->
  state_disjoint f1 f2 ->
  state_agree f2 f3.
Proof using.
  introv M D. rewrite~ (@state_union_comm_disjoint f1 f2) in M.
  applys* state_agree_union_ll_inv.
Qed.

Lemma state_agree_union_rr_inv_agree_disjoint : forall f1 f2 f3,
  state_agree f1 (f2 \+ f3) ->
  state_disjoint f2 f3 ->
  state_agree f1 f3.
Proof using.
  hint state_agree_union_lr_inv_agree_agree, state_disjoint, state_agree_sym. 
  eauto.
Qed.