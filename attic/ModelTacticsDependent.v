

(********************************************************************)
(********************************************************************)
(********************************************************************)

(*

(*------------------------------------------------------------------*)
(* ** Separation Logic monoid *)

Require Import LibStruct.
Definition sep_monoid := monoid_ heap_is_star heap_is_empty.

Global Instance sep_Monoid : Monoid sep_monoid.
Proof using.
  constructor; simpl.
  apply star_assoc.
  apply star_neutral_l.
  apply star_neutral_r.
Qed.

Global Instance sep_Monoid_commutative : Monoid_commutative sep_monoid.
Proof using.
  constructor; simpl.
  applys sep_Monoid.
  apply star_comm.
Qed.


(********************************************************************)
(* ** Shared tactics *)

Ltac on_formula_pre cont :=
  match goal with
  | |- _ ?H ?Q => cont H
  | |- _ _ ?H ?Q => cont H
  | |- _ _ _ ?H ?Q => cont H
  | |- _ _ _ _ ?H ?Q => cont H
  | |- _ _ _ _ _ ?H ?Q => cont H
  | |- _ _ _ _ _ _ ?H ?Q => cont H
  | |- _ _ _ _ _ _ _ ?H ?Q => cont H
  | |- _ _ _ _ _ _ _ _ ?H ?Q => cont H
  | |- _ _ _ _ _ _ _ _ _ ?H ?Q => cont H
  | |- _ _ _ _ _ _ _ _ _ _ ?H ?Q => cont H
  end.

Ltac on_formula_post cont :=
  match goal with
  | |- _ ?H ?Q => cont Q
  | |- _ _ ?H ?Q => cont Q
  | |- _ _ _ ?H ?Q => cont Q
  | |- _ _ _ _ ?H ?Q => cont Q
  | |- _ _ _ _ _ ?H ?Q => cont Q
  | |- _ _ _ _ _ _ ?H ?Q => cont Q
  | |- _ _ _ _ _ _ _ ?H ?Q => cont Q
  | |- _ _ _ _ _ _ _ _ ?H ?Q => cont Q
  | |- _ _ _ _ _ _ _ _ _ ?H ?Q => cont Q
  | |- _ _ _ _ _ _ _ _ _ _ ?H ?Q => cont Q
  end.

Ltac remove_empty_heaps_formula tt :=
  repeat (on_formula_pre ltac:(remove_empty_heaps_from)).




(********************************************************************)
(* ** Other stuff *)

Lemma heap_weaken_star : forall H1' H1 H2 H3,
  (H1 ==> H1') -> (H1' \* H2 ==> H3) -> (H1 \* H2 ==> H3).
Proof using.
  introv W M (h1&h2&N). unpack N. apply M. exists~ h1 h2.
Qed. (* todo: move *)

Lemma hsimpl_to_qunit : forall (H:hprop) (Q:unit->hprop),
  Q = (fun _ => H) ->
  H ==> Q tt.
Proof using. intros. subst. auto. Qed. (* todo: needed? *)
Hint Resolve hsimpl_to_qunit.




Lemma hprop_empty_core_eq_empty : 
  hprop_empty_core = \[].
Proof using. 
  unfold hprop_empty, hprop_pure. applys hprop_eq; intros h; autos*.
Qed.

Lemma hprop_star_empty_l : neutral_l hprop_star hprop_empty.
Proof using.
  intros H. rewrite <- hprop_empty_core_eq_empty.
  applys hprop_star_empty_core_l.
Qed.











========================
(** 

This file provides tactics for automatically simplifying heap
entailment relations.

WORK IN PROGRESS: this file is not complete yet.

Author: Arthur Charguéraud.
License: MIT.

*)

Require Import LibTactics Shared LibOperation LibStruct.


(********************************************************************)
(** * Structure *)

(*------------------------------------------------------------------*)
(* ** Operators of a Separation Logic *)

Module SepLogicOps.

Record SepLogicOps (heap : Type) := 
  make_SepLogicOps {
  (* let hprop := heap -> Prop in *)
  pure : Prop -> (heap->Prop);
  star : (heap->Prop) -> (heap->Prop) -> (heap->Prop);
  exist : forall {A}, A -> (heap->Prop);
  gc : (heap->Prop);
  }.

Implicit Arguments pure [heap].
Implicit Arguments star [heap].
Implicit Arguments exist [heap A].
Implicit Arguments gc [heap].

End SepLogicOps.


(*------------------------------------------------------------------*)
(* ** Properties of a Separation Logic *)

Module SepLogicProp.

Export SepLogicOps.

Section Def.
Variables (heap : Type) (S : @SepLogicOps heap).

Local Notation "H1 '\*' H2" := (star S H1 H2)
  (at level 41, right associativity).

Definition hprop := heap -> Prop.
Definition empty := pure S True.

Record SepLogicProp := make_SepLogicProp {
  star_neutral_l : neutral_l (star S) empty;
  star_comm : comm (star S);
  star_assoc : assoc (star S);
  star_exists : forall A (J:A->hprop) H,
    H \* (exist S J) = exist S (fun x => H \* (J x));
  extract_prop : forall H (P:Prop) H',
    (P -> (H ==> H')) ->
    H \* (pure S P) ==> H';
  extract_exists : forall A (J:A->hprop) H',
    (forall x, (J x ==> H')) ->
    exist S J ==> H';
  }.

End Def.

Implicit Arguments make_SepLogicProp [heap].
Implicit Arguments SepLogicProp [heap].

End SepLogicProp.


(*------------------------------------------------------------------*)
(* ** Package of a Separation Logic *)

Module SepLogic.

Export SepLogicProp.

Record SepLogic := make_SepLogic {
  heap : Type;
  ops :> SepLogicOps heap;
  prop : SepLogicProp ops; }.

Implicit Arguments make_SepLogic [heap ops].

End SepLogic.




(********************************************************************)
(** * Derived definitions *)

Module Type SepLogicArg.
Import SepLogic.
Parameter S : SepLogic.
End SepLogicArg.

(*---*)

Module SepLogicSetup (SL:SepLogicArg).

Import SepLogic SL.


Hint Extern 1 (SepLogicProp (ops S)) => apply (prop SL.S).


(*------------------------------------------------------------------*)
(* ** Notation *)

Notation "'heap'" := (heap S).

Definition hprop := heap -> Prop.

Definition empty := pure S True.

Notation "\[ L ]" := (pure S L) 
  (at level 0, L at level 99) : heap_scope.

Notation "\[]" := (empty) 
  (at level 0) : heap_scope.

Notation "H1 '\*' H2" := (star S H1 H2)
  (at level 41, right associativity) : heap_scope.

Notation "Q \*+ H" := (fun x => star S (Q x) H)
  (at level 40) : heap_scope.

Notation "'Hexists' x1 , H" := (exist S (fun x1 => H))
  (at level 39, x1 ident, H at level 50) : heap_scope.
Notation "'Hexists' x1 : T1 , H" := (exist S (fun x1:T1 => H))
  (at level 39, x1 ident, H at level 50, only parsing) : heap_scope.
Notation "'Hexists' ( x1 : T1 ) , H" := (exist S (fun x1:T1 => H))
  (at level 39, x1 ident, H at level 50, only parsing) : heap_scope.

Notation "\GC" := (gc S) : heap_scope. 

(*
Notation "r '~~>' v" := (hprop_single r v)
  (at level 32, no associativity) : heap_scope.
*)

Lemma star_neutral_r : neutral_r (star S) empty.
Proof using.
  applys neutral_r_from_comm_neutral_l. 
  applys~ star_comm. 
  applys~ star_neutral_l.
Qed.

End SepLogicSetup.




(********************************************************************)
(** * Demo *)

Module MySepLogic : SepLogicArg.
Export SepLogic.

Parameter heap : Type.
Parameter ops : SepLogicOps heap.
Parameter prop : SepLogicProp ops.

Definition S := make_SepLogic prop.

End MySepLogic.

Module MySepLogicSetup.
Import SepLogic.
Module Import SLS := SepLogicSetup MySepLogic.
Open Scope heap_scope.

Lemma demo1 : \[] = \[] :> hprop.
Proof using. auto. Qed.

Lemma demo2 : forall (H:hprop), H \* \[] = H.
Proof using. applys star_neutral_r. Qed.




End MySepLogicSetup.





(********************************************************************)
(********************************************************************)
(********************************************************************)

(*

(*------------------------------------------------------------------*)
(* ** Separation Logic monoid *)

Require Import LibStruct.
Definition sep_monoid := monoid_ heap_is_star heap_is_empty.

Global Instance sep_Monoid : Monoid sep_monoid.
Proof using.
  constructor; simpl.
  apply star_assoc.
  apply star_neutral_l.
  apply star_neutral_r.
Qed.

Global Instance sep_Monoid_commutative : Monoid_commutative sep_monoid.
Proof using.
  constructor; simpl.
  applys sep_Monoid.
  apply star_comm.
Qed.


(********************************************************************)
(* ** Shared tactics *)

Ltac prepare_goal_hpull_himpl tt :=
  match goal with 
  | |- @rel_le unit _ _ _ => let t := fresh "_tt" in intros t; destruct t
  | |- @rel_le _ _ _ _ => let r := fresh "r" in intros r
  | |- pred_le _ _ => idtac
  end.

Ltac remove_empty_heaps_from H :=
  match H with context[ ?H1 \* \[] ] =>
    rewrite (@star_neutral_r H1) end.
  
Ltac remove_empty_heaps_left tt :=
  repeat match goal with |- ?H1 ==> _ => remove_empty_heaps_from H1 end.

Ltac remove_empty_heaps_right tt :=
  repeat match goal with |- _ ==> ?H2 => remove_empty_heaps_from H2 end.

Ltac on_formula_pre cont :=
  match goal with
  | |- _ ?H ?Q => cont H
  | |- _ _ ?H ?Q => cont H
  | |- _ _ _ ?H ?Q => cont H
  | |- _ _ _ _ ?H ?Q => cont H
  | |- _ _ _ _ _ ?H ?Q => cont H
  | |- _ _ _ _ _ _ ?H ?Q => cont H
  | |- _ _ _ _ _ _ _ ?H ?Q => cont H
  | |- _ _ _ _ _ _ _ _ ?H ?Q => cont H
  | |- _ _ _ _ _ _ _ _ _ ?H ?Q => cont H
  | |- _ _ _ _ _ _ _ _ _ _ ?H ?Q => cont H
  end.

Ltac on_formula_post cont :=
  match goal with
  | |- _ ?H ?Q => cont Q
  | |- _ _ ?H ?Q => cont Q
  | |- _ _ _ ?H ?Q => cont Q
  | |- _ _ _ _ ?H ?Q => cont Q
  | |- _ _ _ _ _ ?H ?Q => cont Q
  | |- _ _ _ _ _ _ ?H ?Q => cont Q
  | |- _ _ _ _ _ _ _ ?H ?Q => cont Q
  | |- _ _ _ _ _ _ _ _ ?H ?Q => cont Q
  | |- _ _ _ _ _ _ _ _ _ ?H ?Q => cont Q
  | |- _ _ _ _ _ _ _ _ _ _ ?H ?Q => cont Q
  end.

Ltac remove_empty_heaps_formula tt :=
  repeat (on_formula_pre ltac:(remove_empty_heaps_from)).


(********************************************************************)
(* ** Extraction from [H1] in [H1 ==> H2] *)

(** Lemmas *)

Lemma hpull_start : forall H H',
  \[] \* H ==> H' -> H ==> H'.
Proof using. intros. rew_heap in *. auto. Qed.

Lemma hpull_stop : forall H H',
  H ==> H' -> H \* \[] ==> H'.
Proof using. intros. rew_heap in *. auto. Qed.

Lemma hpull_keep : forall H1 H2 H3 H',
  (H2 \* H1) \* H3 ==> H' -> H1 \* (H2 \* H3) ==> H'.
Proof using. intros. rewrite (star_comm H2) in H. rew_heap in *. auto. Qed.

Lemma hpull_starify : forall H1 H2 H',
  H1 \* (H2 \* \[]) ==> H' -> H1 \* H2 ==> H'.
Proof using. intros. rew_heap in *. auto. Qed.

Lemma hpull_assoc : forall H1 H2 H3 H4 H',
  H1 \* (H2 \* (H3 \* H4)) ==> H' -> H1 \* ((H2 \* H3) \* H4) ==> H'.
Proof using. intros. rew_heap in *. auto. Qed.

Lemma hpull_prop : forall H1 H2 H' (P:Prop),
  (P -> H1 \* H2 ==> H') -> H1 \* (\[P] \* H2) ==> H'.
Proof using.
  introv W. intros h Hh.
  destruct Hh as (h1&h2'&?&(h2&h3&(?&?)&?&?&?)&?&?).
  apply~ W. exists h1 h3. subst h h2 h2'.
  rewrite heap_union_neutral_l in *. splits~.
Qed.

Lemma hpull_empty : forall H1 H2 H',
  (H1 \* H2 ==> H') -> H1 \* (\[] \* H2) ==> H'.
Proof using.
  introv W. intros h Hh. destruct Hh as (h1&h2'&?&(h2&h3&M&?&?&?)&?&?).
  apply~ W. inverts M. exists h1 h3. subst h h2'.
  rewrite heap_union_neutral_l in *. splits~.
Qed.

Lemma hpull_exists : forall A H1 H2 H' (J:A->hprop),
  (forall x, H1 \* J x \* H2 ==> H') -> H1 \* (heap_is_pack J \* H2) ==> H'.
Proof using.  
  introv W. intros h Hh.
  destruct Hh as (h1&h2'&?&(h2&h3&(?&?)&?&?&?)&?&?).
  applys~ W x. exists h1 (heap_union h2 h3). subst h h2'.
  splits~. exists h2 h3. splits~.
Qed.

(** Tactics *)

Ltac hpull_setup tt :=
  prepare_goal_hpull_himpl tt;
  lets: ltac_mark;
  apply hpull_start.

Ltac hpull_cleanup tt :=
  apply hpull_stop;
  remove_empty_heaps_left tt;
  tryfalse;
  gen_until_mark.

Ltac hpull_step tt :=
  match goal with |- _ \* ?HN ==> _ => 
  match HN with 
  | ?H \* _ =>
     match H with
     | \[] => apply hpull_empty
     | \[_] => apply hpull_prop; intros
     | heap_is_pack _ => apply hpull_exists; intros
     | _ \* _ => apply hpull_assoc
     | _ => apply hpull_keep
     end
  | \[] => fail 1
  | ?H => apply hpull_starify
  end end.

Ltac hpull_main tt :=
  hpull_setup tt;
  (repeat (hpull_step tt));
  hpull_cleanup tt.

Ltac hpull_post tt :=
  reflect_clean tt.

Ltac hpull_core :=
  hpull_main tt; hpull_post tt.

Ltac hpull_if_needed tt := (* deprecated  ==> see hpullable *)
  match goal with |- ?H ==> _ => match H with
  | context [ heap_is_pack _ ] => hpull_core
  | context [ \[ _ ] ] => hpull_core
  end end.

Tactic Notation "hpull" := 
  hpull_core.
Tactic Notation "hpull" "as" simple_intropattern(I1) := 
  hpull; intros I1.
Tactic Notation "hpull" "as" simple_intropattern(I1) simple_intropattern(I2) := 
  hpull; intros I1 I2.
Tactic Notation "hpull" "as" simple_intropattern(I1) simple_intropattern(I2)
 simple_intropattern(I3) := 
  hpull; intros I1 I2 I3.
Tactic Notation "hpull" "as" simple_intropattern(I1) simple_intropattern(I2)
 simple_intropattern(I3) simple_intropattern(I4) := 
  hpull; intros I1 I2 I3 I4. 
Tactic Notation "hpull" "as" simple_intropattern(I1) simple_intropattern(I2)
 simple_intropattern(I3) simple_intropattern(I4) simple_intropattern(I5) := 
  hpull; intros I1 I2 I3 I4 I5. 
Tactic Notation "hpull" "as" simple_intropattern(I1) simple_intropattern(I2)
 simple_intropattern(I3) simple_intropattern(I4) simple_intropattern(I5)
 simple_intropattern(I6) := 
  hpull; intros I1 I2 I3 I4 I5 I6. 
Tactic Notation "hpull" "as" simple_intropattern(I1) simple_intropattern(I2)
 simple_intropattern(I3) simple_intropattern(I4) simple_intropattern(I5)
 simple_intropattern(I6) simple_intropattern(I7) := 
  hpull; intros I1 I2 I3 I4 I5 I6 I7. 

Tactic Notation "hpulls" :=
  let E := fresh "TEMP" in hpull as E; subst_hyp E.


(********************************************************************)
(* ** Simplification in [H2] on [H1 ==> H2] *)

(** Hints *)

Inductive Hsimpl_hint : list Boxer -> Type :=
  | hsimpl_hint : forall (L:list Boxer), Hsimpl_hint L.

Ltac hsimpl_hint_put L := 
  let H := fresh "Hint" in 
  generalize (hsimpl_hint L); intros H.

Ltac hsimpl_hint_next cont :=
  match goal with H: Hsimpl_hint ((boxer ?x)::?L) |- _ =>
    clear H; hsimpl_hint_put L; cont x end.

Ltac hsimpl_hint_remove tt :=
  match goal with H: Hsimpl_hint _ |- _ => clear H end.


(** Lemmas *)

Lemma hsimpl_start : forall H' H1,
  H' ==> \[] \* H1 -> H' ==> H1.
Proof using. intros. rew_heap in *. auto. Qed.

Lemma hsimpl_stop : forall H' H1,
  H' ==> H1 -> H' ==> H1 \* \[].
Proof using. intros. rew_heap in *. auto. Qed.

Lemma hsimpl_keep : forall H' H1 H2 H3,
  H' ==> (H2 \* H1) \* H3 -> H' ==> H1 \* (H2 \* H3).
Proof using. intros. rewrite (star_comm H2) in H. rew_heap in *. auto. Qed.

Lemma hsimpl_assoc : forall H' H1 H2 H3 H4,
  H' ==> H1 \* (H2 \* (H3 \* H4)) -> H' ==> H1 \* ((H2 \* H3) \* H4).
Proof using. intros. rew_heap in *. auto. Qed.

Lemma hsimpl_starify : forall H' H1 H2,
  H' ==> H1 \* (H2 \* \[]) -> H' ==> H1 \* H2.
Proof using. intros. rew_heap in *. auto. Qed.

Lemma hsimpl_empty : forall H' H1 H2,
  H' ==> H1 \* H2 -> H' ==> H1 \* (\[] \* H2).
Proof using.
  introv W PH1. destruct (W _ PH1) as (h1&h2&?&?&?&?).
  exists h1 h2. splits~. exists heap_empty h2. splits~.
Qed.

Lemma hsimpl_prop : forall H' H1 H2 (P:Prop),
  H' ==> H1 \* H2 -> P -> H' ==> H1 \* (\[P] \* H2).
Proof using.
  introv W HP PH1. destruct (W _ PH1) as (h1&h2&?&?&?&?).
  exists h1 h2. splits~. exists heap_empty h2. splits~.
Qed.

Lemma hsimpl_exists : forall A (x:A) H' H1 H2 (J:A->hprop),
  H' ==> H1 \* J x \* H2 -> H' ==> H1 \* (heap_is_pack J \* H2).
Proof using.
  introv W. intros h' PH'. destruct (W _ PH') as (h2&h4&?&(hx&h3&?&?&?&?)&?&?).
  exists h2 (heap_union hx h3). subst h' h4. splits~.
  exists hx h3. splits~. exists~ x.
Qed.

Lemma hsimpl_gc : forall H, 
  H ==> \GC.
Proof using. intros. unfold heap_is_gc. introv M. exists~ H. Qed.

Lemma hsimpl_cancel_1 : forall H HA HR HT,
  HT ==> HA \* HR -> H \* HT ==> HA \* (H \* HR).
Proof using. intros. rewrite star_comm_assoc. apply~ star_cancel. Qed.

Lemma hsimpl_cancel_2 : forall H HA HR H1 HT,
  H1 \* HT ==> HA \* HR -> 
  H1 \* H \* HT ==> HA \* (H \* HR).
Proof using. intros. rewrite (star_comm_assoc H1). apply~ hsimpl_cancel_1. Qed.

Lemma hsimpl_cancel_3 : forall H HA HR H1 H2 HT,
  H1 \* H2 \* HT ==> HA \* HR -> H1 \* H2 \* H \* HT ==> HA \* (H \* HR).
Proof using. intros. rewrite (star_comm_assoc H2). apply~ hsimpl_cancel_2. Qed.

Lemma hsimpl_cancel_4 : forall H HA HR H1 H2 H3 HT,
  H1 \* H2 \* H3 \* HT ==> HA \* HR ->
  H1 \* H2 \* H3 \* H \* HT ==> HA \* (H \* HR).
(*Proof using. intros. rewrite (star_comm_assoc H3). apply~ hsimpl_cancel_3. Qed.*)
Admitted. (* commented out for faster compilation *)

Lemma hsimpl_cancel_5 : forall H HA HR H1 H2 H3 H4 HT,
  H1 \* H2 \* H3 \* H4 \* HT ==> HA \* HR -> 
  H1 \* H2 \* H3 \* H4 \* H \* HT ==> HA \* (H \* HR).
(*Proof using. intros. rewrite (star_comm_assoc H4). apply~ hsimpl_cancel_4. Qed.*)
Admitted. (* commented out for faster compilation *)

Lemma hsimpl_cancel_6 : forall H HA HR H1 H2 H3 H4 H5 HT,
  H1 \* H2 \* H3 \* H4 \* H5 \* HT ==> HA \* HR -> 
  H1 \* H2 \* H3 \* H4 \* H5 \* H \* HT ==> HA \* (H \* HR).
(*Proof using. intros. rewrite (star_comm_assoc H5). apply~ hsimpl_cancel_5. Qed.*)
Admitted. (* commented out for faster compilation *)

Lemma hsimpl_cancel_7 : forall H HA HR H1 H2 H3 H4 H5 H6 HT,
  H1 \* H2 \* H3 \* H4 \* H5 \* H6 \* HT ==> HA \* HR -> 
  H1 \* H2 \* H3 \* H4 \* H5 \* H6 \* H \* HT ==> HA \* (H \* HR).
(*Proof using. intros. rewrite (star_comm_assoc H6). apply~ hsimpl_cancel_6. Qed.*)
Admitted. (* commented out for faster compilation *)

Lemma hsimpl_cancel_8 : forall H HA HR H1 H2 H3 H4 H5 H6 H7 HT,
  H1 \* H2 \* H3 \* H4 \* H5 \* H6 \* H7 \* HT ==> HA \* HR -> 
  H1 \* H2 \* H3 \* H4 \* H5 \* H6 \* H7 \* H \* HT ==> HA \* (H \* HR).
(*Proof using. intros. rewrite (star_comm_assoc H7). apply~ hsimpl_cancel_7. Qed.*)
Admitted. (* commented out for faster compilation *)

Lemma hsimpl_cancel_9 : forall H HA HR H1 H2 H3 H4 H5 H6 H7 H8 HT,
  H1 \* H2 \* H3 \* H4 \* H5 \* H6 \* H7 \* H8 \* HT ==> HA \* HR -> 
  H1 \* H2 \* H3 \* H4 \* H5 \* H6 \* H7 \* H8 \* H \* HT ==> HA \* (H \* HR).
(*Proof using. intros. rewrite (star_comm_assoc H8). apply~ hsimpl_cancel_8. Qed.*)
Admitted. (* commented out for faster compilation *)

Lemma hsimpl_cancel_10 : forall H HA HR H1 H2 H3 H4 H5 H6 H7 H8 H9 HT,
  H1 \* H2 \* H3 \* H4 \* H5 \* H6 \* H7 \* H8 \* H9 \* HT ==> HA \* HR -> 
  H1 \* H2 \* H3 \* H4 \* H5 \* H6 \* H7 \* H8 \* H9 \* H \* HT ==> HA \* (H \* HR).
(*Proof using. intros. rewrite (star_comm_assoc H9). apply~ hsimpl_cancel_9. Qed.*)
Admitted. (* commented out for faster compilation *)

Lemma hsimpl_cancel_eq_1 : forall H H' HA HR HT,
  H = H' -> HT ==> HA \* HR -> H \* HT ==> HA \* (H' \* HR).
Proof using. intros. subst. apply~ hsimpl_cancel_1. Qed.

Lemma hsimpl_cancel_eq_2 : forall H H' HA HR H1 HT,
  H = H' -> H1 \* HT ==> HA \* HR -> 
  H1 \* H \* HT ==> HA \* (H' \* HR).
Proof using. intros. subst. apply~ hsimpl_cancel_2. Qed.

Lemma hsimpl_cancel_eq_3 : forall H H' HA HR H1 H2 HT,
  H = H' -> H1 \* H2 \* HT ==> HA \* HR -> 
  H1 \* H2 \* H \* HT ==> HA \* (H' \* HR).
Proof using. intros. subst. apply~ hsimpl_cancel_3. Qed.

Lemma hsimpl_cancel_eq_4 : forall H H' HA HR H1 H2 H3 HT,
  H = H' -> H1 \* H2 \* H3 \* HT ==> HA \* HR -> 
  H1 \* H2 \* H3 \* H \* HT ==> HA \* (H' \* HR).
Proof using. intros. subst. apply~ hsimpl_cancel_4. Qed.

Lemma hsimpl_cancel_eq_5 : forall H H' HA HR H1 H2 H3 H4 HT,
  H = H' -> H1 \* H2 \* H3 \* H4 \* HT ==> HA \* HR -> 
  H1 \* H2 \* H3 \* H4 \* H \* HT ==> HA \* (H' \* HR).
Proof using. intros. subst. apply~ hsimpl_cancel_5. Qed.

Lemma hsimpl_cancel_eq_6 : forall H H' HA HR H1 H2 H3 H4 H5 HT,
  H = H' -> H1 \* H2 \* H3 \* H4 \* H5 \* HT ==> HA \* HR ->
  H1 \* H2 \* H3 \* H4 \* H5 \* H \* HT ==> HA \* (H' \* HR).
Proof using. intros. subst. apply~ hsimpl_cancel_6. Qed.

Lemma hsimpl_cancel_eq_7 : forall H H' HA HR H1 H2 H3 H4 H5 H6 HT,
  H = H' -> H1 \* H2 \* H3 \* H4 \* H5 \* H6 \* HT ==> HA \* HR ->
  H1 \* H2 \* H3 \* H4 \* H5 \* H6 \* H \* HT ==> HA \* (H' \* HR).
Proof using. intros. subst. apply~ hsimpl_cancel_7. Qed.

Lemma hsimpl_cancel_eq_8 : forall H H' HA HR H1 H2 H3 H4 H5 H6 H7 HT,
  H = H' -> H1 \* H2 \* H3 \* H4 \* H5 \* H6 \* H7 \* HT ==> HA \* HR ->
  H1 \* H2 \* H3 \* H4 \* H5 \* H6 \* H7 \* H \* HT ==> HA \* (H' \* HR).
Proof using. intros. subst. apply~ hsimpl_cancel_8. Qed.

Lemma hsimpl_cancel_eq_9 : forall H H' HA HR H1 H2 H3 H4 H5 H6 H7 H8 HT,
  H = H' -> H1 \* H2 \* H3 \* H4 \* H5 \* H6 \* H7 \* H8 \* HT ==> HA \* HR ->
  H1 \* H2 \* H3 \* H4 \* H5 \* H6 \* H7 \* H8 \* H \* HT ==> HA \* (H' \* HR).
Proof using. intros. subst. apply~ hsimpl_cancel_9. Qed.

Lemma hsimpl_cancel_eq_10 : forall H H' HA HR H1 H2 H3 H4 H5 H6 H7 H8 H9 HT,
  H = H' -> H1 \* H2 \* H3 \* H4 \* H5 \* H6 \* H7 \* H8 \* H9 \* HT ==> HA \* HR ->
  H1 \* H2 \* H3 \* H4 \* H5 \* H6 \* H7 \* H8 \* H9 \* H \* HT ==> HA \* (H' \* HR).
Proof using. intros. subst. apply~ hsimpl_cancel_10. Qed.

Lemma hsimpl_start_1 : forall H1 H', 
  H1 \* \[] ==> H' -> H1 ==> H'.
Proof using. intros. rew_heap in H. auto. Qed.

Lemma hsimpl_start_2 : forall H1 H2 H', 
  H1 \* H2 \* \[] ==> H' -> 
  H1 \* H2 ==> H'.
Proof using. intros. rew_heap in H. auto. Qed.

Lemma hsimpl_start_3 : forall H1 H2 H3 H', 
  H1 \* H2 \* H3 \* \[] ==> H' -> 
  H1 \* H2 \* H3 ==> H'.
(* Proof using. intros. rew_heap in H. auto. Qed.*)
Admitted. (* commented out for faster compilation *)

Lemma hsimpl_start_4 : forall H1 H2 H3 H4 H', 
  H1 \* H2 \* H3 \* H4 \* \[] ==> H' -> 
  H1 \* H2 \* H3 \* H4 ==> H'.
(* Proof using. intros. rew_heap in H. auto. Qed. *)
Admitted. (* commented out for faster compilation *)

Lemma hsimpl_start_5 : forall H1 H2 H3 H4 H5 H', 
  H1 \* H2 \* H3 \* H4 \* H5 \* \[] ==> H' -> 
  H1 \* H2 \* H3 \* H4 \* H5 ==> H'.
(* Proof using. intros. rew_heap in H. auto. Qed. *)
Admitted. (* commented out for faster compilation *)

Lemma hsimpl_start_6 : forall H1 H2 H3 H4 H5 H6 H', 
  H1 \* H2 \* H3 \* H4 \* H5 \* H6 \* \[] ==> H' -> 
  H1 \* H2 \* H3 \* H4 \* H5 \* H6 ==> H'.
(* Proof using. intros. rew_heap in H. auto. Qed. *)
Admitted. (* commented out for faster compilation *)

Lemma hsimpl_start_7 : forall H1 H2 H3 H4 H5 H6 H7 H', 
  H1 \* H2 \* H3 \* H4 \* H5 \* H6 \* H7 \* \[] ==> H' -> 
  H1 \* H2 \* H3 \* H4 \* H5 \* H6 \* H7 ==> H'.
(* Proof using. intros. rew_heap in H. auto. Qed. *)
Admitted. (* commented out for faster compilation *)

Lemma hsimpl_start_8 : forall H1 H2 H3 H4 H5 H6 H7 H8 H', 
  H1 \* H2 \* H3 \* H4 \* H5 \* H6 \* H7 \* H8 \* \[] ==> H' -> 
  H1 \* H2 \* H3 \* H4 \* H5 \* H6 \* H7 \* H8 ==> H'.
(* Proof using. intros. rew_heap in H. auto. Qed. *)
Admitted. (* commented out for faster compilation *)

Lemma hsimpl_start_9 : forall H1 H2 H3 H4 H5 H6 H7 H8 H9 H', 
  H1 \* H2 \* H3 \* H4 \* H5 \* H6 \* H7 \* H8 \* H9 \* \[] ==> H' -> 
  H1 \* H2 \* H3 \* H4 \* H5 \* H6 \* H7 \* H8 \* H9 ==> H'.
(* Proof using. intros. rew_heap in H. auto. Qed. *)
Admitted. (* commented out for faster compilation *)

Lemma hsimpl_start_10 : forall H1 H2 H3 H4 H5 H6 H7 H8 H9 H10 H', 
  H1 \* H2 \* H3 \* H4 \* H5 \* H6 \* H7 \* H8 \* H9 \* H10 \* \[] ==> H' -> 
  H1 \* H2 \* H3 \* H4 \* H5 \* H6 \* H7 \* H8 \* H9 \* H10 ==> H'.
(* Proof using. intros. rew_heap in H. auto. Qed. *)
Admitted. (* commented out for faster compilation *)

(** Tactics *)

Ltac hsimpl_left_empty tt :=
  match goal with |- ?HL ==> _ =>
  match HL with
  | \[] => idtac
  | _ \* \[] => idtac
  | _ \* _ \* \[] => idtac
  | _ \* _ \* _ \* \[] => idtac
  | _ \* _ \* _ \* _ \* \[] => idtac
  | _ \* _ \* _ \* _ \* _ \* \[] => idtac
  | _ \* _ \* _ \* _ \* _ \* _ \* \[] => idtac
  | _ \* _ \* _ \* _ \* _ \* _ \* _ \* \[] => idtac
  | _ \* _ \* _ \* _ \* _ \* _ \* _ \* _ \* \[] => idtac
  | _ \* _ \* _ \* _ \* _ \* _ \* _ \* _ \* _ \* \[] => idtac
  | _ \* _ \* _ \* _ \* _ \* _ \* _ \* _ \* _ \* _ \* \[] => idtac
  | _ \* _ \* _ \* _ \* _ \* _ \* _ \* _ \* _ \* ?H => apply hsimpl_start_10
  | _ \* _ \* _ \* _ \* _ \* _ \* _ \* _ \* ?H => apply hsimpl_start_9
  | _ \* _ \* _ \* _ \* _ \* _ \* _ \* ?H => apply hsimpl_start_8
  | _ \* _ \* _ \* _ \* _ \* _ \* ?H => apply hsimpl_start_7
  | _ \* _ \* _ \* _ \* _ \* ?H => apply hsimpl_start_6
  | _ \* _ \* _ \* _ \* ?H => apply hsimpl_start_5
  | _ \* _ \* _ \* ?H => apply hsimpl_start_4
  | _ \* _ \* ?H => apply hsimpl_start_3
  | _ \* ?H => apply hsimpl_start_2
  | ?H => apply hsimpl_start_1
  end end.


Ltac check_arg_true v :=
  match v with
  | true => idtac
  | false => fail 1 
  end.

Ltac hsimpl_setup process_credits := 
  prepare_goal_hpull_himpl tt;
  hsimpl_left_empty tt;
  apply hsimpl_start.

Ltac hsimpl_cleanup tt :=
  try apply hsimpl_stop;
  try apply hsimpl_stop;
  try apply pred_le_refl;
  try hsimpl_hint_remove tt;
  try remove_empty_heaps_right tt;
  try remove_empty_heaps_left tt;
  try apply hsimpl_gc.

Ltac hsimpl_try_same tt :=
  first 
  [ apply hsimpl_cancel_1
  | apply hsimpl_cancel_2
  | apply hsimpl_cancel_3
  | apply hsimpl_cancel_4
  | apply hsimpl_cancel_5
  | apply hsimpl_cancel_6 
  | apply hsimpl_cancel_7 
  | apply hsimpl_cancel_8 
  | apply hsimpl_cancel_9 
  | apply hsimpl_cancel_10 
  ].

Ltac hsimpl_find_same H HL :=
  match HL with
  | H \* _ => apply hsimpl_cancel_1
  | _ \* H \* _ => apply hsimpl_cancel_2
  | _ \* _ \* H \* _ => apply hsimpl_cancel_3
  | _ \* _ \* _ \* H \* _ => apply hsimpl_cancel_4
  | _ \* _ \* _ \* _ \* H \* _ => apply hsimpl_cancel_5
  | _ \* _ \* _ \* _ \* _ \* H \* _ => apply hsimpl_cancel_6
  | _ \* _ \* _ \* _ \* _ \* _ \* H \* _ => apply hsimpl_cancel_7
  | _ \* _ \* _ \* _ \* _ \* _ \* _ \* H \* _ => apply hsimpl_cancel_8
  | _ \* _ \* _ \* _ \* _ \* _ \* _ \* _ \* H \* _ => apply hsimpl_cancel_9
  | _ \* _ \* _ \* _ \* _ \* _ \* _ \* _ \* _ \* H \* _ => apply hsimpl_cancel_10
  end.


Ltac hsimpl_extract_exists tt :=
  first [ 
    hsimpl_hint_next ltac:(fun x =>
      match x with
      | __ => eapply hsimpl_exists
      | _ => apply (@hsimpl_exists _ x)
      end)
  | eapply hsimpl_exists ].

Ltac hsimpl_find_data_post tt :=
  try solve 
   [ reflexivity
   | fequal; fequal; first [ eassumption | symmetry; eassumption ] ].

(* todo: better implemented in cps style ? *)


(** Maintain the goal in the form
     H1 \* ... \* HN \* [] ==> HA \* HR
   where HA is initially empty and accumulates elements not simplifiable
   and HR contains the values that are to be cancelled out;
   the last item of HR is always a [].
   As long as HR is of the form H \* H', we try to match H with one of the Hi.
  *)

Ltac check_noevar2 M := (* todo: merge *)
  first [ has_evar M; fail 1 | idtac ].


Ltac check_noevar3 M := (* todo: rename *)
  first [ is_evar M; fail 1 | idtac ].

Ltac hsimpl_hook H := fail.


Ltac hsimpl_step process_credits :=
  match goal with |- ?HL ==> ?HA \* ?HN =>
  match HN with
  | ?H \* _ =>
    match H with
    | ?H => hsimpl_hook H
    | \[] => apply hsimpl_empty
    | \[_] => apply hsimpl_prop
    | heap_is_pack _ => hsimpl_extract_exists tt
    | _ \* _ => apply hsimpl_assoc
    | heap_is_single _ _ => hsimpl_try_same tt
    | ?H => (* should be check_noevar3 on the next line TODO *)
       first [ is_evar H; fail 1 | idtac ];
       hsimpl_find_same H HL (* may fail *)
    | _ => apply hsimpl_keep
    end
  | \[] => fail 1
  | _ => apply hsimpl_starify
  end end.

(* TODO: factorize the logging version of the code with the normal code *)
Ltac hsimpl_step_debug process_credits :=
  match goal with |- ?HL ==> ?HA \* ?HN =>
idtac HN;
  match HN with
  | ?H \* _ =>
    match H with
    | \[] => apply hsimpl_empty
    | \[_] => apply hsimpl_prop
    | heap_is_pack _ => hsimpl_extract_exists tt
    | _ \* _ => idtac "sep"; apply hsimpl_assoc
    | heap_is_single _ _ => hsimpl_try_same tt
    | ?H => idtac "find"; 
        first [ has_evar H; idtac "has evar"; fail 1 | idtac "has no evar" ];
         hsimpl_find_same H HL (* may fail *)
    | ?X => idtac "keep"; apply hsimpl_keep
    end
  | \[] => fail 1
  | _ => idtac "starify"; apply hsimpl_starify
  end end.

Ltac hsimpl_main process_credits :=
  hsimpl_setup process_credits;
  (repeat (hsimpl_step process_credits));
  hsimpl_cleanup tt.

(* todo: rename hsimpl into hcancel above *)

Tactic Notation "hcancel" := 
  hsimpl_main false.
Tactic Notation "hcancel" constr(L) :=
  match type of L with 
  | list Boxer => hsimpl_hint_put L
  | _ => hsimpl_hint_put (boxer L :: nil)
  end; hcancel.

Ltac hpull_and_hcancel tt := 
  hpull; intros; hcancel.

Tactic Notation "hsimpl" := hpull_and_hcancel tt.
Tactic Notation "hsimpl" "~" := hsimpl; auto_tilde.
Tactic Notation "hsimpl" "*" := hsimpl; auto_star.
Tactic Notation "hsimpl" constr(L) :=
  match type of L with 
  | list Boxer => hsimpl_hint_put L
  | _ => hsimpl_hint_put (boxer L :: nil)
  end; hsimpl.
Tactic Notation "hsimpl" constr(X1) constr(X2) :=
  hsimpl (>> X1 X2).
Tactic Notation "hsimpl" constr(X1) constr(X2) constr(X3) :=
  hsimpl (>> X1 X2 X3).

Tactic Notation "hsimpl" "~" constr(L) :=
  hsimpl L; auto_tilde.
Tactic Notation "hsimpl" "~" constr(X1) constr(X2) :=
  hsimpl X1 X2; auto_tilde.
Tactic Notation "hsimpl" "~" constr(X1) constr(X2) constr(X3) :=
  hsimpl X1 X2 X3; auto_tilde.

Tactic Notation "hsimpl" "*" constr(L) :=
  hsimpl L; auto_star.
Tactic Notation "hsimpl" "*" constr(X1) constr(X2) :=
  hsimpl X1 X2; auto_star.
Tactic Notation "hsimpl" "*" constr(X1) constr(X2) constr(X3) :=
  hsimpl X1 X2 X3; auto_star.

(** [hsimpls] is the same as [hsimpl; subst] *)

Tactic Notation "hsimpls" :=
  hsimpl; subst.
Tactic Notation "hsimpls" "~" :=
  hsimpls; auto_tilde.
Tactic Notation "hsimpls" "*" :=
  hsimpls; auto_star.

Tactic Notation "hsimpls" constr(L) :=
  hsimpl L; subst.
Tactic Notation "hsimpls" "~" constr(L) :=
  hsimpls L; auto_tilde.
Tactic Notation "hsimpls" "*" constr(L) :=
  hsimpls L; auto_star.


(********************************************************************)
(* ** Other stuff *)

Lemma heap_weaken_star : forall H1' H1 H2 H3,
  (H1 ==> H1') -> (H1' \* H2 ==> H3) -> (H1 \* H2 ==> H3).
Proof using.
  introv W M (h1&h2&N). unpack N. apply M. exists~ h1 h2.
Qed. (* todo: move *)

Lemma hsimpl_to_qunit : forall (H:hprop) (Q:unit->hprop),
  Q = (fun _ => H) ->
  H ==> Q tt.
Proof using. intros. subst. auto. Qed. (* todo: needed? *)
Hint Resolve hsimpl_to_qunit.


(*------------------------------------------------------------------*)
(* ** Tactic [hchange] *)

Lemma hchange_lemma : forall H1 H1' H H' H2,
  (H1 ==> H1') -> (H ==> H1 \* H2) -> (H1' \* H2 ==> H') -> (H ==> H').
Proof using.
  intros. applys* (@pred_le_trans heap) (H1 \* H2). 
  applys* (@pred_le_trans heap) (H1' \* H2). hsimpl~. 
Qed.

Ltac hchange_apply L cont1 cont2 :=
  eapply hchange_lemma; 
    [ applys L | cont1 tt | cont2 tt ].

Ltac hchange_forwards L modif cont1 cont2 :=
  forwards_nounfold_then L ltac:(fun K =>
  match modif with
  | __ => 
     match type of K with
     | _ = _ => hchange_apply (@pred_le_proj1 _ _ _ K) cont1 cont2
     | _ => hchange_apply K cont1 cont2
     end
  | _ => hchange_apply (@modif _ _ _ K) cont1 cont2
  end).

Ltac hcancel_cont tt :=
  instantiate; hcancel.
Ltac hsimpl_cont tt :=
  instantiate; hsimpl.

Ltac hchange_core E modif cont1 cont2 :=
  hpull; intros;
  match E with
  (*  | ?H ==> ?H' => hchange_with_core H H' --todo*)
  | _ => hchange_forwards E modif ltac:(cont1) ltac:(cont2)
  end.

Ltac hchange_debug_base E modif :=
  hchange_forwards E modif ltac:(idcont) ltac:(idcont).

Tactic Notation "hchange_debug" constr(E) :=
  hchange_debug_base E __.
Tactic Notation "hchange_debug" "->" constr(E) :=
  hchange_debug_base E pred_le_proj1.
Tactic Notation "hchange_debug" "<-" constr(E) :=
  hchange_debug_base pred_le_proj2.

Ltac hchange_base E modif :=
  hchange_core E modif ltac:(hcancel_cont) ltac:(idcont).

Tactic Notation "hchange" constr(E) :=
  hchange_base E __.
Tactic Notation "hchange" "->" constr(E) :=
  hchange_base E pred_le_proj1.
Tactic Notation "hchange" "<-" constr(E) :=
  hchange_base E pred_le_proj2.

Tactic Notation "hchange" "~" constr(E) :=
  hchange E; auto_tilde.
Tactic Notation "hchange" "*" constr(E) :=
  hchange E; auto_star.

Ltac hchanges_base E modif :=
  hchange_core E modif ltac:(hcancel_cont) ltac:(hsimpl_cont).

Tactic Notation "hchanges" constr(E) :=
  hchanges_base E __.
Tactic Notation "hchanges" "->" constr(E) :=
  hchanges_base E pred_le_proj1.
Tactic Notation "hchange" "<-" constr(E) :=
  hchanges_base E pred_le_proj2.

Tactic Notation "hchanges" "~" constr(E) :=
  hchanges E; auto_tilde.
Tactic Notation "hchanges" "*" constr(E) :=
  hchanges E; auto_star.

Tactic Notation "hchange" constr(E1) constr(E2) :=
  hchange E1; hchange E2.
Tactic Notation "hchange" constr(E1) constr(E2) constr(E3) :=
  hchange E1; hchange E2 E3.


*)