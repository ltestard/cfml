(* DEPRECATEd: int_nat_plus_abs =>  *)

(* DEPRECATED: see plus_nat_int
Lemma int_nat_plus : forall (x y:nat),
  my_Z_of_nat (x + y)%nat = (my_Z_of_nat x + my_Z_of_nat y).
Proof using. math. Qed.
*)

(* DEPRECATED => lt_nat_of_lt_int
Lemma int_nat_lt : forall (x y:nat),
  ((x:int) < (y:int)) -> x < y.
Proof using. math. Qed.
*)

(* DEPRECATED => eq_int_of_eq_nat
Lemma nat_int_eq : forall (x y:nat),
  (x = y :> nat) -> (x = y :> int).
Proof using. math. Qed.
*)




(* DEPRECATED update_map => map_update *)
