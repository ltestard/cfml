(********************************************************************)
(* ** Simplification for hdata --DEPRECATED (replaced with hunfold) *)

Lemma hdata_fun : forall a (S:a->hprop) x,
  (x ~> (fun y => S y)) = (S x).
Proof using. auto. Qed.

(* TODO: old implementation
Ltac hdata_simpl_core :=
  repeat rewrite hdata_fun.

Ltac hdata_simpl_step :=
  match goal with |- context [ ?l ~> (fun _ => _) ] =>
    rewrite (hdata_fun' l) end.
*)

Lemma hdata_fun' : forall a x (S:a->hprop),
  (x ~> (fun y => S y)) = (S x).
Proof using. auto. Qed.

Ltac hdata_simpl_step :=
  match goal with |- context C [ ?l ~> ?S ] => 
    match S with (fun _ => _) =>
      rewrite (hdata_fun' l S)
    end
  end.
    
Ltac hdata_simpl_core :=
  repeat hdata_simpl_step.

Tactic Notation "hdata_simpl" := 
  hdata_simpl_core.
Tactic Notation "hdata_simpl" constr(E) := 
  unfold E; hdata_simpl.



(**************************************************)
(** hunfold *)

(** [hunfold] simplies expressions of the form
    [ p ~> (fun x => t x) ] into [p ~> t]. *)

(** [hunfold E] simplies expressions of the form
    [ p ~> (fun x => E t1 ... tn x) ] into [p ~> E t1 .. tn]. *)

Lemma hunfold_base_eq : forall a x (S:a->hprop),
  (x ~> (fun y => S y)) = (S x).
Proof using. auto. Qed.

Lemma hunfold_base_eq_name : forall (a : Type) (x : a) g (S : a -> hprop),
  g = (x ~> fun y => S y) ->
  g = S x.
Proof using. intros. subst. auto. Qed.

Ltac hunfold_step tt :=
  match goal with |- context C [ ?l ~> ?S ] => 
    match S with (fun _ => _) =>
      rewrite (@hunfold_base_eq _ l S)
    end
  end.
    
Ltac hunfold_core tt :=
  repeat (hunfold_step tt).

Ltac hunfold_name_step tt :=
  match goal with |- context [ ?s ~> ?S ] =>
    match S with (fun _ => _) => 
      let G := fresh "TEMP" in
      let GEQ := fresh "TEMPEQ" in
      set_eq G GEQ: (s ~> S);
      rewrite (@hunfold_base_eq_name _ _ _ _ GEQ);
      subst G
   end end.

Ltac hunfold_name_core tt :=
  repeat (hunfold_name_step tt).

Ltac hunfold_base tt := 
  first [ progress ( hunfold_name_core tt ) 
        | progress ( hunfold_core tt) ]. (* todo: should not be needed? *)

Tactic Notation "hunfold" :=
  hunfold_base tt.

Ltac hunfold_for_core E :=
  unfold E; hunfold_base tt; fold E.

Ltac hunfold_for_base E :=
  match E with
  | blocker ?X => unblock X; hunfold_for_core X
  | _ => hunfold_for_core E
  end.

Tactic Notation "hunfold" constr(E) := 
  hunfold_for_base E.

Ltac hunfold_name_at E K :=  (* todo: factorize *)
  let p := fresh "TEMPP" in
  let TEMPX := fresh "TEMP" in
  let TEMPX_EQ := fresh "TEMP_EQ" in
  set_eq TEMPX TEMPX_EQ: E;
  ltac_pattern TEMPX at K;
  match goal with |- ?P ?X => 
    set (p:=P); rewrite TEMPX_EQ; subst p 
  end;
  hunfold E; instantiate; subst TEMPX.

Tactic Notation "hunfold" constr(E) "at" constr(K) := 
  hunfold_name_at E K.

















Set Implicit Arguments.
Require Export LibInt CFSpec CFPrint.



(********************************************************************)
(* ** Tactics *)

(*--------------------------------------------------------*)
(* ** Tools for specifications *)

(** [spec_fun_arity S] returns the function which is being
    specified or reasoned about in the term [S], together
    with its arity, as a pair [(n,f)]. The tactic [spec_fun S]
    returns only the function [f], while the tactic [spec_arity S] 
    returns only the arity [n]. *)

Ltac spec_fun_arity S :=
  match S with 
  | spec_1 _ ?f => constr:(1%nat,f)
  | spec_2 _ ?f => constr:(2%nat,f)
  | spec_3 _ ?f => constr:(3%nat,f)
  | spec_4 _ ?f => constr:(4%nat,f)
  | app_1 ?f _ _ _ => constr:(1%nat,f)
  | app_2 ?f _ _ _ _ => constr:(2%nat,f)
  | app_3 ?f _ _ _ _ _ => constr:(3%nat,f)
  | app_4 ?f _ _ _ _ _ _ => constr:(4%nat,f)
  | App ?f _; _ _ => constr:(1%nat,f)
  | App ?f _ _; _ _ => constr:(2%nat,f)
  | App ?f _ _ _; _ _ => constr:(3%nat,f)
  | App ?f _ _ _ _; _ _ => constr:(4%nat,f)
  | curried_1 _ _ ?f => constr:(1%nat,f)
  | curried_2 _ _ _ ?f => constr:(2%nat,f)
  | curried_3 _ _ _ _ ?f => constr:(3%nat,f)
  | curried_4 _ _ _ _ _ ?f => constr:(4%nat,f)
  | context [ spec_1 _ ?f ] => constr:(1%nat,f)
  | context [ spec_2 _ ?f ] => constr:(2%nat,f)
  | context [ spec_3 _ ?f ] => constr:(3%nat,f)
  | context [ spec_4 _ ?f ] => constr:(4%nat,f)
  | context [ app_1 ?f _ _ _ ] => constr:(1%nat,f)
  | context [ app_2 ?f _ _ _ _ ] => constr:(2%nat,f)
  | context [ app_3 ?f _ _ _ _ _ ] => constr:(3%nat,f)
  | context [ app_4 ?f _ _ _ _ _ _ ] => constr:(4%nat,f)
  end. 

Ltac spec_fun S :=
  match spec_fun_arity S with (_,?f) => constr:(f) end.

Ltac spec_arity S :=
  match spec_fun_arity S with (?n,_) => constr:(n) end.

(** [spec_term_arity] is similar to [spec_arity] except that
    it can perform one step of unfolding in order to get to
    a form on which [spec_arity] can succeed. *)

Ltac spec_term_arity T := 
  let S := type of T in
  match tt with
  | tt => spec_arity S 
  | _ => let h := get_head S in 
         let S' := (eval unfold h in S) in
         spec_arity S'
         (* todo: several unfold: call spec_term_arity T' -- check no loop *)
  end.

(** [spec_goal_fun] and [spec_goal_arity] are specialized versions
   of [spec_fun] and [spec_arity] that apply to the current goal *)

Ltac spec_goal_fun tt :=
  match goal with |- ?S => spec_fun S end.

Ltac spec_goal_arity tt :=
  match goal with |- ?S => spec_arity S end.

(** [get_spec_hyp f] returns the hypothesis that contains a 
    specification for the function [f]. *)

Ltac get_spec_hyp f :=
  match goal with 
  | H: context [ spec_1 _ f ] |- _ => constr:(H)
  | H: context [ spec_2 _ f ] |- _ => constr:(H)
  | H: context [ spec_3 _ f ] |- _ => constr:(H) 
  | H: context [ spec_4 _ f ] |- _ => constr:(H) 
  | H: ?P f |- _ => constr:(H) (* todo: higher order pattern *)
  (* deprecated (coq changed):  | H: context [ ?P f ] |- _ => constr:(H) *)
  end.


(** [get_app_hyp f] returns the hypothesis that contains
    a proposition regarding an application of [f] to arguments *)

Ltac get_app_hyp f :=
  match goal with
  | H: context [ app_1 f _ _ ] |- _ => constr:(H)
  | H: context [ app_2 f _ _ _ ] |- _ => constr:(H)
  | H: context [ app_3 f _ _ _ _ ] |- _ => constr:(H) 
  | H: context [ app_4 f _ _ _ _ ] |- _ => constr:(H) 
  end.

(** [unfolds_to_spec tt] is a helper tactic that unfolds definition
    at the head of the goal until reaching a [spec_n] predicate. *)
Ltac unfolds_to_spec tt := 
  match goal with 
  | |- spec_1 _ ?f => idtac
  | |- spec_2 _ ?f => idtac
  | |- spec_3 _ ?f => idtac
  | |- spec_4 _ ?f => idtac
  | _ => progress(unfolds); unfolds_to_spec tt
  end. 

(** [term_is_app E] returns a boolean indicating whether [E]
    is an instance of the App predicate. *)

Ltac term_is_app E :=
  match E with 
  | app_1 ?f _ _ _ => constr:(true)
  | app_2 ?f _ _ _ _ => constr:(true)
  | app_3 ?f _ _ _ _ _ => constr:(true)
  | app_4 ?f _ _ _ _ _ _ => constr:(true)
  | App ?f _; _ _ => constr:(true)
  | App ?f _ _; _ _ => constr:(true)
  | App ?f _ _ _; _ _ => constr:(true)
  | App ?f _ _ _ _; _ _ => constr:(true)
  | _ => constr:(false)
  end.

(** [goal_is_app tt] returns a boolean indicating whether the 
    the goal is an instance of the App predicate. *)

Ltac goal_is_app tt :=
  match goal with |- ?S => term_is_app S end.


(*--------------------------------------------------------*)
(* ** Return lemmas from [FuncDefs] depending on the arity *)

(** Returns the lemma [app_spec_n] *)

Ltac get_app_spec_x n :=
  match n with
  | 1%nat => constr:(app_spec_1)
  | 2%nat => constr:(app_spec_2)
  | 3%nat => constr:(app_spec_3)
  | 4%nat => constr:(app_spec_4)
  end.

(** Returns the lemma [spec_elim_n_m] *)

Ltac get_spec_elim_x_y x y := 
  match constr:(x,y) with 
     | (1%nat,1%nat) => constr:(spec_elim_1_1)
     | (1%nat,2%nat) => constr:(spec_elim_1_2)
     | (1%nat,3%nat) => constr:(spec_elim_1_3)
     | (1%nat,4%nat) => constr:(spec_elim_1_4)
     | (2%nat,1%nat) => constr:(spec_elim_2_1)
     | (2%nat,2%nat) => constr:(spec_elim_2_2)
     | (2%nat,3%nat) => constr:(spec_elim_2_3)
     | (2%nat,4%nat) => constr:(spec_elim_2_4)
     | (3%nat,1%nat) => constr:(spec_elim_3_1)
     | (3%nat,2%nat) => constr:(spec_elim_3_2)
     | (3%nat,3%nat) => constr:(spec_elim_3_3)
     | (3%nat,4%nat) => constr:(spec_elim_3_4)
     | (4%nat,1%nat) => constr:(spec_elim_4_1)
     | (4%nat,2%nat) => constr:(spec_elim_4_2)
     | (4%nat,3%nat) => constr:(spec_elim_4_3)
     | (4%nat,4%nat) => constr:(spec_elim_4_4)
   end.

(** Returns the lemma [spec_intro_n] *)

Ltac get_spec_intro_x x :=
  match x with
     | 1%nat => constr:(spec_intro_1)
     | 2%nat => constr:(spec_intro_2)
     | 3%nat => constr:(spec_intro_3)
     | 4%nat => constr:(spec_intro_4)
  end.

(** Returns the lemma [spec_weaken_n] *)

Ltac get_spec_weaken_x x :=
  match x with
     | 1%nat => constr:(spec_weaken_1)
     | 2%nat => constr:(spec_weaken_2)
     | 3%nat => constr:(spec_weaken_3)
     | 4%nat => constr:(spec_weaken_4)
  end.

(** Returns the lemma [get_app_intro_n_m] *)

Lemma id_proof : forall (P:Prop), P -> P.
Proof. auto. Qed.


(*--------------------------------------------------------*)
(* ** tools for post-conditions *)

Ltac is_evar_as_bool E :=
  constr:(ltac:(first 
    [ is_evar E; exact true 
    | exact false ])).

Ltac get_post tt :=
  match goal with |- ?E => 
  match get_fun_arg E with (_,?Q) => constr:(Q) 
  end end.

Ltac post_is_meta tt := 
  let Q := get_post tt in is_evar_as_bool Q.


(*--------------------------------------------------------*)
(* ** [xextractible] *)

(** [xextractible tt] applies to a goal of the form (R H Q)
    and raises an error if [H] contains extractible quantifiers 
    or facts. *)

Ltac xextractible tt :=
  match goal with |- ?R ?H ?Q => hextractible_rec H end.



(*--------------------------------------------------------*)
(* ** [xclean] *)

(** [xclean] performs some basic simplification is the
    context in order to beautify hypotheses that have been 
    inferred. 
    Remark: this tactic is automatically called by [xextract]. *)

Ltac xclean_core :=
  reflect_clean tt.
Tactic Notation "xclean" :=
  reflect_clean tt.


(*--------------------------------------------------------*)
(* ** [xok] *)

Ltac xok_core cont := 
  solve [ cbv beta; apply rel_le_refl
        | apply pred_le_refl
        | apply hsimpl_to_qunit; reflexivity
        | hsimpl; cont tt ].

Tactic Notation "xok" := 
  xok_core ltac:(idcont).
Tactic Notation "xok" "~" := 
  xok_core ltac:(fun _ => auto_tilde).
Tactic Notation "xok" "*" := 
  xok_core ltac:(fun _ => auto_star).


(*--------------------------------------------------------*)
(* ** [xauto] *)

(* [xauto] is a specialized version of [auto] that works
   well in program verification. One of its main strength
   is the ability to perform substitution before calling auto. *)

Ltac math_0 ::= xclean.

Ltac check_not_a_tag tt :=
  match goal with 
  | |- tag _ _ _ _ => fail 1 (* todo: not needed? *)
  | |- tag _ _ _ _ _ => fail 1
  | |- _ => idtac
  end.


Ltac xauto_common cont :=
  check_not_a_tag tt;  
  try solve [ cont tt 
            | solve [ apply refl_equal ]
            | xok_core ltac:(fun _ => solve [ cont tt | substs; cont tt ] ) 
            | substs; if_eq; solve [ cont tt | apply refl_equal ]  ].

Ltac xauto_tilde_default cont := xauto_common cont.
Ltac xauto_star_default cont := xauto_common cont.

Ltac xauto_tilde := xauto_tilde_default ltac:(fun _ => auto_tilde).
Ltac xauto_star := xauto_star_default ltac:(fun _ => auto_star). 

Tactic Notation "xauto" "~" := xauto_tilde.
Tactic Notation "xauto" "*" := xauto_star.
Tactic Notation "xauto" := xauto~.

Tactic Notation "hsimpl" "~" constr(L) :=
  hsimpl L; xauto~.
Tactic Notation "hsimpl" "~" constr(X1) constr(X2) :=
  hsimpl X1 X2; xauto~.
Tactic Notation "hsimpl" "~" constr(X1) constr(X2) constr(X3) :=
  hsimpl X1 X2 X3; xauto~.


(*--------------------------------------------------------*)
(* ** [xisspec] *)

(** [xisspec] is a helper function to prove a goal of the
    form [is_spec K], which basically amounts to showing
    that [K x1 .. xN] is weakenable. The tactic [intuition eauto]
    called by [xisspec] discharges this obligation is most cases.
    Cases that are not handled by this tactic are typically those
    involving case analysis. *)

Ltac xisspec_core :=
  solve [ intros_all; unfolds rel_le, pred_le; auto; auto_star ].

Tactic Notation "xisspec" :=
  (* check_noevar_goal; *) xisspec_core.


(*--------------------------------------------------------*)
(* ** [xlocal] *)

Ltac xlocal_core tt ::=
  first [ assumption
 	| apply local_is_local 
        | apply app_local_1 
        | match goal with H: is_local_pred ?S |- is_local (?S _) => apply H end ].


(*--------------------------------------------------------*)
(* ** [xcf] *)

(** [xcf] applies to a goal of the form [Spec_n f K]
    and uses the characteristic formula known for [f]
    in order to get started proving the goal.

    It also applies to a goal of the form 
    [app_n f x1 .. xN H Q], and exploits the characteristic
    formula for [f] in order to get started proving the goal. *)

Ltac remove_head_unit tt :=
  repeat match goal with 
  | |- unit -> _ => intros _
  end.

Ltac xcf_post tt :=
  cbv beta; remove_head_unit tt.

Ltac solve_type :=
  match goal with |- Type => exact unit end.

Ltac xcf_for_core_hyp H :=
  match type of H with
  | @tag tag_top_fun _ _ _ => sapply H; instantiate; try solve_type; [ try xisspec | ]
  | _ => sapply H; try solve_type
  end; clear H; xcf_post tt.

Ltac xcf_for_core f :=
  ltac_database_get database_cf f;
  let H := fresh "TEMP" in intros H; 
  xcf_for_core_hyp H.

Tactic Notation "xcf" constr(Sf) :=
  generalize Sf; 
  let H := fresh "TEMP" in intros H; 
  xcf_for_core_hyp H.

Ltac xcf_core :=
  intros; first 
  [ let f := spec_goal_fun tt in xcf_for_core f 
  | match goal with |- ?f = _ => xcf_for_core f end
  | let f := spec_goal_fun tt in let H := get_spec_hyp f in sapply H; [ try xisspec | ] ].

(** [xcf_for f] is a tactic used for specifying explicitly  
    the name of the function for which a characteristic formula
    should be searched. *)

Tactic Notation "xcf_for" constr(f) := xcf_for_core f.

(** [xcf_app] applies to a goal of the form 
    [app_n f x1 .. xN H Q], and exploits the characteristic
    formula for [f] in order to get started proving the goal. *)

Ltac intro_subst_arity n :=
  let x1 := fresh "TEMP" in let x2 := fresh "TEMP" in
  let x3 := fresh "TEMP" in let x4 := fresh "TEMP" in 
  let H1 := fresh "TEMP" in let H2 := fresh "TEMP" in
  let H3 := fresh "TEMP" in let H4 := fresh "TEMP" in
  match n with
  | 1%nat => intros x1 H1; subst x1
  | 2%nat => intros x1 x2 H1 H2; subst x1 x2
  | 3%nat => intros x1 x2 x3 H1 H2 H3; subst x1 x2 x3
  | 4%nat => intros x1 x2 x3 x4 H1 H2 H3 H4; subst x1 x2 x3 x4
  end.

Ltac xcf_app_core :=
  let n := spec_goal_arity tt in 
  let H := get_app_spec_x n in
  apply H; xcf_core; try intro_subst_arity n.

Ltac xcf_app_base :=
  try (xuntag tag_apply);
  xcf_app_core.

Tactic Notation "xcf_app" := xcf_app_base.

Ltac xcf_select_core :=
  intros;
  match goal_is_app tt with
  | true => xcf_app
  | false => xcf_core
  end.

Tactic Notation "xcf" := xcf_select_core.


(*--------------------------------------------------------*)
(* ** [xfind] *)

Ltac xfind_by_core db f :=
  ltac_database_get db f.

Ltac xfind_ctx f :=
  let H := get_spec_hyp f in generalize H.

(** [xfind_by db f] displays the specification registered with [f]
    either in the context or in the database [db].
    (by inserting it as new hypothesis at head of the goal). *)

Tactic Notation "xfind_by" constr(db) constr(f) :=  
  xfind_by_core db f.

(** [xfind_by db] calls [xfind_by db f] on the function that 
    appears in the goal. *)

Tactic Notation "xfind_by" constr(db) := 
  let f := spec_goal_fun tt in xfind_by db f.

(** [xfind f] first tries [xfind_by database_spec_credits f]
    then tries [xfind_by database_spec f]. *)

Ltac xfind_core f := 
  first [ xfind_ctx f | xfind_by database_spec_credits f | xfind_by database_spec f ].

Tactic Notation "xfind" constr(f) :=
  xfind_core f.

(** [xfind] without argument calls [xfind f] for the function
    [f] that appear in the current goal *)

Tactic Notation "xfind" := 
  let f := spec_goal_fun tt in xfind f.


(*--------------------------------------------------------*)
(* ** [xcurried] *)

(** [xcurried] helps proving a goal of the form [curried_n f],
    by proving that [f] accepts [True] as post-condition.
    The latter proof is set up by invoking the characteristic
    formula for [f]. *)

Ltac xcurried_core :=
  try solve [ 
    unfold curried_1, curried_2, curried_3, curried_4;
    xcf; check_noevar_goal; auto ].

Tactic Notation "xcurried" := xcurried_core.

Ltac xcurried_using H :=
  try solve [ 
    unfold curried_1, curried_2, curried_3, curried_4;
    eapply H; check_noevar_goal; auto ].

Tactic Notation "xcurried" constr(H) :=
  sapply H; instantiate; try solve_type; [ try xisspec | auto ].

Ltac xcurried_debug :=
  unfold curried_1, curried_2, curried_3, curried_4; 
  idtac "type xcf".



(*--------------------------------------------------------*)
(* ** [xextract] *)

Ltac xextract_core :=
  match goal with
  | |- _ ==> _ => hextract; xclean
  | |- _ ===> _ => let r := fresh "r" in intros r; hextract; xclean
  | |- _ => simpl; hclean; instantiate
  end.

(* todo: use continuations *)
(* todo: check that an arrow is visible before doing intros *)
Tactic Notation "xextract" := 
  xextract_core; xclean.
Tactic Notation "xextract" "as" simple_intropattern(I1) := 
  xextract; intros I1; xclean.
Tactic Notation "xextract" "as" simple_intropattern(I1) simple_intropattern(I2) := 
  xextract; intros I1 I2; xclean. 
Tactic Notation "xextract" "as" simple_intropattern(I1) simple_intropattern(I2) 
 simple_intropattern(I3) := 
  xextract; intros I1 I2 I3; xclean.
Tactic Notation "xextract" "as" simple_intropattern(I1) simple_intropattern(I2) 
 simple_intropattern(I3) simple_intropattern(I4) := 
  xextract; intros I1 I2 I3 I4; xclean.
Tactic Notation "xextract" "as" simple_intropattern(I1) simple_intropattern(I2) 
 simple_intropattern(I3) simple_intropattern(I4) simple_intropattern(I5) := 
  xextract; intros I1 I2 I3 I4 I5; xclean.
Tactic Notation "xextract" "as" simple_intropattern(I1) simple_intropattern(I2) 
 simple_intropattern(I3) simple_intropattern(I4) simple_intropattern(I5)
 simple_intropattern(I6) := 
  xextract; intros I1 I2 I3 I4 I5 I6; xclean.
Tactic Notation "xextract" "as" simple_intropattern(I1) simple_intropattern(I2) 
 simple_intropattern(I3) simple_intropattern(I4) simple_intropattern(I5)
 simple_intropattern(I6) simple_intropattern(I7) := 
  xextract; intros I1 I2 I3 I4 I5 I6 I7; xclean.
Tactic Notation "xextract" "as" simple_intropattern(I1) simple_intropattern(I2) 
 simple_intropattern(I3) simple_intropattern(I4) simple_intropattern(I5)
 simple_intropattern(I6) simple_intropattern(I7) simple_intropattern(I8) := 
  xextract; intros I1 I2 I3 I4 I5 I6 I7 I8; xclean.
Tactic Notation "xextract" "as" simple_intropattern(I1) simple_intropattern(I2) 
 simple_intropattern(I3) simple_intropattern(I4) simple_intropattern(I5)
 simple_intropattern(I6) simple_intropattern(I7) simple_intropattern(I8) 
 simple_intropattern(I9) := 
  xextract; intros I1 I2 I3 I4 I5 I6 I7 I8 I9; xclean.
Tactic Notation "xextract" "as" simple_intropattern(I1) simple_intropattern(I2) 
 simple_intropattern(I3) simple_intropattern(I4) simple_intropattern(I5)
 simple_intropattern(I6) simple_intropattern(I7) simple_intropattern(I8) 
 simple_intropattern(I9) simple_intropattern(I10) := 
  xextract; intros I1 I2 I3 I4 I5 I6 I7 I8 I9 I10; xclean.


Tactic Notation "xextracts" :=
  let E := fresh "TEMP" in xextract as E; subst_hyp E.


(*--------------------------------------------------------*)
(* ** [xsimpl] *)

(** [xsimpl] performs a heap entailement simplification using 
  [hsimpl], then calls the tactic [xclean]. *)

Ltac xsimpl_core := hsimpl; xclean.

Tactic Notation "xsimpl" := xsimpl_core. 
Tactic Notation "xsimpl" "~" := xsimpl; xauto~.
Tactic Notation "xsimpl" "*" := xsimpl; xauto*.

(* TODO: factorize below with above *)

Ltac xsimpl_core_with E := hsimpl E; xclean.
Tactic Notation "xsimpl" constr(E) := xsimpl_core_with E. 
Tactic Notation "xsimpl" "~" constr(E) := xsimpl E; xauto~.
Tactic Notation "xsimpl" "*" constr(E) := xsimpl E; xauto*.


(*--------------------------------------------------------*)
(* ** [xunfold] *)

Tactic Notation "xunfold" :=
  hunfold.
Tactic Notation "xunfold" constr(E) := 
  hunfold E.
Tactic Notation "xunfold" constr(E) "at" constr(K) := 
  hunfold E at K.



(*--------------------------------------------------------*)
(* ** [xlet] *)

(** [xlet] is used to reason on a let-node, i.e. on a goal
    of the form [(Let x := Q1 in Q2) P]. The general form
    is [xlet Q as y], where [y] is the name to be used
    in place of [x].
    The arguments are optional, so the following forms are
    allowed: [xlet], [xlet as x], [xlet Q], [xlet Q as x]. *)

Ltac xlet_core cont0 cont1 cont2 :=
  xextractible tt;
  apply local_erase; cont0 tt; split; [ | cont1 tt; cont2 tt ].

Tactic Notation "xlet_def" tactic(c0) tactic(c1) tactic(c2) :=
  xlet_core ltac:(c0) ltac:(c1) ltac:(c2).

Tactic Notation "xlet" constr(Q) "as" simple_intropattern(x) :=
  xlet_def (fun _ => exists Q) (fun _ => intros x) (fun _ => try xextract).
Tactic Notation "xlet" constr(Q) :=
  xlet_def (fun _ => exists Q) (fun _ => intro) (fun _ => try xextract).
Tactic Notation "xlet" "as" simple_intropattern(x) :=
  xlet_def (fun _ => esplit) (fun _ => intros x) (fun _ => idtac).
Tactic Notation "xlet" :=
  xlet_def (fun _ => esplit) (fun _ => intro) (fun _ => idtac).

Tactic Notation "xlets" constr(Q) :=
  xlet Q; [ | intro_subst ].


Tactic Notation "xseq" :=
  xlet_def (fun _ => esplit) (fun _ => idtac) (fun _ => idtac).
Tactic Notation "xseq" constr(H) :=
  xlet_def (fun _ => first [ exists (#H) | exists H ]) (fun _ => idtac) (fun _ => try xextract).

(** TODO: comment xseq *)

Tactic Notation "xseq_no_xextract" constr(H) :=
  xlet_def (fun _ => first [ exists (#H) | exists H ]) (fun _ => idtac) (fun _ => idtac).

Tactic Notation "xlet" "~" := xlet; auto_tilde. (* todo: xauto ! *)
Tactic Notation "xlet" "~" "as" simple_intropattern(x) := xlet as x; auto_tilde.
Tactic Notation "xseq" "~" := xseq; auto_tilde. (* todo: xauto ! *)
Tactic Notation "xlet" "~" constr(Q) := xlet Q; auto_tilde.
Tactic Notation "xlet" "~" constr(Q) "as" simple_intropattern(x) := xlet Q as x; auto_tilde.
Tactic Notation "xseq" "~" constr(H) := xseq H; auto_tilde.
Tactic Notation "xlet" "*" := xlet; auto_star.
Tactic Notation "xlet" "*" "as" simple_intropattern(x) := xlet as x; auto_star.
Tactic Notation "xseq" "*" := xseq; auto_star. (* todo: xauto ! *)
Tactic Notation "xlet" "*" constr(Q) := xlet Q; auto_star.
Tactic Notation "xlet" "*" constr(Q) "as" simple_intropattern(x) := xlet Q as x; auto_star.
Tactic Notation "xseq" "*" constr(H) := xseq H; auto_star.


(*--------------------------------------------------------*)
(* ** [xval] *)

(** [xval] is used to reason on a let-node binding a value. *)

Ltac xval_impl x Hx :=
  apply local_erase; intros x Hx.

Tactic Notation "xval" "as" simple_intropattern(x) simple_intropattern(Hx) :=
  xval_impl x Hx.

Tactic Notation "xval" "as" simple_intropattern(x) :=
  let Hx := fresh "P" x in xval_impl x Hx.

Ltac xval_anonymous_impl tt :=
  apply local_erase; intro; let x := get_last_hyp tt in revert x; 
  let Hx := fresh "P" x in intros x Hx.

Tactic Notation "xval" :=
  xval_anonymous_impl tt.

(** [xvals] substitutes the bound value right away. *)

Ltac xvals_impl tt :=
  apply local_erase; intro; intro_subst.

Tactic Notation "xvals" :=
  xvals_impl tt. 

(** [xval_st P] is used to assign an abstract specification to the value *)

Ltac xval_st_core P x Hx :=
  let E := fresh in intros x E; asserts Hx: (P x); [ subst x | clear E ].

Ltac xval_st_impl P x Hx :=
  apply local_erase; xval_st_core P x Hx.

Tactic Notation "xval_st" constr(P) "as" simple_intropattern(x) simple_intropattern(Hx) :=
  xval_st_impl P x Hx.

Tactic Notation "xval_st" constr(P) "as" simple_intropattern(x) :=
  let Hx := fresh "P" x in xval_st_impl P x Hx.

Ltac xval_st_anonymous_impl P :=
  apply local_erase; intro; let x := get_last_hyp tt in revert x; 
  let Hx := fresh "P" x in xval_st_core P x Hx.

Tactic Notation "xval_st" constr(P) :=
  xval_st_anonymous_impl P.


(*--------------------------------------------------------*)
(* ** [xfail], [xdone] *)

(** [xfail] simplifies a proof obligation of the form [Fail],
    which is in fact equivalent to [False].
    [xfail_noclean] is also available. *)

Tactic Notation "xfail_noclean" :=
  xextractible tt; xuntag tag_fail; apply local_erase.
Tactic Notation "xfail" := 
  xfail_noclean; xclean.
Tactic Notation "xfail" "~" :=  
  xfail; xauto~.
Tactic Notation "xfail" "*" :=  
  xfail; xauto*.

(** [xfail C] is like [xfail; false C]. *)

Tactic Notation "xfail" constr(C) := 
  xfail; false C.

(** [xdone] proves a goal of the form [Done],
    which is in fact equivalent to [True]. *)

Tactic Notation "xdone" :=
  xuntag tag_done; apply local_erase; split.

(*--------------------------------------------------------*)
(* ** [xpay] *)

(** [xpay] is used to pay one credit *)

Ltac xpay_start tt :=
  xuntag tag_pay; apply local_erase; esplit; split.

Ltac xpay_core tt :=
  xpay_start tt; [ unfold pay_one; hsimpl | ].

Ltac xpay_nat_core tt :=
  xpay_start tt; [ rewrite pay_one_nat; hsimpl | ].

Tactic Notation "xpay" := xpay_core tt.
 
(** [xpay_skip] is used to skip the paying of one credit;
    only for development purpose. *)

Ltac xpay_fake tt :=
  xpay_start tt; 
  [ assert (pay_one_fake: forall H, pay_one H H); 
     [ admit (* for development only *)
     | apply pay_one_fake ] 
  | ].

Tactic Notation "xpay_skip" := xpay_fake tt.


(*--------------------------------------------------------*)
(* ** [xskip_credits] *)

(** Tactic [xskip_credits] runs [skip_credits] then [hsimpl].
    Should only be used for development purpose. *)

Ltac xskip_credits_base := 
  skip_credits_core; hsimpl.

Tactic Notation "xskip_credits" := 
  xskip_credits_base.


(*--------------------------------------------------------*)
(* ** [xret] *)

(** Lemma used by [xret] *)

Lemma xret_lemma : forall HG B (v:B) H (Q:B->hprop),
  H ==> Q v \* HG -> 
  local (fun H' Q' => H' ==> Q' v) H Q.
Proof using.  
  introv W. eapply (@local_gc_pre HG).
  auto. rewrite star_comm. apply W.
  apply~ local_erase.
Qed.

(** Lemma used by [xret_no_gc] *)

Lemma xret_no_gc_lemma : forall B (v:B) H (Q:B->hprop),
  H ==> Q v -> 
  local (fun H' Q' => H' ==> Q' v) H Q.
Proof using.  
  introv W. apply~ local_erase.
Qed.

(** Lemma used by [xret] and [xret_no_gc] 
    for when post-condition unifies trivially *)

Lemma xret_lemma_unify : forall B (v:B) H,
  local (fun H' Q' => H' ==> Q' v) H (fun x => \[x = v] \* H).
Proof using.  
  intros. apply~ local_erase. hsimpl. auto. 
Qed.


(** [xret]. *)

Ltac xret_core :=
  first [ apply xret_lemma_unify
        | eapply xret_lemma ].

Ltac xret_no_gc_core :=
  first [ apply xret_lemma_unify
        | eapply xret_no_gc_lemma ].

Ltac xret_pre cont := 
  xextractible tt;
  match ltac_get_tag tt with
  | tag_ret => cont tt
  | tag_let_trm => xlet; [ cont tt | instantiate ]
  end. 

  (* todo: special treatment of xlet/xret
  Ltac xret_pre cont := 
    match ltac_get_tag tt with
    | tag_ret => cont tt
    | tag_let_pure => xlet; [ cont tt | instantiate ]
    end. *)

(** [xret_no_clean] is like [xret] but it does not call 
    [xclean] on the goal. *)

Tactic Notation "xret_no_clean" := 
  xret_pre ltac:(fun _ => xret_core).

Tactic Notation "xret" := 
  xret_pre ltac:(fun _ => xret_core; xclean).
Tactic Notation "xret" "~" :=  
  xret; xauto~.
Tactic Notation "xret" "*" :=  
  xret; xauto*.

(** [xret_no_gc] can be used to not introduce an existentially-
    quantified heap for garbage collection *)

Tactic Notation "xret_no_gc" := 
  xret_no_gc_core.
Tactic Notation "xret_no_gc" "~" :=  
  xret_no_gc; xauto~.
Tactic Notation "xret_no_gc" "*" :=  
  xret_no_gc; xauto*.

(** [xrets] is short for [xret; xsimpl] *)

Ltac xrets_base :=
  xret; xsimpl.

Tactic Notation "xrets" :=
  xrets_base.
Tactic Notation "xrets" "~" :=  
  xrets; xauto~.
Tactic Notation "xrets" "*" :=  
  xrets; xauto*.


(*--------------------------------------------------------*)
(* ** [xpre] *)

Tactic Notation "xpre" constr(H) :=
  eapply (@local_weaken_pre_gc H); [ try xlocal | | ].

(*--------------------------------------------------------*)
(* ** [xpost] *)

(** Lemma used by [xpost], 
    for introducing an evar for the post-condition *)

Lemma xpost_lemma : forall B Q' Q (F:~~B) H,
  is_local F -> 
  F H Q' -> 
  Q' ===> Q ->
  F H Q.
Proof using. intros. applys* local_weaken. Qed.

Tactic Notation "xpost" :=
  eapply xpost_lemma; [ try xlocal | | ].

Lemma fix_post : forall (B:Type) (Q':B->hprop) (F:~~B) (H:hprop) Q,
  is_local F ->
  F H Q' -> 
  Q' ===> Q ->
  F H Q.
Proof. intros. apply* local_weaken. Qed.

Tactic Notation "xpost" constr(Q) := 
  apply (@fix_post _ Q); [ try xlocal | | try apply rel_le_refl ].


(*--------------------------------------------------------*)
(* ** [xgen] *)

Lemma xgen_lemma : forall A (J:A->hprop) (E:A),
  J E ==> heap_is_pack J.
Proof. intros. hsimpl*. Qed.

Ltac xgen_abstract H E :=
  let Jx := eval pattern E in H in
  match Jx with ?J _ => constr:(J) end.

Ltac xgen_nosimpl E :=
  match goal with |- ?H ==> _ =>
    let J := xgen_abstract H E in 
    eapply pred_le_trans; [ apply (@xgen_lemma _ J E) | ] end.

Ltac xgen_base E := 
  xgen_nosimpl E.    

Tactic Notation "xgen" constr(E1) :=
  xgen_base E1.
Tactic Notation "xgen" constr(E1) constr(E2) :=
  xgen_base E1; xgen_base E2.

Lemma xgen_demo : forall (x E y F:int) H1 R,
  (forall H2, x ~> R E \* y ~> R F \* H1 ==> H2 -> H2 = H2 -> True) -> True.
Proof.
  introv H. dup.
  eapply H. xgen E. xgen F. xok. auto.
  eapply H. xgen E F. xok. auto.
Qed.


(*--------------------------------------------------------*)
(* ** [xgc] *)

(** Lemma used by [xgc_all],
    to remove everything from the pre-condition *)

Lemma local_gc_pre_all : forall B Q (F:~~B) H,
  is_local F -> 
  F \[] Q ->
  F H Q.
Proof using. intros. apply* (@local_gc_pre H). hsimpl. Qed.



Ltac xgc_core :=
  xextractible tt;
  eapply local_gc_post; 
  [ try xlocal | | ].

Ltac xgc_remove_core H :=
  xextractible tt;
  eapply local_gc_pre with (HG := H);
    [ try xlocal
    | hsimpl
    | ].

Ltac xgc_keep_core H :=
  xextractible tt;
  eapply local_gc_pre with (H' := H);
    [ try xlocal
    | hsimpl
    | ].

Tactic Notation "xgc" constr(H) := 
  xgc_remove_core H.

Tactic Notation "xgc" "-" constr(H) := 
  xgc_keep_core H.

Tactic Notation "xgc" := 
  xgc_core.

Tactic Notation "xgc_all" := 
  eapply local_gc_pre_all; [ try xlocal | ].


(*--------------------------------------------------------*)
(* ** [xframe] *)

(** Lemma used by [xframe] *)

Lemma xframe_lemma : forall H1 H2 B Q1 (F:~~B) H Q,
  is_local F -> 
  H ==> H1 \* H2 -> 
  F H1 Q1 -> 
  Q1 \*+ H2 ===> Q ->
  F H Q.
Proof using. intros. apply* local_wframe. Qed.


Ltac xframe_remove_core H :=
  xextractible tt;
  eapply xframe_lemma with (H2 := H);
    [ try xlocal
    | hsimpl
    | 
    | ].

Ltac xframe_keep_core H :=
  xextractible tt;
  eapply xframe_lemma with (H1 := H);
    [ try xlocal
    | hsimpl
    | 
    | ].

Tactic Notation "xframe" constr(H) := 
  xframe_remove_core H.

Tactic Notation "xframe" "-" constr(H) := 
  xframe_keep_core H.


(*--------------------------------------------------------*)
(* ** [xframes] *)

Ltac xframes_core_1 s := 
  match goal with |- ?R ?H ?Q =>
    match H with context [ s ~> ?M ] =>
      xframe (s ~> M) end end.

Tactic Notation "xframes" constr(s1) := 
  xframes_core_1 s1.

Ltac xframes_core_2 s1 s2 := 
  match goal with |- ?R ?H ?Q =>
    match H with context [ s1 ~> ?M1 ] =>
      match H with context [ s2 ~> ?M2 ] =>
        xframe (s1 ~> M1 \* s2 ~> M2) end end end.

Tactic Notation "xframes" constr(s1) constr(s2) := 
  xframes_core_2 s1 s2.

Ltac xframes_core_3 s1 s2 s3 := 
  match goal with |- ?R ?H ?Q =>
    match H with context [ s1 ~> ?M1 ] =>
      match H with context [ s2 ~> ?M2 ] =>
        match H with context [ s3 ~> ?M3 ] =>
          xframe (s1 ~> M1 \* s2 ~> M2 \* s3 ~> M3) 
  end end end end.

Tactic Notation "xframes" constr(s1) constr(s2) constr(s3) := 
  xframes_core_3 s1 s2 s3.



(*--------------------------------------------------------*)
(* ** [xchange] *)

(** Lemma used by [xchange] *)

Lemma xchange_lemma : forall H1 H1' H2 B H Q (F:~~B),
  is_local F -> 
  (H1 ==> H1') -> 
  (H ==> H1 \* H2) -> 
  F (H1' \* H2) Q -> 
  F H Q.
Proof using.
  introv W1 L W2 M. applys local_wframe __ \[]; eauto.
  hsimpl. hchange~ W2. hsimpl~. rew_heap~. 
Qed.


Ltac xchange_apply L cont :=
   eapply xchange_lemma; 
     [ try xlocal | applys L | cont tt | ].

(* note: modif should be himpl_proj1 or himpl_proj2 *)
Ltac xchange_forwards L modif cont :=
  forwards_nounfold_then L ltac:(fun K =>
  match modif with
  | __ => 
     match type of K with
     | _ = _ => xchange_apply (@pred_le_proj1 _ _ _ K) cont
     | _ => xchange_apply K cont
     end
  | _ => xchange_apply (@modif _ _ _ K) cont
  end).

Ltac xchange_with_core H H' :=
  eapply xchange_lemma with (H1:=H) (H1':=H'); 
    [ try xlocal | | hsimpl | ].

Ltac xchange_core E modif :=
  match E with
  | ?H ==> ?H' => xchange_with_core H H'
  | _ => xchange_forwards E modif ltac:(fun _ => instantiate; hsimpl)
  end.

Ltac xchange_base E modif :=
  xextractible tt;
  match goal with
  | |- _ ==> _ => hchange_base E modif
  | |- _ ===> _ => hchange_base E modif
  | _ => xchange_core E modif
  end.

Tactic Notation "xchange_debug" constr(E) :=
  xchange_forwards E __ ltac:(idcont).
Tactic Notation "xchange_debug" "->" constr(E) :=
  xchange_forwards E pred_le_proj1 ltac:(idcont).
Tactic Notation "xchange_debug" "<-" constr(E) :=
  xchange_forwards pred_le_proj2 ltac:(idcont).

Tactic Notation "xchange" constr(E) :=
  xchange_base E __.
Tactic Notation "xchange" "->" constr(E) :=
  xchange_base E pred_le_proj1.
Tactic Notation "xchange" "<-" constr(E) :=
  xchange_base E pred_le_proj2.

Tactic Notation "xchange" constr(E) "as" := 
  xchange E; try xextract.
Tactic Notation "xchange" constr(E) "as" simple_intropattern(I1) := 
  xchange E; try xextract as I1.
Tactic Notation "xchange" constr(E) "as" simple_intropattern(I1) simple_intropattern(I2) := 
  xchange E; try xextract as I1 I2.
Tactic Notation "xchange" constr(E) "as" simple_intropattern(I1) simple_intropattern(I2)
 simple_intropattern(I3) := 
  xchange E; try xextract as I1 I2 I3.
Tactic Notation "xchange" constr(E) "as" simple_intropattern(I1) simple_intropattern(I2)
 simple_intropattern(I3) simple_intropattern(I4) := 
  xchange E; try xextract as I1 I2 I3 I4. 
Tactic Notation "xchange" constr(E) "as" simple_intropattern(I1) simple_intropattern(I2)
 simple_intropattern(I3) simple_intropattern(I4) simple_intropattern(I5) := 
  xchange E; try xextract as I1 I2 I3 I4 I5. 

Tactic Notation "xchange" "~" constr(E) :=
  xchange E; auto_tilde.
Tactic Notation "xchange" "~" constr(E) "as" := 
  xchange~ E; try xextract.
Tactic Notation "xchange" "~" constr(E) "as" simple_intropattern(I1) := 
  xchange~ E; try xextract as I1.
Tactic Notation "xchange" "~" constr(E) "as" simple_intropattern(I1) simple_intropattern(I2) := 
  xchange~ E; try xextract as I1 I2.
Tactic Notation "xchange" "~" constr(E) "as" simple_intropattern(I1) simple_intropattern(I2)
 simple_intropattern(I3) := 
  xchange~ E; try xextract as I1 I2 I3.
Tactic Notation "xchange" "~" constr(E) "as" simple_intropattern(I1) simple_intropattern(I2)
 simple_intropattern(I3) simple_intropattern(I4) := 
  xchange~ E; try xextract as I1 I2 I3 I4. 
Tactic Notation "xchange" "~" constr(E) "as" simple_intropattern(I1) simple_intropattern(I2)
 simple_intropattern(I3) simple_intropattern(I4) simple_intropattern(I5) := 
  xchange~ E; try xextract as I1 I2 I3 I4 I5. 

Tactic Notation "xchange" "*" constr(E) :=
  xchange E; auto_star.
Tactic Notation "xchange" "*" constr(E) "as" := 
  xchange* E; try xextract.
Tactic Notation "xchange" "*" constr(E) "as" simple_intropattern(I1) := 
  xchange* E; try xextract as I1.
Tactic Notation "xchange" "*" constr(E) "as" simple_intropattern(I1) simple_intropattern(I2) := 
  xchange* E; try xextract as I1 I2.
Tactic Notation "xchange" "*" constr(E) "as" simple_intropattern(I1) simple_intropattern(I2)
 simple_intropattern(I3) := 
  xchange* E; try xextract as I1 I2 I3.
Tactic Notation "xchange" "*" constr(E) "as" simple_intropattern(I1) simple_intropattern(I2)
 simple_intropattern(I3) simple_intropattern(I4) := 
  xchange* E; try xextract as I1 I2 I3 I4. 
Tactic Notation "xchange" "*" constr(E) "as" simple_intropattern(I1) simple_intropattern(I2)
 simple_intropattern(I3) simple_intropattern(I4) simple_intropattern(I5) := 
  xchange* E; try xextract as I1 I2 I3 I4 I5. 

Tactic Notation "xchange" constr(E1) constr(E2) :=
  xchange E1; xchange E2.
Tactic Notation "xchange" constr(E1) constr(E2) constr(E3) :=
  xchange E1; xchange E2 E3.


(* TODO: xchanges *)

(*--------------------------------------------------------*)
(* ** [xspec_show_types] *)

(* [xspec_show_types H] displays the type involved in the 
   specification lemma [H]. *)

Ltac xspec_show_types_impl H :=
  let one T := 
    idtac T; idtac " -> " in
  match type of H with
  | @spec_1 ?A1 ?B _ _ => one A1; idtac B
  | @spec_2 ?A1 ?A2 ?B _ _ => one A1; one A2; idtac B
  | @spec_3 ?A1 ?A2 ?A3 ?B _ _ => one A1; one A2; one A3; idtac B
  | @spec_4 ?A1 ?A2 ?A3 ?A4 ?B _ _ => one A1; one A2; one A3; one A4; idtac B
  end.

Tactic Notation "xspec_show_types" constr(H) :=
  xspec_show_types_impl H.


(*--------------------------------------------------------*)
(* ** [xapp_show_types] *)

(* [xapp_show_types] displays the type involved in an application *)

Ltac show_post_type Q :=
  match type of Q with
  | ?A -> _ => idtac A
  end.

Ltac xapp_show_types_app tt := 
  let one T := 
    idtac T; idtac " -> " in
  let common B Q := 
    idtac B; idtac "  specified as "; show_post_type Q in
  match goal with
  | |- @app_1 ?A1 ?B _ _ _ ?Q =>
    one A1; common B Q
  | |- @app_2 ?A1 ?A2 ?B _ _ _ ?Q =>
    one A1; one A2; common B Q
  | |- @app_3 ?A1 ?A2 ?A3 ?B _ _ _ ?Q =>
    one A1; one A2; one A3; common B Q
  | |- @app_4 ?A1 ?A2 ?A3 ?A4 ?B _ _ _ ?Q =>
    one A1; one A2; one A3; one A4; common B Q
  end.

Ltac xapp_show_types_impl tt := 
  let go tt := xuntag tag_apply; xapp_show_types_app tt in
  match ltac_get_tag tt with
  | tag_apply => go tt
  | tag_let_trm => xlet; [ go tt | ]
  | tag_seq => xseq; [ go tt | ]
  end.

Tactic Notation "xapp_show_types" :=
  xapp_show_types_impl tt.


(*--------------------------------------------------------*)
(* ** [xapp_show_spec] *)

(* [xapp_show_spec] and [xapp_show_spec_by db] show the spec
   that [xapp] would use. *)

Ltac xapp_show_spec_core xfind_tactic := 
  xuntag; let f := spec_goal_fun tt in
  xfind_tactic f; let H := fresh in intro H.

Ltac xapp_show_spec_pre xfind_tactic := 
  let go tt := xapp_show_spec_core xfind_tactic in
  match ltac_get_tag tt with
  | tag_apply => go tt
  | tag_let_trm => xlet; [ go tt | ]
  | tag_seq => xseq; [ go tt | ]
  end.

Tactic Notation "xapp_show_spec" :=
  let xfind_tactic := fun f => xfind f in
  xapp_show_spec_pre xfind_tactic.

Tactic Notation "xapp_show_spec_by" constr(db) :=
  let xfind_tactic := fun f => xfind_by db f in
  xapp_show_spec_pre xfind_tactic.


(*--------------------------------------------------------*)
(* ** [xapp] *)

(* todo: when arities differ *)

Ltac xapp_compact KR args :=
  let args := list_boxer_of args in
  constr:((boxer KR)::args).

Ltac xapp_final HR :=
  eapply local_wframe; 
     [ xlocal
     | eapply HR
     | hsimpl
     | try xok ].

Ltac xapp_inst args solver :=
  let R := fresh "R" in let LR := fresh "L" R in 
  let KR := fresh "K" R in let IR := fresh "I" R in
  intros R LR KR; hnf in KR; (* lazy beta in *)
  let H := xapp_compact KR args in
  forwards_then H ltac:(fun HR => try xapp_final HR);    
  try clears R; solver tt.
(* todo: should clear R in indirect subgoals *)

Ltac xapp_spec_core H cont :=
   let arity_goal := spec_goal_arity tt in
   let arity_hyp := spec_term_arity H in
   match constr:(arity_goal, arity_hyp) with (?n,?n) => idtac | _ => fail 1 end;
   let lemma := get_spec_elim_x_y arity_hyp arity_goal in
   eapply lemma; [ sapply H | cont tt ]. 

Ltac xapp_manual_intros tt :=
  let R := fresh "R" in let LR := fresh "L" R in 
  let KR := fresh "K" R in intros R LR KR; lazy beta in KR.

   
Inductive Spec_database : Type :=
  | spec_database : forall A, A -> Spec_database.

Implicit Arguments spec_database [A].

Ltac xapp_core spec cont :=
  let f := spec_goal_fun tt in
  match spec with
  | @spec_database _ ?db =>  
      xfind_by db f; let H := fresh in intro H;
      xapp_spec_core H cont; clear H
  | ___ => 
    first [ xfind_ctx f
          | xfind f ];
    let H := fresh in intro H;
    xapp_spec_core H cont; clear H
  | ?H => xapp_spec_core H cont
  end.

Ltac xapp_pre cont :=
  xextractible tt;
  match ltac_get_tag tt with
  | tag_apply =>
    match post_is_meta tt with
    | false => xgc; [ xuntag tag_apply; cont tt | ]
    | true => xuntag tag_apply; cont tt
    end
  | tag_let_trm => xlet; [ xuntag tag_apply; cont tt | instantiate; xextract ]
  | tag_seq => xseq; [ xuntag tag_apply; cont tt | instantiate; xextract ]
  end.

Ltac xapp_then spec cont :=
  xapp_pre ltac:(fun _ => xapp_core spec cont).

Ltac xapp_with spec args solver :=
  xapp_then spec ltac:(fun _ => xapp_inst args solver).
  

Tactic Notation "xapp" := 
  xapp_with ___ (>>) ltac:(fun _ => idtac).
Tactic Notation "xapp" constr(E) := 
  xapp_with ___ E ltac:(fun _ => idtac).
Tactic Notation "xapp" constr(E1) constr(E2) := 
  xapp (>> E1 E2).
Tactic Notation "xapp" constr(E1) constr(E2) constr(E3) := 
  xapp (>> E1 E2 E3).
Tactic Notation "xapp" constr(E1) constr(E2) constr(E3) constr(E4) := 
  xapp (>> E1 E2 E3 E4).
Tactic Notation "xapp" constr(E1) constr(E2) constr(E3) constr(E4) constr(E5) := 
  xapp (>> E1 E2 E3 E4 E5).

Tactic Notation "xapp" "~" := 
  xapp_with ___ (>>) ltac:(fun _ => xauto~). (* ; xauto~.*)
Tactic Notation "xapp" "~" constr(E) := 
  xapp_with ___ E ltac:(fun _ => xauto~).
Tactic Notation "xapp" "~" constr(E1) constr(E2) := 
  xapp~ (>> E1 E2).
Tactic Notation "xapp" "~" constr(E1) constr(E2) constr(E3) := 
  xapp~ (>> E1 E2 E3).
Tactic Notation "xapp" "~" constr(E1) constr(E2) constr(E3) constr(E4) := 
  xapp~ (>> E1 E2 E3 E4).
Tactic Notation "xapp" "~" constr(E1) constr(E2) constr(E3) constr(E4) constr(E5) :=
   xapp~ (>> E1 E2 E3 E4 E5).

Tactic Notation "xapp" "*" := 
  xapp_with ___ (>>) ltac:(fun _ => xauto*).
Tactic Notation "xapp" "*" constr(E) := 
  xapp_with ___ E ltac:(fun _ => xauto*).
Tactic Notation "xapp" "*" constr(E1) constr(E2) := 
  xapp* (>> E1 E2).
Tactic Notation "xapp" "*" constr(E1) constr(E2) constr(E3) := 
  xapp* (>> E1 E2 E3).
Tactic Notation "xapp" "*" constr(E1) constr(E2) constr(E3) constr(E4) := 
  xapp* (>> E1 E2 E3 E4).
Tactic Notation "xapp" "*" constr(E1) constr(E2) constr(E3) constr(E4) constr(E5) :=
   xapp* (>> E1 E2 E3 E4 E5).

Tactic Notation "xapp_spec" constr(H) := 
  xapp_with H (>>) ltac:(fun _ => idtac).
Tactic Notation "xapp_spec" constr(H) constr(E) := 
  xapp_with H E ltac:(fun _ => idtac).
Tactic Notation "xapp_spec" "~" constr(H) := 
  xapp_with H (>>) ltac:(fun _ => xauto~). (* ; xauto~.*)
Tactic Notation "xapp_spec" "~" constr(H) constr(E) := 
  xapp_with H E ltac:(fun _ => xauto~).
Tactic Notation "xapp_spec" "*" constr(H) := 
  xapp_with H (>>) ltac:(fun _ => xauto*).
Tactic Notation "xapp_spec" "*" constr(H) constr(E) := 
  xapp_with H E ltac:(fun _ => xauto*).

Tactic Notation "xapp_manual" := 
  xapp_then ___ ltac:(xapp_manual_intros).
Tactic Notation "xapp_spec_manual" constr(H) := 
  xapp_then H ltac:(xapp_manual_intros).
Tactic Notation "xapp_manual" "as" := 
  xapp_then ___ ltac:(fun _ => idtac).
Tactic Notation "xapp_spec_manual" constr(H) "as" := 
  xapp_then H ltac:(fun _ => idtac).

Tactic Notation "xapp" "as" simple_intropattern(x) :=
  xlet as x; [ xapp | instantiate; xextract ].

Ltac xapps_core spec args solver := 
  let cont1 tt :=
    xapp_with spec args solver in
  let cont2 tt :=
    instantiate; xextract; try intro_subst in    
  match ltac_get_tag tt with
  | tag_let_trm => xlet; [ cont1 tt | cont2 tt ]
  | tag_seq =>     xseq; [ cont1 tt | cont2 tt ]
  | tag_apply => xapp_with spec args solver
  end.

Tactic Notation "xapps" := 
  xapps_core ___ (>>) ltac:(fun _ => idtac).
Tactic Notation "xapps" constr(E) := 
  xapps_core ___ E ltac:(fun _ => idtac).
Tactic Notation "xapps" constr(E1) constr(E2) := 
  xapps_core (>> E1 E2).
Tactic Notation "xapps" constr(E1) constr(E2) constr(E3) := 
  xapps_core (>> E1 E2 E3).
Tactic Notation "xapps" constr(E1) constr(E2) constr(E3) constr(E4) := 
  xapps_core (>> E1 E2 E3 E4).
Tactic Notation "xapps" constr(E1) constr(E2) constr(E3) constr(E4) constr(E5) := 
  xapps_core (>> E1 E2 E3 E4 E5).

Tactic Notation "xapps" "~" := 
  xapps; auto_tilde.
Tactic Notation "xapps" "*" := 
  xapps; auto_star.
Tactic Notation "xapps" "~" constr(E) := 
  xapps E; auto_tilde.
Tactic Notation "xapps" "*" constr(E) := 
  xapps E; auto_star.

(* todo: when hypothesis in an app instance *)

Tactic Notation "xapp_body" :=
  xuntag; let f := spec_goal_fun tt in
  xfind f; let H := fresh "TEMP" in intro H; 
  eapply app_spec_1; apply H; clear H; try xisspec.

Tactic Notation "xapp_hyp" := (* todo: remove*)
  eapply local_weaken; 
    [ xlocal
    | let f := spec_goal_fun tt in let H := get_spec_hyp f in 
      apply (proj2 H) (* todo generalize to arities*)
    | hsimpl
    | hsimpl ].


(* TODO: merge with xapp_final, and make xapp work directly *)

Ltac xapp_final' HR dosimpl :=
  eapply local_wframe; 
     [ xlocal
     | eapply HR
     | match dosimpl with
       | true => hsimpl
       | false => idtac
       end
     | try xok ].

Ltac xapp_inst' args solver dosimpl :=
  let R := fresh "R" in let LR := fresh "L" R in 
  let KR := fresh "K" R in let IR := fresh "I" R in
  intros R LR KR; hnf in KR; (* lazy beta in *)
  let H := xapp_compact KR args in
  forwards_then H ltac:(fun HR => try xapp_final' HR dosimpl);    
  try clears R; solver tt.

Ltac xapp_with' spec args solver dosimpl :=
  xapp_then spec ltac:(fun _ => xapp_inst' args solver dosimpl).

Tactic Notation "xapp_nosimpl" := 
  xapp_with' ___ (>>) ltac:(fun _ => idtac) false.
Tactic Notation "xapp_nosimpl" constr(E) := 
  xapp_with' ___ E ltac:(fun _ => idtac) false.
Tactic Notation "xapp_nosimpl" "~" := 
  xapp_with' ___ (>>) ltac:(fun _ => xauto~) false. (* ; xauto~.*)
Tactic Notation "xapp_nosimpl" "~" constr(E) := 
  xapp_with' ___ E ltac:(fun _ => xauto~) false.
Tactic Notation "xapp_nosimpl" "*" := 
  xapp_with' ___ (>>) ltac:(fun _ => xauto*) false.
Tactic Notation "xapp_nosimpl" "*" constr(E) := 
  xapp_with' ___ E ltac:(fun _ => xauto*) false.


Ltac xapp_body_core :=
  xuntag; let f := spec_goal_fun tt in
  xfind f; let H := fresh "TEMP" in intro H; 
  let n := spec_goal_arity tt in 
  let E := get_app_spec_x n in
  eapply E; apply H; clear H; try xisspec;
  try intro_subst_arity n.

Tactic Notation "xapp_body" :=
  xapp_body_core.









Ltac xapp_as_base spec args solver x :=
  let cont tt := xapp_inst args solver in
  xlet as x; 
  [ xuntag tag_apply; xapp_core spec cont
  | instantiate; xextract ].

Tactic Notation "xapp" "as" simple_intropattern(x) :=
  xapp_as_base ___ (>>) ltac:(fun _ => idtac) x.
Tactic Notation "xapp" "~" "as" simple_intropattern(x) :=
  xapp_as_base ___ (>>) ltac:(fun _ => xauto~) x.
Tactic Notation "xapp" "*" "as" simple_intropattern(x) :=
  xapp_as_base ___ (>>) ltac:(fun _ => xauto* ) x.
Tactic Notation "xapp" constr(E) "as" simple_intropattern(x) :=
  xlet as x; [ xapp E | instantiate; xextract ].
Tactic Notation "xapp" "*" constr(E) "as" simple_intropattern(x) :=
  xapp E as K; auto_star.

(* Specifying the database *)

Tactic Notation "xapp_by" constr(db) := 
  xapp_with (spec_database db) (>>) ltac:(fun _ => idtac).
Tactic Notation "xapp_by" constr(db) constr(E) := 
  xapp_with (spec_database db) E ltac:(fun _ => idtac).
Tactic Notation "xapp_by" "~" constr(db) := 
  xapp_with (spec_database db) (>>) ltac:(fun _ => xauto~ ). 
Tactic Notation "xapp_by" "~" constr(db) constr(E) := 
  xapp_with (spec_database db) E ltac:(fun _ => xauto~).
Tactic Notation "xapp_by" "*" constr(db) := 
  xapp_with (spec_database db) (>>) ltac:(fun _ => xauto* ).
Tactic Notation "xapp_by" "*" constr(db) constr(E) := 
  xapp_with (spec_database db) E ltac:(fun _ => xauto* ).

Tactic Notation "xapps_by" constr(db) := 
  xapps_core (spec_database db) (>>) ltac:(fun _ => idtac).
Tactic Notation "xapps_by" constr(db) constr(E) := 
  xapps_core (spec_database db) E ltac:(fun _ => idtac).
Tactic Notation "xapps_by" "~" constr(db) := 
  xapps_by db; auto_tilde.
Tactic Notation "xapps_by" "*" constr(db) := 
  xapps_by db; auto_star.
Tactic Notation "xapps_by" "~" constr(db) constr(E) := 
  xapps_by db E; auto_tilde.
Tactic Notation "xapps_by" "*" constr(db) constr(E) := 
  xapps_by db E; auto_star.


(** Tactics for debugging xapp *)

Ltac xapp_manual_inst := 
  xapp_inst (>>) ltac:(fun _ => idtac).

Ltac xapp_inst_1 :=
  let R := fresh "R" in let LR := fresh "L" R in 
  let KR := fresh "K" R in let IR := fresh "I" R in
  intros R LR KR; hnf in KR. (* lazy beta in *)

Ltac xapp_inst_2 :=
  match goal with KR: _ |- _ => 
    forwards_then KR ltac:(fun HR => try xapp_final HR)
  end.



(*--------------------------------------------------------*)
(* ** [xapp_partial] *)

Ltac xapp_partial_spec_inner H :=
  let arity_goal := spec_goal_arity tt in
   let arity_hyp := spec_term_arity H in
   let lemma := get_spec_elim_x_y arity_hyp arity_goal in
   applys (>> lemma H).

(* todo: factorize with xapp_final HR *)
Ltac xapp_frame_around cont :=
   eapply local_wframe; [ xlocal | cont tt | hsimpl | try xok ].

Ltac xapp_partial_spec_core H :=
  xapp_frame_around ltac:(fun _ => xapp_partial_spec_inner H).

Ltac xapp_partial_core spec :=
  match spec with
  | ___ =>
      let f := spec_goal_fun tt in
      xfind f; let H := fresh in intro H;
      xapp_partial_spec_core H; clear H
  | ?H => xapp_partial_spec_core H
  end.

Ltac xapp_partial_then spec :=
  xapp_pre ltac:(fun _ => xapp_partial_core spec).

Tactic Notation "xapp_partial" :=
  xapp_partial_then ___.
Tactic Notation "xapp_partial_spec" constr(S) :=
  xapp_partial_then S.

Tactic Notation "xapp_partial" "as" simple_intropattern(x) :=
  xlet as x; [ xapp_partial | instantiate; xextract ].
Tactic Notation "xapp_partial_spec" constr(S) "as" simple_intropattern(x) :=
  xlet as x; [ xapp_partial_spec S | instantiate; xextract ].



(*--------------------------------------------------------*)
(* ** [xinduction] *)

(** [xinduction_heap E] applies to a goal of the form 
    [Spec_n f (fun x1 .. xN R => forall x0, L x0 x1 xN R)] 
    and replaces it with a weaker goal, which describes the same
    specification but including an induction hypothesis. 
    The argument [E] describes the termination arguments. 
    If [f] has type [A1 -> .. -> AN -> B], then [E] should be one of
    - a measure of type [A0*A1*..*AN -> nat] 
    - a binary relation of type [A0*A1*..*AN -> A0*A1*..*AN -> Prop] 
    - a proof that a well-foundedness for such a relation.
    
    [xinduction E] is similar but does not depend on [x0]. 

    Measures and binary relations can also be provided in
    a curried fashion, at type [A0 -> A1 -> .. -> AN -> nat] and
    [A0 -> A0 -> A1 -> A1 -> A2 -> A2 -> .. -> AN -> AN -> Prop], respectively.
    
    The combinators [unprojNK] are useful for building new binary
    relations. For example, if [R] has type [A->A->Prop], then
    [unproj21 B R] has type [A*B -> A*B -> Prop] and compares pairs
    with respect to their first component only, using [R]. *)

(* todo: reimplement using  goal_arity and options *)

Ltac xinduction_heap_base_lt lt :=
  first   
    [ apply (spec_induction_1 (lt:=lt))
    | apply (spec_induction_2 (lt:=lt))
    | apply (spec_induction_3 (lt:=lt)) 
    | apply (spec_induction_4 (lt:=lt))
    | apply (spec_induction_2 (lt:=uncurryp2 lt))
    | apply (spec_induction_3 (lt:=uncurryp3 lt))
    | apply (spec_induction_4 (lt:=uncurryp4 lt)) 
    | apply (spec_induction_1_noarg (lt:=lt)) 
    | apply (spec_induction_2_noarg (lt:=lt)) 
    | apply (spec_induction_3_noarg (lt:=lt)) 
    | apply (spec_induction_4_noarg (lt:=lt)) ];
  [ try prove_wf | try xisspec | unfolds_wf ].

Ltac xinduction_heap_base_wf wf :=
  first   
    [ apply (spec_induction_1 wf)
    | apply (spec_induction_2 wf)
    | apply (spec_induction_3 wf) 
    | apply (spec_induction_4 wf) 
    | apply (spec_induction_1_noarg wf) ];
   [ try xisspec | unfolds_wf ].

Ltac xinduction_heap_base_measure m :=
  first   
    [ apply (spec_induction_1 (measure_wf m))
    | apply (spec_induction_2 (measure_wf m))
    | apply (spec_induction_3 (measure_wf m))
    | apply (spec_induction_4 (measure_wf m))
    | apply (spec_induction_2 (measure_wf (uncurry2 m)))
    | apply (spec_induction_3 (measure_wf (uncurry3 m)))
    | apply (spec_induction_3 (measure_wf (uncurry4 m)))
    | apply (spec_induction_1_noarg (measure_wf m)) ];
    [ try xisspec | unfolds_wf; unfold measure ].


Tactic Notation "xinduction_heap" constr(arg) :=
  first [ xinduction_heap_base_lt arg
        | xinduction_heap_base_wf arg
        | xinduction_heap_base_measure arg ].

(** [xinduction] implementation *)

Ltac xinduction_noheap_base_lt lt :=
  (match spec_goal_arity tt with
  | 1%nat => first [ eapply (spec_induction_1_noheap (lt:=lt)) ]
  | 2%nat => first [ eapply (spec_induction_2_noheap (lt:=lt)) 
                   | eapply (spec_induction_2_noheap (lt:=uncurryp2 lt)) ]
  | 3%nat => first [ eapply (spec_induction_3_noheap (lt:=lt)) 
                   | eapply (spec_induction_3_noheap (lt:=uncurryp3 lt)) ]
  | 4%nat => first [ eapply (spec_induction_4_noheap (lt:=lt)) 
                   | eapply (spec_induction_4_noheap (lt:=uncurryp4 lt)) ]
  end);
  [ try prove_wf | try xisspec | unfolds_wf ].

Ltac xinduction_noheap_base_wf wf :=
  (match spec_goal_arity tt with
  | 1%nat => eapply (spec_induction_1_noheap wf)
  | 2%nat => eapply (spec_induction_2_noheap wf)
  | 3%nat => eapply (spec_induction_3_noheap wf) 
  | 4%nat => eapply (spec_induction_4_noheap wf)
  end);
   [ try xisspec | unfolds_wf ].

Ltac xinduction_noheap_base_measure m :=
  (match spec_goal_arity tt with
  | 1%nat => first [ apply (spec_induction_1_noheap (measure_wf m)) ]
  | 2%nat => first [ apply (spec_induction_2_noheap (measure_wf m))
                   | apply (spec_induction_2_noheap (measure_wf (uncurry2 m))) ]
  | 3%nat => first [ apply (spec_induction_3_noheap (measure_wf m))
                   | apply (spec_induction_3_noheap (measure_wf (uncurry3 m))) ]
  | 4%nat => first [ apply (spec_induction_4_noheap (measure_wf m))
                   | apply (spec_induction_4_noheap (measure_wf (uncurry4 m))) ]
  end);
  [ try xisspec | unfolds_wf; unfold measure ].

Tactic Notation "xinduction" constr(arg) :=
  first [ xinduction_noheap_base_lt arg
        | xinduction_noheap_base_wf arg
        | xinduction_noheap_base_measure arg ].

(** [xinduction_skip as I], or just [xinduction_skip],
    allows to assume the current goal to be already true.
    It is useful to test a proof before justifying termination.
    Use it for development purpose only. *)

Tactic Notation "xinduction_skip" "as" simple_intropattern(H) :=
  skip_goal H.
Tactic Notation "xinduction_skip" :=
  let H := fresh "IH" in xinduction_skip as H.


(*--------------------------------------------------------*)
(* ** [xname_..] *)

(** [xname_spec X] applies to a goal of the form [Spec_n f K]
    and defines [X] as the predicate [fun f => Spec_n f K]. *)

Tactic Notation "xname_spec" ident(X) :=
  let f := spec_goal_fun tt in pattern f;
  match goal with |- ?S _ => set (X := S) end;
  unfold X.

(** [xname_pre X] defines [X] as a shortname for the current
    pre-condition *)

Tactic Notation "xname_pre" ident(X) :=
  match goal with |- ?R ?H ?Q => sets X: H end.

(** [xname_post X] defines [X] as a shortname for the current
    post-condition *)

Tactic Notation "xname_post" ident(X) :=
  match goal with |- ?R ?H ?Q => sets X: Q end.


(*--------------------------------------------------------*)
(* ** [xbody] *)

(** [xbody] applies to a goal of the form 
    [H: (Body f x1 .. xN => Q) |- Spec_n f K]
    where H is the last hypothesis in the goal.
    It applies this hypothesis to prove the goal. 
    Two subgoals are generated. The first one
    is [is_spec_n K], and [xisspec] is called to try and solve it.
    The second one is [forall x1 .. xN, K x1 .. xN Q].
    The arguments are introduced automatically, unless the
    tactic [xbody_nointro] is called.

    The tactic [xbody H] can be used to specify explicitly
    the hypothesis [H] (of the form [Body ..]) to be used for 
    proving a goal [Spec_n f K], and it does not clear [H]. *)

Ltac xbody_core cont :=
   let Bf := fresh "TEMP" in 
   intros Bf; sapply Bf; clear Bf;
   [ try xisspec | cont tt ].

Ltac xbody_pre tt :=
  let H := get_last_hyp tt in generalizes H.

Ltac xbody_base_intro tt :=
  xbody_core ltac:(fun _ => remove_head_unit tt; introv).

Ltac xbody_base_nointro tt :=
  xbody_core ltac:(fun _ => remove_head_unit tt).

Tactic Notation "xbody" :=
  xbody_pre tt; xbody_base_intro tt.
Tactic Notation "xbody_nointro" :=
  xbody_pre tt; xbody_base_nointro tt.

Tactic Notation "xbody" ident(Bf) :=
  generalizes Bf; xbody_base_intro tt.
Tactic Notation "xbody_nointro" ident(Bf) :=
  generalizes Bf; xbody_base_nointro tt.

(* todo: add xuntag body *)

(*--------------------------------------------------------*)
(* ** [xfun] *)

(** [xfun S] applies to a goal of the form [LetFunc f := Q1 in Q].
    [S] is the specification for [f1], which should be a predicate
    of type [Val -> Prop]. Typically, [S] takes the form
    [fun f => Spec f K]. By default, the tactic [xbody] is
    called subsequently, so as to exploit the body assumption
    stored in [Q1]. This behaviour can be modified by calling
    [xfun_noxbody S]. The variant [xfun_noxbody S as H] allows to 
    name explicitly the body assumption. *)

Ltac xfun_core s cont :=
  apply local_erase;
  intro; let f := get_last_hyp tt in
  let Sf := fresh "S" f in
  exists s; split; [ cont tt | intros Sf ].

Ltac xfun_namebody tt :=
  let f := get_last_hyp tt in 
  let Bf := fresh "B" f in
  intros Bf.

Tactic Notation "xfun" constr(s) :=
  xfun_core s ltac:(fun _ => first [ xbody_base_intro tt | xfun_namebody tt ] ).
Tactic Notation "xfun_nointro" constr(s) :=
  xfun_core s ltac:(fun _ => first [ xbody_base_nointro tt | xfun_namebody tt ] ).
Tactic Notation "xfun" constr(s) "as" ident(B) :=
  xfun_core s ltac:(fun _ => intros B).
    (* --todo: rename to xfun_noxbody *)
Tactic Notation "xfun_noxbody" constr(s) :=
  xfun_core s ltac:(xfun_namebody).

(** [xfun] is equivalent to [xfun S] where [S] is the most
    general specification that can be provided for the function.
    This specification is simply defined in terms of the 
    body assumption.
    This tactic is useful to simulate the inlining of the 
    function at its call sites, and reason only on particular
    applications of the function. *)

Ltac xfun_mg_core tt :=
  apply local_erase;
  intro; let f := get_last_hyp tt in let H := fresh "S" f in
  esplit; split; intros H; [ pattern f in H; eexact H | ].

Tactic Notation "xfun" := xfun_mg_core tt.

Ltac xfun_mg_core_as f := (* todo: factorize *)
  apply local_erase; intro f; let H := fresh "S" f in
  esplit; split; intros H; [ pattern f in H; eexact H | ].

Tactic Notation "xfun" ident(f) := xfun_mg_core_as f.

Tactic Notation "xfun_mg" := xfun. (* todo: remove *)

(** [xfun S1 S2] allows to specify a pair of mutually-recursive
    functions, be providng both their specifications. *)

Tactic Notation "xfun" constr(s1) constr(s2) :=
  intro; let f1 := get_last_hyp tt in
  intro; let f2 := get_last_hyp tt in
  let Bf1 := fresh "B" f1 in let Bf2 := fresh "B" f2 in
  let Sf1 := fresh "S" f1 in let Sf2 := fresh "S" f2 in
  exists s1; exists s2; split; [ intros Bf1 Bf2 | intros Sf1 Sf2 ].

  (* todo: higher degrees *)

(** [xfun_induction S I] is the same as [xfun] except that
    it sets up a proof by induction for a recursive function.
    More precisely, it combines [xfun S] with [xinduction I]. 
    The tactic [xfun_induction_nointro S I] is similar except
    that it does not introduces the arguments of the function. *)
  (* todo: en g\E9n\E9ral les noms des arguments sont perdus,
       donc le d\E9faut pourrait etre nointro, ou un mode "as" *)
  (* todo: xfun_induction I S *)

Tactic Notation "xfun_induction" constr(S) constr(I) :=
  xfun_core S ltac:(fun _ => 
    intro; unfolds_to_spec tt; xinduction I; xbody).

Tactic Notation "xfun_induction_nointro" constr(S) constr(I) :=
  xfun_core S ltac:(fun _ => 
    intro; unfolds_to_spec tt; xinduction I; xbody_nointro).

Tactic Notation "xfun_induction_heap" constr(S) constr(I) :=
  xfun_core S ltac:(fun _ => 
    intro; unfolds_to_spec tt; xinduction_heap I; xbody).

Tactic Notation "xfun_induction_heap_nointro" constr(S) constr(I) :=
  xfun_core S ltac:(fun _ => 
    intro; unfolds_to_spec tt; xinduction_heap I; xbody_nointro).

(** [xfun_induction_skip S IH] is similar to [xfun_induction]
    except that an induction hypothesis IH is provided without
    a proof of termination. *)

Tactic Notation "xfun_induction_skip" constr(S) ident(IH) :=
  xfun_core S ltac:(fun _ => 
    let H := fresh in
    intro H; unfolds_to_spec tt; skip_goal IH; move H after IH; xbody).

Tactic Notation "xfun_induction_nointro_skip" constr(S) ident(IH) :=
  xfun_core S ltac:(fun _ => 
    let H := fresh in
    intro H; unfolds_to_spec tt; skip_goal IH; move H after IH; xbody_nointro).


(*--------------------------------------------------------*)
(* ** [xwhile] *)


Lemma while_loop_cf_inv_measure : 
   forall (I:bool->int->hprop),
   forall (F1:~~bool) (F2:~~unit) H (Q:unit->hprop),
   (exists b m, H ==> I b m \* (Hexists G, G)) ->
   (forall b m, F1 (I b m) (fun b' => I b' m)) ->
   (forall m, F2 (I true m) (# Hexists b m', \[0 <= m' < m] \* I b m')) ->
   (Q = fun _ => Hexists m, I false m) ->
   (While F1 Do F2 Done_) H Q.
Proof using.
  introv (bi&mi&Hi) Hc Hs He. applys~ local_weaken_gc_pre (I bi mi). xlocal.
  xextract as HG. clear Hi. apply local_erase. introv LR HR.
  gen bi. induction_wf IH: (int_downto_wf 0) mi. intros.
  applys (rm HR). xlet. applys Hc. simpl. xif.
   xseq. applys Hs. xextract as b m' E. xapplys IH. applys E. hsimpl. hsimpl.
   xret_no_gc. subst Q. hsimpl.
Qed.


Tactic Notation "xgeneralize" constr(E) "as" ident(H) :=
  cuts H: E; [ eapply local_weaken_pre; [xlocal | | ] | ].

Tactic Notation "xgeneralize" constr(E) :=
  let H := fresh "Inv" in xgeneralize E as H.

Ltac xwhile_intros tt :=
  let R := fresh "R" in let LR := fresh "L" R in
  let HR := fresh "H" R in 
  apply local_erase; intros R LR HR.

Ltac xwhile_pre cont :=
  xextractible tt;
  match ltac_get_tag tt with
  | tag_seq => xseq; [ cont tt | ]
  | tag_while => cont tt
  end.

Tactic Notation "xwhile" :=
  xwhile_pre ltac:(fun _ => xwhile_intros tt).
Tactic Notation "xwhile" constr(E) :=
  xwhile_pre ltac:(fun _ => xwhile_intros tt; xgeneralize E).


(*--------------------------------------------------------*)
(* ** [xfor] *)

Lemma for_loop_cf_to_inv : 
   forall I H',
   forall (a:int) (b:int) (F:int->~~unit) H (Q:unit->hprop),
   (a > (b)%Z -> H ==> (Q tt)) ->
   (a <= (b)%Z ->
          (H ==> I a \* H') 
       /\ (forall i, a <= i /\ i <= (b)%Z -> F i (I i) (# I(i+1))) 
       /\ (I ((b)%Z+1) \* H' ==> Q tt)) ->
  (For i = a To b Do F i _Done) H Q.
Proof.
  introv M1 M2. apply local_erase. intros S LS HS.
  tests C: (a > b). 
  apply (rm HS). split; intros C'. math. xret_no_gc~.
  forwards (Ma&Mb&Mc): (rm M2). math.
   cuts P: (forall i, a <= i <= b+1 -> S i (I i) (# I (b+1))).
     xapply P. math. hchanges Ma. hchanges Mc.
   intros i. induction_wf IH: (int_upto_wf (b+1)) i. intros Bnd.
   applys (rm HS). split; intros C'.
     xseq. eapply Mb. math. xapply IH; auto with maths; hsimpl.
  xret_no_gc. math_rewrite~ (i = b +1).  
Qed.

Lemma for_loop_cf_to_inv_gen' : 
   forall I H',
   forall (a:int) (b:int) (F:int->~~unit) H,
   (a <= (b)%Z ->
          (H ==> I a \* H') 
       /\ (forall i, a <= i /\ i <= (b)%Z -> F i (I i) (# I(i+1)))) ->
   (a > (b)%Z -> H ==> I ((b)%Z+1) \* H') ->
  (For i = a To b Do F i _Done) H (# I ((b)%Z+1) \* H').
Proof. intros. applys* for_loop_cf_to_inv. Qed.

Lemma for_loop_cf_to_inv_gen : 
   forall I H',
   forall (a:int) (b:int) (F:int->~~unit) H Q,
   (a <= (b)%Z -> H ==> I a \* H') ->
   (forall i, a <= i <= (b)%Z -> F i (I i) (# I(i+1))) ->
   (a > (b)%Z -> H ==> I ((b)%Z+1) \* H') ->  
   (# (I ((b)%Z+1) \* H')) ===> Q ->
  (For i = a To b Do F i _Done) H Q.
Proof. intros. applys* for_loop_cf_to_inv. intros C. hchange (H2 C). hchange (H3 tt). hsimpl. Qed.


Lemma for_loop_cf_to_inv_up : 
   forall I H',
   forall (a:int) (b:int) (F:int->~~unit) H (Q:unit->hprop),
   (a <= (b)%Z) ->
   (H ==> I a \* H') ->
   (forall i, a <= i /\ i <= (b)%Z -> F i (I i) (# I(i+1))) ->
   ((# I ((b)%Z+1) \* H') ===> Q) ->
   (For i = a To b Do F i _Done) H Q.
Proof. intros. applys* for_loop_cf_to_inv. intros. math. Qed.

Ltac xfor_intros tt :=
  let S := fresh "S" in let LS := fresh "L" S in
  let HS := fresh "H" S in 
  apply local_erase; intros S LS HS.

Ltac xfor_pre cont :=
  xextractible tt;
  match ltac_get_tag tt with
  | tag_seq => xseq; [ cont tt | ]
  | tag_for => cont tt
  end.

Tactic Notation "xfor" :=
  xfor_pre ltac:(fun _ => xfor_intros tt).
Tactic Notation "xfor" constr(E) :=
  xfor_pre ltac:(fun _ => xfor_intros tt; xgeneralize E).



(*--------------------------------------------------------*)
(* ** [xintros] *)

(** [xintros] applies to a goal of the form [Spec_n f K]
    where [K] has the form [fun x1 .. xN R => H R]. 
    It replaces the goal with [H (App_n f x1 .. xN)],
    where [x1 .. xN] are fresh variables. [xintros] allows 
    proving a function by induction on the structure of a proof.
    Two side-conditions are also generated, [is_spec_n K]
    and [curried_n f], both of which are attempted to be 
    discharged automatically (using [xisspec] and [xcurried]).
    
    [xintros x1 .. xN] is the same as [xintros] followed
    by [intros x1 .. xN]. *)

Ltac xintros_core cont1 cont2 cont3 :=
   intros; 
   let arity := spec_goal_arity tt in
   let lemma := get_spec_intro_x arity in
   apply lemma; [ instantiate; cont1 tt 
                | instantiate; cont2 tt 
                | instantiate; cont3 tt ]. 

Tactic Notation "xintros" :=
  xintros_core ltac:(fun _ => try xisspec) 
               ltac:(fun _ => try xcurried) (* try solve [ xcf; auto ] *)
               ltac:(fun _ => idtac).

Tactic Notation "xintros_noauto" :=
  xintros_core ltac:(fun _ => idtac) 
               ltac:(fun _ => idtac) 
               ltac:(fun _ => idtac).

Tactic Notation "xintros" simple_intropattern(x1) :=
  xintros; intros x1.
Tactic Notation "xintros" simple_intropattern(x1) simple_intropattern(x2) :=
  xintros; intros x1 x2.
Tactic Notation "xintros" simple_intropattern(x1) simple_intropattern(x2)
  simple_intropattern(x3) :=
  xintros; intros x1 x2 x3.
Tactic Notation "xintros" simple_intropattern(x1) simple_intropattern(x2)
  simple_intropattern(x3) simple_intropattern(x4) :=
  xintros; intros x1 x2 x3 x4.


(*--------------------------------------------------------*)
(* ** [xweaken] *)

(** [xweaken S] applies to a goal of the form [Spec_n f K]
    when [S] has type [Spec_n f K']. It leaves as goal the
    proposition that [K] is weaker than [K'], that is,
    [forall x1 .. xN R, weakenable R -> K' x1 .. xN R -> K x1 .. xN)].
    
    The tactic [xweaken] without argument looks for [S] in 
    the database of specifications. 
    
    [xweaken_noauto] is similar, but does not attempt to 
    discharge the subgoals automatically. *)

Ltac xweaken_core cont1 cont2 :=
  let arity := spec_goal_arity tt in
  let lemma := get_spec_weaken_x arity in
  eapply lemma; [ cont1 tt | try xisspec | cont2 tt ].
  (* todo: prendre 3 continuations *)

Ltac xweaken_try_find_spec tt :=
  let f := spec_goal_fun tt in
  first [ let S := get_spec_hyp f in
          sapply S
        | xfind f; let H := fresh in 
          intro H; sapply H; clear H
        | idtac ].  

Ltac xweaken_post tt :=
  let R := fresh "R" in let WR := fresh "W" R in
  let HR := fresh "H" R in intros R WR HR;
  instantiate; cbv beta in HR.

Tactic Notation "xweaken_noauto" :=
  xweaken_core 
    ltac:(fun _ => idtac) 
    ltac:(fun _ => idtac).
Tactic Notation "xweaken" :=
  xweaken_core 
    ltac:(xweaken_try_find_spec) 
    ltac:(fun _ => idtac).
Tactic Notation "xweaken" "as" ident(x1) :=
  xweaken_core 
    ltac:(xweaken_try_find_spec) 
    ltac:(fun _ => intros x1; xweaken_post tt).
Tactic Notation "xweaken" "as" ident(x1) ident(x2) :=
  xweaken_core 
    ltac:(xweaken_try_find_spec) 
    ltac:(fun _ => intros x1 x2; xweaken_post tt).
Tactic Notation "xweaken" "as" ident(x1) ident(x2) ident(x3) :=
  xweaken_core 
    ltac:(xweaken_try_find_spec) 
    ltac:(fun _ => intros x1 x2 x3; xweaken_post tt).

Tactic Notation "xweaken" constr(S) :=
  xweaken_core 
   ltac:(fun _ => sapply S) 
   ltac:(fun _ => idtac).
Tactic Notation "xweaken" constr(S) "as" ident(x1) :=
  xweaken_core 
    ltac:(fun _ => sapply S) 
    ltac:(fun _ => intros x1; xweaken_post tt).
Tactic Notation "xweaken" constr(S) "as" ident(x1) ident(x2) :=
  xweaken_core 
    ltac:(fun _ => sapply S) 
    ltac:(fun _ => intros x1 x2; xweaken_post tt).
Tactic Notation "xweaken" constr(S) "as" ident(x1) ident(x2) ident(x3) :=
  xweaken_core 
    ltac:(fun _ => sapply S) 
    ltac:(fun _ => intros x1 x2 x3; xweaken_post tt).

Tactic Notation "xweaken" "~" := xweaken; auto_tilde.
Tactic Notation "xweaken" "*" := xweaken; auto_star.
Tactic Notation "xweaken" "~" constr(S) := xweaken S; auto_tilde.
Tactic Notation "xweaken" "*" constr(S) := xweaken S; auto_star.


(*--------------------------------------------------------*)
(* ** [xapply] *)

Ltac xapply_core H cont1 cont2 :=
  xextractible tt;
  forwards_nounfold_then H ltac:(fun K => 
    eapply local_wframe; [xlocal | sapply K | cont1 tt | cont2 tt]).

Ltac xapply_base H :=
  xapply_core H ltac:(fun tt => idtac) ltac:(fun tt => idtac). 

Ltac xapplys_base H :=
  xapply_core H ltac:(fun tt => hcancel) ltac:(fun tt => hsimpl). 

Tactic Notation "xapply" constr(H) :=
  xapply_base H.
Tactic Notation "xapply" "~" constr(H) :=
  xapply H; auto_tilde.
Tactic Notation "xapply" "*" constr(H) :=
  xapply H; auto_star.

Tactic Notation "xapplys" constr(H) :=
  xapplys_base H.
Tactic Notation "xapplys" "~" constr(H) :=
  xapplys H; auto_tilde.
Tactic Notation "xapplys" "*" constr(H) :=
  xapplys H; auto_star.


(*--------------------------------------------------------*)
(* ** [xcleanpat] *)

Definition xnegpat (P:Prop) := P.

Lemma xtag_negpat_intro : forall (P:Prop), P -> xnegpat P.
Proof. auto. Qed.

Ltac xuntag_negpat := 
  unfold xnegpat in *.

Ltac xtag_negpat H :=
  applys_to H xtag_negpat_intro.

Ltac xcleanpat_core :=
  repeat match goal with H: xnegpat _ |- _ => clear H end.

Tactic Notation "xcleanpat" :=
  xcleanpat_core.

(**--todo clean up *)


(************************************************************)
(* ** [xif] *)

Ltac xif_post H :=  (* todo: cleanup *)
   calc_partial_eq tt;
   try fold_bool; fold_prop;
   try fix_bool_of_known tt;
   try solve [ discriminate | false; congruence ];
   first [ subst_hyp H; try fold_bool
         | idtac ];
   try fix_bool_of_known tt;
   try (check_noevar_hyp H; rew_reflect in H; rew_logic in H).

Ltac xif_core H :=
  apply local_erase; split; intro H.

Ltac xif_base H cont :=
  xextractible tt;
  xif_core H; cont tt.

Ltac xif_then H :=
  xif_base H ltac:(fun _ => xif_post H).

Tactic Notation "xif_manual" "as" ident(H) :=
  xif_base H ltac:(fun _ => idtac).
Tactic Notation "xif_manual" :=
  let H := fresh "C" in xif_manual as H.

Tactic Notation "xif" "as" ident(H) :=
  xif_then H.
Tactic Notation "xif" :=
  let H := fresh "C" in xif as H.

Tactic Notation "xif" constr(Q) "as" ident(H) :=
  xpost Q; xif as H.
Tactic Notation "xif" constr(Q) :=
  let H := fresh "C" in xif Q as H.



(************************************************************)
(* ** [xcase] *)

(*--useless?--*)

Ltac xpat_base H cont :=
  apply local_erase;
  match goal with 
  | |- ?P /\ ?Q => split; [ introv H | introv H; xtag_negpat H; cont tt ]
  | |- forall _, _ => introv H
  end.

Ltac xpat_base_anonymous cont :=
  let H := fresh "C" in xpat_base H cont.

Tactic Notation "xcase_one" "as" ident(H) :=
  xextractible tt;
  xpat_base H ltac:(idcont).
  
Tactic Notation "xcase_one" :=
  xextractible tt;
  xpat_base_anonymous ltac:(idcont).

Ltac xcases_core :=
  xextractible tt;
  xpat_base_anonymous ltac:(fun _ => try xcases_core);
  try fold_bool; fold_prop.

Ltac xcases_subst_core :=
  xcases_core; try invert_first_hyp.

Tactic Notation "xcases" :=
  xcases_core.
Tactic Notation "xcases_subst" := 
  xcases_subst_core. 

Ltac xcase_post :=
  try solve [ discriminate | false; congruence ];
  try match ltac_get_tag tt with tag_done => xdone end; 
  try invert_first_hyp; 
  try fold_bool; fold_prop.

Tactic Notation "xcase_one_real" := 
   xcase_one; xcase_post.

Tactic Notation "xcases_real" := 
  xcases; xcase_post.

Tactic Notation "xcase" := 
  xcases_real.



(************************************************************)
(* ** [xalias] *)

(** [xalias as H] applies to a goal of the form  
    [(Alias x := v in Q) P] and adds an assumption [H] of 
    type [x = v]. It leaves [Q P] as new subgoal.
    [xalias] finds automatically a name for [H]. 
    [xalias as x H] allows to modify the name [x].
    [xalias_subst] substitutes the equality right away. *)

Tactic Notation "xalias" "as" ident(x) ident(H) :=
  xuntag tag_alias; intros x H.

Tactic Notation "xalias" "as" ident(x) :=
  let HE := fresh "E" x in xalias as x Hx.

Tactic Notation "xalias" :=
  xuntag tag_alias; intro; 
  let H := get_last_hyp tt in
  let HE := fresh "E" H in 
  intro HE.

Tactic Notation "xalias_subst" :=
  let x := fresh "TEMP" in let Hx := fresh "TEMP" in
  xalias as x Hx; subst x.


(************************************************************)
(* ** deprecated *)

Ltac xpat_core_new H cont1 cont2 :=
  xuntag tag_case; apply local_erase; split; 
    [ introv H; cont1 H 
    | introv H; xtag_negpat H; cont2 H ].

Ltac xpat_post H :=
  try solve [ discriminate | false; congruence ]; 
  try (symmetry in H; inverts H).

Tactic Notation "xpat" :=
  let H := fresh in
  xpat_core_new H ltac:(fun _ => idtac) ltac:(fun _ => idtac);
  xpat_post H.


(************************************************************)
(* ** [xmatch] *)

(* TODO: permettre de choisir tous les noms *)

(** [xmatch] applies to a pattern-matching goal of the form
    [(Match Case x = p1 Then Q1 
       Else Case x = p2 Then Alias y := v in Q2
       Else Done/Fail) P].
    Several variants are available:
    - [xmatch] performs all the case analyses, 
      and introduces all aliases.
    - [xmatch_keep_alias] performs all case analyses,
      and does not introduces aliases.
    - [xmatch_subst_alias] performs all case analyses,
      and introduces and substitutes all aliases.
    - [xmatch_simple] performs all case analyses, 
      but does not try and invoke the inversion tactic on 
      deduced equalities. Tactics [xmatch_simple_keep_alias]
      and [xmatch_simple_subst_alias] are also available. 
    - [xmatch_nocases] simply remove the [Match] tag and
      leaves the cases to be solved manually using [xcase]. 
    - [xmatch_clean] is like [xmatch; xcleanpat]: it does not
      keep the negation of the patterns as hypotheses *)

Ltac xmatch_core cont :=
  xextractible tt;
  let tag := ltac_get_tag tt in
  match tag with
  | tag_match ?n => xuntag (tag_match n); cont n
  end.

Ltac xmatch_case cont :=
  let H := fresh "C" in 
  xpat_core_new H ltac:(xpat_post) ltac:(fun _ => cont tt).

Ltac xmatch_case_simple cont :=
  let H := fresh "C" in 
  xpat_core_new H ltac:(idtac) ltac:(fun _ => cont tt).

Ltac xmatch_cases case_tactic n :=
  match n with
  | 0%nat => first [ xdone | xfail | idtac ]
  | S ?n' => case_tactic ltac:(fun _ => xmatch_cases case_tactic n')
  | _ => idtac
  end.

Tactic Notation "xmatch_nocases" :=
  xmatch_core ltac:(fun _ => idtac).

Tactic Notation "xmatch_keep_alias" :=
  xmatch_core ltac:(fun n => xmatch_cases xmatch_case n).
Tactic Notation "xmatch_simple_keep_alias" :=
  xmatch_core ltac:(fun n => xmatch_cases xmatch_case_simple n).
Tactic Notation "xmatch" :=
   xmatch_keep_alias; repeat xalias.
Tactic Notation "xmatch_simple" :=
   xmatch_simple_keep_alias; repeat xalias.
Tactic Notation "xmatch_subst_alias" :=
   xmatch_keep_alias; repeat xalias_subst.
Tactic Notation "xmatch_simple_subst_alias" :=
   xmatch_simple_keep_alias; repeat xalias_subst.


Tactic Notation "xmatch" constr(Q) :=
  xpost Q; xmatch.

Tactic Notation "xmatch_clean" :=
  xmatch; xcleanpat.


(*--------------------------------------------------------*)
(* ** [xwhile_inv] *)


Ltac xwhile_inv_measure_post_meta I :=
  apply (@while_loop_cf_inv_measure I); [ | | | reflexivity ].

Ltac xwhile_inv_measure_core I :=
  let go tt := xwhile_inv_measure_post_meta I in
  match post_is_meta tt with
  | true => go tt
  | false => xgc; [ go tt | ]
  end.

Tactic Notation "xwhile_inv" constr(I) :=
  xwhile_pre ltac:(fun _ => xwhile_inv_measure_core I).



(*--------------------------------------------------------*)
(* ** [xfor_inv] *)

Ltac xfor_inv_gen_base I i C :=
  eapply (@for_loop_cf_to_inv_gen I); 
  [ intros C
  | intros i C
  | intros C
  | apply rel_le_refl ].

Tactic Notation "xfor_inv_gen" constr(I) "as" ident(i) ident(C) :=
  xfor_inv_gen_base I i C.
Tactic Notation "xfor_inv_gen" constr(I) "as" ident(i) :=
  let C := fresh "C" i in xfor_inv_gen I as i C.
Tactic Notation "xfor_inv_gen" constr(I) :=
  let i := fresh "i" in xfor_inv_gen I as i.

Ltac xfor_inv_base I i C :=
  eapply (@for_loop_cf_to_inv_up I); 
  [ 
  | 
  | intros i C
  | apply rel_le_refl ].

Tactic Notation "xfor_inv" constr(I) "as" ident(i) ident(C) :=
  xfor_inv_gen_base I i C.
Tactic Notation "xfor_inv" constr(I) "as" ident(i) :=
  let C := fresh "C" i in xfor_inv I as i C.
Tactic Notation "xfor_inv" constr(I) :=
  let i := fresh "i" in xfor_inv I as i.



(************************************************************)
(* ** [xopen] *)

(* e.g.
   Hint Extern 1 (Register focus (Tree _)) => 
     Provide tree_focus_contents.

   then [xopen t] or [xopen t as I1 I2]
*)


Ltac get_refocus_args tt :=
  match goal with 
  | |- ?Q1 ===> ?Q2 => constr:(Q1,Q2)
  | |- ?H1 ==> ?H2 => constr:(H1,H2)
  | |- ?R ?H1 ?Q2 => constr:(H1,Q2)
  (* TODO: gérer le cas de fonctions appliquées à R *)
  end.

Ltac get_refocus_constr_in H t :=
  match H with context [ t ~> ?T ] => constr:(T) end.

Ltac xopen_constr t :=
  match get_refocus_args tt with (?H1,?H2) =>
  get_refocus_constr_in H1 t end.

Ltac xopen_core t :=
  let C1 := xopen_constr t in
  ltac_database_get database_spec_focus C1;
  let K := fresh "TEMP" in
  intros K; xchange (K t); clear K.

Ltac xopen_show t := 
  let C1 := xopen_constr t in
  pose C1; try ltac_database_get database_spec_focus C1; intros.

Tactic Notation "xopen" constr(t) :=
  xopen_core t.
Tactic Notation "xopen" "~" constr(t) :=
  xopen t; auto_tilde.
Tactic Notation "xopen" "*" constr(t) :=
  xopen t; auto_star.

Tactic Notation "xopen" constr(t) "as" simple_intropattern(I1) :=
  xopen t; xextract as I1.
Tactic Notation "xopen" constr(t) "as" simple_intropattern(I1) simple_intropattern(I2) :=
  xopen t; xextract as I1 I2.
Tactic Notation "xopen" constr(t) "as" simple_intropattern(I1) simple_intropattern(I2)
  simple_intropattern(I3) :=
  xopen t; xextract as I1 I2 I3.
Tactic Notation "xopen" constr(t) "as" simple_intropattern(I1) simple_intropattern(I2)
  simple_intropattern(I3) simple_intropattern(I4) :=
  xopen t; xextract as I1 I2 I3 I4.
Tactic Notation "xopen" constr(t) "as" simple_intropattern(I1) simple_intropattern(I2)
  simple_intropattern(I3) simple_intropattern(I4) simple_intropattern(I5) :=
  xopen t; xextract as I1 I2 I3 I4 I5.


(************************************************************)
(* ** [xclose] *)

(** e.g.
   Hint Extern 1 (Register unfocus (Ref Id (MNode _ _ _))) => 
      Provide tree_node_unfocus.

   then [xclose t] or [xclose t1 t2 t3]
*)

Ltac xclose_constr t :=
  match get_refocus_args tt with (?H1,?H2) =>
  get_refocus_constr_in H1 t end.

Ltac xclose_core t :=
  let C1 := xclose_constr t in
  ltac_database_get database_spec_unfocus C1;
  let K := fresh "TEMP" in
  intros K; xchange (K t); clear K.

Ltac xclose_show t := 
  let C1 := xclose_constr t in
  pose C1; try ltac_database_get database_spec_unfocus C1; intros.

Tactic Notation "xclose" constr(t) :=
  xclose_core t.
Tactic Notation "xclose" "~" constr(t) :=
  xclose t; auto_tilde.
Tactic Notation "xclose" "*" constr(t) :=
  xclose t; auto_star.

Tactic Notation "xclose" constr(t1) constr(t2) :=
  xclose t1; xclose t2.
Tactic Notation "xclose" constr(t1) constr(t2) constr(t3) :=
  xclose t1; xclose t2 t3.
Tactic Notation "xclose" constr(t1) constr(t2) constr(t3) constr(t4) :=
  xclose t1; xclose t2 t3 t4.











------------


(* FUTURE

    - [xapp_1] is like [xapp1]
    - [xapp_2] is like [xapp1; xapp2]
    - [xapp_3] is like [xapp1; xapp2; xapp3]
    - [xapp_4] is like [xapp1; xapp2; xapp3; xapp4]
    - [xapp_5] is like [xapp1; xapp2; xapp3; xapp4; xapp5]

Tactic Notation "xapp_1" := xapp1.
Tactic Notation "xapp_2" := xapp1; xapp2.
Tactic Notation "xapp_2_spec" constr(H) := xapp_1; xapp2_spec H.
Tactic Notation "xapp_3" := xapp_2; xapp3.
Tactic Notation "xapp_3" constr(args) := xapp_2; xapp3 args.
Tactic Notation "xapp_3_spec" constr(H) := xapp_2_spec H; xapp3.
Tactic Notation "xapp_3_spec" constr(H) constr(args) := xapp_2_spec H; xapp3 args.
Tactic Notation "xapp_4" := xapp_3; xapp4.
Tactic Notation "xapp_4" constr(args) := xapp_3 args; xapp4.
Tactic Notation "xapp_4_spec" constr(H) := xapp_spec H; xapp4.
Tactic Notation "xapp_4_spec" constr(H) constr(args) := xapp_3_spec H args; xapp4.
*)




    
    Hint: call [xgc] prior to [xapply] if you also need a
    step of garbage collection. 
    // LATER: make [xapply] support the gc rule, and introduce
    // [xapply_no_gc E] is like [xapply] but does not allow 
    // for the garbage collection rule to be applied.
    // In that case, change [xapp_prepare] to not do [xgc];
    // but check first that introduce evars later is not an issue.
