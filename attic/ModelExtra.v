
(** We give two definitions equivalent to [triple], called
    [triple'] and [triple'']. *)

Definition triple' t H Q :=
  forall h1 h2, 
  \# h1 h2 -> 
  H h1 -> 
  exists n h1' v, 
       \# h1' h2 
    /\ redn n (h1 \u h2) t (h1' \u h2) v
    /\ on_sub (Q v) h1'
    /\ (heap_credits h1 >= n + heap_credits h1')%nat.

Lemma triple_eq_triple' : triple = triple'.
Proof using.
  applys func_ext_3. intros t H Q. extens.
  unfold triple, triple', credits. iff M.
  { introv D P1. forwards (n&h1'&v&R1&R2&R3&R4): M D P1.
    exists n h1' v. splits~. math. }  
  { introv D P1. forwards (n&h1'&v&R1&R2&R3&R4): M D P1.
    exists n (h1' \u (state_empty,(h1^c - n - h1'^c)%nat)) v. splits~. 
    { state_red. }
    { applys~ on_sub_union_r. }
    { simpl. math. } }
Qed.







(* DEPRECATED
(*------------------------------------------------------------------*)
(* ** Properties of [heap_empty] *)

Lemma heap_empty_state : heap_state heap_empty = state_empty.
Proof. auto. Qed.

Hint Rewrite heap_empty_state : rew_heap.
*)
  (* TODO: DEPRECATED *)
Lemma heap_union_comm_assoc : forall h1 h2 h3,
  \# h1 h2 h3 ->
  (h1 \u h2) \u h3 = h2 \u (h3 \u h1).
Proof using. 
  introv M. rewrite (@heap_union_comm h1 h2).
  rewrite~ heap_union_assoc. fequals.
  rewrite~ heap_union_comm. state_disjoint.
Qed.

(** Disjoint3 *) (* TODO: DEPRECATED *)

Definition heap_disjoint_3 h1 h2 h3 :=
  \# h1 h2 /\ \# h2 h3 /\ \# h1 h3.

Notation "\# h1 h2 h3" := (heap_disjoint_3 h1 h2 h3)
  (at level 40, h1 at level 0, h2 at level 0, h3 at level 0, no associativity).

Hint Extern 1 (\# _ _ _) => state_disjoint_pre.

Lemma heap_disjoint_3_unfold : forall h1 h2 h3,
  \# h1 h2 h3 = (\# h1 h2 /\ \# h2 h3 /\ \# h1 h3).
Proof using. auto. Qed.

Hint Rewrite 
  heap_disjoint_3_unfold : rew_disjoint.

(* DEPRECATED
  | redn_new : forall ma mb v l,
      mb = (state_single l v) ->
      \# ma mb ->
      redn 0 ma (trm_new v) (mb \+ ma) (val_loc l)
  | redn_get : forall ma mb l v,
      ma = (state_single l v) ->
      \# ma mb ->
      redn 0 (ma \+ mb) (trm_get (val_loc l)) (ma \+ mb) v
  | redn_set : forall ma1 ma2 mb l v1 v2,
      ma1 = (state_single l v1) ->
      ma2 = (state_single l v2) ->
      \# ma1 mb ->
      redn 0 (ma1 \+ mb) (trm_set (val_loc l) v2) (ma2 \+ mb) val_unit.



Lemma rule_new : forall v,
  triple (trm_new v) \[] (fun r => Hexists l, \[r = val_loc l] \* l ~~> v).
Proof using.
  Transparent hprop_single.
  introv D H1. unfolds hprop_single.
  lets: hprop_empty_inv H1. subst h1. rewrite heap_union_empty_l.
  asserts (l&Hl): (exists l, (state_data h2) l = None).
  { skip. } (* infinitely many locations -- TODO *)
  asserts Fr: (state_disjoint h2 (state_single l v)).
  { unfolds state_disjoint, pfun_disjoint, state_single. simpls.
    intros x. case_if~. }
  exists 0%nat ((state_single l v),0%nat) heap_empty (val_loc l).
  rewrite heap_union_empty_r. splits.
  { rew_disjoint. splits*. destruct h2 as (m2,n2). split~. }
  { rewrite heap_union_state. applys* redn_new. }
  { exists l heap_empty ((state_single l v),0%nat). splits~. }
  { simpl. math. } 
Qed.

Lemma rule_get : forall v l,
  triple (trm_get (val_loc l)) (l ~~> v) (fun x => \[x = v] \* l ~~> v).
Proof using.
  Transparent hprop_single.
  introv D H1. unfolds hprop_single.  
  exists 0%nat h1 heap_empty v. rewrite heap_union_empty_r. 
  splits.
  { state_disjoint. }
  { rewrite heap_union_state. applys~ redn_get. (* TODO: simplify below *)
    { destruct* h1 as (m1,n1). }
    { destruct h1 as (m1,n1). destruct h2 as (m2,n2). simpl.  
      unfold heap_disjoint in D. hnf in D. autos*. } }
  { exists heap_empty h1. splits~. }
  { math. }
Qed.

Lemma rule_set : forall l v w,
  triple (trm_set (val_loc l) w) (l ~~> v) (fun r => \[r = val_unit] \* l ~~> w).
Proof using.
  Transparent hprop_single.
  introv D H1. unfolds hprop_single.  
  exists 0%nat (state_single l w, 0%nat) heap_empty val_unit.
  rewrite heap_union_empty_r. 
  splits.
  { rew_disjoint. splits*.
    destruct h1 as (m1,n1). destruct h2 as (m2,n2). destruct H1. subst m1. 
    unfolds heap_disjoint, state_disjoint, pfun_disjoint, state_single. simpls.
    split~. destruct D as [D _].
    intros x. specializes D x. case_if; intuition auto_false. }
  { do 2 rewrite heap_union_state. applys~ redn_set v. (* TODO: simplify below *)
    { destruct* h1 as (m1,n1). }
    { destruct h1 as (m1,n1). destruct h2 as (m2,n2). simpl.  
      unfold heap_disjoint in D. hnf in D. autos*. } }
  { exists heap_empty (state_single l w, 0%nat). splits~. }
  { simpl. math. }
Qed.



*)

(* DEPRECATED equivalent formulation
Lemma rule_val_fix : forall f x F V t1 H Q,
  F = (val_fix f x t1) ->
  triple (subst_trm f F (subst_trm x V t1)) H Q ->
  triple (trm_app F V) ((\$ 1%nat) \* H) Q.
Proof using.
  introv EF M. subst F. intros h1 h2 D1 (h1a&h1b&H1a&H1b&E3&E4).
  hnf in H1a. lets H1a': hprop_credits_inv (rm H1a). clear E3.
  lets~ (na&h1'a&v&Da&Ra&Qa&Na): M h1b h2 H1b.
  exists (S na) h1'a v. splits~. 
  { applys~ redn_app. state_red. }
  { subst. simpl. math. }
Qed.
*)


(* DEPRECATED

  Definition app f v H Q :=
    triple (trm_app f v) H Q.

  Lemma rule_app_shorthand : forall f v H Q,
    app f v H Q ->
    triple (trm_app f v) H Q.
  Proof using.
    auto.
  Qed.

  Lemma rule_let_fix' : forall f x t1 t2 H Q,
    (forall (F:val), 
      (forall X H' H'' Q', 
        pay_one H' H'' ->
        triple (subst_trm f F (subst_trm x X t1)) H'' Q' ->
        app F X H' Q') -> 
      triple (subst_trm f F t2) H Q) ->
    triple (trm_let f (trm_val (val_fix f x t1)) t2) H Q.

*)



(* DEPRECATED

Lemma rule_consequence_gc : forall t H Q H' Q',
  H ==> H' \* \GC ->
  triple t H' Q' ->
  Q' ===> Q \*+ \GC ->
  triple t H Q.
Proof using. 
  introv IH M IQ. intros h1 h2 D1 H1. 
  lets H1': IH (rm H1).
  lets (h1a&h1b&H1a&H1b&D1ab&E12): (rm H1').
  lets (n&h1'&h3&r&Da&Ra&Qa&Ca): M (h1b \u h2) (rm H1a).
  { state_disjoint. }
  lets Qa': IQ (rm Qa).
  lets (h1a'&h1b'&H1a'&H1b'&D1ab'&E12'): (rm Qa').  
  exists n h1a' (h1b' \u h1b \u h3) r. splits.
  { state_disjoint. }
  { subst. applys_eq Ra 2 4.
    { rewrite~ heap_union_assoc. }
    { fequals. rewrite (@heap_union_comm h1b h2).
      repeat rewrite heap_union_assoc. fequals.
      rewrite <- heap_union_comm_assoc.
      rewrite <- heap_union_comm_assoc.
      repeat rewrite heap_union_assoc. fequals. fequals.
      rewrite~ heap_union_comm.
      state_disjoint. state_disjoint. state_disjoint. state_disjoint. } }
  { auto. }
  { subst. rewrite heap_union_credits in *. math. }
Qed. (* TODO: tactic for heap simplification *)

Lemma rule_consequence : forall H' Q' t H Q,
  H ==> H' ->
  triple t H' Q ->
  Q ===> Q' ->
  triple t H Q'.
Proof using. 
  introv WH M WQ. applys rule_consequence_gc M.
  { applys~ himpl_remove_gc. }
  { intros r. applys~ himpl_remove_gc. }
Qed.

*)

(*------------------------------------------------------------------*)
(** Predicate over pairs *)

Definition prod_st2 A B (P:binary A) (Q:binary B) (v1 v2:A*B) := 
  let (x1,y1) := v1 in 
  let (x2,y2) := v2 in 
  P x1 x2 /\ Q y1 y2.

Definition prod_func A B (F:A->A->A) (G:B->B->B) (v1 v2:A*B) :=
  let (x1,y1) := v1 in 
  let (x2,y2) := v2 in 
  (F x1 x2, G y1 y2).





Definition app f v H Q :=
  triple (trm_app f v) H Q.



Lemma rule_app : forall f v H Q,
  app f v H Q ->
  triple (trm_app f v) H Q.
Proof using.
  auto.
Qed.



(*
  | red_fix : forall m1 m2 f x t1 t2 r,
      red m1 (subst_trm f (val_fix f x t1) t2) m2 r ->
      red m1 (trm_fix f x t1 t2) m2 r
*)


(********************************************************************)
(* ** More *)

(** Substitution 

Lemma subst_lemma : forall x v t H Q,
  (forall (X:val), triple (subst_trm x X t) (H X) (Q X)) ->
  triple (subst_trm x v t) (H v) (Q v).
Proof using. 
  auto.
Qed.
*)


(* BONUS

Lemma hprop_star_comm_assoc : comm_assoc hprop_star.
Proof using. apply comm_assoc_prove. apply star_comm. apply star_assoc. Qed.

Lemma hprop_star_post_neutral : forall B (Q:B->hprop),
  Q \*+ \[] = Q.
Proof using. extens. intros. rewrite~ hprop_star_empty_r. Qed.

*)

(* BONUS
Notation "\[= v ]" := (fun x => \[x = v])
  (at level 0) : heap_scope.

Notation "P ==+> Q" := (pred_le P%h (hprop_star P Q)) 
  (at level 55, only parsing) : heap_scope.
*)





(*------------------------------------------------------------------*)
(* ** Normalization of [star] *)

(* BONUS
Hint Rewrite 
  hprop_star_empty_l hprop_star_empty_r (*hprop_star_post_neutral*) : rew_heap.
Hint Rewrite <- star_assoc : rew_heap.

Tactic Notation "rew_heap" :=
  autorewrite with rew_heap.
Tactic Notation "rew_heap" "in" "*" :=
  autorewrite with rew_heap in *.
Tactic Notation "rew_heap" "in" hyp(H) :=
  autorewrite with rew_heap in H.

Tactic Notation "rew_heap" "~" :=
  rew_heap; auto_tilde.
Tactic Notation "rew_heap" "~" "in" "*" :=
  rew_heap in *; auto_tilde.
Tactic Notation "rew_heap" "~" "in" hyp(H) :=
  rew_heap in H; auto_tilde.

Tactic Notation "rew_heap" "*" :=
  rew_heap; auto_star.
Tactic Notation "rew_heap" "*" "in" "*" :=
  rew_heap in *; auto_star.
Tactic Notation "rew_heap" "*" "in" hyp(H) :=
  rew_heap in H; auto_star.

*)



(*
Definition is_val (t:trm) : option val :=
  match t with 
  | trm_val v => Some v
  | _ => None
  end.
  | trm_if t0 t1 t2 => 
    match is_val t0 with
    | Some v => Trm_if_val v (aux t1) (aux t2)
    | None => Trm_if (aux t0) (aux t1) (aux t2)
    end
*)

(* REJECTED FOR TERMINATION REASONS

Definition is_val_fix (f:var) (t:trm) : option (vars*trm) :=
  match t with 
  | trm_val (val_fix f' xs t) => 
     if eq_var_dec f f' then None else Some (xs,t)
  | _ => None 
  end.

Fixpoint Trm_of_trm (t : trm) : Trm :=
  let aux := Trm_of_trm in
  match t with
  | trm_val v => Trm_val v
  | trm_if t0 t1 t2 => 
    match is_val t0 with
    | Some v => Trm_if_val v (aux t1) (aux t2)
    | None => Trm_if (aux t0) (aux t1) (aux t2)
    end
  | trm_seq t1 t2 => Trm_seq (aux t1) (aux t2)
  | trm_let x t1 t2 => 
    match is_val_fix x t1 with
    | Some (xs,t1) => Trm_let_fix x xs (aux t1) (aux t2)
    | None => Trm_let x (aux t1) (aux t2)
    end
  | trm_app v1 v2s => Trm_app v1 v2s
  | trm_while t1 t2 => Trm_while (aux t1) (aux t2)
  end.
*)

(*------------------------------------------------------------------*)
(*------------------------------------------------------------------*)
(*------------------------------------------------------------------*)
(* FOR LATER -- Alternative formulation

    | red_new : forall ma mb ks vs l kvs,
      No_duplicates ks ->
      Forall3 (fun kv k v => kv = (val_pair (val_field k) v)) kvs ks vs ->
      mb = (state_record l ks vs) ->
      \# ma mb ->
      red ma (prim_new kvs) (mb \+ ma) (val_loc l)
    | red_if_val : forall m1 m2 v r t1 t2,
      red m1 (If v = val_int 0 then t2 else t1) m2 r ->
      red m1 (trm_if v t1 t2) m2 r
    | red_while_true : forall m1 m2 m3 m4 t1 t2 v1 v2 v3,
      red m1 t1 m2 v1 ->
      v1 <> val_int 0 ->
      red m2 t2 m3 v2 ->
      red m3 (trm_while t1 t2) m4 v3 ->
      red m1 (trm_while t1 t2) m4 val_unit
   | red_while_false : forall m1 m2 t1 t2 v1,
      red m1 t1 m2 v1 ->
      v1 = val_int 0 ->
      red m1 (trm_while t1 t2) m2 val_unit
      
*)


(* FOR LATER

Notation "'And_' v1 `&&` F2" :=
  (!If (fun H Q => (v1 = true -> F2 H Q) /\ (v1 = false -> (Ret false) H Q)))
  (at level 69, v1 at level 0) : trm_scope.

Notation "'Or_' v1 `||` F2" :=
  (!If (fun H Q => (v1 = true -> (Ret true) H Q) /\ (v1 = false -> F2 H Q)))
  (at level 69, v1 at level 0) : trm_scope.

Notation "'For' i '=' a 'To' b 'Do' F1 'Done_'" :=
  (!For (fun H Q => forall S:int->~~unit, CFHeaps.is_local_pred S ->
        (forall i H Q,
             (If_ isTrue (i <= (b)%Z) Then (F1 ;; S (i+1)) Else (Ret tt)) H Q
          -> S i H Q)
       -> S a H Q))
  (at level 69, i ident, a at level 0, b at level 0,
   format "'[v' 'For'  i  '='  a  'To'  b  'Do'  '/' '[' F1 ']' '/'  'Done_' ']'")
  : trm_scope.
*)

(* LATER

Notation "`{ f1 := x1 }'" := 
  ((f1, x1)::nil)
  (at level 0, f1 at level 0) 
  : trm_scope.
Notation "`{ f1 := x1 ; f2 := x2 }'" :=
  ((f1, x1)::(f2, x2)::nil)
  (at level 0, f1 at level 0, f2 at level 0) 
  : trm_scope.
Notation "`{ f1 := x1 ; f2 := x2 ; f3 := x3 }'" :=
  ((f1, x1)::(f2, x2)::(f3, x3)::nil)
  (at level 0, f1 at level 0, f2 at level 0, f3 at level 0) 
  : trm_scope.
Notation "`{ f1 := x1 ; f2 := x2 ; f3 := x3 ; f4 := x4 }'" :=
  ((f1, x1)::(f2, x2)::(f3, x3)::(f4, x4)::nil)
  (at level 0, f1 at level 0, f2 at level 0, f3 at level 0, f4 at level 0)
  : trm_scope.


Notation "`{ f1 := x1 ; f2 := x2 ; f3 := x3 ; f4 := x4 ; f5 := x5 }'" :=
  ((f1, x1)::(f2, x2)::(f3, x3)::(f4, x4)::(f5, x5)::nil)
  (at level 0, f1 at level 0, f2 at level 0, f3 at level 0, f4 at level 0, 
   f5 at level 0)
  : trm_scope.
Notation "`{ f1 := x1 ; f2 := x2 ; f3 := x3 ; f4 := x4 ; f5 := x5 ; f6 := x6 }'" :=
  ((f1, x1)::(f2, x2)::(f3, x3)::(f4, x4)::(f5, x5)::
   (f6, x6)::nil)
  (at level 0, f1 at level 0, f2 at level 0, f3 at level 0, f4 at level 0, 
   f5 at level 0, f6 at level 0)
  : trm_scope.
Notation "`{ f1 := x1 ; f2 := x2 ; f3 := x3 ; f4 := x4 ; f5 := x5 ; f6 := x6 ; f7 := x7 }'" :=
  ((f1, x1)::(f2, x2)::(f3, x3)::(f4, x4)::(f5, x5)::
   (f6, x6)::(f7, x7)::nil)
  (at level 0, f1 at level 0, f2 at level 0, f3 at level 0, f4 at level 0, 
   f5 at level 0, f6 at level 0, f7 at level 0)
  : trm_scope.
Notation "`{ f1 := x1 ; f2 := x2 ; f3 := x3 ; f4 := x4 ; f5 := x5 ; f6 := x6 ; f7 := x7 ; f8 := x8 }'" :=
  ((f1, x1)::(f2, x2)::(f3, x3)::(f4, x4)::(f5, x5)::
   (f6, x6)::(f7, x7)::(f8, x8)::nil)
  (at level 0, f1 at level 0, f2 at level 0, f3 at level 0, f4 at level 0, 
   f5 at level 0, f6 at level 0, f7 at level 0, f8 at level 0)
  : trm_scope.
*)






(*


Lemma Rule_op : forall op (n1 n2:int),
  Triple ((prim_op op) (val_pair (enc n1) (enc n2))) \[] (fun r => \[r = op n1 n2]).
Proof using.
  intros. applys rule_consequence. { auto. } { applys rule_op. }
  { intros r. unfold PostEnc. hsimpl~ (op n1 n2). }
Qed.

Lemma Rule_if_trm : forall t0 t1 t2 H Q0 Q,
  Triple t0 H Q0 ->
  (forall X, Triple (trm_if X t1 t2) (Q0 X) Q) ->
  Triple (trm_if_trm t0 t1 t2) H Q.
Admitted.

Lemma Rule_seq : forall t1 t2 H Q H',
  Triple t1 H (fun v => H') ->
  Triple t2 H' Q ->
  Triple (trm_seq t1 t2) H Q.
Admitted.

Lemma Rule_while : forall t1 t2 H Q,
  (forall (k:trm),
     (forall H Q, triple (trm_if_trm t1 (trm_seq t2 k) val_unit) H Q -> triple k H Q) ->
     triple k H Q) ->
  triple (trm_while t1 t2) H Q.
Admitted.
*)







(* LATER
Definition mlist_incr_val : val :=
  Fix mlist_incr [p] :=   
    _If trm_app val_neq [val_var p; val_loc null] Then (
      Let x := prim_get [val_var p; val_field hd] in
      Let q := prim_get [val_var p; val_field tl] in
      Let y := trm_app val_add [val_var x; val_int 1] in
      prim_set [val_var p; val_field hd; val_var y];;
      trm_app mlist_incr [val_var q]
    ).
(*
Lemma mlist_incr_spec : forall p L, 
  triple (trm_app mlist_incr_val [p]) 
    (p ~> MList L)
    (p ~> MList L)
*)







(** Functions *)

Parameter val_fun : var -> vars -> trm -> trm.

Notation "'Fun' f [ x1 ] ':=' K" :=
  (val_fun f (x1:nil) K)
  (at level 69, f ident, x1 ident) : trm_scope.

Notation "'Fun' f [ x1 ] ':=' K" :=
  (val_fun f (x1::nil) K)
  (at level 69, f ident, x1 ident) : trm_scope.

Notation "'Fun' f [ x1 x2 ] ':=' K" :=
  (val_fun f (x1::x2::nil) K)
  (at level 69, f ident, x1 ident, x2 ident) : trm_scope.

Notation "'Fun' f [ x1 x2 x3 ] ':=' K" :=
  (val_fun f (x1::x2::x3::nil) K)
  (at level 69, f ident, x1 ident, x2 ident, x3 ident) : trm_scope.

Notation "'Fun' f [ x1 x2 x3 x4 ] ':=' K" :=
  (val_fun f (x1::x2::x3::x4::nil) K)
  (at level 69, f ident, x1 ident, x2 ident, x3 ident, x4 ident) : trm_scope.




Notation "'_If' x 'Then' F1" :=
  (trm_if x F1 val_unit)
  (at level 69, x at level 0) : trm_scope.

Notation "x `+` y" := (trm_app val_add [x; y]) 
  (at level 69, right associativity) : trm_scope.

Notation "r `^` f" := 
  ((*trm_app*) prim_get ([ r ; val_field f ]:list val))
  (at level 69, no associativity, f at level 0,
   format "r `^` f") : trm_scope.
Notation "r `^` f `<-` v" :=
  ((*trm_app*) prim_set ([r ; val_field f ; v ]:list val))
  (at level 69, no associativity, f at level 0,
   format "r `^` f `<-` v") : trm_scope.




(*------------------------------------------------------------------*)
(* ** Definition of the CF generator *)

Definition cf_def cf (t:Trm) :=
  match t with
  | Trm_val v => local (cf_val v)
  | Trm_if v t1 t2 => local (cf_if v (cf t1) (cf t2))
  | Trm_let x t1 t2 => local (cf_let (cf t1) (fun `{EA:Enc A} (X:A) => cf (Subst_Trm x X t2)))
  | Trm_let_fix f x t1 t2 => local (cf_fix 
                                    (fun F `{EA:Enc A} (X:A) => cf (subst_Trm f F (Subst_Trm x X t1)))  
                                    (fun F => cf (subst_Trm f F t2)))
  | Trm_app f v => local (cf_app f v)
  | _ => local (cf_fail)
  end.

Definition cf := FixFun cf_def.

Ltac smath := simpl; math.
Hint Extern 1 (lt _ _) => smath.

Lemma cf_unfold : forall t,
  cf t = cf_def cf t.
Proof using.
  applys~ (FixFun_fix (measure Trm_size)). auto with wf.
  intros f1 f2 t IH. unfold measure in IH. unfold cf_def.
  destruct t; fequals.
  { rewrite~ IH. rewrite~ IH. }
  { rewrite~ IH. fequals.
    apply func_ext_dep_3. intros A1 EA1 X. 
    rewrite~ IH. unfold Subst_Trm. rewrite~ Trm_size_subst. }
  { fequals.
    { apply func_ext_dep_4. intros F A1 EA1 X.
      rewrite~ IH. unfold Subst_Trm. do 2 rewrite~ Trm_size_subst. } 
    { apply func_ext_1. intros X. rewrite~ IH. rewrite~ Trm_size_subst. } }
Qed.


(********************************************************************)
(* ** Soundness proof *)

(*------------------------------------------------------------------*)
(* ** Soundness predicate *)

Definition sound_for (t:trm) (F:formula) := 
  forall H `{EA:Enc A} (Q:A->hprop), 'F H Q -> Triple t H Q.


(*------------------------------------------------------------------*)
(* ** Soundness of local *)

Lemma local_sound : forall t (F:formula),
  sound_for t F ->
  sound_for t (local F).
Admitted.


(*------------------------------------------------------------------*)
(* ** Soundness predicate *)

Lemma cf_sound : forall (t:Trm),
  sound_for t (cf t).
Proof using.
  intros t. induction_wf: Trm_size t. 
  rewrite cf_unfold. destruct t; simpl; 
   applys local_sound; intros H A EA Q P.
  { destruct P as (V&EV&HV). applys~ Rule_val V. }

Abort.






(*------------------------------------------------------------------*)
(* ** Specialize version of [cf_fix] for small arities *)

Definition cf_fix0 (F1of : val -> formula) 
                   (F2of : val -> formula) : formula := fun H Q =>
  forall (F:val), 
  (F1of F ===> app F nil) -> 
  (F2of F) H Q.

Definition cf_fix1 (F1of : val -> val -> formula) 
                  (F2of : val -> formula) : formula := fun H Q =>
  forall (F:val), 
  (forall X1, (F1of F X1) ===> app F [X1]) -> 
  (F2of F) H Q.

(* LATER
Definition cf_fix2 (F1of : val -> val -> val -> formula) 
                  (F2of : val -> formula) : formula := fun H Q =>
  forall (F:val), 
  (forall X1 X2, (F1of F X1 X2) ===> app F [X1;X2]) -> 
  (F2of F) H Q.
*)

Lemma cf_fix_specialize_0 : forall (F1of : val -> formula) (F2of : val -> formula),
  cf_fix0 F1of F2of ===>
  cf_fix 0 F1of F2of

cf_fix0 (fun F => let E := List.combine (f::nil) (F::nil) in cf (subst_Trm E t1))  


      let G := match xs with
        | nil => 
        | x::nil => cf_fix1 (fun F X => let E := List.combine (f::x::nil) (F::X::nil) in cf (subst_Trm E t1))
        | xs => cf_fix (List.length xs) (fun F Xs => let E := List.combine (f::xs) (F::Xs) in cf (subst_Trm E t1))


Definition cf_fix (n:nat) (F1of : val -> vals -> formula) 
                  (F2of : val -> formula) : formula := fun H Q =>
Definition cf_fix0 (F1of : val -> formula) 
                   (F2of : val -> formula) : formula := fun H Q =>


