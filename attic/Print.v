
Module TESTNEWNOTATIONS.

Notation "'XPRE' H 'XPOST' Q 'XCODE' F" :=
  (@tag tag_goal _ F H Q) (at level 69,
  format "'[v' '[' 'XPRE'  H  ']'  '/' '[' 'XPOST'  Q  ']'  '/' '[' 'XCODE'  F  ']'  ']'")
  : charac.

Notation "F 'XPRE' H 'XPOST' Q" :=
  (tag tag_goal F H Q)
  (at level 69, H at level 70, only parsing) : charac.

Notation "F 'XINV' H 'XPOST' Q" :=
  (tag tag_goal F H%h (Q \*+ H%h))
  (at level 69, only parsing,
   format "'[v' F '/' '[' 'XINV'  H  ']'  '/' '[' 'XPOST'  Q  ']'  ']'")
   : charac.


Notation "F 'XPRE'' H1 'XINV'' H2 'XPOST' Q" :=
  (tag tag_goal F (H1 \* H2)%h (Q \*+ H2%h))
  (at level 69, only parsing,
   format "'[v' F '/' '[' 'XPRE''  H1  ']'  '/' '[' 'XINV''  H2  ']'  '/' '[' 'XPOST'  Q  ']'  ']'")
   : charac.

(*
Notation "F 'XINV'' H2 'XPRE'' H1 'XPOST' Q" :=
  (tag tag_goal F (H1 \* H2)%h (Q \*+ H2%h))
  (at level 69, only parsing
   (*, format "'[v' F '/' '[' 'XPRE''  H1  ']'  '/' '[' 'XINV''  H2  ']'  '/' '[' 'XPOST'  Q  ']'  ']'" *) )
   : charac.
*)

Lemma notation_pre_inv_post_spec_inv : forall (r s:loc) (n m:int),
  (app notation_pre_inv_post [r s])
    XINV (s ~~> m)
    XPRE (r ~~> n)

    XPOST (fun x => \[x = m] \* r ~~> (n+1)).
Proof using. xcf. xapp. xapp. xsimpl*. Qed.

Lemma notation_inv_post_spec_pre : forall (r:loc) (n:int),
  app notation_inv_post [r]
    XPRE (r ~~> n)
    XPOST (fun x => \[x = n] \* r ~~> n).
Proof using. xcf. xapp. Qed.

Lemma notation_inv_post_spec_inv : forall (r:loc) (n:int),
  app notation_inv_post [r]
    XINV (r ~~> n)
    XPOST \[= n].
Proof using. xcf. xapp. Qed.

Lemma notation_pre_inv_post_spec_pre : forall (r s:loc) (n m:int),
  app notation_pre_inv_post [r s]
    XPRE (r ~~> n \* s ~~> m)
    XPOST (fun x => \[x = m] \* r ~~> (n+1) \* s ~~> m).
Proof using. xcf. xapp. xapp. xsimpl*. Qed.



End TESTNEWNOTATIONS.
