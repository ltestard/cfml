

Definition p := 0%nat.

Lemma test : forall p, p = 0 -> p = Top.p.
Proof using. intros p. intros. replace Top.p with p. auto. Qed.

 coqc Test.v
File "./Test.v", line 5, characters 36-41:
Error: The reference Top.p was not found in the current environment.


