
(************************************************)
(* Functor style *)

module FunctorStyle = struct


  module type ComparableSig = sig
    type t
    val compare : t -> t -> int
  end

  module type HeapSig = sig
    type t
    type elt
    val empty : t
    val add : t -> elt -> t
  end

  module MyHeap (C : ComparableSig) : (HeapSig with type elt = C.t) =
  struct

    type elt = C.t

    type t = elt list

    let empty = []

    let rec add t x = 
      match t with
      | [] -> [x]
      | y::s -> 
         if C.compare x y <= 0 
            then x::t
            else y::(add s x)

  end
 
 (*--- fails!

  module MySort = struct

    let sort (cmp:'a->'a->int) (l:'a list) =
      let module M = MyHeap (struct type t = 'a let compare = cmp end) in
      (* push all then pop all elements from l, simplified to: *)
      M.add l (List.hd l)

  end
  ---*)


end


(* ocamlc test_encode_modules.ml *)


(************************************************)
(* Record style *)

module RecordStyle = struct

  module ComparableSig = struct
  
    type 't contents = {
      compare : 't -> 't -> int }
  
  end

  module HeapSig = struct

    type ('elt,'t) contents = {
      empty : 't;
      add : 't -> 'elt -> 't }

  end

  module MyHeap = struct
    open HeapSig
  
    type 'elt t = 'elt list

    let contents (c:'t ComparableSig.contents) : ('t,'t t) HeapSig.contents  = {
      empty = [];
      add = (let rec add t x = 
                match t with
                | [] -> [x]
                | y::s -> 
                   if c.ComparableSig.compare x y <= 0 
                      then x::t
                      else y::(add s x) 
                in
            add)
    }

  end

  module MySort = struct

    let sort (cmp:'a->'a->int) (l:'a list) =
      let m = MyHeap.contents({ ComparableSig.compare = cmp }) in 
      (* push all then pop all elements from l, simplified to: *) 
      m.HeapSig.add l (List.hd l)

  end


end

