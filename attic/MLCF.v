(** 

This file formalizes characteristic formulae for
program verification using Separation Logic,
for a practical ML-style language rather 
than for a core lambda-calculus with references.

Author: Arthur Charguéraud.
License: MIT.

*)


Set Implicit Arguments.
Require Export LibFix MLSep.
Open Scope heap_scope.


(* ********************************************************************** *)
(* * Type of a formula *)

(** A formula is a binary relation relating a pre-condition
    and a post-condition. *)

Definition formula := hprop -> (val -> hprop) -> Prop.

Global Instance Inhab_formula : Inhab formula.
Proof using. apply (prove_Inhab (fun _ _ => True)). Qed.



(* ********************************************************************** *)
(* * Characteristic formula generator *)

(* ---------------------------------------------------------------------- *)
(* ** Input language for the characteristic formula generator,
      where functions are named by a let-binding. *)

(** Extended syntax of terms, with a new construct [Trm_let_fix]
    to represent the term [let rec f x = t1 in t2] *)

Inductive Trm : Type :=
  | Trm_val : val -> Trm
  | Trm_if_val : val -> Trm -> Trm -> Trm
  | Trm_if : Trm -> Trm -> Trm -> Trm
  | Trm_seq : Trm -> Trm -> Trm
  | Trm_let : var -> Trm -> Trm -> Trm 
  | Trm_let_fix : var -> vars -> Trm -> Trm -> Trm 
  | Trm_app : val -> vals -> Trm
  | Trm_while : Trm -> Trm -> Trm.

(** Definition of capture-avoiding substitution on [Trm] *)

Fixpoint subst_Trm (E:ctx) (t : Trm) : Trm :=
  match t with
  | Trm_val v => Trm_val (subst_val E v)
  | Trm_if_val v t1 t2 => Trm_if_val (subst_val E v) (subst_Trm E t1) (subst_Trm E t2)
  | Trm_if t0 t1 t2 => Trm_if (subst_Trm E t0) (subst_Trm E t1) (subst_Trm E t2)
  | Trm_seq t1 t2 => Trm_seq (subst_Trm E t1) (subst_Trm E t2)
  | Trm_let x t1 t2 => let E' := (ctx_remove [x] E) in Trm_let x (subst_Trm E t1) (subst_Trm E' t2)
  | Trm_let_fix f xs t1 t2 => 
      let E' := ctx_remove (f::xs) E in
      let E'' := (ctx_remove [f] E) in
      Trm_let_fix f xs (subst_Trm E' t1) (subst_Trm E'' t2)
  | Trm_app v1 v2s => Trm_app (subst_val E v1) (List.map (subst_val E) v2s)
  | Trm_while t1 t2 => Trm_while (subst_Trm E t1) (subst_Trm E t2)
  end.

(** Translation from [Trm] to [trm], by encoding [Trm_let_fix]
    using [trm_let] and [val_fix]. *)

Fixpoint trm_of_Trm (t : Trm) : trm :=
  let aux := trm_of_Trm in
  match t with
  | Trm_val v => trm_val v
  | Trm_if t0 t1 t2 => trm_if (aux t0) (aux t1) (aux t2)
  | Trm_if_val v t1 t2 => trm_if (trm_val v) (aux t1) (aux t2)
  | Trm_seq t1 t2 => trm_seq (aux t1) (aux t2)
  | Trm_let x t1 t2 => trm_let x (aux t1) (aux t2)
  | Trm_let_fix f xs t1 t2 => trm_let f (trm_val (val_fix f xs (aux t1))) (aux t2)
  | Trm_while t1 t2 => trm_while (aux t1) (aux t2)
  | Trm_app v1 v2s => trm_app v1 v2s
  end.

Coercion trm_of_Trm : Trm >-> trm.

(** Reciprocal translation from [trm] to [trm]. *)

Fixpoint Trm_of_trm (t : trm) : Trm :=
  let aux := Trm_of_trm in
  match t with
  | trm_val v => Trm_val v
  | trm_if (trm_val v) t1 t2 => Trm_if_val v (aux t1) (aux t2)
  | trm_if t0 t1 t2 => Trm_if (aux t0) (aux t1) (aux t2)
  | trm_seq t1 t2 => Trm_seq (aux t1) (aux t2)
  | trm_let f ((trm_val (val_fix f' xs tf)) as t1) t2 =>
    if eq_var_dec f f' 
      then Trm_let_fix f xs (aux tf) (aux t2)
      else Trm_let f (aux t1) (aux t2)
  | trm_let x t1 t2 => Trm_let x (aux t1) (aux t2) 
  | trm_app v1 v2s => Trm_app v1 v2s
  | trm_while t1 t2 => Trm_while (aux t1) (aux t2)
  end.


(* ---------------------------------------------------------------------- *)
(** Size function used as measure for the CF generator:
    it computes the size of a term, where all values counting 
    for one unit, including closures viewed as values. *)

Fixpoint Trm_size (t:Trm) : nat :=
  match t with
  | Trm_val v => 1
  | Trm_if t0 t1 t2 => 1 + Trm_size t0 + Trm_size t1 + Trm_size t2
  | Trm_if_val v t1 t2 => 1 + Trm_size t1 + Trm_size t2
  | Trm_seq t1 t2 => 1 + Trm_size t1 + Trm_size t2
  | Trm_let x t1 t2 => 1 + Trm_size t1 + Trm_size t2
  | Trm_let_fix f xs t1 t2 => 1 + Trm_size t1 + Trm_size t2
  | Trm_app v1 v2s => 1
  | Trm_while t1 t2 => 1 + Trm_size t1 + Trm_size t2
  end.

Lemma Trm_size_subst : forall t E,
  Trm_size (subst_Trm E t) = Trm_size t.
Proof using.
  intros t. induction t; intros; simpl; repeat case_if; auto.
Qed.


(* ---------------------------------------------------------------------- *)
(** Proof that the reciprocal translation is indeed the reciprocal. *)

Section Reciprocal.

(* size function for the induction in the proof that follows *)
Fixpoint trm_size (t:trm) : nat :=
  match t with
  | trm_let x (trm_val (val_fix _ _ t1)) t2 => 3 + trm_size t1 + trm_size t2
  | trm_val v => 1
  | trm_if t0 t1 t2 => 1 + trm_size t0 + trm_size t1 + trm_size t2
  | trm_seq t1 t2 => 1 + trm_size t1 + trm_size t2
  | trm_let x t1 t2 => 1 + trm_size t1 + trm_size t2
  | trm_app v1 v2s => 1
  | trm_while t1 t2 => 1 + trm_size t1 + trm_size t2
  end.

Ltac simpl_meas := 
  match goal with |- measure _ _ _ => hnf; simpl; math end.

Ltac simpl_IH := 
  simpl; 
  match goal with H: context [trm_of_Trm (_ _) = _] |- _ =>
    repeat rewrite H;
    try simpl_meas end;
  try fequals.

Lemma trm_of_Trm_on_Trm_of_trm : forall t, 
  trm_of_Trm (Trm_of_trm t) = t.
Proof using.
  intros t. induction_wf: trm_size t. destruct t.
  { simpl_IH. }
  { simpl Trm_of_trm. destruct t1;
      try solve [ sets_eq f: Trm_of_trm; simpl_IH ]. }
  { simpl_IH. }
  { simpl. destruct t1;
      try solve [ sets_eq f: Trm_of_trm; simpl_IH ].
    destruct v0;
      try solve [ sets_eq f: Trm_of_trm; simpl_IH ]. 
    { case_if; simpl_IH. } } 
  { simpl_IH. }
  { simpl_IH. }
Qed.

End Reciprocal.


(* ---------------------------------------------------------------------- *)
(* ** Definition of the [app] predicate *)

(** The proposition [app f vs H Q] asserts that the application
    of [f] to [vs] has [H] as pre-condition and [Q] as post-condition. *)

Definition app f vs H Q :=
  triple (trm_app f vs) H Q.


(* ---------------------------------------------------------------------- *)
(* ** Definition of CF blocks *)

(** These auxiliary definitions give the characteristic formula
    associated with each term construct. *)

Definition cf_val v : formula := fun H Q =>
  H ==> Q v.

Definition cf_if_val v (F1 F2 : formula) : formula := fun H Q =>
  (v <> val_int 0 -> F1 H Q) /\ (v = val_int 0 -> F2 H Q).

Definition cf_seq (F1 : formula) (F2 : formula) : formula := fun H Q =>
  exists H', F1 H (fun v => H') /\ F2 H' Q.

Definition cf_let (F1 : formula) (F2of : val -> formula) : formula := fun H Q =>
  exists Q1, 
      F1 H Q1 
   /\ (forall (X:val), (F2of X) (Q1 X) Q).

Definition cf_if (F0 F1 F2 : formula) : formula := 
  cf_let F0 (fun X => cf_if_val X F1 F2).

Definition cf_app f vs : formula := fun H Q =>
  app f vs H Q.

Definition cf_fix (n:nat) (F1of : val -> vals -> formula) 
                  (F2of : val -> formula) : formula := fun H Q =>
  forall (F:val), 
  (forall Xs, List.length Xs = n -> (F1of F Xs) ===> app F Xs) -> 
  (F2of F) H Q.

Definition cf_fix0 (F1of : val -> formula) 
                   (F2of : val -> formula) : formula := fun H Q =>
  forall (F:val), 
  (F1of F ===> app F nil) -> 
  (F2of F) H Q.

Definition cf_fix1 (F1of : val -> val -> formula) 
                  (F2of : val -> formula) : formula := fun H Q =>
  forall (F:val), 
  (forall X1, (F1of F X1) ===> app F [X1]) -> 
  (F2of F) H Q.

(* LATER
Definition cf_fix2 (F1of : val -> val -> val -> formula) 
                  (F2of : val -> formula) : formula := fun H Q =>
  forall (F:val), 
  (forall X1 X2, (F1of F X1 X2) ===> app F [X1;X2]) -> 
  (F2of F) H Q.
*)

Definition cf_fail : formula := fun H Q =>
  False.



(* ---------------------------------------------------------------------- *)
(* ** Instance of [app] for primitive operations *)

(* LATER

Lemma app_ref : forall v,
  app prim_ref v \[] (fun r => Hexists l, \[r = val_loc l] \* l`.`f ~~> v).
Proof using. applys rule_ref. Qed.

Lemma app_get : forall v l,
  app prim_get (val_loc l) (l ~~> v) (fun x => \[x = v] \* (l ~~> v)).
Proof using. applys rule_get. Qed.

Lemma app_set : forall w l v, 
  app prim_set (val_pair (val_loc l) w) (l ~~> v) (fun r => \[r = val_unit] \* l ~~> w).
Proof using. applys rule_set. Qed.

*)


(* ---------------------------------------------------------------------- *)
(* ** Definition of the CF generator *)

(** The CF generator is a recursive function, defined using the
    optimal fixed point combinator (from TLC). [cf_def] gives the
    function, and [cf] is then defined as the fixpoint of [cf_def]. 
    Subsequently, the fixed-point equation is established. *)

Definition cf_def cf (t:Trm) :=
  match t with
  | Trm_val v => local (cf_val v)
  | Trm_if_val v t1 t2 => local (cf_if_val v (cf t1) (cf t2))
  | Trm_if t0 t1 t2 => local (cf_if (cf t0) (cf t1) (cf t2))
  | Trm_seq t1 t2 => local (cf_seq (cf t1) (cf t2))
  | Trm_let x t1 t2 => local (cf_let (cf t1) (fun X => cf (subst_Trm [(x,X)] t2)))
  | Trm_let_fix f xs t1 t2 => 
      let G := match xs with
        | nil => cf_fix0 (fun F => let E := List.combine (f::nil) (F::nil) in cf (subst_Trm E t1))  
        | x::nil => cf_fix1 (fun F X => let E := List.combine (f::x::nil) (F::X::nil) in cf (subst_Trm E t1))
        | xs => cf_fix (List.length xs) (fun F Xs => let E := List.combine (f::xs) (F::Xs) in cf (subst_Trm E t1))
        end in
      local (G (fun F => cf (subst_Trm [(f,F)] t2)))
  | Trm_app f vs => local (cf_app f vs)
  | Trm_while t1 t2 => local cf_fail (* TODO: later *)
  end.

Definition cf := FixFun cf_def.

Ltac smath := simpl; math.
Hint Extern 1 (lt _ _) => smath.

Lemma cf_unfold_iter : forall n t,
  cf t = func_iter n cf_def cf t.
Proof using.
  applys~ (FixFun_fix_iter (measure Trm_size)). auto with wf.
  intros f1 f2 t IH. unfold measure in IH. unfold cf_def.
  destruct t; fequals.
  { do 2 rewrite~ IH. }
  { do 3 rewrite~ IH. }
  { do 2 rewrite~ IH. }
  { rewrite~ IH. fequals.
    apply func_ext_1. intros X. rewrite~ IH. rewrite~ Trm_size_subst. }
  { applys func_equal_1.
    { rename v0 into xs. destruct xs as [|x [|]]; fequals.
      { applys func_ext_1. intros F. rewrite~ IH. do 2 rewrite~ Trm_size_subst. } 
      { applys func_ext_2. intros F X1. rewrite~ IH. do 2 rewrite~ Trm_size_subst. }
      { applys func_ext_2. intros F Xs. rewrite~ IH. do 2 rewrite~ Trm_size_subst. } }
    { apply func_ext_1. intros F. rewrite~ IH. rewrite~ Trm_size_subst. } }
Qed.

Lemma cf_unfold : forall t,
  cf t = cf_def cf t.
Proof using. applys (cf_unfold_iter 1). Qed.

Ltac simpl_cf :=
  rewrite cf_unfold; unfold cf_def.


(* ********************************************************************** *)
(* * Soundness proof *)

(* ---------------------------------------------------------------------- *)
(* ** Two substitution lemmas for the soundness proof *)

Hint Extern 1 (measure Trm_size _ _) => hnf; simpl; math.

(** Substitution commutes with the translation from [Trm] to [trm] *)

Lemma subst_trm_of_Trm : forall (t:Trm) (E:ctx),
  subst_trm E (trm_of_Trm t) = trm_of_Trm (subst_Trm E t).
Proof using.
  intros t. induction t; intros; simpl; auto.
  { rewrite IHt1, IHt2. auto. }
  { rewrite IHt1, IHt2, IHt3. auto. }
  { rewrite IHt1, IHt2. auto. }
  { rewrite IHt1, IHt2. auto. }
  { rewrite IHt1, IHt2. auto. }
  { rewrite IHt1, IHt2. auto. }
Qed.

(** The size of a [Trm] is preserved by substitution of 
    a variable by a value. *)

Lemma Trm_size_subst_Trm_value : forall (t:Trm) (E:ctx),
  Trm_size (subst_Trm E t) = Trm_size t.
Proof using.
  intros t. induction t; intros; simpl; auto.
Qed.


(* ---------------------------------------------------------------------- *)
(* ** Soundness of the CF generator *)

Lemma is_local_cf : forall T,
  is_local (cf T).
Proof. intros. simpl_cf. destruct T; apply is_local_local. Qed.

Definition sound_for (t:trm) (F:formula) := 
  forall H Q, F H Q -> triple t H Q.

Lemma sound_for_local : forall t (F:formula),
  sound_for t F ->
  sound_for t (local F).
Proof using.
  unfold sound_for. introv SF. intros H Q M.
  rewrite is_local_triple. applys local_weaken_body M. applys SF.
Qed.

Lemma sound_for_cf_induction : forall (t:Trm),
  sound_for t (cf t).
Proof using.
  intros t. induction_wf: Trm_size t. 
  rewrite cf_unfold. destruct t; simpl;
   applys sound_for_local; intros H Q P.
  { applys~ rule_val. }
  { destruct P as (P1&P2). applys rule_if_val. case_if. 
    { applys~ IH. }
    { applys~ IH. } }
  { destruct P as (Q1&P1&P2). applys rule_if. 
    { applys* IH. }
    { intros X. specializes P2 X. applys rule_if_val.
      destruct P2 as (P3&P4). case_if; applys~ IH. } }
  { destruct P as (H1&P1&P2). applys rule_seq H1.
    { applys~ IH. }
    { applys~ IH. } } 
  { destruct P as (Q1&P1&P2). applys rule_let Q1.
    { applys~ IH. }
    { intros X. rewrite subst_trm_of_Trm. 
      applys~ IH. hnf. rewrite~ Trm_size_subst_Trm_value. } } 
  { renames v to f, v0 to xs. applys rule_let_fix. 
    intros F HF. rewrite subst_trm_of_Trm. applys IH.
    { hnf. rewrite~ Trm_size_subst_Trm_value. }
    { destruct xs as [|x1 [|x2 xs']].
      { applys P. introv HB. applys~ HF.
        rewrite subst_trm_of_Trm. applys~ IH.
       { hnf. rewrite~ Trm_size_subst_Trm_value. } }
      { applys P. intros X. introv HB. applys~ HF.
        rewrite subst_trm_of_Trm. applys~ IH.
       { hnf. rewrite~ Trm_size_subst_Trm_value. } }
      { sets_eq xs: (x1::x2::xs'). 
        applys P. intros Xs. introv HE HB. applys~ HF. auto.
        rewrite subst_trm_of_Trm. applys~ IH.
       { hnf. rewrite~ Trm_size_subst_Trm_value. } } } }
  { applys P. }
  { hnf in P. false. (* LATER: complete *) }
Qed.

Theorem sound_for_cf_final : forall (t:Trm) H Q, 
  cf t H Q ->
  triple (trm_of_Trm t) H Q.
Proof using. intros. applys* sound_for_cf_induction. Qed.


(* ---------------------------------------------------------------------- *)
(* ** Soundness result, practical versions *)

Theorem sound_for_cf : forall (t:trm) H Q, 
  cf (Trm_of_trm t) H Q -> 
  triple t H Q.
Proof using.
  introv M. rewrite <- (@trm_of_Trm_on_Trm_of_trm t).
  applys~ sound_for_cf_final.
Qed.

Theorem sound_for_cf_app : forall n F vs (f:var) (xs:vars) (t:trm) H Q,
  F = val_fix f xs t ->
  List.length xs = List.length vs ->
  func_iter n cf_def cf (Trm_of_trm (subst_trm (List.combine (f::xs) (F::vs)) t)) H Q ->
  app F vs H Q.
Proof using. 
  introv EF EL M. rewrite <- cf_unfold_iter in M.
  applys* rule_app. applys~ sound_for_cf.
Qed.

Theorem sound_for_cf_app1 : forall n F v (f:var) (x:var) (t:trm) H Q,
  F = val_fix f [x] t ->
  func_iter n cf_def cf (Trm_of_trm (subst_trm (List.combine (f::x::nil) (F::v::nil)) t)) H Q ->
  app F [v] H Q.
Proof using. introv EF M. applys* sound_for_cf_app. Qed.



(* ********************************************************************** *)
(* * CF tactics *)

Module MLCFTactics.

Ltac xlocal_core tt ::=
  try first [ applys is_local_local | applys is_local_triple | assumption ].

(* ---------------------------------------------------------------------- *)
(* ** Notation for characteristic formulae *)

Notation "'Val' v" :=
  (local (cf_val v))
  (at level 69) : charac.

Notation "'If_' v 'Then' F1 'Else' F2" :=
  (local (cf_if_val v F1 F2))
  (at level 69, v at level 0) : charac.

Notation "'Seq_' F1 ;; F2" :=
  (local (cf_seq F1 F2))
  (at level 68, right associativity,
   format "'[v' 'Seq_'  '[' F1 ']'  ;;  '/'  '[' F2 ']' ']'") : charac.

Notation "'Let' x ':=' F1 'in' F2" :=
  (local (cf_let F1 (fun x => F2)))
  (at level 69, x ident, right associativity,
  format "'[v' '[' 'Let'  x  ':='  F1  'in' ']'  '/'  '[' F2 ']' ']'") : charac.

Notation "'App_' f x1 ;" :=
  (local (cf_app f [x1]))
  (at level 68, f at level 0, x1 at level 0) : charac.

Notation "'App_' f x1 x2 ;" :=
  (local (cf_app f [x1; x2]))
  (at level 68, f at level 0, x1 at level 0,
   x2 at level 0) : charac.

Notation "'App_' f x1 x2 x3 ;" :=
  (local (cf_app f [x1; x2; x3]))
  (at level 68, f at level 0, x1 at level 0, x2 at level 0,
   x3 at level 0) : charac.

Notation "'App_' f x1 x2 x3 x4 ;" :=
  (local (cf_app f [x1; x2; x3; x4]))
  (at level 68, f at level 0, x1 at level 0, x2 at level 0,
   x3 at level 0, x4 at level 0) : charac.

Notation "'Fail'" :=
  (local cf_fail)
  (at level 69) : charac.

Open Scope charac.


(* Capturing bound names

Definition test := local (cf_let (cf_val val_unit) (fun a => 
  local (cf_let (local (cf_val val_unit)) (fun b => cf_val (val_pair a b))))).

Lemma test' : forall H Q, test H Q. 
Proof using. 
  unfolds test. intros.
  apply local_erase.
  match goal with |- cf_let _ (fun x => _) _ _ => 
   let w := fresh x in 
   esplit; split; [ | intros w] end. 
  admit.
Qed.

*)




(*--------------------------------------------------------*)
(* * Tactics for conducting proofs *)

Tactic Notation "xcf" :=
  let f := match goal with |- app ?f _ _ _ => constr:(f) end in
  applys sound_for_cf_app1 20%nat; 
  [ try reflexivity
  | simpl; try fold f].

Tactic Notation "xval" :=
  applys local_erase; unfold cf_val.

Tactic Notation "xif" :=
  applys local_erase; split.

Tactic Notation "xlet" :=
  applys local_erase; esplit; split.

Tactic Notation "xapp" constr(E) := 
  applys local_erase; unfold cf_app; 
  xapplys E.

Tactic Notation "xapp" := 
  applys local_erase; unfold cf_app;
  let G := match goal with |- ?G => constr:(G) end in
  xspec G;
  let H := fresh "TEMP" in intros H; 
  xapplys H;
  clear H.

Tactic Notation "xfail" :=
  applys local_erase; unfold cf_fail.


(*--------------------------------------------------------*)
(* ** Registering specifications for primitive functions *)

Hint Extern 1 (RegisterSpec (app (val_prim prim_neq) _ _ _)) => 
  Provide rule_neq.

Hint Extern 1 (RegisterSpec (app (val_prim prim_add) _ _ _)) =>
  Provide rule_add.


End MLCFTactics.
