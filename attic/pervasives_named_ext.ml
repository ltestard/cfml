


(************************************************************)
(** Exception *)

external raise : exn -> 'a = "%raise"

external failwith : string -> 'a = "%failwith"


(************************************************************)
(** Type conversion *)

external magic : 'a -> 'b = "%magic"



(************************************************************)
(** Boolean *)

let not x = 
  if x then false else true



(************************************************************)
(** Physical equality *)

external physical_eq : 'a -> 'a -> bool = "%physical_eq"

let ( == ) = physical_eq

let physical_neq x y = 
  not (x == y)

let ( !== ) = physical_neq


(************************************************************)
(** Comparison *)

external compare_eq : 'a -> 'a -> bool = "%compare_eq"
external compare_neq : 'a -> 'a -> bool = "%compare_neq"
external compare_lt : 'a -> 'a -> bool = "%compare_lt"
external compare_gt : 'a -> 'a -> bool = "%compare_gt"
external compare_le : 'a -> 'a -> bool = "%compare_le"
external compare_ge : 'a -> 'a -> bool = "%compare_ge"

let ( = ) = compare_eq
let ( <> ) = compare_neq
let ( < ) = compare_lt
let ( > ) = compare_gt
let ( <= ) = compare_le
let ( >= ) = compare_ge

let min x y = 
   if x <= y then x else y

let max x y = 
  if x >= y then x else y


(************************************************************)
(** Boolean *)

external bool_and : bool -> bool -> bool = "%bool_and"
external bool_or : bool -> bool -> bool = "%bool_or"

let ( && ) = bool_and
let ( || ) = bool_or



(************************************************************)
(** Integer *)

external int_neg : int -> int = "%int_neg"
external int_add : int -> int -> int = "%int_add"
external int_sub : int -> int -> int = "%int_sub"
external int_mul : int -> int -> int = "%int_mul"
external int_div : int -> int -> int = "%int_div"
external int_mod : int -> int -> int = "%int_mod"

let ( ~- ) = int_neg
let ( + ) = int_add
let ( - ) = int_sub
let ( * ) = int_mul
let ( / ) = int_div
let ( mod ) = int_mod

let succ n =
  n + 1

let pred n =
  n - 1

let abs x = 
  if x >= 0 then x else -x


(************************************************************)
(** References *)

type 'a ref = { mutable contents : 'a }

let ref x = 
  { contents = x }

let get r = 
  r.contents

let (!) = get 

let set r x = 
  r.contents <- x

let (:=) = set 

let incr r =
  r := !r + 1

let decr r =
  r := !r - 1

(** [ref_free r] is a logical free operation, useful for translating 
   to languages without garbage collection *)

let ref_free r =
  ()

(** [ref_unsafe_set r x] allows to modifies the contents of a reference *)
(* unsupported currently, needs explicit types
let ref_unsafe_set r x =
  r.contents <- (magic x)
*)

(************************************************************)
(** Pairs *)

let fst (x,y) =
  x

let snd (x,y) = 
  y


(************************************************************)
(** Unit *)

let ignore x =
  ()

