Require Import List.

Lemma test: True.
 let x := eval compute in (List.length (3::4::nil)) in 
  pose x.
admit.





(*----
Notation "F 'XINV'' H1 'XPRE' H2 'XPOST' H3" := (F /\ H1 /\ H2 /\ H3)
  (at level 71, H1 at level 72) : test_scope.
Notation "F 'XINV' H1 'XPOST' H2" := (F /\ H1 /\ H2)
  (at level 71) : test_scope.
Notation "F 'XPRE' H1 'XPOST' H2" := (F /\ H1 /\ H2)
  (at level 73) : test_scope.

Open Scope test_scope.


Axiom test_notation2 : true XINV true XPOST true.
Axiom test_notation1 : true XINV' true XPRE true XPOST true  .
Axiom test_notation3 : true XPRE true XPOST true.

Definition F := true.
Notation " 'XINV' H1 'XPRE' H2 'XPOST' H3" := (F /\ H1 /\ H2 /\ H3)
  (at level 71) : test_scope.
Notation " 'XINV' H1 'XPOST' H2" := (F /\ H1 /\ H2)
  (at level 71) : test_scope.
Notation " 'XPRE' H1 'XPOST' H2" := (F /\ H1 /\ H2)
  (at level 70) : test_scope.

Open Scope test_scope.

Axiom test_notation1 : XINV true XPRE true XPOST true.
Axiom test_notation2 : XINV true XPOST true.
Axiom test_notation3 : XPRE true XPOST true.



Notation "'test' x 'type' t 'pre' p" := (ex (fun x:t => p))
  (at level 71)
  : test_scope.
Notation "'test' x 'pre' p" := (ex (fun x => p))
  (at level 71) : test_scope.
Notation "'pre' p" := (p)
  (at level 72) : test_scope.

Open Scope test_scope.
Axiom test_notation : test x pre x = 2.
Axiom test_notation2 : test x type int pre x = 2.
Axiom test_notation3 : pre 2 = 2.





Notation "F 'INV2' H 'POST' Q" :=
  (tag tag_goal F H%h (Q \*+ H%h))
  (at level 70,
   format "'[v' F '/' '[' 'INV2'  H  ']'  '/' '[' 'POST'  Q  ']'  ']'")
   : charac2.

Notation "F 'INV2' H1 'PRE' H2 'POST' Q" :=
  (tag tag_goal F (H2 \* H1)%h (Q \*+ H1%h))
  (at level 69,
   format "'[v' F '/' '[' 'INV2'  H1  ']'  '/' '[' 'PRE'  H2  ']'  '/' '[' 'POST'  Q  ']'  ']'")
   : charac2.

Notation "F 'PRE' H 'POST' Q" :=
  (tag tag_goal F H Q)
  (at level 69, only parsing) : charac2.


Close Scope charac.
Open Scope charac2.

Notation "'app' f xs" :=
  (@app_def f (xs)%dynlist _)  (* (@app_def f (xs)%dynlist _) *)
  (at level 80, f at level 0, xs at level 0) : charac2.
-----*)
