
(* TODO DEPRECATED

  Definition nats := list nat.

  Fixpoint nat_fresh (y:nat) (xs:nats) : bool :=
    match xs with
    | nil => true
    | x::xs' => if eq_nat_dec x y then false else nat_fresh y xs'
    end.

  Fixpoint nat_distinct (xs:nats) : bool :=
    match xs with
    | nil => true
    | x::xs' => nat_fresh x xs' && nat_distinct xs'
    end.

  (** [nat_seq i n] generates a list of variables [x1;x2;..;xn]
      with [x1=i] and [xn=i+n-1]. Such lists are useful for
      generic programming. *)

  Fixpoint nat_seq (start:nat) (nb:nat) :=
    match nb with
    | O => nil
    | S nb' => start :: nat_seq (S start) nb'
    end.

  Section Nat_seq.
  Implicit Types start nb : nat.

  Lemma var_fresh_nat_seq_lt : forall x start nb,
    (x < start)%nat ->
    nat_fresh x (nat_seq start nb).
  Proof using.
    intros. gen start. induction nb; intros.
    { auto. }
    { simpl. case_if. { math. } { applys IHnb. math. } }
  Qed.

  Lemma var_fresh_nat_seq_ge : forall x start nb,
    (x >= start+nb)%nat ->
    nat_fresh x (nat_seq start nb).
  Proof using.
    intros. gen start. induction nb; intros.
    { auto. }
    { simpl. case_if. { math. } { applys IHnb. math. } }
  Qed.

  Lemma var_distinct_nat_seq : forall start nb,
    nat_distinct (nat_seq start nb).
  Proof using. 
    intros. gen start. induction nb; intros.
    { auto. }
    { simpl. rew_istrue. split.
      { applys var_fresh_nat_seq_lt. math. }
      { auto. } }
  Qed.

  Lemma length_nat_seq : forall start nb,
    length (nat_seq start nb) = nb.
  Proof using. 
    intros. gen start. induction nb; simpl; intros.
    { auto. } { rew_list. rewrite~ IHnb. }
  Qed.

  (* TODO: convert nat to var
  Lemma var_funs_nat_seq : forall start nb,
    (nb > 0%nat)%nat ->
    var_funs nb (nat_seq start nb).
  Proof using.
    introv E. splits.
    { applys var_distinct_nat_seq. }
    { applys length_nat_seq. }
    { destruct nb. { false. math. } { simpl. auto_false. } }
  Qed.
  *)

  End Nat_seq.
*)