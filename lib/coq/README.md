#####################################################################
# Organization of the code

`Shared.v`
:    Contains general-purpose definition and tactics used in CFML.

`CFHeaps.v`
:    Contains the formalization of heaps and heap predicates. It also includes the definition of `local` and of tactics such as `hsimpl`.

`CFApp.v`
:    Contains an axiomatization of the behavior of ML function applications, as well as record operations.

`CFPrint.v`
:    Describes notation for pretty-printing characteristic formulae.

`CFHeader.v`
:    Packages the libraries used for compiling a `*_ml.v` file.

`CFTactics.v`
:    Contains the implementation of CFML tactics.

`CFLib.v`
:    Packages the libraries used for compiling a `*_proof.v` file.


#####################################################################
# For debugging

`CFDemos.v`
:    This file contains a bunch of unit tests.
