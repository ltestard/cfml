Set Implicit Arguments.
Require Import CFLibCredits.
Require Export Facts.
Open Scope R_scope.
Open Scope Z_scope.

Parameter array_make_cst : R.

Parameter array_make_cst_pos : (array_make_cst >= 0)%R.

Hint Resolve array_make_cst_pos : credits_nonneg.

Parameter ml_array_make_spec_credits : forall A,
  Spec ml_array_make (n:int) (v:A) |R>> 
    forall `{Inhab A}, n >= 0 ->
     R ($ (n * array_make_cst)) (fun t => Hexists L, t ~> Arr L \* 
     \[len L = n /\ (forall i, index n i -> L[i] = v)]).

Hint Extern 1 (RegisterSpecCredits ml_array_make) => Provide ml_array_make_spec_credits.
