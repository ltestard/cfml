Set Implicit Arguments.
Require Import CFLibCredits DynArray_ml FactsCredits.

Generalizable Variables A.




(*******************************************************************)
(** * Instantiations *)

Notation "'RecDynArray'" := (Dyn_array Id Id Id).


(*******************************************************************)
(** * Invariants *)

Definition valid_capacity capa :=
  exists (n:nat), n >= 1 /\ capa = Zpower_nat 2 n.

Record inv (A:Type) {IA:Inhab A} (L:list A) size (D:list A) capa := build_inv {
  inv_size : size = len L;
  inv_capa : capa = len D;
  inv_size_max : size <= capa;
  inv_size_min : capa / 4 < size;
  inv_capa_valid : valid_capacity capa;
  inv_items : forall i, index size i -> D[i] = L[i] }.

Definition op_cst := (4 * (array_make_cst + pay_cst))%R.

Definition potential (size:int) (capa:int) :=
  (Rabs (size - capa/2) * op_cst)%R.

Definition hinv (A:Type) {IA:Inhab A} (L:list A) size data (default:A) D capa t :=
     t ~> RecDynArray default size data 
  \* data ~> Arr D 
  \* $(potential size capa)
  \* \[inv L size D capa].

Definition DynArray (A:Type) {IA:Inhab A} (L:list A) (t:dyn_array A) :=
  Hexists size data default D capa, t ~> hinv L size data default D capa.


(*******************************************************************)
(** * Focus/unfocus lemmas *)

Lemma hinv_unfocus : forall t (A:Type) {IA:Inhab A}
                      (L:list A) size data (default:A) D capa,
  inv L size D capa ->
  (t ~> RecDynArray default size data \* data ~> Arr D)
     ==> (t ~> hinv L size data default D capa).
Proof.
  introv I. xunfold hinv. xsimpl. auto.
Admitted. (* faster *)

Implicit Arguments hinv_unfocus [ A [IA] ].

Lemma DynArray_unfocus : forall t (A:Type) {IA:Inhab A}
                      (L:list A) size data (default:A) D capa,
  (t ~> hinv L size data default D capa)
     ==> (t ~> DynArray L).
Proof.
  introv. xunfold DynArray. xsimpl.
Admitted.

Implicit Arguments DynArray_unfocus [ A [IA] ].


(*******************************************************************)
(** * Tactics *)

Ltac pot_simpl := 
  unfold potential; try simpl_credits_core; 
  repeat case_if; rew_length; try simpl_credits.

Ltac lmath :=
  try rew_length in *; math.

(* Hint Extern 10 (_ = _ :> list _) => subst; rew_list. *)
Hint Extern 1 ((_ < _)) => lmath.
Hint Extern 1 ((_ > _)) => lmath.
Hint Extern 1 ((_ >= _)) => lmath.
Hint Extern 1 ((_ <= _)) => lmath.
Hint Extern 1 ((_ < _)%Z) => lmath.
Hint Extern 1 ((_ > _)%Z) => lmath.
Hint Extern 1 ((_ >= _)%Z) => lmath.
Hint Extern 1 ((_ <= _)%Z) => lmath.


(*******************************************************************)
(** * Verification *)



Lemma length_spec : forall A,
  Spec length_ (t:dyn_array A) |B>>
    forall {IA:Inhab A} (L:list A),
    keep B (t ~> DynArray L) (fun (x:int) => \[x = len L]).
Admitted. (* faster *)

Hint Extern 1 (RegisterSpec length_) => Provide length_spec.

Section Rlemmas.
Implicit Types a b c : R.
Open Scope R_scope.

End Rlemmas.







Lemma potential_use' : forall capa new_capa s,
  (s=capa/4 \/ s=capa) -> capa > 0 ->
  new_capa = 2 * s -> 
  (potential s capa >= new_capa * (array_make_cst + pay_cst))%R.
Proof.
  introv C P E. unfold potential, op_cst.
  rewrite R_mul_assoc. simpl_ineq_mul. credits_nonneg.
  destruct C; subst.
    rew_real_int. skip_rewrite ((capa / 4)%Z = capa / 4 :> R)%R.
      skip_rewrite (capa / 4 - capa / 2 = capa / 4)%R.
      skip_rewrite (2 * (capa / 4) = capa / 2)%R.
      rewrite Rabs_pos_eq; [ | skip~: (0 <= capa / 4)%R ].
      skip_rewrite (capa / 4 * 4 = capa)%R.
      skip~: (capa / 2 <= capa)%R.
    skip_rewrite (capa - capa / 2 = capa / 2)%R.
      rewrite Rabs_pos_eq; [ | skip~: (0 <= capa / 2)%R ].
        skip_rewrite ((capa / 2) * 4 = 2 * capa)%R. rew_real_int. simpl_ineq.
Admitted.

Axiom potential_use : forall capa new_capa s,
  (s=capa/4 \/ s=capa) -> capa > 0 ->
  new_capa = 2 * s -> 
  (potential s capa >= new_capa * array_make_cst + new_capa * pay_cst)%R.




Lemma transfer_spec : forall A,
  Spec transfer src dst nb |B>>
    forall {IA:Inhab A} (L1:list A) (L2:list A),
    len L1 >= nb -> len L2 >= nb -> nb >= 0 ->
    B (src ~> Arr L1 \* dst ~> Arr L2 \* $ (nb * pay_cst))
      (# Hexists L2', src ~> Arr L1
      \* dst ~> Arr L2'
      \* \[len L2' = len L2] \* \[forall k, index nb k -> L2'[k] = L1[k]]).
Proof.
  intros. xcf. introv LL1 LL2 Nb.
  xfor. xname_post QF.
  cuts M: (forall i,
    S i (Hexists L2', \[0 <= i <= nb] \* $ ((nb-i) * pay_cst) \*
          src ~> Arr L1 \*
          dst ~> Arr L2' \*
          \[len L2' = len L2] \*
          \[(forall k, index i k -> L2'[k] = L1[k])])
        QF).
   xapply M. xsimpl. pot_simpl. math. auto. auto~. xsimpl.
   intros i. induction_wf IH: (int_upto_wf (nb+1)) i.
   hide IH. apply HS. clear HS. split. clearbody QF.  
     introv Inb. xextract as L2' Ii LL2' VL2'. xseq.
      xapps~. xapps~. show IH. xapply IH.
        auto~.
        xsimpl.
          pot_simpl. 
          introv Ik. rew_arr. case_If. subst~. auto~.
          rew_arr~.
          auto~.
       xsimpl.
     introv Inb. xret. xextract as IM L2' EL2' VL2'.
      subst QF. xsimpl. (* auto~. ==> introv G. apply~ VL2'. *)
        asserts_rewrite~ (nb = i).
        auto.
Admitted. (* faster *)

Hint Extern 1 (RegisterSpec transfer) => Provide transfer_spec.

Lemma valid_capacity_nonneg : forall capa, 
  valid_capacity capa -> (capa >= 0).
Proof.
  introv (n&F&E). subst. lets~: Zpower_NR0 2 n __.
Qed.

Hint Resolve valid_capacity_nonneg.

Lemma valid_capacity_pos : forall capa, 
  valid_capacity capa -> capa > 0.
Admitted.

Hint Resolve valid_capacity_pos.


Lemma resize_spec : forall A,
  Spec resize (t:dyn_array A) (new_capa:int) |B>>
    forall (L:list A) s d def D capa {IA:Inhab A},
    new_capa = 2 * s -> valid_capacity new_capa ->
    (s=capa/4 \/ s=capa) ->
    B (t ~> hinv L s d def D capa)
      (# Hexists d' D', t ~> hinv L s d' def D' new_capa).
Proof.
  xcf. introv Is Ic1 Im. xunfold hinv at 1. xextract as R.
  xapps. xapps.
  lets Vc: inv_capa_valid R. asserts~: (new_capa >= 0)%R. skip.
  forwards~ M: potential_use capa new_capa s. clear Vc.
  xchange (credits_ge M). clear M. credits_split.
  xapps~.
  rename _x2 into d'. intros D' (LD'&_). xapps. xapps. xapps. 
  destruct R as [Rs Ri Rf Rc Vc' M]. xapp~. 
  simpl_ineq_mul. credits_nonneg. apply R_le_Z. math.
  xextract as D'' LD'' HD''. xchange* (hinv_unfocus t); [ | xsimpl~ ].
    constructor~.
      destruct Im. math. subst s. rewrite Is. skip_rewrite (2 * capa / 4 = capa / 2). 
        skip~: (capa / 2 < capa).
      introv Ii. rewrite~ HD''.
    (* rewrite HD''; [ |auto]. rewrite~ Ri. *)
Admitted. (* faster *)

Hint Extern 1 (RegisterSpec resize) => Provide resize_spec.

Lemma pop_spec : forall A,
  Spec pop (t:dyn_array A) |B>>
    forall (L:list A) (x:A) {IA:Inhab A},
    B (t ~> DynArray (L&x)) (fun y => \[y = x] \* (t ~> DynArray L)).
Proof.
  intros. xcf. intros. xunfold DynArray. sets_eq four: 4.
  xextract as s d def D. xunfold hinv at 1. xextract as ID. 
  destruct ID as [IDs IDi IDf IDc]. rew_list in IDs. 
  xapps. xname_pre Hcur. xseq Hcur.
  xif as C.
    xfail. math.
    xrets. 
  subst Hcur. 
  auto~.
  xapps. xapps. xapps. xapps. xapps~. xapps. xapps. xapps.
  xchange~ (hinv_unfocus t L). constructor~.
    introv Isi. rewrite IDi. rew_arr.
   case_If. false. math. auto. auto~. 
  xseq  (# Hexists data' D', t ~> hinv L (s - 1) data' def D').
  xif as C.
    xapps (s - 1). auto. auto~. subst four. auto~. intros (Is'&Gs').
    xapps~.
    xsimpl. 
    xrets. intros data' D'.
  xrets. rewrite IDi. rew_arr. case_If. auto. false. math. auto~.
Admitted. (* faster *)

Hint Extern 1 (RegisterSpec pop) => Provide pop_spec.

Lemma push_spec : forall A,
  Spec push (t:dyn_array A) (x:A) |B>>
    forall {IA:Inhab A} (L:list A),
    B (t ~> DynArray L) (# t ~> DynArray (L&x)).
Proof.
  xcf. intros. xunfold DynArray. sets_eq two: 2. xextract as size data def D.
  xunfold hinv at 1. xextract. introv IL. xapps. xapps. xapps. 
  xchange~ (hinv_unfocus t L). 
  xseq (Hexists data' D', t ~> hinv L size data' def D' 
         \* \[len D' > size]). 
    lets IL2: IL. destruct IL as [ILs ILi ILf ILc]. xif. subst two.
      xapps~. xsimpl~. 
      xret. xsimpl~.
    intros t' D' I1. xunfold hinv at 1. xextract as ID'.
    destruct ID' as [IL's IL'i IL'f IL'c]. 
    xapps. xapps. xseq. xapps~. xapps. xapps.
    xchange (hinv_unfocus t); [ | xsimpl ]. constructor.
    rew_list~.
    introv Ii. rew_arr. case_If; case_If; auto~.
    rew_arr~.
    rew_arr~.
Admitted. (* faster *)

Hint Extern 1 (RegisterSpec push) => Provide push_spec.







(*
Rabs_triang: forall a b : R, (Rabs (a + b) <= Rabs a + Rabs b)%R
Rabs_triang_inv: forall a b : R, (Rabs a - Rabs b <= Rabs (a - b))%R
Rabs_triang_inv2: forall a b : R, (Rabs (Rabs a - Rabs b) <= Rabs (a - b))%R
*)
