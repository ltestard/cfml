Set Implicit Arguments.
Require Import CFLib LibListZ.



(*************************************************************************)


Generalizable Variables a A.

Parameter znth : forall `{Inhab A}, int -> list A -> A.
Parameter zreplace : forall A, int -> A -> list A -> list A.

(*
Notation "'len' L" := ((LibList.length L) : Z) 
  (at level 33).
*)

(* at level 33 -> priorité des opérateurs *)
(* TODO: use typeclass *)
Notation "'index' n i" := (0 <= i < n)%Z
  (at level 33, n at level 0).

(* at level 0 -> pour pouvoir mettre à la suite les arguments  *)

(*
Notation "L [ i ]" := (znth i L)
  (at level 9, format "L [ i ]").

Notation "L [ i := v ]" := (zreplace i v L)
  (at level 9, format "L [ i := v ]").
*)

Notation "L [ i ]" := (L\(i))
  (at level 9, format "L [ i ]").

Notation "L [ i := v ]" := (L\( i := v))
  (at level 9, format "L [ i := v ]").


Parameter ArraySeq : forall A, list A -> loc -> hprop.

Notation "'Arr'" := (ArraySeq).


(* todo: move *)

Parameter database_spec_named : Prop.

Notation "'named'" := database_spec_named.


Parameter ml_array_make_spec : forall A,
  Spec ml_array_make (n:int) (v:A) |R>> 
    forall `{Inhab A}, n >= 0 ->
     R \[] (fun t => Hexists L, t ~> Arr L \* 
     [len L = n /\ (forall i, index n i -> L[i] = v)]).

Parameter ml_array_get_spec : forall A,
  Spec ml_array_get (t:loc) (i:int) |R>> 
    forall `{Inhab A} (L:list A), index (len L) i ->
    keep R (t ~> Arr L) (fun v => [ v = L[i] ]).

Parameter ml_array_set_spec : forall A,
  Spec ml_array_set (t:loc) (i:int) (v:A) |R>> 
    forall (L:list A), index (len L) i -> 
    R (t ~> Arr L) (# t ~> Arr (L[i:=v])).

Parameter ml_array_set_named_spec : forall A,
  Spec ml_array_set (t:loc) (i:int) (v:A) |R>> 
    forall (L:list A), index (len L) i -> 
    R (t ~> Arr L) (# Hexists L', t ~> Arr L' \* [L' = L[i:=v] ]).

Parameter ml_array_length_spec : forall A,
  Spec ml_array_length (t:loc) |R>> 
   forall (L:list A),
    keep R (t ~> Arr L) (fun n => [n = len L]).

Hint Extern 1 (RegisterSpec ml_array_make) => Provide ml_array_make_spec.
Hint Extern 1 (RegisterSpec ml_array_get) => Provide ml_array_get_spec.
Hint Extern 1 (RegisterSpec ml_array_set) => Provide ml_array_set_spec.
Hint Extern 1 (Register database_spec_named ml_array_set) => Provide ml_array_set_named_spec.
Hint Extern 1 (RegisterSpec ml_array_length) => Provide ml_array_length_spec.


(* utilité des lignes ci-dessous :
quand on tape xapp, cela fait appel aux spec adéquates.
sinon il faudrait faire xapp_spec +"nom de la spec"
 *)


Parameter length_write : forall A (L:list A) i v,
  len (L[i:=v]) = len L.


Parameter read_write_eq : forall A {IA:Inhab A} i v (L:list A),
  (L[i:=v])[i] = v.


Parameter read_write_neq : forall A {IA:Inhab A} i j v (L:list A),
  i <> j -> (L[i:=v])[j] = L[j].

Parameter read_write_case : forall A {IA:Inhab A} i j v (L:list A),
  (L[i:=v])[j] = (If i = j then v else L[j]).

Parameter read_cons_case : forall A {IA:Inhab A} (i:int) v (L:list A),
  (v::L)[i] = (If i = 0 then v else L[i-1]).

Parameter read_last_case : forall A {IA:Inhab A} (i:int) v (L:list A),
  (L & v)[i] = (If i = LibList.length L then v else L[i]).


Hint Rewrite length_write read_write_eq read_write_case 
  read_write_eq read_cons_case  read_last_case : rew_array_seq.

Tactic Notation "rew_arr" :=
  autorewrite with rew_array_seq.
Tactic Notation "rew_arr" "~" :=
  rew_arr; auto_tilde.
Tactic Notation "rew_arr" "*" :=
  rew_arr; auto_star.
Tactic Notation "rew_arr" "in" hyp(H) :=
  autorewrite with rew_array_seq in H.
Tactic Notation "rew_arr" "in" "*" :=
  autorewrite with rew_array_seq in *.
Tactic Notation "rew_arrs" :=
  rew_arr in *.



(*************************************************************************)
(* TLC tactic *)

(* temp *)
Ltac extens_base ::=
  first [ extens_core | intros; extens_core ];
  rew_refl.



(*************************************************************************)
(* TLC List *)

Lemma isTrue_eq_list : forall A {IA:Inhab A} (L1 L2:list A),
  len L1 = len L2 ->
  ((forall i, index (len L1) i -> L1[i] = L2[i]) ->
  (L1 = L2)).
Proof.
  introv. xclean. gen L2. induction L1;  
   introv E M; destruct L2; rew_list in *; try math. 
  auto.
  lets N: M (0%Z) __. math. rew_arr in N. case_If. subst.
   rewrite (IHL1 L2). auto. math. introv Ei. 
   lets N: M (i+1) __. math. rew_arr in N. case_if. math.
   math_rewrite (i+1-1 = i) in N. auto.
   false. math.
Qed.


Axiom get_last : forall A (L:list A),
  L <> nil -> exists X Q, L = Q&X.
(* use last_neq_nil instead *)
Implicit Arguments get_last [A].


Lemma length_nil_eq : forall (A: Type) (L:list A),
  (len L '= 0) = (L '= nil).
Proof.
  intros. extens. iff M. apply~ length_zero_inv. math. subst L. rew_list~.
Admitted. (* faster *)
(* see length_zero_iff_nil *)

Lemma list_neq_len : forall A (L1 L2:list A),
  len L1 <> len L2 -> (L1 <> L2).
Proof.
  intros. unfold not. introv Abs. subst. auto.
Qed.
(* see length_neq_elim *)


(*************************************************************************)
(* Math *)
(*
Parameter facto : int -> int.
Parameter facto_zero : facto 0 = 1.
Parameter facto_one : facto 1 = 1.
Parameter facto_succ : forall n, n > 0 ->
  facto n = n * facto (n-1).


Lemma facto_succ' : forall (n:int), n >= 0 ->
  facto (n+1) = (n+1) * facto n.
Proof.
  introv H. rewrites (>> facto_succ (n+1)). math. 
  math_rewrite (n+1-1 = n). auto.
Qed.



Lemma facto_le_one : forall (n:int), 0 <= n <= 1 ->
  facto n = 1.
Proof.
  introv H. tests: (n = 0).
  apply facto_zero.
  tests: (n=1).
    apply facto_one.
    false. math.
Qed.

*)

(*******************************************************************)
(** * Tactics *)

Ltac auto_tilde ::= auto with maths.

Ltac auto_star ::= rew_list in *; eauto with maths.


(*******************************************************************)
(** * TEMP 

Parameter ml_div_spec : Spec ml_div x y |R>>  
  forall s, y = 2 -> x >= 2 -> x > s * 4 ->
  R \[] (fun z => [z >= s * 2 /\ z >= 1] ).

Hint Extern 1 (RegisterSpec ml_div) => Provide ml_div_spec.

Lemma time_gz : forall x, x > 0 -> x * 2 > 0.
Admitted.

*)

