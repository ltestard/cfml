Set Implicit Arguments.
Require Import CFLib Gensym_ml.
Require Import MutableList_ml MutableList_proof.


(********************************************************************)
(* ** Gensym function *)

(** Concrete specification of a Gensym function *)

Definition GensymSpec I f :=
  Spec f () |R>> forall m, R (I m) (fun x => \[x = m+1] \* (I (m+1))).

Definition Gensym (n:int) (f:func) : hprop := 
  Hexists I:int->hprop, I n \* \[GensymSpec I f].

(** Abstract specification of a Gensym function *)

Lemma Gensym_apply : forall f n,
  AppReturns f tt (f ~> Gensym n) (fun x => \[x = n+1] \* f ~> Gensym (n+1)).
Proof.
  intros. hdata_simpl Gensym. xextract as I Sf.
  xapply (spec_elim_1 Sf tt n); hsimpl~.
Qed.

(** Verification of the Gensym generator *)

Lemma make_gensym_spec : 
  Spec make_gensym () |R>> 
    R \[] (~> Gensym 0).
Proof.
  xcf. xapps. sets I: (fun n:int => r ~~> n).
  xfun (GensymSpec I). unfold I. xgo*.
  xret. hdata_simpl Gensym. hsimpl I. unfold I. hsimpl. auto.
Qed.


(********************************************************************)
(* ** Ignore function *)

Lemma ignore_spec : forall a,
  Spec ignore (x:a) |R>> 
    R \[] (#\[]).
Proof. xcf. intros. xret~. Qed.

Hint Extern 1 (RegisterSpec ignore) => Provide ignore_spec.


(********************************************************************)
(* ** Iterating calls to an imperative list of Gensym functions *)

Lemma step_all_imper_spec : 
  Spec step_all_imper (l:loc) |R>> forall L, 
    R (l ~> MList Gensym L) (# l ~> MList Gensym (LibList.map (fun i => i+1) L)).
Proof.
  xcf. intros. xfun (fun g => Spec g f |R>> forall (Q':int->hprop) H (Q:unit->hprop),
    (App f tt;) H Q' -> (forall x, Q' x ==> Q tt) -> R H Q).
    intros M W. xlet. apply M. xapp.
    hsimpl. applys pred_le_trans (W _x3). hsimpl.
    renames _f0 to g, S_f0 to Sg.
  lets K: (>> (spec_elim_2 (@miter_spec func int)) \[] \[]). xapply K; try hsimpl.
  introv In Ic. induction L; intros. apply In.
  rewrite map_cons. eapply (Ic \[]).  
    intros f. xapp (>> Gensym_apply).
      intros r. hextract. intros. apply hsimpl_to_qunit. reflexivity.
      hsimpl.
    apply IHL.
Qed.





