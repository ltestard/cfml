Set Implicit Arguments.
Require Import CFLibCredits MutableBatchedQueue_ml.

(*-------------------------------------------------------------*)


(* Specialization of the queue record representation predicate *)
Notation "'QueueRec'" := (Queue Id Id Id).

Definition potential A (b:list A): nat := length b.

Definition Queue A (L:list A) (r:queue A) :=
  Hexists (n:int) (f:list A) (b:list A),
  r ~> QueueRec n f b \* \[n = length L /\ L = f ++ rev b]
                      \* \$ (potential b).


(*-------------------------------------------------------------*)

Ltac auto_tilde ::= eauto with maths.

(* todo: merge in CFTactics? *)
Ltac xpat_post H ::=
  try solve [ discriminate | false; congruence ].


(*-------------------------------------------------------------*)
(* Should be generated *)

Lemma Queue_focus : forall A (r:queue A) (L:list A),
  r ~> Queue L ==>
  Hexists (n:int) (f:list A) (b:list A),
  r ~> QueueRec n f b \* \[n = length L /\ L = f ++ rev b] \* \$(potential b).
Proof using. intros. hunfold Queue. hsimpl~. Qed.

Lemma Queue_unfocus : forall A (r:queue A) (n:int) f b,
  n = length (f ++ rev b) ->
  r ~> QueueRec n f b \* \$(potential b) ==> r ~> Queue (f ++ rev b).
Proof using. intros. hunfold Queue. hsimpl~. Qed.

Implicit Arguments Queue_focus [A].
Implicit Arguments Queue_unfocus [A].

(*
Lemma Queue_focus : forall A (r:queue A) (L:list A),
  r ~> Queue L ==>
  Hexists (n:int) (f:list A) (b:list A),
  r ~> QueueRec n f b \* \[n = length L /\ L = f ++ rev b] .
Proof using. intros. hunfold Queue. hsimpl~. Qed.

Lemma Queue_unfocus : forall A (r:queue A) (n:int) f b,
  n = length (f ++ rev b) -> 
  r ~> QueueRec n f b ==> r ~> Queue (f ++ rev b).
Proof using. intros. hunfold Queue. hsimpl~. Qed.

Implicit Arguments Queue_focus [A].
Implicit Arguments Queue_unfocus [A].
*)

(*-------------------------------------------------------------*)
(* Arthur *)

Lemma is_empty_spec' : forall A,
  Spec is_empty (r:queue A) |B>>
    forall (L:list A),
    B (r ~> Queue L \* \$1)
      (fun b => r ~> Queue L \* \[b = isTrue (L=nil)]).
Proof using.
  xcf. intros. hunfold Queue. xextract as n f b (En&EL).
  xpay.
  repeat xapps. xret. hsimpl~. subst.
  apply eq_bool_prove; intro H; rew_refl in *; [
    apply length_zero_inv
  | rewrite H; rew_list
  ]; math.
Qed.

Lemma push_spec : forall A,
  Spec push (x:A) (r:queue A) |B>>
    forall (L:list A),
    B (r ~> Queue L \* \$2) (# r ~> Queue (L&x)).
Proof using.
  xcf. intros x q L.
  hunfold Queue. xextract as n f b (En&EL).
  xpay. csimpl.
  (* rewrite credits_eq with 2%nat (1+1)%nat. chsimpl. math. *)
  repeat xapps. hsimpl.
  { csimpl. unfold potential. rew_list~. }
  { subst. rew_list~. }
Qed.

Lemma pop_spec' : forall A,
  Spec pop (r:queue A) |B>>
    forall (L:list A),
    L <> nil ->
    B (r ~> Queue L \* \$1)
      (fun y => Hexists L', r ~> Queue L' \* \[L = y::L']).
Proof using.
  xcf. intros x L H.
  hunfold Queue at 1. xextract as n f b (En&EL).
  xpay.
  repeat xapps.
  xmatch.
  { repeat xapps. xlet as b'. xret.
    xextracts. xmatch.
    { xfail. apply H. subst. rew_list~. }
    { xgo. hchange (Queue_unfocus x).
      subst f L. lets Eb: func_eq_1 (@length A) C1.
       rew_list in *. math.
      csimpl. unfold potential. rew_list~.
      hsimpl. subst. rew_list~. } }
  { xapp. xret. hchange (Queue_unfocus x).
    subst f L. rew_list in En. rew_list. math.
    hsimpl. subst. rew_list~. }
Qed.


(*-------------------------------------------------------------*)

(* Kept for documentation or future use in other lemmas 

Lemma pot_nil : forall A, @potential A nil = 0%nat.
  intros. unfold potential. rew_list~.
Qed.

Lemma pot_cons : forall A (x: A) (L: list A),
    potential (x::L) = (1 + potential L)%nat.
  intros. unfold potential. rew_list~.
Qed.

*)