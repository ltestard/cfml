
let conjugate a = 
   let m = Array.length a in
   let b = Array.make m 0 in
   let i = ref 0 in
   let j = ref (m-1) in
   while !j >= 0 do
      while !j >= a.(!i) do
         b.(!j) <- !i;
         decr j;
      done;
      incr i;
   done;
   b
      

