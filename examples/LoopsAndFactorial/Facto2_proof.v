Set Implicit Arguments.
Require Import CFLib Facts Facto_ml.





Lemma facto_imp2 : 
  Spec facto_imp (n:int) |B>>
    n >= 0 ->
    B \[] (fun m => \[m = facto n]).
Proof.
  xcf. introv In.
  xapp. xapp. xwhile.
  asserts M: (forall vm vr, 
      vr = facto (vm - 1) -> (0 < vm <= n+1) ->
      R (m ~~> vm \* r ~~> vr) (# r ~~> facto n)).
    intros vm. induction_wf IH: (int_upto_wf (n+1)) vm.
    introv E Ivm. applys HR. xlet. xapps. xret. xextracts.
    xif. xseq. xapps. xapps. xapps. xapps. simpl. xapply (>>IH (vm+1) (vm*vr)).
       auto~.
           rewrite E. math_rewrite (vm+1 - 1 = vm). 
            rewrites~ (>> facto_succ vm).
           auto~.
           hsimpl.
           hsimpl.
        xret. hsimpl. subst vr. fequals. math. 
    xapply (>> M 1 1).
      simpl. rewrite facto_zero. auto~.
      auto~.
      hsimpl.
      xok.
   xapp. hsimpl~.   




    Admitted.















































Lemma facto_imp : 
  Spec facto_imp (n:int) |B>>
    n >= 0 ->
    B \[] (fun m => \[m = facto n]).

Proof.
  xcf. introv In.
  xapp. xapp.
  xwhile.
  asserts M: (forall vm vr, 
      vr = facto (vm - 1) -> (0 < vm <= n+1) ->
      R (m ~~> vm \* r ~~> vr) (# r ~~> facto n)).
    intros vm. induction_wf IH: (int_upto_wf (n+1)) vm.
    introv E Ivm. applys (rm HR).
      xlet. xapps. xret. simpl. xextracts. xif.
        xseq. xapps. xapps. xapps. xapps. simpl.
         (* xapply; try hsimpl *)
         xapply (>> IH (vm+1) (vm * vr)). 
           auto~.
           rewrite E. math_rewrite (vm+1 - 1 = vm). 
            rewrites~ (>> facto_succ vm).
           auto~.
           hsimpl.
           hsimpl.
        xret. hsimpl. subst vr. fequals. math. 
    xapply (>> M 1 1).
      simpl. rewrite facto_zero. auto~.
      auto~.
      hsimpl.
      xok.
   xapp. hsimpl~.   
  (* xpost (fun c => \[c = (vm '<= n)] \* r ~~> vr \* m ~~> vm). *) 
  (*  #    =    fun (_:unit) =>  *)
Qed.
*)


Lemma facto_tailrec_aux_spec : 
  Spec facto_tailrec_aux (acc:int) (i:int) (n:int) |B>>
    0 <= i <= n -> acc = facto i ->
    B \[] (fun m => \[m = facto n]).
Proof.
  skip_goal.
  xcf. introv I E. xif.
  xrets~.
  xapp_spec IH. auto~. rewrite facto_succ.
   (* subst acc. do 2 fequals. math.*)
    math_rewrite ((i + 1) - 1 = i). congruence.
    auto~.
  (* intros m. hextract as M. hsimpl. auto. *)
  hsimpl~. 
Qed.

Lemma facto_tailrec_spec :
  Spec facto_tailrec (n:int) |B>>
    n >= 0 ->
    B \[] (fun m => \[m = facto n]).
Proof.
  xcf. introv I. xapp_spec~ facto_tailrec_aux_spec.
  rewrite~ facto_zero. hsimpl~. 
Qed.



Lemma facto_rec_spec : forall n, n >= 0 ->
  (App facto_rec n;) \[] (fun m => \[m = facto n]).
Proof.
  intros n. induction_wf IH: (int_downto_wf 0) n.
  intros Gn. unfolds downto.
  xcf_app. xif.
  xrets. rewrite~ facto_le_one.
  xlet. applys~ IH. (* lets M: IH (n-1) __ __. auto~. auto~. apply M. *)
   simpl. xextracts. xrets. rewrites~ (>> facto_succ n).
Qed.

Lemma facto_rec_spec' : 
  Spec facto_rec (n:int) |B>>
    n >= 0 ->
    B \[] (fun m => \[m = facto n]).
Proof.
  (* xintros. intros n Gn. *)
  (* xinduction (int_downto_wf 0). *)
  applys (>> spec_induction_1_noheap (int_downto_wf 0)).
  xcf. introv IH Gn. unfolds downto.
  xif.
  xrets. rewrite~ facto_le_one.
  (* xlet. xapp_spec~ IH. simpl. xextracts. *)
  xapp_spec~ IH. intros K. subst.
  xrets. rewrites~ (>> facto_succ n).
Qed.



