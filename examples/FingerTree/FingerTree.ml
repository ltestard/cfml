open ReducerSig
(*-----------------------------------------------------------------------------*)

module Make (R : Reducer) = 
struct
   
(*-----------------------------------------------------------------------------*)

type meas = R.meas
type item = R.item

let combine2 m1 m2 =
  R.combine m1 m2

let combine3 m1 m2 m3 = 
  R.combine m1 (combine2 m2 m3)

let combine4 m1 m2 m3 m4 = 
  R.combine m1 (combine3 m2 m3 m4)

(* Note: the constructor "Tree_intro", "Digit_intro" and "Ftree_intro"
   are currently needed because of a limitation of my verification tool CFML. 
   This should have no incidence on peformance. *)

type tree = 
  | Tree_intro of meas * treestr

and treestr = 
  | Tree_leaf of item
  | Tree_node2 of tree * tree 
  | Tree_node3 of tree * tree * tree

type digit = 
  | Digit_intro of meas * digitstr

(* Note: Digit0 is only used during transitions *)  

and digitstr = 
  | Digit0  
  | Digit1 of tree
  | Digit2 of tree * tree
  | Digit3 of tree * tree * tree
  | Digit4 of tree * tree * tree * tree

type ftree = 
  | Ftree_intro of meas * ftreestr

and ftreestr = 
  | Ftree_empty
  | Ftree_single of tree
  | Ftree_deep of digit * ftree * digit

type t = ftree

(*---*)

let tree_cached = function
  | Tree_intro (m,_) -> m

let tree_str = function
  | Tree_intro (_,ts) -> ts

let digit_cached = function
  | Digit_intro (m,_) -> m

let digit_str = function
  | Digit_intro (_,ds) -> ds

let ftree_cached = function
  | Ftree_intro (m,_) -> m

let ftree_str = function
  | Ftree_intro (_,fs) -> fs

(*---*)

let tree_leaf x =
  Tree_intro (R.measure x, Tree_leaf x)

let tree_node2 t1 t2 =
  let s = tree_cached in
  Tree_intro (combine2 (s t1) (s t2), Tree_node2 (t1, t2))

let tree_node3 t1 t2 t3 =
  let s = tree_cached in
  Tree_intro (combine3 (s t1) (s t2) (s t3), Tree_node3 (t1, t2, t3))

let digit0 = 
  Digit_intro (R.zero, Digit0)

let digit1 t1 =
  let s = tree_cached in
  Digit_intro (s t1, Digit1(t1))

let digit2 t1 t2 =
  let s = tree_cached in
  Digit_intro (combine2 (s t1) (s t2), Digit2(t1, t2))

let digit3 t1 t2 t3 =
  let s = tree_cached in
  Digit_intro (combine3 (s t1) (s t2) (s t3), Digit3(t1, t2, t3))

let digit4 t1 t2 t3 t4 =
  let s = tree_cached in
  Digit_intro (combine4 (s t1) (s t2) (s t3) (s t4), Digit4(t1, t2, t3, t4))

let ftree_empty =
  Ftree_intro (R.zero, Ftree_empty)
 
let ftree_single t =
  Ftree_intro (tree_cached t, Ftree_single(t))

let ftree_deep dl sub dr =
  let m = combine3 (digit_cached dl) (ftree_cached sub) (digit_cached dr) in
  Ftree_intro (m, Ftree_deep(dl, sub, dr))

(*---*)

let ftree_of_digit d =
  match digit_str d with
  | Digit0 -> ftree_empty
  | Digit1 (t1) -> ftree_single t1
  | Digit2 (t1, t2) -> ftree_deep (digit1 t1) ftree_empty (digit1 t2) 
  | Digit3 (t1, t2, t3) -> ftree_deep (digit2 t1 t2) ftree_empty (digit1 t3) 
  | Digit4 (t1, t2, t3, t4) -> ftree_deep (digit2 t1 t2) ftree_empty (digit2 t3 t4) 

let digit_of_tree t = 
  match tree_str t with
  | Tree_leaf _ -> assert false
  | Tree_node2 (t1, t2) -> digit2 t1 t2
  | Tree_node3 (t1, t2, t3) -> digit3 t1 t2 t3

(*---*)

let digit_push_front t0 d = 
  match digit_str d with
  | Digit0 -> assert false
  | Digit1 (t1) -> digit2 t0 t1
  | Digit2 (t1, t2) -> digit3 t0 t1 t2
  | Digit3 (t1, t2, t3) -> digit4 t0 t1 t2 t3
  | Digit4 (_, _, _, _) -> assert false

let rec ftree_push_front t0 f =
 match ftree_str f with
   | Ftree_empty -> 
       ftree_single t0
   | Ftree_single t1 -> 
       ftree_deep (digit1 t0) ftree_empty (digit1 t1)
   | Ftree_deep (dl, fm, dr) ->
       match digit_str dl with
       | Digit4 (t1, t2, t3, t4) -> 
           let t = tree_node3 t2 t3 t4 in
           let fm2 = ftree_push_front t fm in
           let dl2 = digit2 t0 t1 in
           ftree_deep dl2 fm2 dr
       | _ -> 
           let dl2 = digit_push_front t0 dl in
           ftree_deep dl2 fm dr

let push_front x f =
  ftree_push_front (tree_leaf x) f

(*---*)

let digit_pop_front d =
  match digit_str d with
  | Digit0 -> assert false
  | Digit1 (t1) -> (t1, digit0)
  | Digit2 (t1, t2) -> (t1, digit1 t2)
  | Digit3 (t1, t2, t3) -> (t1, digit2 t2 t3)
  | Digit4 (t1, t2, t3, t4) -> (t1, digit3 t2 t3 t4)

let rec ftree_pop_front f = 
  match ftree_str f with
  | Ftree_empty -> 
      None
  | Ftree_single t1 -> 
      Some (t1, ftree_empty)
  | Ftree_deep (dl, fm, dr) ->
      let (t1, dl2) = digit_pop_front dl in
      Some (t1, ftree_fix_front dl2 fm dr)

and ftree_fix_front dl fm dr =
  match digit_str dl with
  | Digit0 ->
      begin match ftree_pop_front fm with
      | None -> 
          ftree_of_digit dr
      | Some (t2, fm2) -> 
          let dl2 = digit_of_tree t2 in
          ftree_deep dl2 fm2 dr
      end
  | _ -> ftree_deep dl fm dr

let pop_front ff =
  match ftree_pop_front ff with
  | None -> assert false
  | Some (t, ff2) -> 
     match tree_str t with
     | Tree_leaf x -> (x, ff2) 
     | _ -> assert false

(*---*)

let digit_push_back t0 d = 
  match digit_str d with
  | Digit0 -> assert false
  | Digit1 (t1) -> digit2 t1 t0
  | Digit2 (t1, t2) -> digit3 t1 t2 t0
  | Digit3 (t1, t2, t3) -> digit4 t1 t2 t3 t0
  | Digit4 (_, _, _, _) -> assert false

let rec ftree_push_back t0 f =
  match ftree_str f with
  | Ftree_empty -> 
      ftree_single t0
  | Ftree_single t1 -> 
      ftree_deep (digit1 t1) ftree_empty (digit1 t0)
  | Ftree_deep (dl, fm, dr) ->
      match digit_str dr with
      | Digit4 (t1, t2, t3, t4) -> 
          let t = tree_node3 t1 t2 t3 in
          let fm2 = ftree_push_back t fm in
          let dr2 = digit2 t4 t0 in
          ftree_deep dl fm2 dr2
      | _ -> 
          let dr2 = digit_push_back t0 dr in
          ftree_deep dl fm dr2

let push_back x f =
  ftree_push_back (tree_leaf x) f

(*---*)

let digit_pop_back d =
  match digit_str d with
  | Digit0 -> assert false
  | Digit1 (t1) -> (t1, digit0)
  | Digit2 (t1, t2) -> (t2, digit1 t1)
  | Digit3 (t1, t2, t3) -> (t3, digit2 t1 t2)
  | Digit4 (t1, t2, t3, t4) -> (t4, digit3 t1 t2 t3)

let rec ftree_pop_back f = 
  match ftree_str f with
  | Ftree_empty -> 
      None
  | Ftree_single t1 -> 
      Some (t1, ftree_empty)
  | Ftree_deep (dl, fm, dr) ->
      let (t1, dr2) = digit_pop_back dr in
      Some (t1, ftree_fix_back dl fm dr2)

and ftree_fix_back dl fm dr =
  match digit_str dr with
  | Digit0 ->
      begin match ftree_pop_back fm with
      | None -> 
          ftree_of_digit dl
      | Some (t2, fm2) -> 
          let dr2 = digit_of_tree t2 in
          ftree_deep dl fm2 dr2
      end
  | _ -> ftree_deep dl fm dr

let pop_back ff =
  match ftree_pop_back ff with
  | None -> assert false
  | Some (t, ff2) ->  
      match tree_str t with 
      | Tree_leaf x -> (x, ff2) 
      | _ -> assert false

(*---*)

let empty =
   ftree_empty

(*---*)

let is_empty ff =
   match ftree_str ff with
   | Ftree_empty -> true
   | _ -> false

(*---*)

let digit_split p i d : digit * tree * digit = 
  match digit_str d with
  | Digit0 -> assert false
  | Digit1 (t1) -> 
      (digit0, t1, digit0)
  | Digit2 (t1, t2) ->
      let i1 = combine2 i (tree_cached t1) in
      if p i1 then (digit0, t1, digit1 t2) else 
         (digit1 t1, t2, digit0)
  | Digit3 (t1, t2, t3) ->
      let i1 = combine2 i (tree_cached t1) in
      if p i1 then (digit0, t1, digit2 t2 t3) else
         let i11 = combine2 i1 (tree_cached t2) in
         if p i11 then (digit1 t1, t2, digit1 t3) else
            (digit2 t1 t2, t3, digit0)
  | Digit4 (t1, t2, t3, t4) ->
       let i1 = combine2 i (tree_cached t1) in
       if p i1 then (digit0, t1, digit3 t2 t3 t4) else 
         let i11 = combine2 i1 (tree_cached t2) in
         if p i11 then (digit1 t1, t2, digit2 t3 t4) else
            let i111 = combine2 i11 (tree_cached t3) in
            if p i111 then (digit2 t1 t2, t3, digit1 t4) else 
               (digit3 t1 t2 t3, t4, digit0)

let rec ftree_split p i f : ftree * tree * ftree =
  match ftree_str f with
  | Ftree_empty -> 
      assert false
  | Ftree_single t ->
      (ftree_empty, t, ftree_empty)
  | Ftree_deep (dl, fm, dr) ->
      let vdl = combine2 i (digit_cached dl) in
      if p vdl then
         let (dll, dlm, dlr) = digit_split p i dl in
         (ftree_of_digit dll, dlm, ftree_fix_front dlr fm dr)
      else
         let vfm = combine2 vdl (ftree_cached fm) in
         if p vfm then
           let (fml, fmm, fmr) = ftree_split p vdl fm in
           let fmmd = digit_of_tree fmm in
           let i2 = (combine2 vdl (ftree_cached fml)) in
           let (fmml, fmmm, fmmr) = digit_split p i2 fmmd in
           (ftree_fix_back dl fml fmml, fmmm, ftree_fix_front fmmr fmr dr)
         else
           let (drl, drm, drr) = digit_split p vfm dr in
           (ftree_fix_back dl fm drl, drm, ftree_of_digit drr)
  
let split p ff : ftree * R.item * ftree =   
  let (ftl, t, ftr) = ftree_split p R.zero ff in
  match tree_str t with
  | Tree_leaf x -> (ftl, x, ftr)
  | _ -> assert false

(*---*)

let merge_digit_list d acc : tree list =
  match digit_str d with
  | Digit0 -> acc
  | Digit1 (t1) -> t1::acc
  | Digit2 (t1, t2) -> t1::t2::acc
  | Digit3 (t1, t2, t3) -> t1::t2::t3::acc
  | Digit4 (t1, t2, t3, t4) -> t1::t2::t3::t4::acc

let digit_from_treestr_list ts : digit =
  match ts with
  | [] -> assert false
  | t1::[] -> digit1 t1
  | t1::t2::[] -> digit2 t1 t2
  | t1::t2::t3::[] -> digit3 t1 t2 t3
  | t1::t2::t3::t4::[] -> digit4 t1 t2 t3 t4
  | _ -> assert false

let ftree_digit_push_front d f = 
  let ts = merge_digit_list d [] in
  List.fold_right (fun t f' -> ftree_push_front t f') ts f

let ftree_digit_push_back d f =
  let ts = merge_digit_list d [] in
  List.fold_left (fun f' t -> ftree_push_back t f') f ts

let merge_digits d1 d2 d3 : digit =
  let rec aux = function
    | [] | _::[] -> assert false
    | t1::t2::[] -> [tree_node2 t1 t2]
    | t1::t2::t3::[] -> [tree_node3 t1 t2 t3]
    | t1::t2::t3::t4::[] -> [tree_node2 t1 t2; tree_node2 t3 t4]
    | t1::t2::t3::ts -> (tree_node3 t1 t2 t3)::(aux ts)
    in
  let m = merge_digit_list in
  let ts = m d1 (m d2 (m d3 [])) in
  digit_from_treestr_list (aux ts)
  
let rec merge_with_digit fl d fr : ftree = 
  match ftree_str fl, ftree_str fr with
  | Ftree_empty, _ -> 
      ftree_digit_push_front d fr
  | _, Ftree_empty -> 
      ftree_digit_push_back d fl
  | Ftree_single t, _ -> 
      ftree_push_front t (ftree_digit_push_front d fr)
  | _, Ftree_single t -> 
      ftree_push_back t (ftree_digit_push_back d fl)
  | Ftree_deep (dll, flm, dlr), Ftree_deep (drl, frm, drr) ->
      let d = merge_digits dlr d drl in
      let fm = merge_with_digit flm d frm in
      ftree_deep dll fm drr

let merge ffl ffr =
  merge_with_digit ffl digit0 ffr

end

    
