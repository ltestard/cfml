
Set Implicit Arguments. 
Require Import CFLib Perso_ml.
Require Import LibListZ Buffer.


(*--------------------------------------------------------*)


Lemma test_spec : forall n,
  (App test n;) 
    (\[])
    (fun (v:int) => \[v = n+1]).
Proof using.
  intros.
  xcf.
  xret.
  xsimpl.
  auto.
Qed.
