
type node = contents ref
and contents = MLeaf | MNode of node * int * node

let is_empty t = 
  match !t with
  | MLeaf -> true
  | MNode (t1, y, t2) -> false
 
let rec search x t = 
  match !t with
  | MLeaf -> false 
  | MNode (t1, y, t2) ->
      if x < y then search x t1
      else if x > y then search x t2
      else true

let rec insert x t = 
  match !t with
  | MLeaf -> 
      t := MNode (ref MLeaf, x, ref MLeaf) 
  | MNode (t1, y, t2) ->
      if x < y then insert x t1
      else if x > y then insert x t2
      else ()

let rec extract_max t =
  match !t with 
  | MLeaf -> assert false
  | MNode (t1, x, t2) ->
     match !t2 with
     | MLeaf -> t := !t1; x
     | _ -> extract_max t2

let rec delete x t =
  match !t with
  | MLeaf -> ()
  | MNode (t1, y, t2) ->
      if x < y then delete x t1
      else if x > y then delete x t2
      else match !t1 with
           | MLeaf -> t := !t2
           | _ -> let m = extract_max t1 in 
                  t := MNode (t1, m, t2)


