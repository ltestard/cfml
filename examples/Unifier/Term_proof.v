Set Implicit Arguments.
Require Import CFLib Term_ml.

(* The syntax of terms is defined in the ML code: see file [Term.ml]. *)

(* For the sake of concreteness, we fix a concrete definition of terms.  For
   the moment, this concrete definition involves two cases only, namely
   [TUnit] and [TArrow]. It would be desirable to abstract away and to prove
   that the unifier works for any first-order signature.  However, a naive
   attempt to do so may run into certain limitations of Coq (e.g., the
   definition of the type [tree] below cannot be accepted if the type [term]
   is abstract; Coq needs to know that [term] is strictly positive). *)

(* The type [term] is shallow: it is parameterized over the type [A] of the
   immediate sub-terms. *)

(* ---------------------------------------------------------------------------- *)

(* Term equality. *)

(* Term equality is parameterized over leaf equality. *)

Inductive term_eq (A1 A2 : Type) (leaf_eq : A1 -> A2 -> Prop) : term A1 -> term A2 -> Prop :=
| TermEqUnit:
    term_eq leaf_eq TUnit TUnit
| TermEqArrow:
    forall codomain1 domain1 codomain2 domain2,
    leaf_eq domain1 domain2 ->
    leaf_eq codomain1 codomain2 ->
    term_eq leaf_eq (TArrow domain1 codomain1) (TArrow domain2 codomain2).

Hint Constructors term_eq.

(* Term equality is reflexive, symmetric, and transitive, provided of course
   that leaf equality satisfies the same properties. *)

Lemma term_eq_reflexive:
  forall (A : Type) (leaf_eq : A -> A -> Prop),
  (forall leaf, leaf_eq leaf leaf) ->
  forall tm : term A,
  term_eq leaf_eq tm tm.
Proof.
  intros. destruct tm; eauto.
Defined. (* must be transparent *)

Lemma term_eq_symmetric:
  forall (A : Type) (leaf_eq : A -> A -> Prop),
  (forall leaf1 leaf2, leaf_eq leaf1 leaf2 -> leaf_eq leaf2 leaf1) ->
  forall tm1 tm2 : term A,
  term_eq leaf_eq tm1 tm2 ->
  term_eq leaf_eq tm2 tm1.
Proof.
  inversion 2; eauto.
Defined. (* must be transparent *)

Lemma term_eq_transitive:
  forall (A : Type) (leaf_eq : A -> A -> Prop),
  (forall leaf1 leaf2 leaf3, leaf_eq leaf1 leaf2 -> leaf_eq leaf2 leaf3 -> leaf_eq leaf1 leaf3) ->
  forall tm1 tm2 tm3 : term A,
  term_eq leaf_eq tm1 tm2 ->
  term_eq leaf_eq tm2 tm3 ->
  term_eq leaf_eq tm1 tm3.
Proof.
  inversion 2; inversion 1; eauto.
Defined. (* must be transparent *)

(* ---------------------------------------------------------------------------- *)

(* A [map] function over terms. *)

Definition map (A B : Type) (f : A -> B) (tm : term A) : term B :=
  match tm with
  | TUnit =>
      TUnit
  | TArrow domain codomain =>
      TArrow (f domain) (f codomain)
  end.

(* [map] is parametric. *)

Lemma map_parametric:
  forall
  (A1 A2 : Type) (eqA : A1 -> A2 -> Prop)
  (B1 B2 : Type) (eqB : B1 -> B2 -> Prop)
  (f1 : A1 -> B1) (f2 : A2 -> B2),
  (forall a1 a2, eqA a1 a2 -> eqB (f1 a1) (f2 a2)) ->
  forall (tm1 : term A1) (tm2 : term A2),
  term_eq eqA tm1 tm2 ->
  term_eq eqB (map f1 tm1) (map f2 tm2).
Proof.
  inversion 2; simpl; eauto.
Qed.

(* ---------------------------------------------------------------------------- *)

(* We now define the potentially infinite trees over the signature [term]. *)

CoInductive tree :=
| Tree: term tree -> tree.

(* There exists a tree. This is true, in fact, as soon as the signature is
   non-empty, i.e., there is at least one case in the definition of [term]. *)

Definition dummy_tree : tree :=
  Tree TUnit.

(* ---------------------------------------------------------------------------- *)

(* Tree equality. *)

CoInductive tree_eq : tree -> tree -> Prop :=
| TreeEq:
    forall tm1 tm2 : term tree,
    term_eq tree_eq tm1 tm2 ->
    tree_eq (Tree tm1) (Tree tm2).

(* Tree equality is reflexive, symmetric, and transitive. *)

Lemma tree_eq_reflexive:
  forall t,
  tree_eq t t.
Proof.
  cofix. destruct t. constructor. eauto using term_eq_reflexive.
Qed.

Lemma tree_eq_symmetric:
  forall t1 t2,
  tree_eq t1 t2 ->
  tree_eq t2 t1.
Proof.
  cofix. inversion 1. constructor. eauto using term_eq_symmetric.
Qed.

Lemma tree_eq_transitive:
  forall t1 t2 t3,
  tree_eq t1 t2 ->
  tree_eq t2 t3 ->
  tree_eq t1 t3.
Proof.
  cofix. inversion 1; inversion 1. constructor. eauto using term_eq_transitive.
Qed.

Hint Resolve tree_eq_reflexive.

(* ---------------------------------------------------------------------------- *)

(* A decomposition function, lemma, and tactic, in the style of Bertot and
   Castéran. *)

Definition tree_decomp (t : tree) : tree :=
  match t with
  | Tree tm => Tree tm
  end.

Lemma tree_decompose:
  forall t, t = tree_decomp t.
Proof.
  destruct t; eauto.
Qed.

Ltac tree_decompose t :=
  rewrite (tree_decompose t); simpl.

