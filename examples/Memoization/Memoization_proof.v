Set Implicit Arguments.
Require Import CFLib Memoization_ml LibSet.
Generalizable Variables A B.

(* to: fix the need for this *)

Lemma hsimpl_exists_give : forall A (x:A) H' (J:A->hprop),
  H' ==> J x -> H' ==> (heap_is_pack J).
Proof. intros. hsimpl*. Qed.



(*--------------*)

Module Type HashSigSpec.

Declare Module H : MLHashSig.
Import H.
Parameter Hash : forall A B, map A B -> H.t A B -> hprop.

Section HashSpecs.
Context (A:Type) `{Inhab B}.

Notation "'mapAB'" := (map A B).
Notation "'hashAB'" := (H.t A B).

Parameter create_spec : 
  Spec create (n:int) |R>>
    R \[] (fun h => h ~> Hash (\{}:mapAB)).

Parameter mem_spec : 
  Spec mem (h:hashAB) (x:A) |R>> 
    forall (M:mapAB),
    keep R (h ~> Hash M) (fun b => \[b = isTrue (x \indom M)]).

Parameter find_spec : 
  Spec find (h:hashAB) (x:A) |R>> 
    forall (M:mapAB), x \indom M ->
    keep R (h ~> Hash M) (fun v => \[v = M[x]]).

Parameter add_spec : 
  Spec add (h:hashAB) (x:A) (v:B) |R>> 
    forall (M:mapAB), (*x \notindom M ->*)
    R (h ~> Hash M) (# h ~> Hash (M[x:=v])).

End HashSpecs.

Hint Extern 1 (RegisterSpec create) => Provide create_spec.
Hint Extern 1 (RegisterSpec mem) => Provide mem_spec.
Hint Extern 1 (RegisterSpec find) => Provide find_spec.
Hint Extern 1 (RegisterSpec add) => Provide add_spec.

End HashSigSpec.

(*--------------*)



Module MakeSpec (H:MLHashSig) (HS:HashSigSpec with Module H:=H).

(** instantiations *)

Module Import HashMod := MLMemoFunc H.
Import HashMod.
Import HS.


Section MemoSpecs.
Context (A:Type) `{Inhab B}.
Implicit Type P : A -> B -> Prop.
Implicit Type M : map A B.


(*--------------*)


Definition functional_st P funct :=
  exists R, wf R /\ 
  forall H f x,  
  (forall x', R x' x -> App f x'; H (fun y => \[P x' y] \* H)) ->
  (App funct f x; H (fun y => \[P x y] \* H)).

Definition MemoTableSt P M :=
  forall x, x \indom M -> P x (M[x]).

Definition MemoState P h :=
  Hexists M, (h ~> Hash M) \* \[MemoTableSt P M].

Definition MemoFunc h P f :=
  forall x, App f x;
     (h ~> MemoState P) 
     (fun v => \[P x v] \* h ~> MemoState P).

Definition Memo P f :=
  Hexists h, (h ~> MemoState P) \* \[MemoFunc h P f].

(*--------------*)

Lemma memo_spec : 
  Spec memo (funct:func) |R>> 
    forall P, functional_st P funct ->
    R \[] (fun f => f ~> Memo P).
Proof.
  xcf. introv (R&WR&Hfunct). xapp_spec (@create_spec A B).
  xfun (MemoFunc h P). unfolds. intros x. induction_wf IH: WR x.  
    hdata_simpl MemoState. xextract as M HT.
    apply app_spec_1. xbody Bf. intro_subst.
    xapps. xif.
      xapp*. hsimpl*. subst*.
      xlet (fun v => Hexists M', h ~> Hash M' \* \[MemoTableSt P M'] \* \[P x v]).
        xapply Hfunct.
          introv HR. apply* IH.
          hdata_simpl MemoState. hsimpl*.
          hdata_simpl MemoState. intros v. 
           hextract as Hv M' HM'. hsimpl*.
        intros M' HM' Hv. xapp*. xret. hsimpl*.
         intros y Hd. rewrite* indom_update in Hd. 
         tests: (x = y).
           rewrite* update_read_eq.
           rewrite* update_read_neq. destruct Hd; tryfalse; autos*.         
  xret_no_gc. unfold Memo. hdata_simpl.
   applys hsimpl_exists_give h.
   hdata_simpl MemoState. hsimpl*.
   unfolds. introv K. rewrite dom_empty in K. set_in K.
Admitted. (* faster *)

(*--------------*)

End MemoSpecs.
End MakeSpec.


(*--------------

let memo func =
   let t = Hash.create 0 in
   let rec f x =
      if Hash.mem t x then begin
         Hash.find t x
      end else begin
         let v = func f x in
         Hash.add t x v;
         v
      end in
   f

*)

























