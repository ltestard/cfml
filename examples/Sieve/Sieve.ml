
(* Compute primes in the range [0,n), for n > 1 *)

(* consider all numbers, starting at twice the number *)

let sieve_very_naive n =
  let t = Array.make n true in
  t.(0) <- false;
  t.(1) <- false;
  let i = ref 2 in
  while !i < n do
    let r = ref 2 in
    while !r * !i < n do
      t.(!r * !i) <- false;
      incr r;
    done;
    incr i;
  done;
  t

(* consider all numbers, starting at the square *)

let sieve_naive n =
  let t = Array.make n true in
  t.(0) <- false;
  t.(1) <- false;
  let i = ref 2 in
  while !i < n do
    let r = ref !i in
    while !r * !i < n do
      t.(!r * !i) <- false;
      incr r;
    done;
    incr i;
  done;
  t

(* consider only prime numbers, starting at the square *)

let sieve n =
  let t = Array.make n true in
  t.(0) <- false;
  t.(1) <- false;
  let i = ref 2 in
  while !i < n do
    if t.(!i) then begin
      let r = ref !i in
      while !r * !i < n do
        t.(!r * !i) <- false;
        incr r;
      done;
    end;
    incr i;
  done;
  t

(*

let demo = 
  let n = 100000 in
  let x = Array.fold_left (fun acc v -> acc + (if v then 1 else 0)) 0 (sieve n) in
  let y = Array.fold_left (fun acc v -> acc + (if v then 1 else 0)) 0 (sieve_chains n) in
  Printf.printf "%d\n%d\n" x y



let sieve_chains n =
  let t = Array.make n true in
  t.(0) <- false;
  t.(1) <- false;
  let prev = Array.init (n+1) (fun i -> i-1) in
  let next = Array.init (n+1) (fun i -> i+1) in
  let remove k =
    assert(t.(k));
    t.(k) <- false;
    let pnext = next.(k) in
    let pprev = prev.(k) in
    next.(pprev) <- pnext;
    prev.(pnext) <- pprev;
    in
  let i = ref 2 in
  while !i < n do
    if t.(!i) then begin
      let r = ref !i in
      while !r * !i < n do
        let m = ref (!r * !i) in
        while !m < n && t.(!m) do
          remove !m;
          m := !m * !i;
        done;
        r := next.(!r);
      done;
    end;
    incr i;
  done;
  t


let show t =
  Array.iteri (fun i v -> if v then Printf.printf "%d " i) t;
  print_newline()


let demo = 
  let n = 100000000 in
  let _t = sieve_chains n in
  print_string "done"; 
  print_newline()
*)

(*


let demo =
  let n = 1000 in
  show (sieve n);
  show (sieve_chains n)
*)






(*

let sieve_bis n =
  let t = Array.make n true in
  t.(0) <- false;
  t.(1) <- false;
  let i = ref 2 in
  while !i < n do
    if t.(!i) then begin
      let r = ref !i in
      while !r * !i < n do
        if t.(!r * !i) 
          then t.(!r * !i) <- false;
        incr r;
      done;
    end;
    incr i;
  done;
  t



let sieve_linear_maybe n =
 let x = ref 0 in
  let factor = Array.init n (fun i -> i) in
  let primes = Array.make n 0 in
  let nb_primes = ref 0 in
  for p = 2 to pred n do 
	  if factor.(p) = p then begin
      primes.(!nb_primes) <- p;
      incr nb_primes;
    end;
    let m = ref 0 in
    let i = ref 0 in
    while !m < n && !i < !nb_primes && primes.(!i) <= factor.(p) do
      m := p * primes.(!i); 
      if !m < n 
				then factor.(!m) <- primes.(!i);
      incr i;
      incr x;
    done;
  done;
  Printf.printf "%f\n" (float_of_int !x /. float_of_int n);
  (* conversion: t.(k) = (k > 1 && factor.(k) = k) *)
  (* for i = 0 to pred !nb_primes do
    Printf.printf "%d " primes.(i)
  done; *)
  [||]

*)

