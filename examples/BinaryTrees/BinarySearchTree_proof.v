Set Implicit Arguments. 
Require Import CFLib BinarySearchTree_ml.




(********************************************************************)
(** Search function *)

Module Search.

(***--------------------------------------------------------***)

Inductive tree :=
  | tree_leaf : tree
  | tree_node : int -> tree -> tree -> tree.

Instance tree_Inhab : Inhab tree. 
Proof using. apply (prove_Inhab tree_leaf). Qed.

(** Representation predicate as trees
    [p ~> Tree T] *)

Fixpoint Tree (T:tree) (p:loc) : hprop :=
  match T with
  | tree_leaf => \[p = null]
  | tree_node x T1 T2 => p ~> Node Id Tree Tree x T1 T2
  end.

(** Characterization of binary search trees *)

Definition is_lt (X Y:int) := Y < X.
Definition is_gt (X Y:int) := X < Y.

Inductive stree : tree -> LibSet.set int -> Prop :=
  | stree_leaf : 
      stree tree_leaf \{}
  | stree_node : forall y T1 T2 E1 E2 E,
      stree T1 E1 ->
      stree T2 E2 -> 
      foreach (is_lt y) E1 -> 
      foreach (is_gt y) E2 ->
      E =' (\{y} \u E1 \u E2) ->
      stree (tree_node y T1 T2) E.

(** Representation predicate for binary search trees
    [p ~> Stree T] *)

Definition Stree (E:set int) p :=
  Hexists (T:tree), p ~> Tree T \* \[stree T E].


(***--------------------------------------------------------***)
(** automation *)

Hint Extern 1 (_ = _ :> LibSet.set _) => permut_simpl : set.

Ltac auto_tilde ::= eauto.
Ltac auto_star ::= try solve [ intuition (eauto with set) ].

Implicit Types E : LibSet.set int.

Lemma foreach_gt_notin : forall E X Y,
  foreach (is_gt Y) E -> lt X Y -> X \notin E.
Proof using. introv F L N. lets: (F _ N). apply* lt_slt_false. Qed.

Lemma foreach_lt_notin : forall E X Y,
  foreach (is_lt Y) E -> lt Y X -> X \notin E.
Proof using. introv F L N. lets: (F _ N). apply* lt_slt_false. Qed.


(***--------------------------------------------------------***)

(** Focus lemmas on leaves *)

Lemma unfocus_tree_leaf : 
  \[] ==> null ~> Tree tree_leaf.
Proof using. intros. simpl. hdata_simpl. hsimpl~. Qed.

Lemma focus_tree_leaf : forall (t:loc),
  t ~> Tree tree_leaf ==> \[t = null].
Proof using. intros. simpl. hdata_simpl. hsimpl~. Qed.

Lemma focus_tree_null : forall (T:tree),
  null ~> Tree T ==> \[T = tree_leaf].
Proof using.
  intros. destruct T; simpl.
  hdata_simpl. xsimpl~.
  hdata_simpl. unfold Node. hdata_simpl.
   hchanges (@heap_is_single_impl_null null).
Qed.

(** Focus lemmas on nodes *)

Notation "'Node_'" := (Node Id Id Id).

Lemma focus_tree_node : forall (t:loc) x T1 T2,
  (t ~> Tree (tree_node x T1 T2)) ==> 
  Hexists t1 t2,
  (t ~> Node_ x t1 t2) \* (t1 ~> Tree T1) \* (t2 ~> Tree T2).
Proof using. intros. simpl. hdata_simpl. hchanges (@Node_focus t). Qed.

Lemma focus_tree_not_null : forall (t:loc) (T:tree),
  t <> null -> 
  (t ~> Tree T) ==> 
  Hexists (x:int) (t1:loc) (t2:loc), Hexists T1 T2, \[T = tree_node x T1 T2] \*
    t ~> Node_ x t1 t2 \* t1 ~> Tree T1 \* t2 ~> Tree T2. 
Proof using.
  introv N. destruct T.
  hchange (@focus_tree_leaf t). hextract. 
  hchange (@focus_tree_node t). hsimpl~.
Qed.

Lemma unfocus_tree_node : forall (t:loc) x t1 t2 T1 T2,
  (t ~> Node_ x t1 t2) \* (t1 ~> Tree T1) \* (t2 ~> Tree T2) ==>
  (t ~> Tree (tree_node x T1 T2)).  
Proof using. intros. hchanges (@Node_unfocus t). Qed.

(** Unfolding lemmas for TreeList *)

Lemma focus_Stree : forall t E,
  t ~> Stree E ==>
  Hexists (T:tree), t ~> Tree T \* \[stree T E].
Proof using. intros. unfold Stree. hdata_simpl. auto. Qed.

Lemma unfocus_Stree : forall t T E,
  t ~> Tree T \* \[stree T E] ==> t ~> Stree E.
Proof using. intros. unfold Stree. hdata_simpl. hsimpl~. Qed.

Opaque Tree Stree.


(***--------------------------------------------------------***)

(** Well-founded order on trees *)

Inductive tree_sub : binary (tree) :=
  | tree_sub_1 : forall x T1 T2,
      tree_sub T1 (tree_node x T1 T2)
  | tree_sub_2 : forall x T1 T2,
      tree_sub T2 (tree_node x T1 T2).

Lemma tree_sub_wf : wf tree_sub.
Proof using.
  intros T. induction T; constructor; intros t' H; inversions~ H.
Qed.

Hint Resolve tree_sub_wf : wf.


(***--------------------------------------------------------***)
(** Verification *)

Hint Constructors tree_sub.

Lemma search_spec_ind :
  Spec search (x:int) (p:loc) |R>> 
     forall T E, stree T E ->
     R (p ~> Tree T)
       (fun b => \[b = isTrue (x \in E)] \* p ~> Tree T).
Proof using.
  xinduction_heap (tree_sub).
  xcf. intros x p T. introv IH IE. 
  xapps. xif.
  xchange focus_tree_null. xextracts. xret.
   inverts IE. hsimpl. hchanges unfocus_tree_leaf.
   fold_bool; rew_refl. intros H. set_in H.
  xchange~ (@focus_tree_not_null p). 
   xextract as y p1 p2 T1 T2 EQ. subst T.
   inverts IE. xapps. xif.
  xapps. xapp*. intros b. 
   hchange (@unfocus_tree_node p). hsimpl.
   subst b. extens; rew_refl. subst E. iff M.
    eauto. 
    set_in M.
      math.
      auto.
      false* foreach_gt_notin H6 C0.
  xif. xapps. xapps*. intros b.
    hchange (@unfocus_tree_node p). hsimpl.
   subst b. extens; rew_refl. subst E. iff M.
    rewrite <- for_set_union_empty_r.
     repeat rewrite <- for_set_union_assoc.
     apply in_union_get_3. assumption.
    set_in M.
      math.
      lets: foreach_lt_notin x H4 __. math. false.
      auto.
  xret. 
    hchange (@unfocus_tree_node p). hsimpl. subst E. 
    asserts_rewrite (x = y). math. auto.
Qed.

Lemma search_spec :
  Spec search (x:int) (p:loc) |R>> 
     forall E,
     R (p ~> Stree E)
       (fun b => \[b = isTrue (x \in E)] \* p ~> Stree E).
Proof using.
  xweaken search_spec_ind. simpl. introv M S1. intros.
  xchange (@focus_Stree x2). xextract as T1 R1.
  xapply S1. eauto. hsimpl. intros. hextracts. 
  hchange* (@unfocus_Stree x2). hsimpl*.
Qed.


End Search.



(********************************************************************)
(** Normal version *)

Module Normal.

(***--------------------------------------------------------***)
(** Representation predicate *)

(** Functional model of the tree data structure *)

Inductive tree :=
  | tree_leaf : tree
  | tree_node : int -> tree -> tree -> tree
  | tree_hole : loc -> tree.

Instance tree_Inhab : Inhab tree. 
Proof using. apply (prove_Inhab tree_leaf). Qed.

(** Representation predicate as trees with holes
    [t ~> Tree T] *)

Fixpoint Tree (T:tree) (t:loc) : hprop :=
  match T with
  | tree_leaf => \[t = null]
  | tree_node x T1 T2 => t ~> Node Id Tree Tree x T1 T2
  | tree_hole l => \[t = l]
  end.

(** [no_hole_in T] asserts that [T] contains no hole. *)

Inductive no_hole_in : tree -> Prop :=
  | no_hole_in_leaf : no_hole_in tree_leaf
  | no_hole_in_node : forall x T1 T2, 
      no_hole_in T1 -> no_hole_in T2 -> no_hole_in (tree_node x T1 T2).

(** [fringe T] computes the sequence of values stored in [T]. *)

Fixpoint fringe T :=
  match T with
  | tree_leaf => nil
  | tree_hole l => arbitrary
  | tree_node x T1 T2 => (fringe T1) ++ (x::nil) ++ (fringe T2)
  end.

(** Representation predicate as sequence, for [t ~> TreeList L] *)

Definition TreeList (L:list int) t :=
  Hexists (T:tree), t ~> Tree T \* \[no_hole_in T] \* \[L = fringe T].
  

(***--------------------------------------------------------***)
(** -- All this section should be generated material -- *)

(** Well-founded order on trees *)

Inductive tree_sub : binary (tree) :=
  | tree_sub_1 : forall x T1 T2,
      tree_sub T1 (tree_node x T1 T2)
  | tree_sub_2 : forall x T1 T2,
      tree_sub T2 (tree_node x T1 T2).

Lemma tree_sub_wf : wf tree_sub.
Proof using.
  intros T. induction T; constructor; intros t' H; inversions~ H.
Qed.

Hint Resolve tree_sub_wf : wf.

(** Characterization of non hole nodes *)

Definition not_hole T :=
  forall l, T <> tree_hole l.

Lemma no_hole_in_not_hole : forall T, 
 no_hole_in T -> not_hole T.
Proof using. introv H. unfolds. inverts H; congruence. Qed.

Hint Resolve no_hole_in_not_hole.

(** Focus lemmas on holes *)

Lemma focus_tree_hole : forall (t:loc) l,
  t ~> Tree (tree_hole l) ==> \[t = l].
Proof using. intros. simpl. hdata_simpl. hsimpl~. Qed.

Lemma unfocus_tree_hole : forall l,
  \[] ==> l ~> Tree (tree_hole l).
Proof using. intros. simpl. hdata_simpl. hsimpl~. Qed.

(** Focus lemmas on leaves *)

Lemma unfocus_tree_leaf : 
  \[] ==> null ~> Tree tree_leaf.
Proof using. intros. simpl. hdata_simpl. hsimpl~. Qed.

Lemma focus_tree_leaf : forall (t:loc),
  t ~> Tree tree_leaf ==> \[t = null].
Proof using. intros. simpl. hdata_simpl. hsimpl~. Qed.

Lemma focus_tree_null : forall (T:tree),
  not_hole T ->
  null ~> Tree T ==> \[T = tree_leaf].
Proof using.
  introv N. destruct T; simpl.
  hdata_simpl. xsimpl~.
  hdata_simpl. unfold Node. hdata_simpl.
   hchanges (@heap_is_single_impl_null null).
  hdata_simpl. hextract. subst. false* N.
Qed.

(** Focus lemmas on nodes *)

Notation "'Node_'" := (Node Id Id Id).

Lemma focus_tree_node : forall (t:loc) x T1 T2,
  (t ~> Tree (tree_node x T1 T2)) ==> 
  Hexists t1 t2,
  (t ~> Node_ x t1 t2) \* (t1 ~> Tree T1) \* (t2 ~> Tree T2).
Proof using. intros. simpl. hdata_simpl. hchanges (@Node_focus t). Qed.

Lemma focus_tree_not_null : forall (t:loc) (T:tree),
  t <> null -> not_hole T ->
  (t ~> Tree T) ==> 
  Hexists (x:int) (t1:loc) (t2:loc), Hexists T1 T2, \[T = tree_node x T1 T2] \*
    t ~> Node_ x t1 t2 \* t1 ~> Tree T1 \* t2 ~> Tree T2. 
Proof using.
  introv N M. destruct T.
  hchange (@focus_tree_leaf t). hextract. 
  hchange (@focus_tree_node t). hsimpl~.
  false* M.
Qed.

Lemma unfocus_tree_node : forall (t:loc) x t1 t2 T1 T2,
  (t ~> Node_ x t1 t2) \* (t1 ~> Tree T1) \* (t2 ~> Tree T2) ==>
  (t ~> Tree (tree_node x T1 T2)).  
Proof using. intros. hchanges (@Node_unfocus t). Qed.

(** Unfolding lemmas for TreeList *)

Lemma focus_TreeList : forall t (L:list int),
  t ~> TreeList L ==>
  Hexists (T:tree), t ~> Tree T \* \[no_hole_in T] \* \[L = fringe T].
Proof using. intros. unfold TreeList. hdata_simpl. auto. Qed.

Lemma unfocus_TreeList : forall t T,
  t ~> Tree T \* \[no_hole_in T] ==> t ~> TreeList (fringe T).
Proof using. intros. unfold TreeList. hdata_simpl. hsimpl~. Qed.

Opaque Tree.


(***--------------------------------------------------------***)
(** Auxiliary definitions *)

(** [hole_at l T] asserts that tree [T] contains a hole on the
    leftmost path, at a node of location [l]. *)

Inductive lhole_at : loc -> tree -> Prop := 
  | lhole_at_hole : forall l,
      lhole_at l (tree_hole l) 
  | lhole_at_node : forall l T1 T2 x,
      lhole_at l T1 ->
      no_hole_in T2 ->
      lhole_at l (tree_node x T1 T2).

(** [lplug Th Tc] returns the result of filling the hole in [Tc],
    which is assumed to satisfy [lhole_at], with a subtree [Th]. *)

Fixpoint lplug Th Tc :=
  match Tc with
  | tree_leaf => arbitrary
  | tree_hole l => Th
  | tree_node x T1 T2 => tree_node x (lplug Th T1) T2
  end.

(** Auxiliary definition describing the portion of subtree
    being manipulated during loop iterations. *)

Definition lview lx ly lz x y tx ty Tx Ty Tb :=
  lx ~> Node_ x ly tx \* tx ~> Tree Tx \* 
  ly ~> Node_ y lz ty \* ty ~> Tree Ty \*
  lz ~> Tree Tb \* \[no_hole_in Tx] \* \[no_hole_in Ty].

(** Auxiliary definition describing the content of local variables
    during the loop iteration *)

Definition lvars p c b (lx ly lz:loc) :=
  p ~~> lx \* c ~~> ly \* b ~~> lz. 


(***--------------------------------------------------------***)
(** Auxiliary lemmas *)

Hint Constructors tree_sub no_hole_in lhole_at.   

(** Two useful facts about lists *)

Lemma cons_mid_not_nil : forall A (x:A) l1 l2, 
  l1 ++ x :: l2 <> nil.
Proof using. intros. induction l1; rew_list; congruence. Qed.

Lemma cons_eq_app_left_not_nil : forall A (x:A) l l1 l2,
  x::l = l1 ++ l2 -> l1 <> nil ->
  exists l', x::l' = l1 /\ l = l'++l2.
Proof using. introv E N. destruct l1; tryfalse. rew_list in E. inverts* E. Qed.

(** Lemma showing how to merge back a hole into the tree where
    it belongs. *)

Lemma lplug_glue : forall l t Tc Th,
  lhole_at l Tc -> no_hole_in Th ->
  (t ~> Tree Tc) \* (l ~> Tree Th) ==>
  (t ~> Tree (lplug Th Tc)) \* \[no_hole_in (lplug Th Tc)].
Proof using.
  introv H N. gen t. induction H; introv.
  hchange (@focus_tree_hole t). hextracts. hsimpl. simple*.
  hchange (@focus_tree_node t). hextract as t1 t2. simpl.
   hchange (@IHlhole_at t1). hchange (@unfocus_tree_node t).
   hsimpl. simple*.
Qed.

(** Lemma showing that the manipulation of the subtrees in
    the hole extracts exactly the head item from the fringe. *)

Lemma lplug_fringe : forall x y Ta Tb T1 T2 L m T' l,
  Ta = tree_node x (tree_node y tree_leaf T1) T2 ->
  Tb = tree_node x T1 T2 ->
  lhole_at l T' -> no_hole_in T1 -> no_hole_in T2 ->
  m::L = fringe (lplug Ta T') ->
  y = m /\ L = fringe (lplug Tb T').
Proof using.
  introv E1 E2 H H1 H2. gen L. induction H; introv E; simpls.
  subst Ta Tb. simpls. rew_list in E. inverts E. rew_list. splits~.
  rew_list in E.
   asserts N: (fringe (lplug Ta T0) <> nil).
     subst Ta. inverts H. 
       simpl. rew_list. congruence.
       simpl. apply cons_mid_not_nil.
   forwards~ (L'&EQ'1&EQ'2): cons_eq_app_left_not_nil E.
   forwards* (IHF&IHE): IHlhole_at. subst~.
Qed.


(***--------------------------------------------------------***)
(** Verification *)

(* forall m L t, premise ->
  App tree_delete_min t (t ~> TreeList (m::L))
       (fun p => Hexists t', [p = (t',m)] \* (t' ~> TreeList L)).

   (fun p => let '(t',m') := p in [m
*)

Lemma tree_delete_min_clean_spec :
  Spec tree_delete_min_clean (t:loc) |R>> 
     forall m L,
     R (t ~> TreeList (m::L))
       (fun p => Hexists t', \[p = (t',m)] \* (t' ~> TreeList L)).
Proof using.
  xcf. introv. xchange (@focus_TreeList t). xextract as T N E.
  asserts (x&T1&Tx&?): (exists x T1 Tx, T = tree_node x T1 Tx).
    destruct T; simpls; eauto; false_invert. subst T.
  xchange (@focus_tree_node t). xextract as ly tx.
  xapps. xapps. xif.
  (* Case left null *)
  inverts N. xchange~ focus_tree_null. xextracts. simpls.
  rew_list in E. inverts E. xapps. xapps. xret.
  hchange* (@unfocus_TreeList tx). hsimpl~ tx. 
  (* Case left not null *)
  inverts N as Ny Nx. xchange~ (@focus_tree_not_null ly). 
  xextract as y lz ty Tb Ty EB. subst T1. inverts Ny as Nb Ny.
  xapps. xapps. xapps. xapps.
  (* Loop invariant *)
  xwhile. xgeneralize (forall Tb lr T, 
     R (Hexists ly lz tx ty, Hexists (Tx Ty:tree), Hexists x y,
         \[T = tree_node x (tree_node y Tb Ty) Tx] \* \[no_hole_in Tb] \* 
         lvars p c b lr ly lz \* lview lr ly lz x y tx ty Tx Ty Tb)
       (# Hexists lx ly tx ty, Hexists (T' Tx Ty:tree), Hexists x y,
         \[lhole_at lx T'] \* \[T = lplug (tree_node x (tree_node y tree_leaf Ty) Tx) T'] \*
         lr ~> Tree T' \* lvars p c b lx ly null \* 
         lview lx ly null x y tx ty Tx Ty tree_leaf)).
   (* Before loop *)
   applys Inv Tb t. unfold lview, lvars. 
   hsimpl~ (>> ly lz tx ty Tx Ty Tb x y). 
   (* Inside loop *)
   clears_but LR HR. intros Tb. induction_wf IH: tree_sub_wf Tb. hide IH.
   intros. xextract. intros ly lz tx ty Tx Ty x y E N. applys (rm HR).
   unfold lvars. xapps. intro_subst. xapp. xextracts. xif.
   (* Body of loop *)
   unfold lview. xextract as Nx Ny. xextract.
   xchange~ (@focus_tree_not_null lz). xextract as z lw tz Tw Tz EB.
   subst Tb. xapps. intro_subst. xapps. xapps. xapps. xapps. 
   xapps. xapps. inverts N as Nw Nz.
   show IH. xapply (>> (rm IH) Tw ly (tree_node y (tree_node z Tw Tz) Ty)).
     auto.
     unfold lvars, lview. hsimpl~ (>> lz lw ty tz Ty Tz y z). 
     unfold lvars, lview. hextract. intros lx' ly' tx' ty' T' Tx' Ty' x' y' ? EQ ? ?.
      intros. hchange (@unfocus_tree_node lr x ly tx T' Tx). hsimpl.
      hsimpl~ (>> lx' ly' tx' ty' (tree_node x T' Tx) Tx' Ty' x' y').
      subst T. rewrite EQ. simple~.
   (* Exiting loop *)
   unfold lview. xret. hextract as Nx Ny. hchange~ focus_tree_null.
   hchange (@unfocus_tree_hole lr). hchange unfocus_tree_leaf.
   subst T Tb. hsimpl~ (>> lr ly tx ty (tree_hole lr) Tx Ty x y).
   (* After loop *)
   sets_eq T EQT: (tree_node x (tree_node y Tb Ty) Tx).
   clear EQT. clears_but E. xextract. intros lx ly tx ty T' Tx Ty x y H EQ.
   unfold lview, lvars. xextract as Nx Ny.
   xchange (@focus_tree_leaf null). xextract as _.
   xapps. xapps. xapps. xapps. xapps. xapps. xret.
   hchange (@unfocus_tree_node lx). hchange~ (@lplug_glue lx t T'). 
   subst T. hextract. intros. forwards* (F&M): lplug_fringe E. 
   hchange* (@unfocus_TreeList t). subst. hsimpl~ t.
Qed.

End Normal.



(********************************************************************)
(********************************************************************)
(** Simple version *)

Module Simple.

(***--------------------------------------------------------***)
(** Model of imperative trees: functional trees *)

Inductive tree :=
  | tree_leaf : tree
  | tree_node : int -> tree -> tree -> tree.

(** Representation predicate form mutable trees *)

Fixpoint Tree (T:tree) (t:loc) : hprop :=
  match T with
  | tree_leaf => \[t = null]
  | tree_node x T1 T2 => t ~> Node Id Tree Tree x T1 T2
  end.

Fixpoint tree_list (T:tree) : list int :=
  match T with
  | tree_leaf => nil
  | tree_node x T1 T2 => (tree_list T1) ++ (x::nil) ++ (tree_list T2)
  end.

Definition TreeList (L:list int) t :=
  Hexists (T:tree), t ~> Tree T \* \[L = tree_list T].
  

(***--------------------------------------------------------***)
(** -- All this section should be generated material -- *)

(** Well-founded order on trees *)

Inductive tree_sub : binary (tree) :=
  | tree_sub_1 : forall x T1 T2,
      tree_sub T1 (tree_node x T1 T2)
  | tree_sub_2 : forall x T1 T2,
      tree_sub T2 (tree_node x T1 T2).

Lemma tree_sub_wf : wf tree_sub.
Proof using.
  intros T. induction T; constructor; intros t' H; inversions~ H.
Qed.

Hint Resolve tree_sub_wf : wf.

(** Lemmas about tree_list *)

Lemma tree_not_empty_not_null : forall T,
  tree_list T <> nil -> T <> tree_leaf.
Proof using.
  introv N. destruct T.
  simpls. false.
  congruence.
Qed.

(** Focus lemmas on leaves *)

Lemma unfocus_tree_leaf : 
  \[] ==> null ~> Tree tree_leaf.
Proof using. intros. simpl. hdata_simpl. hsimpl~. Qed.

Lemma focus_tree_leaf : forall (t:loc),
  t ~> Tree tree_leaf ==> \[t = null].
Proof using. intros. simpl. hdata_simpl. hsimpl~. Qed.

Lemma focus_tree_null : forall (T:tree),
  null ~> Tree T ==> \[T = tree_leaf].
Proof using.
  intros. destruct T; simpl.
  hdata_simpl. xsimpl~.
  hdata_simpl. unfold Node. hdata_simpl.
  hchanges (@heap_is_single_impl_null null).
Qed.

(** Focus lemmas on nodes *)

Notation "'Node_'" := (Node Id Id Id).

Lemma focus_tree_node : forall (t:loc) x T1 T2,
  (t ~> Tree (tree_node x T1 T2)) ==> 
  Hexists t1 t2,
  (t ~> Node_ x t1 t2) \* (t1 ~> Tree T1) \* (t2 ~> Tree T2).
Proof using.
skip.
Qed.

Lemma focus_tree_node' : forall (t:loc) (T:tree),
  t <> null -> 
  (t ~> Tree T) ==> 
  Hexists (x:int) (t1:loc) (t2:loc), Hexists T1 T2, \[T = tree_node x T1 T2] \*
    t ~> Node_ x t1 t2 \* t1 ~> Tree T1 \* t2 ~> Tree T2. 
Proof using.
skip.
Qed.

Lemma unfocus_tree_node : forall (t:loc) x t1 t2 T1 T2,
  (t ~> Node_ x t1 t2) \* (t1 ~> Tree T1) \* (t2 ~> Tree T2) ==>
  (t ~> Tree (tree_node x T1 T2)).  
Proof using.
skip.
Qed.

(** Unfolding lemmas for TreeList *)

Lemma focus_TreeList : forall t (L:list int),
  t ~> TreeList L ==>
  Hexists (T:tree), t ~> Tree T \* \[L = tree_list T].
Proof using. 
  intros. unfold TreeList. hdata_simpl. auto.
Qed.
  
Lemma unfocus_TreeList : forall t T,
  t ~> Tree T ==> t ~> TreeList (tree_list T).
Proof using. 
  intros. unfold TreeList. hdata_simpl. hsimpl~.
Qed.

Opaque Tree.


(***--------------------------------------------------------***)
(** Key Lemma about lists *)

Lemma cons_mid_not_nil : forall A (x:A) l1 l2, 
  l1 ++ x :: l2 <> nil.
Proof using. intros. induction l1; rew_list; congruence. Qed.

Lemma fringe_decompose : forall A (x:A) l l1 l2,
  x::l = l1 ++ l2 -> l1 <> nil ->
  exists l', x::l' = l1 /\ l = l'++l2.
Proof using. introv E N. destruct l1; tryfalse. rew_list in E. inverts* E. Qed.


(***--------------------------------------------------------***)
(** Verification *)

Lemma tree_delete_min_simple_spec :
  Spec tree_delete_min_simple (t:loc) |R>> 
     forall m L,
     R (t ~> TreeList (m::L))
       (fun p => Hexists t', \[p = (t',m)] \* (t' ~> TreeList L)).
Proof using.
  xcf. introv.
  xchange (@focus_TreeList t). xextract as T E.
  forwards N: tree_not_empty_not_null T. congruence.
  destruct T as [|x T1 T2]; tryfalse. clear N.
  xchange (@focus_tree_node t). xextract as t1 t2.
  xapps. xapps. xif.
  (* Case left null *)
  xchange focus_tree_null. xextracts. simpl in E.
  rew_list in E. inverts E.
  xapps. xapps. xret.
  hchange (@unfocus_TreeList t2). hsimpl~ t2. 
  (* Case left not null *)  
  xfun_induction_heap (fun f => Spec f p c b |R>> 
    forall Tb x y tp tc Tp Tc m L,
    m::L = tree_list Tb ++ y :: tree_list Tc ++ x :: tree_list Tp ->
    R ((p ~> Node_ x c tp) \* (c ~> Node_ y b tc) 
       \* (tp ~> Tree Tp) \* (tc ~> Tree Tc) \* (b ~> Tree Tb)) 
      (fun r => Hexists Tf, p ~> Tree Tf \* \[L = tree_list Tf] \* \[r = m])
    ) (unproj41 loc loc loc tree_sub).
    (* Body of the loop *)
    clears_all. rename x0 into Tb. introv IH EQ. xapps. xif.
    (* Body: case null *)
    clear IH. xchange focus_tree_null. xextracts. inverts EQ.
    xapps. xapps. xgc. xapps. intros r. hextracts. 
    hchange (@unfocus_tree_node p). hsimpl~.
    (* Body: case not null *)
    xchange~ (@focus_tree_node' b). xextract as z tb1 tb2 Tb1 Tb2 EB.
    xapps. subst Tb. simpl in EQ. rew_list in EQ.
    forwards (L'&EQ'1&EQ'2): fringe_decompose m L (tree_list Tb1 ++ z :: tree_list Tb2 ++ y :: tree_list Tc).
      rewrite EQ. rew_list. eauto. apply cons_mid_not_nil. clear EQ.
    subst L. xapp (>> y z tc tb2 EQ'1). constructor. 
    intros r. hextract as Tf E1 E2. subst.
    hchange (@unfocus_tree_node p). hsimpl~.
    (* Main call to the loop *)
    xchange~ (@focus_tree_node' t1). xextract as y b c Tb Tc EB.
    subst T1. simpl in E. rew_list in E.
    xapps. xapp* (>> x y). intros. subst. xret.
    hchange (@unfocus_TreeList t). hsimpl~ t.
Qed.

End Simple.



