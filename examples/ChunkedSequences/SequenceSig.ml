
module type S = 
sig
   type 'a t 
   val create : unit -> 'a t
   val create_def : 'a -> 'a t
   val is_empty : 'a t -> bool
   val length : 'a t -> int
   val back : 'a t -> 'a
   val front : 'a t -> 'a 
   val push_front :'a -> 'a t -> unit
   val pop_front : 'a t -> 'a
   val push_back : 'a -> 'a t -> unit
   val pop_back : 'a t -> 'a
   val append : 'a t -> 'a t -> unit
   val carve_back_at : int -> 'a t -> 'a * 'a t
   val iter : ('a -> unit) -> 'a t -> unit
   val null : 'a t
   (* 
   val fold_left : ('a -> witem -> 'a) -> 'a -> t -> 'a
   val fold_right : (witem -> 'a -> 'a) -> t -> 'a -> 'a
   val to_list : t -> witem list 
   *)
end

(*---------------------------------------------------------------*)
(** functor version *)

module type FixedCapacityS = 
sig
   include S
   val capacity : int
   val is_full : 'a t -> bool
end


(*---------------------------------------------------------------*)
(** functor version *)

module type SItem = 
sig
   type item
   type t
   val create : unit -> t
   val is_empty : t -> bool
   val length : t -> int
   val push_front : item -> t -> unit
   val pop_front : t -> item
   val push_back : item -> t -> unit
   val pop_back : t -> item
   val append : t -> t -> unit
   val carve_back_at : int -> t -> t
   (* 
   val fold_left : ('a -> item -> 'a) -> 'a -> t -> 'a
   val fold_right : (item -> 'a -> 'a) -> t -> 'a -> 'a
   val to_list : t -> item list 
   *)
end
