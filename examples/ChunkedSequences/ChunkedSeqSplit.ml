  (* todo: below, "require" rather than "open" *)
open Weighted
open CapacitySig
open SequenceSig
open WeightedSequenceSig

(*-----------------------------------------------------------------------------*)

(*
Module Make 
   (Middle : WeightedSequenceSig.SPoly) 
   (Capa : CapacitySig.S) =
struct

module Chunk = CircularArrayPoly.Make(Capa)
*)

module Make 
   (Chunk : SequenceSig.SPoly)
   (Middle : WeightedSequenceSig.SPoly) 
   (Capa : CapacitySig.S) =
struct

type 'a chunk = 'a Chunk.t

type 'a middle = ('a chunk) Middle.t (* a sequence of [('a chunk) Weighted.t] *)

let capacity = Capa.capacity


(*-----------------------------------------------------------------------------*)

(** Representation 
    -- filling outer buffers before inner buffers, unlike in the paper *)

type 'a t = {
  mutable front_outer : 'a chunk;
  mutable front_inner : 'a chunk;
  mutable middle : 'a middle;
  mutable back_inner : 'a chunk;
  mutable back_outer : 'a chunk; }

(*-----------------------------------------------------------------------------*)

(** Auxiliary operations on the middle sequence *)

let middle_push_front c m =
   let wc = Weighted.make (Chunk.length c) c in
   Middle.push_front wc m

let middle_push_back c m =
   let wc = Weighted.make (Chunk.length c) c in
   Middle.push_back wc m

let middle_pop_front m =
   Weighted.get_value (Middle.pop_front m)

let middle_pop_back m =
   Weighted.get_value (Middle.pop_back m)

let middle_front m =
   Weighted.to_pair (Middle.pop_front m)

let middle_back m =
   Weighted.to_pair (Middle.pop_back m)


(*-----------------------------------------------------------------------------*)

(** Creation *)

let create () = {
   front_outer = Chunk.create();
   front_inner = Chunk.create();
   middle = Middle.create();
   back_inner = Chunk.create();
   back_outer = Chunk.create();
   }

(** Emptiness test *)    
  
let is_empty s =
     Chunk.is_empty s.front_outer
  && Chunk.is_empty s.front_inner
  && Middle.is_empty s.middle
  && Chunk.is_empty s.back_inner
  && Chunk.is_empty s.back_outer

(** Length *)    
  
let length s =
    Chunk.length s.front_outer
  + Chunk.length s.front_inner
  + Middle.weight s.middle
  + Chunk.length s.back_inner
  + Chunk.length s.back_outer

(** Push front *)

let push_front x s =
   let co = s.front_outer in
   if Chunk.is_full co then begin
      let ci = s.front_inner in
      s.front_inner <- co;
      if Chunk.is_empty ci then begin
         s.front_outer <- ci;
      end else begin
         (* assert (Chunk.is_full ci); *)
         middle_push_front ci s.middle;
         s.front_outer <- Chunk.create();
      end 
   end;
   Chunk.push_front x s.front_outer

(** Push back *)

let push_back x s =
   assert false

(** Pop front *)

let pop_front s =
   let co = s.front_outer in
   if not (Chunk.is_empty co) then begin
      Chunk.pop_front co
   end else begin
      let ci = s.front_inner in
      let m = s.middle in
      if not (Chunk.is_empty ci) then begin
         s.front_outer <- ci;
         s.front_inner <- co;
         Chunk.pop_front s.front_outer
      end else if not (Middle.is_empty m) then begin
         s.front_outer <- middle_pop_front m;
         Chunk.pop_front s.front_outer
      end else if not (Chunk.is_empty s.back_inner) then begin
         s.front_outer <- s.back_inner;
         s.back_inner <- co;
         Chunk.pop_front s.front_outer
      end else begin
         (* assert (not (Chunk.is_empty s.back_outer)); *)
         Chunk.pop_front s.back_outer
      end
   end

(** Pop back *)

let pop_back s =
   assert false

(** Push a buffer to the back of the middle sequence, 
    possibly merging it with the back chunk in the middle sequence *)

let push_or_merge_chunk_back m c =
   let sc = Chunk.length c in
   if sc > 0 then begin
      if Middle.is_empty m then begin
         middle_push_back c m
      end else begin
         let (sb,b) = middle_back m in
         let st = sc + sb in
         if st > capacity then begin
            middle_push_back c m
         end else begin
            let b = middle_pop_back m in 
            Chunk.append b c;
            middle_push_back b m;
         end
      end
   end

(** -- Symmetric to above *)

let push_or_merge_chunk_front m c =
   assert false

(** Append to the back of s1 the items of s2; s2 becomes invalid *)

let append s1 s2 =
   let m1 = s1.middle in
   let ci = s1.back_inner in
   let co = s1.back_outer in
   if Chunk.is_empty ci then begin
      push_or_merge_chunk_back m1 co
   end else begin
      middle_push_back ci m1;
      if not (Chunk.is_empty co)
         then middle_push_back co m1;
   end;
   let m2 = s2.middle in
   let fi = s2.front_inner in
   let fo = s2.front_outer in
   if Chunk.is_empty fi then begin
      push_or_merge_chunk_front m2 fo
   end else begin
      middle_push_front fi m2;
      if not (Chunk.is_empty fo)
         then middle_push_front fo m2;
   end;
   s1.back_inner <- s2.back_inner;
   s1.back_outer <- s2.back_outer;
   if   not (Middle.is_empty m1)
     && not (Middle.is_empty m2) then begin
      let (sc1,c1) = middle_back m1 in
      let (sc2,c2) = middle_front m2 in
      if sc1 + sc2 <= capacity then begin
         let c1 = middle_pop_back m1 in
         let c2 = middle_pop_front m2 in
         Chunk.append c1 c2;
         middle_push_back c1 m1;
      end
   end;
   Middle.append m1 m2


(*------


(** Split *)

let carve_back_at i s =
   let fo = s.front_outer in
   let fi = s.front_inner in
   let m = s.middle in
   let bi = s.back_inner in
   let bo = s.back_outer in
   let n1 = Chunk.length fo in
   let n2 = n1 + Chunk.length fi in
   let n3 = n2 + Middle.weight m in
   let n4 = n3 + Chunk.length bi in
   let n5 = n4 + Chunk.length bo in
   (* assert (i >= 0 && i <= n5); *)
   let r = create() in
   if i <= n1 then begin
      s.front_outer <- r.front_outer;
      s.front_inner <- r.front_inner;
      s.middle <- r.middle;
      s.back_inner <- r.back_inner;
      s.back_outer <- r.back_outer;
      r.front_outer <- fo;
      r.front_inner <- fi;
      r.middle <- m;
      r.back_inner <- bi;
      r.back_outer <- bo;
      Chunk.transfer_front_to_back i r.front_outer s.front_outer;
   end else if i <= n2 then begin
      s.front_inner <- r.front_outer;
      s.middle <- r.middle;
      s.back_inner <- r.back_inner;
      s.back_outer <- r.back_outer;
      r.front_outer <- fi;
      r.middle <- m;
      r.back_inner <- bi;
      r.back_outer <- bo;
      Chunk.transfer_front_to_back (i-n1) r.front_outer s.back_outer;
   end else if i <= n3 then begin
      s.back_inner <- r.back_inner;
      s.back_outer <- r.back_outer;
      r.back_inner <- bi;
      r.back_outer <- bo;
      let (wc,m2) = Middle.carve_back_at (i-n2) m in
      let c = wc.value in
      r.middle <- m2;
      r.front_outer <- c;
      let j = i - n2 - Middle.weight m in
      Chunk.transfer_front_to_back j r.front_outer s.back_outer;
   end else if i <= n4 then begin
      s.back_inner <- r.front_outer;
      s.back_outer <- r.back_outer;
      r.front_outer <- bi;
      r.back_outer <- bo;
      Chunk.transfer_front_to_back (i-n3) r.front_outer s.back_outer;
   end else begin
      s.back_outer <- r.front_outer;
      r.front_outer <- bo;
      Chunk.transfer_front_to_back (i-n4) r.front_outer s.back_outer;
   end;
   r

----*)

end







(*-----------------------------------------------------------------------------*)
(*
   module Make 
      (MiddleQueue : functor (T : InhabType.S) -> (WeightedSequenceSig.S with type item = T.t)) (* *)
      (Capa : CapacitySig.S) 
      (Item : InhabType.S) 
    (* : SequenceSig.S *) = 
   struct

   module Chunk = CircularArray.Make(Capa)(Item)

   module ChunkInhab = struct type t = Chunk.t let inhab = Chunk.create() end

   module Middle = MiddleQueue(ChunkInhab) 

   type item = Item.t
   type chunk = Chunk.t
   type middle = Middle.t

   let capacity = Capa.capacity


*)

(*-----------------------------------------------------------------------------*)

(*
   module Make 
      (Middle : WeightedSequenceSig.SPoly) 
      (Capa : CapacitySig.S) 
      (Item : InhabType.S) 
      (* : SequenceSig.S *) = 
   struct

   module Chunk = CircularArray.Make(Capa)(Item)

   type item = Item.t
   type chunk = Chunk.t
   type middle = chunk Middle.t

   let capacity = Capa.capacity


   (** Representation 
       -- filling outer buffers before inner buffers, unlike in the paper *)

   type t = {
     mutable front_outer : chunk;
     mutable front_inner : chunk;
     mutable middle : middle;
     mutable back_inner : chunk;
     mutable back_outer : chunk; }

*)