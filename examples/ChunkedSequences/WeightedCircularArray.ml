open Weighted

(** Representation of fixed-size circular buffers of weighted items. *)

module Make (Capa : CapacitySig.S) (Item : InhabType.S) = 
struct

module Circ = CircularArray.Make(Capa)(Item)

type item = Item.t

type t = Circ.t Weighted.t

(*--------------------------------------------------------------------------*)

(** Builds a new queue *)

let create () = 
   Weighted.make 0 (Circ.create())

(** Returns the size of the queue *)

let length wq =
  Circ.length wq.value

(** Tests whether the queue is empty *)

let is_empty wq =
  Circ.is_empty wq.is_empty
  (* alternative: q.weight = 0 *)

(** Tests whether the queue is full *)

let is_full wq =
  Circ.is_full wq.value

(** Swap the content of two circular buffers *)

let swap wq1 wq2 =
  Weighted.swap wq1 wq2

(*--------------------------------------------------------------------------*)

(** Pop an element from the front (assumes non-empty queue) *)

let pop_front wq = 
  let wx = Circ.pop_front wq.value in
  rem_weight wx.weight wq;
  wx
 
(** Pop an element from the back (assumes non-empty queue) *)

let pop_back wq = 
  let wx = Circ.pop_back wq.value in
  rem_weight wx.weight wq;
  wx

(** Push an element to the front (assumes non-full queue) *)

let push_front wx wq =
  add_weight wx.weight wq;
  Circ.push_front wx wq.value

(** Push an element to the back (assumes non-full queue) *)

let push_back wx q =
  add_weight wx.weight wq;
  Circ.push_back wx wq.value


(*--------------------------------------------------------------------------*)

(** Split a weighted buffer so as to isolate the weighted element that covers
    the targeted weight; returns a pair of a weighted item and a weighted queue,
    all these items being transfered from those in the argument wq. *)

let carve_back_at w wq1 = 
   let wq2 = create() in
   swap wq1 wq2;
   let wx0 = pop_front wq2 in
   let cur = ref wx0 in
   let wtotal = ref wx0.weight in
   while !wtotal <= w do
      push_front !cur wq1;
      cur := pop_front wq2;
      wtotal := !wtotal + cur.weight;
   done;
   (!cur, wq2)


(*--------------------------------------------------------------------------*)

(** Transfer all items from a buffer to the front of another buffer *)

let transfer_all_to_front wq1 wq2 =
   add_weight wq1.weight wq2;
   transfer_all_to_front wq1.value wq2.value

(** Transfer all items from a buffer to the back of another buffer *)

let transfer_all_to_back q1 q2 =
   add_weight wq1.weight wq2;
   transfer_all_to_back wq1.value wq2.value


end
