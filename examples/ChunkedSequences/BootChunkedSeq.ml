open NullPointers
open Weighted

(*-----------------------------------------------------------------------------*)

module MakePoly (Capa : CapacitySig.S) =
struct

(*-----------------------------------------------------------------------------*)

(** Aliases *)

let capacity = Capa.capacity

module Chunk = WeightedCircularArrayPoly.Make(Capa)

type 'a chunk = ('a Weighted.t) Chunk.t   (* a fixed-capacity sequence of objects, each of type ['a Weighted.t] *)
type 'a chunk_implem = ('a Weighted.t) Chunk.unweighted_t  


(*-----------------------------------------------------------------------------*)

(** Representation 
    -- filling outer buffers before inner buffers, unlike in the paper;
    -- a level is shallow iff the middle pointer is null; in this case:
         - all the data gets stored in the front_outer buffer,
         - the back_outer pointer is equal to the front_outer pointer, 
         - both inner buffers are null. *)

type 'a item = 'a Weighted.t 

type 'a t = ('a str) Weighted.t   (* a weighted sequence of objects, each of type ['a item] *)

and 'a str = {   (* a sequence of objects, each of type ['a item] *)
  mutable front_outer : 'a chunk;
  mutable front_inner : 'a chunk;
  mutable middle : ('a chunk_implem) t;  (* a sequence of chunks, each of type: ['a chunk] *)
  mutable back_inner : 'a chunk;
  mutable back_outer : 'a chunk; }


(*-----------------------------------------------------------------------------*)

(** Test if level is shallow *)    

let is_shallow s =
   s.middle == null

(** Creation of a shallow level *)

let shallow_create () =
  let c = Chunk.create() in
  { front_outer = c;
    front_inner = null;
    middle = null;
    back_inner = null;
    back_outer = c; }

(** Weight *)

let weight q = 
   Weighted.weight q

(** Creation *)

let create () = 
   Weighted.make 0 (shallow_create())

(** Conversion from shallow to deep, storing data in back_outer *)

let convert_shallow_to_deep_with_empty_front s =
   s.front_outer <- Chunk.create();  
   s.front_inner <- Chunk.create();  
   s.middle <- create();
   s.back_inner <- Chunk.create()

(** Conversion from shallow to deep, storing data in front_outer *)

let convert_shallow_to_deep_with_empty_back s =
   s.front_inner <- Chunk.create();  
   s.middle <- create();
   s.back_inner <- Chunk.create();
   s.back_outer <- Chunk.create()  

(** Conversion from deep to shallow, where chunk c holds all the data *)

let convert_deep_to_shallow c s =
   s.front_outer <- c;  
   s.front_inner <- null;
   s.middle <- null;
   s.back_inner <- null;
   s.back_outer <- c 

(** Emptiness test *)    
  
let is_empty q = 
   let s = q.value in
   assert (is_shallow s);
   Chunk.is_empty s.front_outer

(** Push front *)

let rec push_front : 'a. 'a item -> 'a t -> unit = fun x q ->
   let s = q.value in
   let co = s.front_outer in
   if Chunk.is_full co then begin
      if is_shallow s then begin
         convert_shallow_to_deep_with_empty_front s
      end else begin
         let ci = s.front_inner in
         s.front_inner <- co;
         if Chunk.is_empty ci then begin
            s.front_outer <- ci;
         end else begin
            assert (Chunk.is_full ci);
            push_front ci s.middle;
            s.front_outer <- Chunk.create();
         end
      end
   end;
   Chunk.push_front x s.front_outer;
   Weighted.add_weight_of x q

(** Push back *)

let push_back : 'a. 'a item -> 'a t -> unit = fun x q ->
   assert false

(** Pop front *)

let rec pop_front : 'a. 'a t -> 'a item = fun q -> 
   let s = q.value in
   let co = s.front_outer in
   let x =
      if not (Chunk.is_empty co) then begin
         Chunk.pop_front co 
      end else begin
         assert (not (is_shallow s));
         let ci = s.front_inner in
         let m = s.middle in
         if not (Chunk.is_empty ci) then begin
            s.front_outer <- ci;
            s.front_inner <- co;
            Chunk.pop_front s.front_outer
         end else if not (is_empty m) then begin
            s.front_outer <- pop_front m;
            Chunk.pop_front s.front_outer
         end else if not (Chunk.is_empty s.back_inner) then begin
            s.front_outer <- s.back_inner;
            s.back_inner <- co;
            Chunk.pop_front s.front_outer
         end else begin
            assert (not (Chunk.is_empty s.back_outer));
            Chunk.pop_front s.back_outer
         end
      end in
   Weighted.rem_weight_of x q;
   convert_deep_to_shallow_if_needed s;
   x

(** Pop back *)

and pop_back : 'a. 'a t -> 'a item = fun s ->
   assert false

(** Front -- very similar to pop_front *)

and front : 'a. 'a t -> 'a item = fun q -> 
   let s = q.value in 
   let co = s.front_outer in
   let x =
      if not (Chunk.is_empty co) then begin
         Chunk.front co 
      end else begin
         assert (not (is_shallow s));
         let ci = s.front_inner in
         let m = s.middle in
         if not (Chunk.is_empty ci) then begin
            s.front_outer <- ci;
            s.front_inner <- co;
            Chunk.front s.front_outer
         end else if not (is_empty m) then begin
            s.front_outer <- pop_front m;
            Chunk.front s.front_outer
         end else if not (Chunk.is_empty s.back_inner) then begin
            s.front_outer <- s.back_inner;
            s.back_inner <- co;
            Chunk.front s.front_outer
         end else begin
            assert (not (Chunk.is_empty s.back_outer));
            Chunk.front s.back_outer
         end
      end in
   convert_deep_to_shallow_if_needed s;
   x

(** Back *)

and back : 'a. 'a t -> 'a item = fun q -> 
   assert false

(** Conversion from deep to shallow if the layer stores only 0 or 1 item *)

and convert_deep_to_shallow_if_needed : 'a. 'a str -> unit = fun s ->
   let sfo = Chunk.length s.front_outer in
   let sbo = Chunk.length s.back_outer in
   if sfo <= 1 && sbo <= 1 then begin
      if sfo = 0 && sbo = 0 && not (is_empty s.middle) then begin
         let c = pop_front s.middle in
         s.front_outer <- c;
      end;
      let sfo = Chunk.length s.front_outer in
      if is_empty s.middle then begin
         if sfo == 1 && sbo == 0 
            then convert_deep_to_shallow s.front_outer s
         else if sfo == 0 && sbo == 1
            then convert_deep_to_shallow s.back_outer s
      end
   end

(** Push a buffer to the back of the middle sequence, 
    possibly merging it with the back chunk in the middle sequence *)

let push_or_merge_chunk_back m c =
   let sc = Chunk.length c in
   if sc > 0 then begin
      if is_empty m then begin
         push_back c m
      end else begin
         let b = back m in
         let sb = Chunk.length b in
         let st = sc + sb in
         if st > capacity then begin
            push_back c m
         end else begin
            let _ = pop_back m in 
            Chunk.transfer_all_to_back c b;
            push_back b m;
         end
      end
   end

(** -- Symmetric to above *)

let push_or_merge_chunk_front m c =
   assert false

(** Append to the back of q1 the items of q2; q2 becomes invalid *)

let rec append : 'a. 'a t -> 'a t -> unit = fun q1 q2 ->
   let s1 = q1.value in 
   let s2 = q2.value in
   if is_shallow s1 then begin
      Weighted.swap q1 q2;
      iter_shallow (fun x -> push_front x q1) s1
   end else if is_shallow s2 then begin
      iter_shallow (fun x -> push_back x q1) s2
   end else begin
      let m1 = s1.middle in
      let m2 = s2.middle in
      begin
         let ci = s1.back_inner in
         let co = s1.back_outer in
         if Chunk.is_empty ci then begin
            push_or_merge_chunk_back m1 co
         end else begin
            push_back ci m1;
            push_back co m1;
         end
      end;
      begin
         let ci = s2.front_inner in
         let co = s2.front_outer in
         if Chunk.is_empty ci then begin
            push_or_merge_chunk_front m2 co
         end else begin
            push_front ci m2;
            push_front co m2;
         end
      end;
      s1.back_inner <- s2.back_inner;
      s1.back_outer <- s2.back_outer;
      if not (is_empty m1) && not (is_empty m2) then begin
         let c1 = back m1 in
         let c2 = front m2 in
         let sc1 = Chunk.length c1 in
         let sc2 = Chunk.length c2 in
         if sc1 + sc2 <= capacity then begin
            let _ = pop_back m1 in
            let _ = pop_front m2 in
            Chunk.transfer_all_to_back c2 c1;
            push_back c1 m1;
         end
      end;
      append m1 m2;
      Weighted.add_weight_of q2 q1
   end
   
(** Iter on a shallow level *)

and iter_shallow : 'a. ('a item -> unit) -> 'a str -> unit = fun f s ->
   assert (is_shallow s);
   Chunk.iter f s.front_outer


(*-------- TODO


(** Split *)

let carve_back_at i s =
   let fo = s.front_outer in
   let fi = s.front_inner in
   let m = s.middle in
   let bi = s.back_inner in
   let bo = s.back_outer in
   let n1 = Chunk.length fo in
   let n2 = n1 + Chunk.length fi in
   let n3 = n2 + weight m in
   let n4 = n3 + Chunk.length bi in
   let n5 = n4 + Chunk.length bo in
   assert (i >= 0 && i <= n5);
   let r = create() in
   if i <= n1 then begin
      s.front_outer <- r.front_outer;
      s.front_inner <- r.front_inner;
      s.middle <- r.middle;
      s.back_inner <- r.back_inner;
      s.back_outer <- r.back_outer;
      r.front_outer <- fo;
      r.front_inner <- fi;
      r.middle <- m;
      r.back_inner <- bi;
      r.back_outer <- bo;
      Chunk.transfer_front_to_back i r.front_outer s.front_outer;
   end else if i <= n2 then begin
      s.front_inner <- r.front_outer;
      s.middle <- r.middle;
      s.back_inner <- r.back_inner;
      s.back_outer <- r.back_outer;
      r.front_outer <- fi;
      r.middle <- m;
      r.back_inner <- bi;
      r.back_outer <- bo;
      Chunk.transfer_front_to_back (i-n1) r.front_outer s.back_outer;
   end else if i <= n3 then begin
      s.back_inner <- r.back_inner;
      s.back_outer <- r.back_outer;
      r.back_inner <- bi;
      r.back_outer <- bo;
      let ((_,c),m2) = carve_back_at (i-n2) m in
      r.middle <- m2;
      r.front_outer <- c;
      let j = i - n2 - weight m in
      Chunk.transfer_front_to_back j r.front_outer s.back_outer;
   end else if i <= n4 then begin
      s.back_inner <- r.front_outer;
      s.back_outer <- r.back_outer;
      r.front_outer <- bi;
      r.back_outer <- bo;
      Chunk.transfer_front_to_back (i-n3) r.front_outer s.back_outer;
   end else begin
      s.back_outer <- r.front_outer;
      r.front_outer <- bo;
      Chunk.transfer_front_to_back (i-n4) r.front_outer s.back_outer;
   end;
   r

--------*)

end






(*-----------------------------------------------------------------------------*)
(*-----------------------------------------------------------------------------*)
(*-----------------------------------------------------------------------------*)

module Make 
   (Capa : CapacitySig.S) 
   (Item : InhabType.S) 
   (* : WeightedSequenceSig.S *) = 
struct
   module Seq = MakePoly(Capa)
   type item = Item.t
   type t = item Seq.t
   (* bind functions *)
end

