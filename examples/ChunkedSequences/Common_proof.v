Set Implicit Arguments.
Require Import CFLib LibSet LibMap LibArray LibInt LibOrder.
(*
Open Scope R_scope.
Open Scope Z_scope.
*)

(* todo: focus/unfocus lemmas set implicit arguments *)

(**************************************************)
(** list *)

(** Notation for lists *)

Notation "'concat'" := LibList.concat.


Definition sum_length A (L:list (list A)) := 
  LibList.fold_right (fun x acc => (acc + LibList.length x)%nat) 0%nat L.

Lemma length_concat : forall A (L:list (list A)),
  LibList.length (concat L) = sum_length L :> nat.
Proof. intros. unfold sum_length. induction L; rew_list; math. Qed.


Ltac list_eq_nil_inv_core H :=
  match type of H with
  | nil = ?X ++ ?Y => list_eq_nil_inv_core (sym_eq H)
  | ?X ++ ?Y = nil => 
     let H1 := fresh "EQ" in
     let H2 := fresh "EQ" in
     forwards [H1 H2]: app_eq_nil_inv (rm H);
     list_eq_nil_inv_core H1;
     list_eq_nil_inv_core H2
  | _ => idtac
  end.

Tactic Notation "list_eq_nil_inv" constr(H) :=
  list_eq_nil_inv_core H.

(* Notation "'len'" := (fun L => my_Z_of_nat (LibList.length L)). *)


Axiom get_last : forall A (L:list A),
  L <> nil -> exists X Q, L = Q&X.
Implicit Arguments get_last [A].

Axiom case_last : forall A (L:list A),
  L = nil \/ exists X Q, L = Q&X.
Implicit Arguments case_last [A].



Axiom Forall_last_inv : forall A (P:A->Prop) l x, 
  Forall P (l & x) ->
  Forall P l /\ P x.

Lemma cons_eq_last_inv : forall A x1 (x2:A) l1 l2, 
  x1::l1 = l2&x2 ->
  ((l1 = nil /\ l2 = nil /\ x1 = x2)  
  \/ (exists l2', l2 = x1::l2' /\ l1 = l2'&x2)).
Proof. introv E. destruct l2; rew_app in E; inverts* E. Qed.




Inductive ForallConseq (A:Type) (P:A->A->Prop) : list A -> Prop :=
  | ForallConseq_nil : 
     ForallConseq P nil
  | ForallConseq_one : forall x,
     ForallConseq P (x::nil) 
  | ForallConseq_next : forall x y L,
     P x y -> ForallConseq P (y::L) -> ForallConseq P (x::y::L).

Section ForallConseqLemmas.
Hint Constructors ForallConseq.

Lemma ForallConseq_last : forall A (P:A->A->Prop) x y L,
  P y x -> ForallConseq P (L&y) -> ForallConseq P (L&y&x).
Proof.
  introv Pyx H. induction L; rew_app in *.
  constructors~.
  inverts H as.
    intros E. false* nil_eq_last_inv.
    introv M1 M2 M3. lets [ (?&?&?) | (?&?&?)]: (>> cons_eq_last_inv M3).
    subst. rew_list. auto.
    subst. rew_list. auto.
Qed.

Lemma ForallConseq_last_inv : forall A (P:A->A->Prop) x y L,
  ForallConseq P (L&x&y) ->
  P x y /\ ForallConseq P (L&x).
Proof.
  introv M. induction L; rew_app in *.
  inverts~ M.
  inverts M as.
    intros E. false. forwards* (?&?): nil_eq_app_inv E. false.
    introv M1 M2 E. destruct L.
      rew_list in E. inverts E. inverts M2. rew_list. splits~.
      rew_list in E. inverts E. rew_list in *.
       forwards (?&?): IHL M2. splits~.
Qed.

Implicit Arguments Forall_last_inv [A P l x].


Lemma ForallConseq_concat : forall A (P:A->A->Prop) x y L1 L2,
  P x y -> ForallConseq P (L1&x) -> ForallConseq P (y::L2) -> 
  ForallConseq P (L1&x ++ y::L2).
Proof.
  introv Pyx H1 H2. sets_eq L0: (L1 & x). gen L1.
  induction H1; intros; rew_app in *.
  false* nil_eq_last_inv.
  destruct L1; rew_list in *; inverts EQL0.
    constructors~.
    false* nil_eq_last_inv.
  destruct L1; rew_list in *; inverts EQL0. forwards*: IHForallConseq.
Qed.

Lemma ForallConseq_cons_fuse : forall A (P:A->A->Prop) y z L,
  (forall a, P y a -> P z a) ->
  ForallConseq P (y :: L) ->
  ForallConseq P (z :: L).
Proof.
  introv Pza H1. inverts H1; constructors~.
Qed.

Lemma ForallConseq_concat_fuse : forall A (P:A->A->Prop) x y z L1 L2,
  (forall a, P a x -> P a z) -> (forall a, P y a -> P z a) ->
  ForallConseq P (L1&x) -> ForallConseq P (y::L2) -> 
  ForallConseq P (L1&z ++ L2).
Proof.
  introv Paz Pza H1 H2. sets_eq L0: (L1 & x). gen L1.
  induction H1; intros; rew_app in *.
  false* nil_eq_last_inv.
  destruct L1; rew_list in *; inverts EQL0.
    applys* ForallConseq_cons_fuse.
    false* nil_eq_last_inv.
  destruct L1; rew_list in *; inverts EQL0.
   forwards*: IHForallConseq.
    destruct L1; rew_list in *; inverts H4; constructors~.
Qed.
  
Lemma ForallConseq_cons_inv : forall A (P:A->A->Prop) x L,
  ForallConseq P (x::L) -> ForallConseq P L.
Proof. introv M. inverts M as C M. auto. inverts M as C M. auto. auto. Qed.

Lemma ForallConseq_cons2_inv : forall A (P:A->A->Prop) x y L,
  ForallConseq P (x::y::L) -> P x y /\ ForallConseq P L.
Proof. introv M. inverts M as C M. splits~. inverts M as C M. auto. auto. Qed.

End ForallConseqLemmas.


(**************************************************)
(** reduce *)

Open Scope list_scope.

Section Reduce.
Variables (A C : Type).
Parameter reduce : monoid_def C -> (A -> C) -> list A -> C.
Parameter reduce_empty : forall m f, Monoid m ->
  reduce m f nil = monoid_neutral m.
Parameter reduce_single : forall x m f, Monoid m ->
  reduce m f (x::nil) = f x.
Parameter reduce_cons : forall X L m f, Monoid m ->
  reduce m f (X :: L) = (monoid_oper m) (f X) (reduce m f L).
Parameter reduce_app : forall L1 L2 m f, Monoid m ->
  reduce m f (L1 ++ L2) = (monoid_oper m) (reduce m f L1) (reduce m f L2).
End Reduce.

Hint Rewrite reduce_empty reduce_single reduce_cons reduce_app : rew_reduce.
Tactic Notation "rew_reduce" :=
   autorewrite with rew_reduce; [ | try typeclass ].
Tactic Notation "rew_reduce" "~" :=
  rew_reduce; auto_tilde.
Tactic Notation "rew_reduce" "*" :=
  rew_reduce; auto_star.



(**************************************************)
(** cf heaps *)

Open Scope heap_scope_advanced.


(**************************************************)
(** xcf types *)

Ltac xcf_for_core_with args :=
  let f := spec_goal_fun tt in 
  let bargs := list_boxer_of args in
  ltac_database_get database_cf f;
  let H := fresh "TEMP" in intros H; 
  let fct := constr:((boxer H)::bargs) in 
  applys (fct); clear H;
  instantiate; try solve_type; [ try xisspec | ];
  xcf_post tt.

Tactic Notation "xcf_types" constr(args) :=
  xcf_for_core_with args.


(**************************************************)
(** tactics *)

Ltac unfolds_all_base :=
  match goal with |- ?G => 
   apply_to_head_of G ltac:(fun P => unfold P in *) end.

Tactic Notation "unfolds" "in" "*" :=
  unfolds_all_base.



(**************************************************)
(** wand *)

Definition heap_wand (H1 H2 : hprop) :=
  Hexists HW, HW \* \[H1 \* HW ==> H2].

Notation "H1 '==>*' H2" := (heap_wand H1 H2) 
  (at level 100).

Notation "H1 '\*==>*' H2" := 
  (H1%h \* (H1 ==>* H2)) (at level 100).

Lemma heap_wand_elim : forall H1 H2,
  H1 \* (H1 ==>* H2) ==> H2.
Proof. intros. unfold heap_wand. hextract as HW M. hchanges M. Qed.

Implicit Arguments heap_wand_elim [ ].


Axiom heap_wand_elim' : forall H1 H2 H3,
  H1 \* (H1 ==>* H2) \* H3 = H2 \* H3.

Implicit Arguments heap_wand_elim' [ ].


Ltac find_wand tt := 
  match goal with |- ?R ?H ?Q =>  
    match H with context [?H1 ==>* ?H2] =>
      constr:(H1) end end.

(*
Ltac hsimpl_wand_base tt :=  
  let H1 := find_wand tt in hchange (heap_wand_elim H1); [ hsimpl | ].
*)
Ltac hsimpl_wand_base tt :=  
  let H1 := find_wand tt in
  (* hchange (heap_wand_elim H1); [ hsimpl | ].*)
  hchange_core (heap_wand_elim H1) __ ltac:(idcont) ltac:(idcont);  [ hcancel_cont tt | ].


Ltac xsimpl_wand_base tt :=  
  let H1 := find_wand tt in xchange (heap_wand_elim H1); [ hsimpl | ].

Tactic Notation "hsimpl_wand" :=
  hsimpl_wand_base tt.

Tactic Notation "xsimpl_wand" :=
  xsimpl_wand_base tt.


(**************************************************)
(** bin *)


(*
Definition chunk_seq (A : Type) : list A -> Chunk.t A -> hprop :=
  (@ChunkSpec.Seq A).
Implicit Arguments chunk_seq [ ].
*)

(*
Definition chunk_seq (A : Type) : list A -> Chunk.t A -> hprop :=
  (@ChunkSpec.Seq A).
Implicit Arguments chunk_seq [ ].
*)

(**
Definition middle_seq (A : Type) :=
  MiddleSpec.Seq len (@ChunkSpec.Seq A).

Implicit Arguments middle_seq [ A ].
*)
(*
Definition str (A : Type) (fo fi : list A) (mid : list (list A)) (bi bo : list A) :=
  Q.T (chunk_seq A) (chunk_seq A) (chunk_seq A) (chunk_seq A) (middle_seq A)
  bi bo fi fo mid.



Axiom frac : Type.
Axiom build_frac : int -> int -> frac.

Notation "a '//' b" := (build_frac a b) (at level 31, format "a // b")
  : frac_scope.
Open Scope frac_scope.
Open Scope Z_scope.

Coercion frac_of_int (n:int) := (n // 1).

Delimit Scope frac_scope with Q.

Axiom frac_add : frac -> frac -> frac.
Axiom frac_sub : frac -> frac -> frac.
Axiom frac_mul : frac -> frac -> frac.
Axiom frac_div : frac -> frac -> frac.
Axiom frac_neg : frac -> frac.

Infix "+" := frac_add : frac_scope.
Notation "- x" := (frac_neg x) : frac_scope.
Infix "-" := frac_sub : frac_scope.
Infix "*" := frac_mul : frac_scope.
Infix "/" := frac_div : frac_scope.

Axiom frac_le : frac -> frac -> Prop.
Instance le_frac_inst : Le frac := Build_Le frac_le.

Instance frac_le_total_order : Le_total_order (A:=frac).
Admitted.


Axiom frac_add_same_dividor : forall n m p,
  ( n/p + m/p = (n+m)/p )%Q.
  
Axiom frac_distrib : forall a b c : frac,
  ((a+b)*c = a*c + b*c)%Q.

Axiom frac_mult_div_same : forall a b : frac,
  (a*(b/a) = b)%Q.

Axiom frac_distrib_one : forall a b c : frac,
  ((1+b)*c = c + b*c)%Q.


Axiom int_nat_plus : forall (n m:nat),
  (n + m)%nat = (n + m)%Z :> int.

Axiom frac_of_int_distrib : forall (n m:int),
  frac_of_int(n + m) = (frac_of_int(n) + frac_of_int(m))%Q.

Axiom frac_mut_one : forall (a:frac),
   (frac_of_int 1 * a)%Q = a.
*)
