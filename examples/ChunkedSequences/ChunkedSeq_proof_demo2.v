Set Implicit Arguments.
Require Export Rbase Rfunctions.
Require Import CFLib LibSet LibMap LibArray LibInt.
Require Import CapacityParamSig_ml.
Require Import SequenceSig_ml SequenceSig_proof.
Require Import ChunkedSeq_ml.
Require Import Common_proof.

Open Scope R_scope.
Open Scope Z_scope.
Open Scope list_scope.
Require Import LibOrder.

(*------------------------------------------------------*)
(** instantiations *)

Module MakeSpec
   (Chunk : SequenceSig_ml.MLFixedCapacityS)
   (ChunkSpec : SequenceSigFixedCapacitySpec with Module Q':=Chunk)
   (Middle : SequenceSig_ml.MLS) 
   (MiddleSpec : SequenceSigSpec with Module Q:=Middle).

Module Import Q := MLMake Middle Chunk.

Module MiddleSpecOwn := SequenceSigOwnSpec_from_SigSpec (Middle)(MiddleSpec).


(*------------------------------------------------------*)
(** representation predicates *)

Notation "'RChunk'" := (@ChunkSpec.Seq _).

Notation "'RMiddle'" := (MiddleSpecOwn.Seq RChunk).

Notation "'TF' fo fi mid bi bo" :=
  (Q.T RChunk RChunk RMiddle RChunk RChunk fo fi mid bi bo)
  (fo at level 0, fi at level 0, mid at level 0, bi at level 0, bo at level 0, at level 30).

Notation "'TU_'" := (T Id Id Id Id Id).

Notation "'length'" := (fun L => my_Z_of_nat (LibList.length L)).

(*------------------------------------------------------*)
(** invariants *)

Definition capa := Chunk.capacity.

Definition full (A:Type) (L:list A) :=
  length L = capa. 

Definition middle_conseq_ok (A:Type) (mid:list(list A)) :=
  ForallConseq (fun c1 c2 => length c1 + length c2 > capa) mid.

Definition chunk_ok {A:Type} (L:list A) :=
  0 < length L <= capa.

Definition middle_chunks_ok (A:Type) (mid:list(list A)) :=
  Forall chunk_ok mid.

Record inv (A : Type) (L fo fi : list A) mid bi bo := build_inv {
  inv_items : L = fo ++ fi ++ concat mid ++ bi ++ bo;
  inv_front_fill : fi = nil \/ full fi;
  inv_back_fill : bi = nil \/ full bi;
  inv_middle_chunks : middle_chunks_ok mid;
  inv_middle_conseq : middle_conseq_ok mid }.

Definition Seq (A : Type) (L : list A) (q : loc) :=
  Hexists fo fi mid bi bo,
    q ~> TF fo fi mid bi bo \* 
    \[inv L fo fi mid bi bo].


(*------------------------------------------------------*)
(** tactics *)

Ltac lmath :=
  unfolds chunk_ok, capa; try rew_length in *; math.

Hint Unfold middle_chunks_ok middle_conseq_ok.
Hint Constructors Forall ForallConseq.
Hint Resolve Forall_last Forall_app.
Hint Extern 10 (_ = _ :> list _) => subst; rew_list.
Hint Extern 1 ((_ > _)) => lmath.
Hint Extern 1 ((_ >= _)) => lmath.
Hint Extern 1 ((_ <= _)) => lmath.
Hint Extern 1 ((_ > _)%Z) => lmath.
Hint Extern 1 ((_ >= _)%Z) => lmath.
Hint Extern 1 ((_ <= _)%Z) => lmath.
Hint Extern 1 (chunk_ok _) => lmath.


(*------------------------------------------------------*)
(** auxiliary definitions *)

Definition inv_middle A L (mid:list (list A)) :=
  concat mid = L /\ middle_chunks_ok mid /\ middle_conseq_ok mid.


(*------------------------------------------------------*)
(** lemmas *)

Lemma inv_middle_from_inv : forall A (L:list A) fo fi mid bi bo,
  inv L fo fi mid bi bo -> inv_middle (concat mid) mid.
Proof. introv [MI MF MB MN MC]. splits~. Qed.

Hint Resolve inv_middle_from_inv.

Ltac math_0 ::= hint ChunkSpec.capacity_spec. (* capa > 0 *)

Lemma not_full_nil : forall A,
  ~ full (@nil A).
Proof. introv H. unfolds in H. rew_list in *. lmath. Qed.

Hint Resolve not_full_nil.

Lemma middle_conseq_push_front_full : forall A (fi:list A) mid,
  middle_conseq_ok mid -> 
  middle_chunks_ok mid ->
  full fi -> 
  middle_conseq_ok (fi :: mid).
Proof. 
  introv M K N. unfolds in M N K.
  destruct mid as [|f mid]; inverts K; constructors~.  
Admitted. (* faster *)


(*------------------------------------------------------*)
(** For the demo: specification without potential *)

Parameter chunk_create_spec : forall A,
  Spec Chunk.create () |B>> 
     B \[] (fun q => q ~> ChunkSpec.Seq (@nil A)).

Parameter chunk_push_front_spec : forall A,
  Spec Chunk.push_front (X:A) (q:Chunk.t A) |B>>  
     forall L,
     B (q ~> ChunkSpec.Seq L) (# q ~> ChunkSpec.Seq (X::L)).

Parameter middle_push_front_spec : forall a A,
  Spec Middle.push_front (x:a) (q:Middle.t a) |B>>  
     forall (R:htype A a), forall X L,
     B (q ~> MiddleSpecOwn.Seq R L \* x ~> R X) (# q ~> MiddleSpecOwn.Seq R (X::L)).

Hint Extern 1 (RegisterSpec Chunk.create) => Provide chunk_create_spec.
Hint Extern 1 (RegisterSpec Chunk.push_front) => Provide chunk_push_front_spec.
Hint Extern 1 (RegisterSpec Middle.push_front) => Provide middle_push_front_spec.


(*------------------------------------------------------*)
(** For the demo: verification without potential *)

Lemma push_front_spec : forall A X (L:list A) (q:Q.t A),
  (App push_front X q;) (q ~> Seq L) (# q ~> Seq (X::L)).
Proof.
  intros. xcf_app. intros. rename q into s. hunfold Seq at 1.
  xextract as fo fi mid bi bo Inv. xapp_by focus. xapps. xseq.
  xif (# Hexists (fo2 fi2 : list A), Hexists mid2,
       s ~> TF fo2 fi2 mid2 bi bo \* \[inv L fo2 fi2 mid2 bi bo]
    \* \[~ full fo2]).
  (* case full *) 
  xapp_by focus. xapp_by unfocus. xapps. xif.
  (* case inner empty *)
  xapp_by unfocus. hsimpl~. destruct Inv. constructor~.
  (* case inner full *)
  tryfalse. xapp_by focus. 
  destruct Inv as [MI MF MB MN MC]. destruct MF; tryfalse.
  xapp~. xapp. xapp_by unfocus.
  hchange (@_unfocus_middle s). hsimpl~. constructor~.
    unfolds_hd in *. unfolds~ full.
    applys* middle_conseq_push_front_full.
  (* case not full *)
  xret. hchange (@_unfocus_front_outer s). hsimpl~. 
  (* push front *)
  clears Inv. clears. xextract as fo fi mid Inv Nfo.
  xapp_by focus. xapp. hchange (@_unfocus_front_outer s).
  hunfold Seq. hsimpl. destruct Inv. constructor~. 
Qed.


End MakeSpec.