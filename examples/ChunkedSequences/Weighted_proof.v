Set Implicit Arguments.
Require Import CFLib Weighted_ml LibCore. 

(* Note: [T] takes as arguments [value] then [weight] *)

Definition Weighted (A a:Type) (W:A->int) (R:A->a->hprop) (X:A) (x:loc) :=
  x ~> T Id R (W X) X.
