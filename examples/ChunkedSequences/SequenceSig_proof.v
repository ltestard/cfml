Set Implicit Arguments.
Require Import CFLibCredits SequenceSig_ml Common_proof.
Open Scope R_scope.
Open Scope Z_scope.
Open Scope heap_scope_advanced.

(*------------------------------------------------------*)
(** Sequence *)

Module Type SequenceSigSpec.

Declare Module Q : SequenceSig_ml.MLS.
Import Q.

Parameter create_cst : R.
Parameter op_cst : R.
Parameter append_cst : R. (* later: generalize to a function *)

Parameter create_cst_nonneg : (create_cst >= 0)%R.
Parameter op_cst_nonneg : (op_cst >= 0)%R.
Parameter append_cst_nonneg : (append_cst >= 0)%R.

Hint Resolve create_cst_nonneg op_cst_nonneg append_cst_nonneg : credits_nonneg.

Parameter Seq : forall (A : Type), list A -> (Q.t A) -> hprop.

Parameter length_spec : forall A,
  forall (L:list A),
  Spec Q.length (q:Q.t A) |B>> 
     keep B (q ~> Seq L) (\= (LibList.length L : Z)).

Parameter create_spec : forall A,
  Spec create () |B>> 
     B ($ create_cst) (fun q => q ~> Seq (@nil A)).

Parameter is_empty_spec : forall A,
  Spec is_empty (q:Q.t A) |B>>  
     forall (L:list A),
     keep B (q ~> Seq L) (\= isTrue (L = nil)).

Parameter push_front_spec : forall A,
  Spec push_front (X:A) (q:Q.t A) |B>>  
     forall L,
     B (q ~> Seq L \* $ op_cst) (# q ~> Seq (X::L)).

Parameter push_back_spec : forall A, 
  Spec push_back (X:A) (q:Q.t A) |B>>  
     forall L,
     B (q ~> Seq L \* $ op_cst) (# q ~> Seq (L&X)).

Parameter pop_front_spec : forall A, 
  Spec pop_front (q:Q.t A) |B>>  
     forall X L,
     B (q ~> Seq (X::L)) (fun r => \[r = X] \* q ~> Seq L).

Parameter pop_back_spec : forall A, 
  Spec pop_back (q:Q.t A) |B>>  
     forall X L,
     B (q ~> Seq (L&X)) (fun r => \[r = X] \* q ~> Seq L).

Parameter append_spec : forall A, 
  Spec append (q1:Q.t A) (q2:Q.t A) |B>>  
     forall L1 L2,
     B (q1 ~> Seq L1 \* q2 ~> Seq L2 \* $ append_cst) (# q1 ~> Seq (L1 ++ L2)).

Hint Extern 1 (RegisterSpec create) => Provide create_spec.
Hint Extern 1 (RegisterSpec is_empty) => Provide is_empty_spec.
Hint Extern 1 (RegisterSpec length) => Provide length_spec.
Hint Extern 1 (RegisterSpec push_front) => Provide push_front_spec.
Hint Extern 1 (RegisterSpec push_back) => Provide push_back_spec.
Hint Extern 1 (RegisterSpec pop_front) => Provide pop_front_spec.
Hint Extern 1 (RegisterSpec pop_back) => Provide pop_back_spec.
Hint Extern 1 (RegisterSpec append) => Provide append_spec.

End SequenceSigSpec.


(*------------------------------------------------------*)
(** Fixed Capacity Sequence *)

Module Type SequenceSigFixedCapacitySpec.

Declare Module Q : SequenceSig_ml.MLFixedCapacityS.
Import Q.

Parameter create_cst : R.
Parameter op_cst : R.
Parameter append_cst : R. (* later: generalize to a function *)

Parameter create_cst_nonneg : (create_cst >= 0)%R.
Parameter op_cst_nonneg : (op_cst >= 0)%R.
Parameter append_cst_nonneg : (append_cst >= 0)%R.

Hint Resolve create_cst_nonneg op_cst_nonneg append_cst_nonneg : credits_nonneg.

Parameter Seq : forall (A : Type), list A -> (Q.t A) -> hprop.

Parameter capacity_spec : Q.capacity > 0.

Parameter Seq_le_capacity : forall A (q:Q.t A) L,
  hkeep (q ~> Seq L) ==> \[(LibList.length L: Z) <= Q.capacity].

Implicit Arguments Seq_le_capacity [A].

Parameter is_full_spec : forall A,
  Spec is_full (q:Q.t A) |B>>  
     forall (L:list A),
     keep B (q ~> Seq L) (\= isTrue (LibList.length L = Q.capacity :> Z)).

Parameter length_spec : forall A,
  forall (L:list A),
  Spec Q.length (q:Q.t A) |B>> 
     keep B (q ~> Seq L) (\= (LibList.length L : Z)).

Parameter create_spec : forall A,
  Spec create () |B>> 
     B ($ create_cst) (fun q => q ~> Seq (@nil A)).

Parameter is_empty_spec : forall A,
  Spec is_empty (q:Q.t A) |B>>  
     forall (L:list A),
     keep B (q ~> Seq L) (\= isTrue (L = nil)).

Parameter push_front_spec : forall A,
  Spec push_front (X:A) (q:Q.t A) |B>>  
     forall L, (LibList.length L : Z) < Q.capacity ->
     B (q ~> Seq L \* $ op_cst) (# q ~> Seq (X::L)).

Parameter push_back_spec : forall A, 
  Spec push_back (X:A) (q:Q.t A) |B>>  
     forall L, (LibList.length L : Z) < Q.capacity ->
     B (q ~> Seq L \* $ op_cst) (# q ~> Seq (L&X)).

Parameter pop_front_spec : forall A, 
  Spec pop_front (q:Q.t A) |B>>  
     forall X L,
     B (q ~> Seq (X::L)) (fun r => \[r = X] \* q ~> Seq L).

Parameter pop_back_spec : forall A, 
  Spec pop_back (q:Q.t A) |B>>  
     forall X L,
     B (q ~> Seq (L&X)) (fun r => \[r = X] \* q ~> Seq L).

Parameter append_spec : forall A, 
  Spec append (q1:Q.t A) (q2:Q.t A) |B>>  
     forall L1 L2, (LibList.length L1 : Z) + (LibList.length L2 : Z) <= Q.capacity ->
     B (q1 ~> Seq L1 \* q2 ~> Seq L2 \* $ append_cst) (# q1 ~> Seq (L1 ++ L2)).

Hint Extern 1 (RegisterSpec is_full) => Provide is_full_spec.
Hint Extern 1 (RegisterSpec create) => Provide create_spec.
Hint Extern 1 (RegisterSpec is_empty) => Provide is_empty_spec.
Hint Extern 1 (RegisterSpec length) => Provide length_spec.
Hint Extern 1 (RegisterSpec push_front) => Provide push_front_spec.
Hint Extern 1 (RegisterSpec push_back) => Provide push_back_spec.
Hint Extern 1 (RegisterSpec pop_front) => Provide pop_front_spec.
Hint Extern 1 (RegisterSpec pop_back) => Provide pop_back_spec.
Hint Extern 1 (RegisterSpec append) => Provide append_spec.

End SequenceSigFixedCapacitySpec.



(*------------------------------------------------------*)
(** Sequence with recursive ownership of the items *)

Module Type SequenceSigOwnSpec.

Declare Module Q : SequenceSig_ml.MLS.
Import Q.

Parameter create_cst : R.
Parameter op_cst : R.
Parameter append_cst : R. (* later: generalize to a function *)

Parameter create_cst_nonneg : (create_cst >= 0)%R.
Parameter op_cst_nonneg : (op_cst >= 0)%R.
Parameter append_cst_nonneg : (append_cst >= 0)%R.

Hint Resolve create_cst_nonneg op_cst_nonneg append_cst_nonneg : credits_nonneg.

Parameter Seq : forall (a A : Type), 
  htype A a -> list A -> (Q.t a) -> hprop.

Parameter length_spec : forall a A,
  forall (R:htype A a), forall L,
  Spec Q.length (q:Q.t a) |B>> 
     keep B (q ~> Seq R L) (\= (LibList.length L : Z)).

Parameter create_spec : forall a A,
  Spec create () |B>> 
    forall (R:htype A a), 
     B \[] (fun q => q ~> Seq R nil).

Parameter is_empty_spec : forall a A,
  Spec is_empty (q:Q.t a) |B>>  
     forall (R:htype A a), forall L,
     keep B (q ~> Seq R L) (\= isTrue (L = nil)).

Parameter push_front_spec : forall a A,
  Spec push_front (x:a) (q:Q.t a) |B>>  
     forall (R:htype A a), forall X L,
     B (q ~> Seq R L \* x ~> R X \* $ op_cst) (# q ~> Seq R (X::L)).

Parameter push_back_spec : forall a A,
  Spec push_back (x:a) (q:Q.t a) |B>>  
     forall (R:htype A a), forall X L,
     B (q ~> Seq R L \* x ~> R X \* $ op_cst) (# q ~> Seq R (L&X)).

Parameter pop_front_spec : forall a A,
  Spec pop_front (q:Q.t a) |B>>  
     forall (R:htype A a), forall X L,
     B (q ~> Seq R (X::L)) (fun x => x ~> R X \* q ~> Seq R L).

Parameter pop_back_spec : forall a A,
  Spec pop_back (q:Q.t a) |B>>  
     forall (R:htype A a), forall X L,
     B (q ~> Seq R (L&X)) (fun x => x ~> R X \* q ~> Seq R L).

Parameter append_spec : forall a A, 
  Spec append (q1:Q.t a) (q2:Q.t a) |B>>  
     forall (R:htype A a), forall L1 L2,
     B (q1 ~> Seq R L1 \* q2 ~> Seq R L2 \* $ append_cst)
       (# q1 ~> Seq R (L1 ++ L2)).


Hint Extern 1 (RegisterSpec create) => Provide create_spec.
Hint Extern 1 (RegisterSpec is_empty) => Provide is_empty_spec.
Hint Extern 1 (RegisterSpec length) => Provide length_spec.
Hint Extern 1 (RegisterSpec push_front) => Provide push_front_spec.
Hint Extern 1 (RegisterSpec push_back) => Provide push_back_spec.
Hint Extern 1 (RegisterSpec pop_front) => Provide pop_front_spec.
Hint Extern 1 (RegisterSpec pop_back) => Provide pop_back_spec.
Hint Extern 1 (RegisterSpec append) => Provide append_spec.

End SequenceSigOwnSpec.


(*------------------------------------------------------*)
(** Fixed Capacity Sequence, with recursive ownership *)

Module Type SequenceSigFixedCapacityOwnSpec.

Declare Module Q' : SequenceSig_ml.MLFixedCapacityS.
Include Type (SequenceSigOwnSpec with Module Q := Q').

Parameter capacity_spec : Q'.capacity > 0.

Parameter append_cst_eq : 
  append_cst = (Q'.capacity * op_cst)%R.

Parameter Seq_le_capacity : forall a (q:Q.t a) A (R:htype A a) L,
  hkeep (q ~> Seq R L) ==> \[(length L:Z) <= Q'.capacity].

Implicit Arguments Seq_le_capacity [A].

Parameter is_full_spec : forall a A,
  Spec Q'.is_full (q:Q.t a) |B>>  
     forall (R:htype A a), forall (L:list A),
     keep B (q ~> Seq R L) (\= isTrue (LibList.length L = Q'.capacity :> Z)).

Hint Extern 1 (RegisterSpec Q'.is_full) => Provide is_full_spec.

End SequenceSigFixedCapacityOwnSpec.


(*------------------------------------------------------*)
(** Deriving recursive ownership specifications *)

Module SequenceSigOwnSpec_from_SigSpec
  (Q : SequenceSig_ml.MLS) (QS : SequenceSigSpec with Module Q := Q)
  <: (SequenceSigOwnSpec with Module Q := Q).

Import Q.


Definition create_cst := QS.create_cst.
Definition op_cst := QS.op_cst.
Definition append_cst := QS.append_cst.

Definition create_cst_nonneg := QS.create_cst_nonneg.
Definition op_cst_nonneg := QS.op_cst_nonneg.
Definition append_cst_nonneg := QS.append_cst_nonneg.

Hint Resolve create_cst_nonneg op_cst_nonneg append_cst_nonneg : credits_nonneg.


Parameter Seq : forall (a A : Type), 
  htype A a -> list A -> (Q.t a) -> hprop.

Parameter length_spec : forall a A,
  forall (R:htype A a), forall L,
  Spec Q.length (q:Q.t a) |B>> 
     keep B (q ~> Seq R L) (\= (LibList.length L : Z)).

Parameter create_spec : forall a A,
  Spec create () |B>> 
    forall (R:htype A a), 
     B \[] (fun q => q ~> Seq R nil).

Parameter is_empty_spec : forall a A,
  Spec is_empty (q:Q.t a) |B>>  
     forall (R:htype A a), forall L,
     keep B (q ~> Seq R L) (\= isTrue (L = nil)).

Parameter push_front_spec : forall a A,
  Spec push_front (x:a) (q:Q.t a) |B>>  
     forall (R:htype A a), forall X L,
     B (q ~> Seq R L \* x ~> R X \* $ op_cst) (# q ~> Seq R (X::L)).

Parameter push_back_spec : forall a A,
  Spec push_back (x:a) (q:Q.t a) |B>>  
     forall (R:htype A a), forall X L,
     B (q ~> Seq R L \* x ~> R X \* $ op_cst) (# q ~> Seq R (L&X)).

Parameter pop_front_spec : forall a A,
  Spec pop_front (q:Q.t a) |B>>  
     forall (R:htype A a), forall X L,
     B (q ~> Seq R (X::L)) (fun x => x ~> R X \* q ~> Seq R L).

Parameter pop_back_spec : forall a A,
  Spec pop_back (q:Q.t a) |B>>  
     forall (R:htype A a), forall X L,
     B (q ~> Seq R (L&X)) (fun x => x ~> R X \* q ~> Seq R L).

Parameter append_spec : forall a A, 
  Spec append (q1:Q.t a) (q2:Q.t a) |B>>  
     forall (R:htype A a), forall L1 L2,
     B (q1 ~> Seq R L1 \* q2 ~> Seq R L2 \* $ append_cst)
       (# q1 ~> Seq R (L1 ++ L2)).

Hint Extern 1 (RegisterSpec create) => Provide create_spec.
Hint Extern 1 (RegisterSpec is_empty) => Provide is_empty_spec.
Hint Extern 1 (RegisterSpec length) => Provide length_spec.
Hint Extern 1 (RegisterSpec push_front) => Provide push_front_spec.
Hint Extern 1 (RegisterSpec push_back) => Provide push_back_spec.
Hint Extern 1 (RegisterSpec pop_front) => Provide pop_front_spec.
Hint Extern 1 (RegisterSpec pop_back) => Provide pop_back_spec.
Hint Extern 1 (RegisterSpec append) => Provide append_spec.

Module Q := Q. 

End SequenceSigOwnSpec_from_SigSpec.




(*------------------------------------------------------*)
(** Derivation using SeqOf, proof skelton *)

Module SequenceSigOwnSpec_from_SigSpec'
  (Q : SequenceSig_ml.MLS) (QS : SequenceSigSpec with Module Q := Q).

Import Q.

Definition create_cst := QS.create_cst.
Definition op_cst := QS.op_cst.
Definition append_cst := QS.append_cst.

Definition create_cst_nonneg := QS.create_cst_nonneg.
Definition op_cst_nonneg := QS.op_cst_nonneg.
Definition append_cst_nonneg := QS.append_cst_nonneg.

Definition Seq a A (R:htype A a) (L:list A) (q:Q.t a) := 
  Hexists M, q ~> QS.Seq M 
   \* \[LibList.length M = LibList.length L]
   \* reduce sep_monoid (fun p => let '(x,X) := p in x ~> R X) 
     (LibList.combine M L).

Lemma Seq_Id : forall a (L:list a) (q:Q.t a),
  q ~> Seq Id L ==> q ~> QS.Seq L.
Proof. 
  intros. hunfold Seq. hextract as M E. 
  cuts Ind: (
    reduce sep_monoid (fun p : a * a => let '(x, X) := p in x ~> Id X)
     (combine M L) ==> \[M = L]).
  hchange Ind. hsimpl.
  gen M. induction L; destruct M; rew_list; simpl; intros; tryfalse; 
    rew_reduce; simpl.
  hsimpl~.
  inverts E as E. hunfold (@Id) at 1. hextracts. hchange (>> IHL E).
   hsimpl~. subst~. 
Qed.

Lemma Seq_unfocus : forall a (q:Q.t a) A (R:htype A a) (L:list A) M,
  LibList.length M = LibList.length L ->
  q ~> QS.Seq M 
   \* reduce sep_monoid (fun p => let '(x,X) := p in x ~> R X) 
     (LibList.combine M L)
  ==> q ~> Seq R L.
Proof. intros. hunfolds~ Seq. Qed.


Lemma push_front_spec : forall a A,
  Spec push_front (x:a) (q:Q.t a) |B>>  
     forall (R:htype A a), forall X L,
     B (q ~> Seq R L \* x ~> R X \* $ op_cst) (# q ~> Seq R (X::L)).
Proof.
  intros. xweaken QS.push_front_spec.
  simpl. intros x q B. introv LB HB. intros.
  hunfold Seq at 1. xextract as M EM. xapply (rm HB).
   unfold op_cst. hsimpl.
  intros _. hunfold Seq. hsimpl; [| rew_length~ ].
   simpl. rew_reduce. simpl. hsimpl. 
Qed.

Lemma pop_front_spec : forall a A,
  Spec pop_front (q:Q.t a) |B>>  
     forall (R:htype A a), forall X L,
     B (q ~> Seq R (X::L)) (fun x => x ~> R X \* q ~> Seq R L).
Proof.
  intros. xweaken QS.pop_front_spec.
  simpl. intros x q B. introv HB. intros.
  hunfold Seq at 1. xextract as M EM.
  destruct M as [|Y M]. rew_list in *. false.
  xapply (rm HB).
   unfold op_cst. hsimpl.
  intros r. hunfold Seq. hsimpl; [| rew_length~ ].
   simpl. rew_reduce. simpl. hsimpl. subst~. 
Qed.

End SequenceSigOwnSpec_from_SigSpec'.


(*------------------------------------------------------*)
(** Deriving recursive ownership specifications, fixed capacity *)

Module Type SequenceSigOwnFixedCapacitySpec.

Declare Module Q : SequenceSig_ml.MLFixedCapacityS.
Import Q.


(*---*)

Parameter create_cst : R.
Parameter op_cst : R.
Parameter append_cst : R. (* later: generalize to a function *)

Parameter create_cst_nonneg : (create_cst >= 0)%R.
Parameter op_cst_nonneg : (op_cst >= 0)%R.
Parameter append_cst_nonneg : (append_cst >= 0)%R.

Hint Resolve create_cst_nonneg op_cst_nonneg append_cst_nonneg : credits_nonneg.

Parameter append_cst_eq : 
  append_cst = (Q.capacity * op_cst)%R.


Parameter Seq : forall (a A : Type), 
  htype A a -> list A -> (Q.t a) -> hprop.

Parameter length_spec : forall a A,
  forall (R:htype A a), forall L,
  Spec Q.length (q:Q.t a) |B>> 
     keep B (q ~> Seq R L) (\= (LibList.length L : Z)).

Parameter create_spec : forall a A,
  Spec create () |B>> 
    forall (R:htype A a), 
     B \[] (fun q => q ~> Seq R nil).

Parameter is_empty_spec : forall a A,
  Spec is_empty (q:Q.t a) |B>>  
     forall (R:htype A a), forall L,
     keep B (q ~> Seq R L) (\= isTrue (L = nil)).

Parameter push_front_spec : forall a A,
  Spec push_front (x:a) (q:Q.t a) |B>>  
     forall (R:htype A a), forall X L,
     (LibList.length L : Z) < Q.capacity ->
     B (q ~> Seq R L \* x ~> R X \* $ op_cst) (# q ~> Seq R (X::L)).

Parameter push_back_spec : forall a A,
  Spec push_back (x:a) (q:Q.t a) |B>>  
     forall (R:htype A a), forall X L,
     (LibList.length L : Z) < Q.capacity ->
     B (q ~> Seq R L \* x ~> R X \* $ op_cst) (# q ~> Seq R (L&X)).

Parameter pop_front_spec : forall a A,
  Spec pop_front (q:Q.t a) |B>>  
     forall (R:htype A a), forall X L,
     B (q ~> Seq R (X::L)) (fun x => x ~> R X \* q ~> Seq R L).

Parameter pop_back_spec : forall a A,
  Spec pop_back (q:Q.t a) |B>>  
     forall (R:htype A a), forall X L,
     B (q ~> Seq R (L&X)) (fun x => x ~> R X \* q ~> Seq R L).

Parameter append_spec : forall a A, 
  Spec append (q1:Q.t a) (q2:Q.t a) |B>>  
     forall (R:htype A a), forall L1 L2,
     (LibList.length L1 : Z) + (LibList.length L2 : Z) <= Q.capacity ->
     B (q1 ~> Seq R L1 \* q2 ~> Seq R L2 \* $ append_cst)
       (# q1 ~> Seq R (L1 ++ L2)).

Hint Extern 1 (RegisterSpec create) => Provide create_spec.
Hint Extern 1 (RegisterSpec is_empty) => Provide is_empty_spec.
Hint Extern 1 (RegisterSpec length) => Provide length_spec.
Hint Extern 1 (RegisterSpec push_front) => Provide push_front_spec.
Hint Extern 1 (RegisterSpec push_back) => Provide push_back_spec.
Hint Extern 1 (RegisterSpec pop_front) => Provide pop_front_spec.
Hint Extern 1 (RegisterSpec pop_back) => Provide pop_back_spec.
Hint Extern 1 (RegisterSpec append) => Provide append_spec.

(*---*)

Parameter capacity_spec : Q.capacity > 0.

Parameter Seq_le_capacity : forall a (q:Q.t a) A (R:htype A a) L,
  hkeep (q ~> Seq R L) ==> \[(LibList.length L : Z) <= Q.capacity].

Implicit Arguments Seq_le_capacity [A].

Parameter is_full_spec : forall a A,
  Spec Q.is_full (q:Q.t a) |B>>  
     forall (R:htype A a), forall (L:list A),
     keep B (q ~> Seq R L) (\= isTrue (LibList.length L = Q.capacity :> Z)).

Hint Extern 1 (RegisterSpec Q.is_full) => Provide is_full_spec.

(*---*)

End SequenceSigOwnFixedCapacitySpec.



(*------------------------------------------------------*)

Module SequenceSigOwnFixedCapacitySpec_from_SigFixedCapacitySpec
  (Q : SequenceSig_ml.MLFixedCapacityS) (QS : SequenceSigFixedCapacitySpec with Module Q := Q)
  <: (SequenceSigOwnFixedCapacitySpec with Module Q := Q).


Import Q.


Definition create_cst := QS.create_cst.
Definition op_cst := QS.op_cst.
Definition append_cst := QS.append_cst.

Definition create_cst_nonneg := QS.create_cst_nonneg.
Definition op_cst_nonneg := QS.op_cst_nonneg.
Definition append_cst_nonneg := QS.append_cst_nonneg.

Hint Resolve create_cst_nonneg op_cst_nonneg append_cst_nonneg : credits_nonneg.

Parameter append_cst_eq : 
  append_cst = (Q.capacity * op_cst)%R.


Parameter Seq : forall (a A : Type), 
  htype A a -> list A -> (Q.t a) -> hprop.

Parameter length_spec : forall a A,
  forall (R:htype A a), forall L,
  Spec Q.length (q:Q.t a) |B>> 
     keep B (q ~> Seq R L) (\= (LibList.length L : Z)).

Parameter create_spec : forall a A,
  Spec create () |B>> 
    forall (R:htype A a), 
     B \[] (fun q => q ~> Seq R nil).

Parameter is_empty_spec : forall a A,
  Spec is_empty (q:Q.t a) |B>>  
     forall (R:htype A a), forall L,
     keep B (q ~> Seq R L) (\= isTrue (L = nil)).

Parameter push_front_spec : forall a A,
  Spec push_front (x:a) (q:Q.t a) |B>>  
     forall (R:htype A a), forall X L,
     (LibList.length L : Z) < Q.capacity ->
     B (q ~> Seq R L \* x ~> R X \* $ op_cst) (# q ~> Seq R (X::L)).

Parameter push_back_spec : forall a A,
  Spec push_back (x:a) (q:Q.t a) |B>>  
     forall (R:htype A a), forall X L,
     (LibList.length L : Z) < Q.capacity ->
     B (q ~> Seq R L \* x ~> R X \* $ op_cst) (# q ~> Seq R (L&X)).

Parameter pop_front_spec : forall a A,
  Spec pop_front (q:Q.t a) |B>>  
     forall (R:htype A a), forall X L,
     B (q ~> Seq R (X::L)) (fun x => x ~> R X \* q ~> Seq R L).

Parameter pop_back_spec : forall a A,
  Spec pop_back (q:Q.t a) |B>>  
     forall (R:htype A a), forall X L,
     B (q ~> Seq R (L&X)) (fun x => x ~> R X \* q ~> Seq R L).

Parameter append_spec : forall a A, 
  Spec append (q1:Q.t a) (q2:Q.t a) |B>>  
     forall (R:htype A a), forall L1 L2,
     (LibList.length L1 : Z) + (LibList.length L2 : Z) <= Q.capacity ->
     B (q1 ~> Seq R L1 \* q2 ~> Seq R L2 \* $ append_cst)
       (# q1 ~> Seq R (L1 ++ L2)).

Hint Extern 1 (RegisterSpec create) => Provide create_spec.
Hint Extern 1 (RegisterSpec is_empty) => Provide is_empty_spec.
Hint Extern 1 (RegisterSpec length) => Provide length_spec.
Hint Extern 1 (RegisterSpec push_front) => Provide push_front_spec.
Hint Extern 1 (RegisterSpec push_back) => Provide push_back_spec.
Hint Extern 1 (RegisterSpec pop_front) => Provide pop_front_spec.
Hint Extern 1 (RegisterSpec pop_back) => Provide pop_back_spec.
Hint Extern 1 (RegisterSpec append) => Provide append_spec.

Module Q := Q. 



Parameter capacity_spec : Q.capacity > 0.

Parameter Seq_le_capacity : forall a (q:Q.t a) A (R:htype A a) L,
  hkeep (q ~> Seq R L) ==> \[(LibList.length L : Z) <= Q.capacity].

Implicit Arguments Seq_le_capacity [a A].

Parameter is_full_spec : forall a A,
  Spec Q.is_full (q:Q.t a) |B>>  
     forall (R:htype A a), forall (L:list A),
     keep B (q ~> Seq R L) (\= isTrue (LibList.length L = Q.capacity :> Z)).

Hint Extern 1 (RegisterSpec Q.is_full) => Provide is_full_spec.


(*
Module Q := Q. 

Definition create_cst := QS.create_cst.
Definition op_cst := QS.op_cst.
Definition append_cst := QS.append_cst.

Definition create_cst_nonneg := QS.create_cst_nonneg.
Definition op_cst_nonneg := QS.op_cst_nonneg.
Definition append_cst_nonneg := QS.append_cst_nonneg.
*)


End SequenceSigOwnFixedCapacitySpec_from_SigFixedCapacitySpec.







(*


(*------------------------------------------------------*)
(*------------------------------------------------------*)
(** demo: amortization of pop over push *)

Parameter base_chunk_push_front_spec : forall A,
  Spec Chunk.push_front (X:A) (q:ChunkSpec.Q.t A) |B>>  
     forall L,
     B (q ~> ChunkSpec.Seq L \* $ ChunkSpec.op_cst) (# q ~> ChunkSpec.Seq (X::L)).

Parameter base_chunk_pop_front_spec : forall A,
  Spec pop_front (q:ChunkSpec.Q.t A) |B>>  
     forall (X:A) L,
     B (q ~> ChunkSpec.Seq (X::L) \* $ ChunkSpec.op_cst) (fun r => \[r = X] \* q ~> ChunkSpec.Seq L).

Definition ChunkSpecSeq2 A (L:list A) (q:ChunkSpec.Q.t A) := 
  q ~> ChunkSpec.Seq L \* $((length L * ChunkSpec.op_cst)%R).

Definition ChunkSpec.op_cst2 := (2 * ChunkSpec.op_cst)%R.

Lemma chunk_push_front_spec' : forall A,
  Spec Chunk.push_front (X:A) (q:ChunkSpec.Q.t A) |B>>  
     forall L,
     B (q ~> ChunkSpecSeq2 L \* $ ChunkSpec.op_cst2) (# q ~> ChunkSpecSeq2 (X::L)).
Proof.
  intros. xweaken base_chunk_push_front_spec. simpl. intros X q R HR H L.
  unfold ChunkSpec.op_cst2. rewrite credits_split_two.
  hunfold ChunkSpecSeq2. xapply H. hsimpl. rew_length.
  rewrite credits_split_one_nat. hsimpl.
Qed.

Lemma chunk_pop_front_spec' : forall A,
  Spec pop_front (q:ChunkSpec.Q.t A) |B>>  
     forall (X:A) L,
     B (q ~> ChunkSpecSeq2 (X::L)) (fun r => \[r = X] \* q ~> ChunkSpecSeq2 L).
Proof.
  intros. xweaken base_chunk_pop_front_spec. simpl. intros q R HR H X L.
  hunfold ChunkSpecSeq2. rew_length. rewrite credits_split_one_nat.
  xapply H. hsimpl. hsimpl~.
Qed.


*)


