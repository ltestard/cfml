Set Implicit Arguments.
Require Import CFTactics.
Require Import BatchedQueue_ml.

Module BatchedQueueSpec.

(** instantiations *)

Module Import Q := MLBatchedQueue.

(** invariant *)

Definition inv A (q:queue A) (Q:list A) :=
  let (f,r) := q in 
     Q = (f ++ rev r)
  /\ (f = nil -> r = nil).

(** automation *)

Hint Unfold inv.
Ltac auto_tilde ::= eauto.

(** useful facts *)

(** verification *)

Lemma empty_spec : forall A,
  inv (@empty A) (@nil A).
Proof.
  (* todo arthur: fix display of top_label, and fix tactic below *) 
  intros. rewrite~ empty_cf.
Qed.

Hint Extern 1 (RegisterSpec empty) => Provide empty_spec.

Lemma is_empty_spec : forall A,
  Spec is_empty (q:queue A) |R>> forall Q, 
    inv q Q ->
    R \[] (fun b => \[b = isTrue (Q = nil)]).
Proof.
  xcf. intros (f,r) Q [H M].   
  xmatch; xrets. (* alternative: xgo. *)
  fold_bool; rew_refl. (* todo: have a tactic for this *) rewrite~ M in H.
  fold_bool; rew_refl. intros E. rewrite E in H.
   destruct (nil_eq_app_rev_inv H). false.
Qed.

Hint Extern 1 (RegisterSpec is_empty) => Provide is_empty_spec.

Lemma checkf_spec : forall A,
    Spec checkf (q: queue A) |R>> forall Q,
    (let (f,r) := q in Q = (f ++ rev r)) ->
    R \[] (fun q' => \[inv q' Q]).
Proof.
  xcf. intros q Q H.
  xmatch.
  - xlet; xret.
    hsimpl. subst. rewrite~ <-app_nil_r.
  - xret. hsimpl. destruct q.
    assert (l <> nil) as H1 by congruence.
    assert (l = nil -> l0 = nil) by (intro; false).
    auto.
Qed.

Hint Extern 1 (RegisterSpec checkf) => Provide checkf_spec.

Lemma pop_spec : forall A,
    Spec pop (q: queue A) |R>> forall Q,
    inv q Q -> Q <> nil ->
    R \[] (fun p => let '(x, q') := p in Hexists Q',
                   \[Q = x::Q' /\ inv q' Q']).
Proof.
  xcf. intros [x q] Q [I1 I2] H.
  xmatch.
  - xfail. rewrite I2 in I1. apply (H I1). auto.
  - xlet. xapp~.
    xret. hsimpl. eauto.
Qed.

Hint Extern 1 (RegisterSpec pop) => Provide pop_spec.

Lemma push_spec : forall A,
    Spec push (q: queue A) (x: A) |R>> forall Q,
    inv q Q ->
    R \[] (fun p => \[inv p (Q & x)]).
Proof.
  xcf. intros [x' q'] x Q [I1 I2].
  xmatch; xapp~.
  hsimpl. subst. rewrite~ <-app_rev_cons.
Qed.

Hint Extern 1 (RegisterSpec push) => Provide push_spec.

End BatchedQueueSpec.

